/********************************************************************************************
Name: FundsTransferTriggerHandler_Test
=============================================================================================
Purpose: To cover the 'FundsTransferTriggerHandler' apex code coverage 
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0        Srinivas D             4th-April-2019    SFD-984   		    Intial version
2.0		   Kakasaheb Ekshinge 	  06-Aug-2020		SFD-1596			Code coverage has increases
********************************************************************************************/
@isTest(SeeAllData = false)
public class FundsTransferTriggerHandler_Test{  
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     06-Aug-2020
*   User Story : SFD-1596
*   Param: None
*   Return: None    
*   Description: In this method we are trying to deleted the fundtransfer record to cover the throwErrorOnDelete
method.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testmethod void throwErrorOnDeleteTest() {
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account acc = UnitTestDataGenerator.TestAccount.buildInsert(); 
            Contact testContact = UnitTestDataGenerator.TestContact.buildInsert(); 
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert();

            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c' => '12w23eeeeedd',
                    'Customer__c' => acc.id,
                    'Customer_Serno__c' => testContact.Id,
                    'Product__c' => prodobj.Id,
                    'Product_Serno__c' => prodobj.Pserno__c
                    });
            
            Card__c cardObj = UnitTestDataGenerator.testCard.buildInsert(new Map<String, Object>{
                'Card_Serno__c' => '2231236',
                    'Card_Number_Truncated__c' => '5456654565665',
                    'People__c' => testContact.Id,
                    'Financial_Account__c' => finAccObj.Id,
                    'Financial_Account_Serno__c' => finAccObj.Id,
                    'People_Serno__c' => testContact.Id,
                    'Product__c' => prodobj.Id,
                    'Prod_Serno__c' =>prodobj.Pserno__c
                    });
            FundsTransfer__c fundObj = UnitTestDataGenerator.TestFundsTransfer.buildInsert(new Map<String, Object>{
                
                'SASRecieved__c'=>false
                    });
            try{
                delete fundObj;
            } catch(Exception e){
                system.debug('----e---'+e.getMessage());
                system.assertNotEquals(StaticConstant.PROCESSED_DELETE_ERROR, e.getMessage());
               
                Test.stopTest();
            }
        }
    }
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     06-Aug-2020
*   User Story : SFD-1596
*   Param: None
*   Return: None    
*   Description: In this method we are trying to deleted the fundtransfer record to cover the else if condtion
of the throwErrorOnDelete method .
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testmethod void throwErrorOnDeleteIfTest() {
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account acc = UnitTestDataGenerator.TestAccount.buildInsert(); 
            
            
            Contact testContact = UnitTestDataGenerator.TestContact.buildInsert(); 
            
            
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c' => '12w23eeeeedd',
                    'Customer__c' => acc.id,
                    'Customer_Serno__c' => testContact.Id,
                    'Product__c' => prodobj.Id,
                    'Product_Serno__c' => prodobj.Pserno__c
                    });
            
            Card__c cardObj = UnitTestDataGenerator.testCard.buildInsert(new Map<String, Object>{
                'Card_Serno__c' => '2231236',
                    'Card_Number_Truncated__c' => '5456654565665',
                    'People__c' => testContact.Id,
                    'Financial_Account__c' => finAccObj.Id,
                    'Financial_Account_Serno__c' => finAccObj.Id,
                    'People_Serno__c' => testContact.Id,
                    'Product__c' => prodobj.Id,
                    'Prod_Serno__c' =>prodobj.Pserno__c
                    });
            
            FundsTransfer__c fundObj = UnitTestDataGenerator.TestFundsTransfer.buildInsert(new Map<String, Object>{
                'SASProcessed__c'=>false,
                    'SASRecieved__c'=>true
                    });
            try{
                delete fundObj;
            } catch(Exception e){
                system.assertNotEquals(StaticConstant.TRANSACTION_DELETE_ERROR, e.getMessage());
                
                Test.stopTest();
            }
        }
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     06-Aug-2020
*   User Story : SFD-1596
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update the fundtransfer record to cover the throwErrorOnUpdate method .
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testmethod void throwErrorOnUpdateTest() {
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            
            Account acc = UnitTestDataGenerator.TestAccount.buildInsert(); 
            
            
            Contact testContact = UnitTestDataGenerator.TestContact.buildInsert(); 
            
            
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c' => '12w23eeeeedd',
                    'Customer__c' => acc.id,
                    'Customer_Serno__c' => testContact.Id,
                    'Product__c' => prodobj.Id,
                    'Product_Serno__c' => prodobj.Pserno__c
                    });
            
            Card__c cardObj = UnitTestDataGenerator.testCard.buildInsert(new Map<String, Object>{
                'Card_Serno__c' => '2231236',
                    'Card_Number_Truncated__c' => '5456654565665',
                    'People__c' => testContact.Id,
                    'Financial_Account__c' => finAccObj.Id,
                    'Financial_Account_Serno__c' => finAccObj.Id,
                    'People_Serno__c' => testContact.Id,
                    'Product__c' => prodobj.Id,
                    'Prod_Serno__c' =>prodobj.Pserno__c
                    });
            FundsTransfer__c fundObj = UnitTestDataGenerator.TestFundsTransfer.buildInsert(new Map<String, Object>{
                'SASProcessed__c'=>True,
                    'SASRecieved__c'=>False
                    });
            fundObj.SASRecieved__c = true;
            fundObj.SASProcessed__c = false;
            try{
                update fundObj; 
            } catch(Exception e){
                system.assertNotEquals(StaticConstant.PROCESSED_UPDATE_ERROR, e.getMessage());
                Test.stopTest();
            }
        }
    }
}