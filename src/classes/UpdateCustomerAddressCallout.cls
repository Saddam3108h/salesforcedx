public class UpdateCustomerAddressCallout extends SoapIO{
    SOAPCSSServiceRequest.UpdateCustomerNameAddressRequestRecordType updatecustomernameaddress;
    String InstitutionId;
    public  void convertInputToRequest(){
     
    UpdateCustomerAddressInput ip = (UpdateCustomerAddressInput)serviceInput;
    updatecustomernameaddress= new SOAPCSSServiceRequest.UpdateCustomerNameAddressRequestRecordType();
    updatecustomernameaddress.Reference=ip.Reference;
    updatecustomernameaddress.CustomerID=ip.CustomerID;
    updatecustomernameaddress.SSN=ip.SSN;
    updatecustomernameaddress.NewIndicator=ip.NewIndicator;
    updatecustomernameaddress.People = new SOAPCSSServiceRequest.PeopleSection();//people section
    updatecustomernameaddress.People.Title = ip.Title;
    updatecustomernameaddress.People.FirstName= ip.FirstName;
    updatecustomernameaddress.People.MiddleName = ip.MiddleName;
    updatecustomernameaddress.People.LastName= ip.LastName;
    updatecustomernameaddress.People.MaritalStatus = (ip.MaritalStatus=='Married'?'M':'S');
    updatecustomernameaddress.People.MotherName = ip.MotherName;
    if(!ip.isOnlyPeopleUpd){
    updatecustomernameaddress.Address = new SOAPCSSServiceRequest.AddressSection();//address section
    updatecustomernameaddress.Address.Location = ip.Location;
    updatecustomernameaddress.Address.Address1 = ip.Address1;
    updatecustomernameaddress.Address.Address2 = ip.Address2;
    updatecustomernameaddress.Address.Address3 = ip.Address3;
    updatecustomernameaddress.Address.Address4 = ip.Address4;
    updatecustomernameaddress.Address.Address5 = ip.Address5;
    updatecustomernameaddress.Address.City = ip.City;
    updatecustomernameaddress.Address.State = ip.State;
    updatecustomernameaddress.Address.County = ip.Country;
    updatecustomernameaddress.Address.CountyCode = ip.CountyCode;
    updatecustomernameaddress.Address.ZIP = ip.ZIP;
    updatecustomernameaddress.Address.Phone1 = ip.Phone1;
    updatecustomernameaddress.Address.Phone2 = ip.Phone2;
    updatecustomernameaddress.Address.Fax = ip.Fax;
    updatecustomernameaddress.Address.Mobile = ip.Mobile;
    updatecustomernameaddress.Address.Email = ip.Email;
    }
    InstitutionId = ip.InstitutionId;
    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        SOAPschemasEntercardComTypelib10.headerType HeadInstance=new SOAPschemasEntercardComTypelib10.headerType();
        HeadInstance.MsgId='FinalCAll';
        HeadInstance.CorrelationId='1';
        HeadInstance.RequestorId='Salesforce';
        HeadInstance.SystemId='1';
        HeadInstance.InstitutionId=InstitutionId;
        SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance= new SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=30000;
        // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('updateCustomerNameAddress');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
        //String username = 'evry_access';
        //String password = '9oKuwQioQ4';
        
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken creds=new SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken();
        creds.Username=serviceObj.Username__c; //'css_soa';//'evry_access';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';//'9oKuwQioQ4';
        SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element security_ele=new SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOAPCSSServiceResponse.UpdateCustomerNameAddressResponseRecordType updaddcustnmResponse_elementinstance = invokeInstance.updateCustomerNameAddress(HeadInstance,updatecustomernameaddress);
        system.debug('updaddcustnmResponse_elementinstance-->'+updaddcustnmResponse_elementinstance);
        return updaddcustnmResponse_elementinstance;
        }  
        
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
       if(response==null)
       return null;
       SOAPCSSServiceResponse.UpdateCustomerNameAddressResponseRecordType respstmt = (SOAPCSSServiceResponse.UpdateCustomerNameAddressResponseRecordType)response; 
       SOAPCSSServiceResponse.Result rr =  respstmt.Result;
       UpdateCustomerAddressOutput op = new UpdateCustomerAddressOutput();
       if(rr.Address!=null) // added by shameel 03-15-2017
       op.AddressSerno = (rr.Address.AddressSerno!=''?Integer.valueOf(rr.Address.AddressSerno):null);
       op.Code = rr.Code;
       op.Description = rr.Description;
       if(rr.ErrorDetails!=null)
       op.ErrorResult = rr.ErrorDetails.ErrorResult;
       return op;
    }        

}