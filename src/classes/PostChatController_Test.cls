/**********************************************************************
Name: PostChatController_Test
=======================================================================
Purpose: This Test class is used to cover the PostChatController class - User Story -SFD-1397

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         1-Jul-2020       Initial Version

**********************************************************************/
@isTest
public class PostChatController_Test {
     /**********************************************************************
*   Author: Saddam Hussain
*   Date:     1-Jul-2020
*   User Story : SFD-1397
*   Param: None
*   Return: None    
*   Description: In this method we are trying to coverage code of updateCustFeedBack method.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testMethod void updateCustFeedBackTest(){
        LiveChatVisitor chatVisitorObj = new LiveChatVisitor();
        insert chatVisitorObj;
        LiveChatTranscript liveChatObj = new LiveChatTranscript();
        liveChatObj.LiveChatVisitorId = chatVisitorObj.Id;
        insert liveChatObj;
        Test.StartTest();
        PostChatController postChatObj = new PostChatController();
        PageReference pageRef = Page.NOPostChatForm;
        pageRef.getParameters().put('chatKey', liveChatObj.ChatKey);
        Test.setCurrentPage(pageRef);
        postChatObj.updateCustFeedBack();
        system.assert(liveChatObj!=null);
        Test.StopTest();
    }
}