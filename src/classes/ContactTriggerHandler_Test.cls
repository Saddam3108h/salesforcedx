/**********************************************************************
Name: ContactTriggerHandler_Test
=======================================================================
Purpose: This Test class is used to cover the ContactTriggerHandle class - User Story -SFD-1489

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         20-May-2020       Initial Version

**********************************************************************/
@isTest(SeeAllData = false)
public class ContactTriggerHandler_Test {
     /**********************************************************************
*   Author: Saddam Hussain
*   Date:     21-May-2020
*   User Story : SFD-1431
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update Mobile number, encrypte the SSN to cover the code.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testmethod void updateEncryptSsnTest() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN,
                    'VIP__c'=>true
                    
                    });
            system.debug('-contactObj--'+contactObj.SSN__c);
            
            List<Contact> contactList = new List<Contact>();
            contactList.add(contactObj);
            List<String>  contactAttr = new List<String> {'AccountId','Encrypted_SSN__c','MobilePhone','SSN__c','Customer_Email__c','VIP__c'};
                List<Contact> contactReload = UnitTestDataGenerator.TestContact.reloadList(contactList,contactAttr);
            system.debug('--cardReload--'+contactReload);
            
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                    'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            Card__c cardObj = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Product__c'=>prodobj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c,
                    'PrimaryCardFlag__c'=> true
                    });
            
            
            Map<Id, Contact> contactNewMap = new Map<Id, Contact>();
            Map<Id, Contact> contactOldMap = new Map<Id, Contact>();
            contactOldMap.put(contactObj.Id, contactObj);
            
            Test.startTest();
            contactObj.MobilePhone = '5667788';
            contactObj.VIP__c = false;
            update contactObj;
            contactNewMap.put(contactObj.Id, contactObj);
            ContactTriggerHandler.updateSavedeskPhone(contactReload, contactOldMap);
            ContactTriggerHandler.updateEncryptSsn(contactReload, contactOldMap, true);
            ContactTriggerHandler.updateVIPOnFinancialAccount(contactReload, contactOldMap);
            Test.stopTest();
            system.assertEquals('5667788', contactObj.MobilePhone);
        }
        
    }
}