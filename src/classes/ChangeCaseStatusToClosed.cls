/***************************************************************************************************
Name: ChangeCaseStatusToClosed
=====================================================================================================
Purpose: This Class is used for closing the cases which are in new status and created today - 30 days
User Story - SFD-1478
===================================================================================================== 
History
-----------------------------------------------------------------------------------------------------        
Version     Author                          Date              Detail
1.0        Harisivaramakrishna          19/05/2020     Initial Version

****************************************************************************************************/
global class ChangeCaseStatusToClosed implements Schedulable{
    /*********************************************************************************************************
*   Author: Harisivaramakrishna
*   Date:     08/04/2020
*   User Story : SFD-1478
*   Param: None
*   Return: None    
*   Description: In this method we are extracting the case based on some criteria and which ware created 
before 30 days and changing the status to closed. 
**********************************************************************************************************/    
    global void execute(SchedulableContext SC) {
        List<Case> casesToBeClosed = new List<Case>();
        List<Case> caseStatusChange = new List<Case>();
        try{
            Id queueId = [SELECT Id FROM Group WHERE Type =: StaticConstant.Queue_TYPE AND Name=: StaticConstant.QUEUE_NAME].Id;
            casesToBeClosed = [SELECT Id, Status,CaseNumber,To_Address__c,Subject
                               FROM Case 
                               WHERE Status =: StaticConstant.CASE_STATUS
                               AND To_Address__c=: StaticConstant.CASE_TOADDRESS 
                               AND EC_Subject__c LIKE: StaticConstant.CASE_SUBJECT
                               AND OwnerId =: queueId 
                               AND Case_View__c=: StaticConstant.CASE_VIEW
                               AND Category__c=: StaticConstant.CASE_CATEGORY
                               AND CreatedDate < LAST_N_DAYS:30];
            if(CasesToBeClosed.size() > 0){ 
                for (Case iCase : casesToBeClosed){
                    iCase.Status= StaticConstant.SAVEDESK_CLOSED_STATUS;  // This is used to change the status to closed
                    caseStatusChange.add(iCase);
                }
                update CaseStatusChange;
            }
            
            List<String> adminEmailIds = new List<String>(); // use custom setting
            //adminEmailIds.add('haathmak@capgemini.com');
           for(SalesforceTeam__c emailAdd : SalesforceTeam__c.getAll().values()){
                adminEmailIds.add(emailAdd.Email__c);
            }
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setSubject( StaticConstant.EMAIL_SUBJECT );
            email.setToAddresses( adminEmailIds );
            email.setPlainTextBody( StaticConstant.EMAIL_BODY );
            Messaging.SendEmailResult [] eSend = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
            
        }catch(Exception exp){
            StatusLogHelper.logSalesforceError('Case Status Chnage to Closed', 'defaultError', 'Error in changing the status', exp, false);
        }
    }
}