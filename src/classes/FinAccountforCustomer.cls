public class FinAccountforCustomer {
    
    @AuraEnabled
    public static Map<String, FinancialAccountWrapper> getFindetails(Id PeopleId){
        
        Contact contactObj = [Select id, Institution_Id__c, SSN__c, MobilePhone, Email from Contact where id=: PeopleId];
        System.debug('---contactObj--'+contactObj);

        List<Financial_Account__c> financialAccountList =new List<Financial_Account__c>();
        set<Id> financialAccIdSet = new set<Id>();
        
        if(contactObj!=null) 
        {
            List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c FROM Card__c where People__c=:PeopleId and Financial_Account__c!=null Limit 50000];
            
            for(Card__c card:cardList)
            {
                financialAccIdSet.add(card.Financial_Account__c);
            }
            
            financialAccountList =[select Id,Name,DirectDebit_Percentage__c,E_invoice_flag__c,Account_Serno__c,Customer__c,Interest_Rate__c,Credit_Limit__c,
                                   Partner_Membership_number__c, Account_Number__c,Segment__c,PPI__c,Risk_Indicator__c,Customer__r.Name,
                                   Product__r.Name,Product__c,Colour_Code__c from Financial_Account__c where Id IN :financialAccIdSet];
        }        
        
        GetCustomerOutput gcoutput;        
        if(contactObj.Institution_Id__c!=null &&  contactObj.SSN__c!=null) {      
            gcoutput= CSSServiceHelper.getCustomerDetails(integer.valueof(contactObj.Institution_Id__c),contactObj.SSN__c,'Y','Y');
        }
        system.debug('----gcoutput---'+gcoutput);
        Map<String,Decimal> mapsernoBalanceFA = new Map<String,Decimal>();
        
        Map<String,GetCustomerOutput.CustomerFinancialAccount> tempSerNoToFinAccMap = new Map<String,GetCustomerOutput.CustomerFinancialAccount>();
        
        if(gcoutput!=null && gcoutput.FinancialAccounts!=null){
            for(GetCustomerOutput.CustomerFinancialAccount cfa : gcoutput.FinancialAccounts){
                if(cfa.Serno!=null)
                    tempSerNoToFinAccMap.put(String.valueOf(cfa.Serno),cfa);
            }  
            
            system.debug('*******tempSerNoToFinAccMap->'+tempSerNoToFinAccMap); 
        }
        
        Map<String,FinancialAccountWrapper> serNoToFinancialAccountMap = new Map<String,FinancialAccountWrapper>();
        
        for(Financial_Account__c objfinancialAcc : financialAccountList){
            GetCustomerOutput.CustomerFinancialAccount custFinAcc = tempSerNoToFinAccMap.get(objfinancialAcc.Account_Serno__c)!=null?tempSerNoToFinAccMap.get(objfinancialAcc.Account_Serno__c):new GetCustomerOutput.CustomerFinancialAccount();
            serNoToFinancialAccountMap.put(objfinancialAcc.Account_Serno__c,new FinancialAccountWrapper(objfinancialAcc.Id,objfinancialAcc.Colour_Code__c, objfinancialAcc.Name,objfinancialAcc.Segment__c,objfinancialAcc.Customer__r.Name,objfinancialAcc.Product__r.name,objfinancialAcc.Account_Number__c,objfinancialAcc.DirectDebit_Percentage__c,objfinancialAcc.Credit_Limit__c,custFinAcc.OTB,custFinAcc.StGen,custFinAcc.Balance,custFinAcc.Einvoice));
        }            
        system.debug('mapsernoBalanceFA->'+mapsernoBalanceFA);
        system.debug('-----fin.size----'+serNoToFinancialAccountMap.size());
        
        
        return serNoToFinancialAccountMap;
    }
    
    public class FinancialAccountWrapper
    {
        @AuraEnabled public Id finId;
        @AuraEnabled public String colCode;
        @AuraEnabled public String name;
        @AuraEnabled public String Segment;
        @AuraEnabled public String Mah;
        @AuraEnabled public String Product;
        @AuraEnabled public String Accnumbr;
        @AuraEnabled public Decimal DDebit;
        @AuraEnabled public Decimal CrLimit;
        @AuraEnabled public Decimal OTB;
        @AuraEnabled public String StGen;
        @AuraEnabled public Decimal Balance;
        @AuraEnabled public string Einvoice;

        public FinancialAccountWrapper(Id finId,String colCode,String name,String Segment,String Mah,String Product,String Accnumbr,Decimal DDebit,Decimal CrLimit,Decimal oTB, String stGen,Decimal balance,string Einvoice)
        {
            this.finId=finId;
            this.colCode=colCode;
            this.name=name;
            this.Segment=Segment;
            this.Mah=Mah;
            this.Product=Product;
            this.Accnumbr=Accnumbr;
            this.DDebit=DDebit;
            this.CrLimit=CrLimit;
            this.OTB=oTB;
            this.StGen=stGen;
            this.Balance=balance;
            this.Einvoice=Einvoice;
        }
    }
}