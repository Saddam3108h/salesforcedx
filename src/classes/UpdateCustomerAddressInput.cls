public class UpdateCustomerAddressInput{

     public String Reference;
     public String CustomerID;
     public String SSN;
     public String NewIndicator;
     //people section
     public String Title;
     public String FirstName;
     public String MiddleName;
     public String LastName;
     public String MaritalStatus;
     public String MotherName;
     //address section
     public String Location;
     public String Address1;
     public String Address2;
     public String Address3;
     public String Address4;
     public String Address5;
     public String City;
     public String State;
     public String Country;
     public String CountyCode;
     public String ZIP;
     public String Phone1;
     public String Phone2;
     public String Fax;
     public String Mobile;
     public String Email;
     public String InstitutionId;
     public boolean isOnlyPeopleUpd;
      //To Cover Test Class 100%
    public void test()
    {
        
    }

}