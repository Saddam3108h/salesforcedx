/**********************************************************************
Name:ConsentMSLwcController_Test
=======================================================================
Purpose: This is the test class for 'ConsentMSLwcController' apex code coverage
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date             CreatedforJira       Detail
1.0         Kakasaheb Ekshinge       20-Jan-2021      SFD-1923             Initial Version
**********************************************************************/
@isTest(SeeAllData = false)
public class ConsentMSLwcController_Test {
    /**********************************************************************
*   Author: Kakasaheb Ekshinge
*   Date:   20-Jan-2021
*   User Story : SFD-1923
*   Param: None
*   Return: None    
*   Description:In this method we are trying cover the code coverage to used to fetch Consents of FA and display correct true/false at UI.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  
**********************************************************************/   
    static testmethod void consentLWCTest(){        
        Test.startTest();
        System.runAs(UnitTestDataGenerator.adminUser){
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
           
            Account accountObj=UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{});
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
            string encryptSSN=EncodingUtil.convertToHex(hash);                       
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                    'SerNo__c'=>'09899',
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id              
                    });             
            Product_Custom__c prodobj=UnitTestDataGenerator.TestProductCustom.buildInsert(new Map<String, Object>{});
            Financial_Account__c finAccObj=UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c' => '12w23eeeeedd',
                    'Customer__c' => accountObj.id,
                    'Customer_Serno__c' => contactObj.Id,
                    'Product__c' => prodobj.Id,
                    'Product_Serno__c' => prodobj.Pserno__c
                    });
            
            Individual induObj= UnitTestDataGenerator.testIndividual.buildInsert(new Map<String, Object>{});
            
            ContactPointTypeConsent consentObj = UnitTestDataGenerator.contactPointTypeConsents.buildInsert(new Map<String, Object>{
                    'PartyId' => induObj.Id,
                    'Financial_Account__c' => finAccObj.Id
                    });
              ContactPointTypeConsent consentObj1 = UnitTestDataGenerator.contactPointTypeConsents.buildInsert(new Map<String, Object>{
                    'PartyId' => induObj.Id,
                    'Financial_Account__c' => finAccObj.Id,
                     'PrivacyConsentStatus' => 'OptOut'
                    });
            
            ConsentMSLwcController.getConsentList(finAccObj.Id);
            
            ManageRESTServices__c manageRest = new ManageRESTServices__c();
            manageRest.Name = 'ConsentListOptInToOptout';
            manageRest.CertificateName__c =   Label.CertificateName;
            manageRest.Method__c = 'POST';
            manageRest.Endpoint__c = Label.label_ConsentEndpoint;
            manageRest.ContentType__c = 'application/json';
            insert manageRest;
            
            ConsentMSLwcController.Loopdata conLoopData =  new ConsentMSLwcController.Loopdata();
            conLoopData.key = consentObj.Id;
            conLoopData.value = true;
            
            ConsentMSLwcController.Loopdata conLoopData1 =  new ConsentMSLwcController.Loopdata();
            conLoopData1.key = consentObj1.Id;
            conLoopData1.value = false;
            
            List<ConsentMSLwcController.Loopdata> listLoopData = new List<ConsentMSLwcController.Loopdata>();
            listLoopData.add(conLoopData);
            listLoopData.add(conLoopData1);            
           
            ConsentMSLwcController.JsonToApex conJsonApex =  new ConsentMSLwcController.JsonToApex();            
            conJsonApex.accessToken = '123456789';
            conJsonApex.loopdata = listLoopData; 
            
            ConsentMSLwcController.sendCallout(JSON.serialize(conJsonApex));            
        }
        
        Test.stopTest();
    }
}