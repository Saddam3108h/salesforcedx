/********************************************************************************************
Name: DeleteCaseData_Test
=============================================================================================
Purpose: This test class is covering 'DeleteCaseData' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0		    Gagan Pathak 	       14-August-2020	SFD-1596		 Intial version
********************************************************************************************/
@isTest
public class DeleteCaseData_Test {
    
        Public static testMethod void myUnitTest() {
        System.runAs(UnitTestDataGenerator.adminUser) {    
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account ac = UnitTestDataGenerator.testAccount.buildInsert(new Map<String, Object>{	
                'Name' => 'Test',
                    'Customer_Serno__c' => '123'
                    });
            
            Contact con = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'LastName' =>'Testing',
                    'SSN__c'=>'353535353535',
                    'SerNo__c'=>'4353077'
                    });
            
            Product_Custom__c pd = UnitTestDataGenerator.testProductCustom.buildInsert(new Map<String, Object>{
                'Name' => 'Test',
                    'Pserno__c' => '1542'
                    });
            
            Financial_Account__c fa = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Product__c' => pd.id,
                    'Account_Serno__c' => '1200',
                    'Account_Number__c' => '125646456',
                    'Customer__c' => ac.Id,
                    'Customer_Serno__c' => '126464',
                    'Product_Serno__c' => '454564'
                    });
            
            Card__c cd = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'Card_Number_Truncated__c' => '353453******5345',
                    'Product__c' => pd.Id,
                    'People_Serno__c' => '12456465',
                    'Card_Serno__c' => '45785415',
                    'Prod_Serno__c' => '4746565',
                    'People__c' => con.Id,
                    'Financial_Account__c' => fa.Id,
                    'Financial_Account_Serno__c' => '784966'
                    });
            
            String clsDat = Date.today().format();
            String errMsg = 'Error';
            List<Case> cslist = new List<Case>();
            Case cs1 = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                //(Status='New',Origin = 'Email', Category__c='None');
                'AccountId' => ac.Id,
                    'Financial_Account__c' => fa.Id,
                    'ContactId' => con.Id,
                    'Origin' => 'email',
                    'Status'=>'Closed',
                    'Card__c' => cd.Id,
                    'X120_Months__c' => clsDat,
                    'X18_Months__c' => clsDat,
                    'Category__c' => 'None'
                    });
            cslist.add(cs1);
            
            
            Test.StartTest();
            DeleteCaseData deletecase1=new DeleteCaseData();
            deletecase1.errorMap.put(cslist[0].id,'test');
            deletecase1.IdToSObjectMap.put(cslist[0].id,cslist[0]); 
            Database.executeBatch(deletecase1,200);
            System.assertEquals('Closed',cs1.Status,'The Status is not correct');    
            Test.StopTest(); 
        }
    }
}