/**********************************************************************
Name: OpenCaseControllerLwc_Test
=======================================================================
Purpose: This Test class is used to cover the code of coverage - OpenCaseControllerLwc 
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Kakasaaheb Ekshinge		07-Dec-2020		 Initial Version
**********************************************************************/
@isTest
public class OpenCaseControllerLwc_Test {
	static testmethod void coverOpenCaseCoverage() {
        System.runAs(UnitTestDataGenerator.adminUser) {	
           
		    TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN,
                    'VIP__c'=>true
                    });
            
            Product_Custom__c prodobj=UnitTestDataGenerator.TestProductCustom.buildInsert(new Map<String, Object>{      
            });
            
            
            Financial_Account__c finAccObj=UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd',
                    'Customer__c'=>accountObj.Id, 
                    'Account_Serno__c'=>'Test31231', 
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            
            Card__c cardObj=UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Financial_Account_Serno__c'=>finAccObj.Id, 
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id
                    });
            
            
            Case caseObj=UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{ 
                'ContactId'=>contactObj.Id, 
                    'AccountId'=>accountObj.Id, 
                    'Card__c'=>cardObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Category__c'=>'Account closure', 
                    'Origin'=>'Web',
                    'status'=>'New',
                    'Description'=>'Kundens personnummer (10 siffror utan streck) 0000000061'
                    });
            OpenCaseControllerLwc.openCaseCount(contactObj.id);
            OpenCaseControllerLwc.openCaseCount(accountObj.id);
        }
    }
}