global class UpdateConsentWebServiceMockclass implements WebServiceMock
{
     global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) 
        {
            SOAConsentResponse.UpdateConsentDataResponse_element  objResEle = new SOAConsentResponse.UpdateConsentDataResponse_element();
            SOAConsentResponse.UpdateConsentDataResponseRecordType objRecType = new SOAConsentResponse.UpdateConsentDataResponseRecordType();
            objRecType.Code = '0';
            objRecType.Description = 'Description';
            objResEle.UpdateConsentDataResponseRecord = objRecType;
            response.put('response_x', objResEle);
        }
}