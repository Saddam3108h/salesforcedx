/********************************************************************************************
Name: PrimeActionOnClosedStatus_Test
=============================================================================================
Purpose: This test class is covering 'PrimeActionOnClosedStatus' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0        Srinivas D             4th-April-2019    SFD-984   		    Intial version
2.0		  Kakasaheb Ekshinge 	  22-July-2020		SFD-1596			Code coverage has increases
********************************************************************************************/
@isTest
public class PrimeActionOnClosedStatus_Test {
    public static testmethod void testPrimeActionOnClosedStatus() {
        Test.startTest();
        
        System.runAs(UnitTestDataGenerator.adminUser){
            
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            }); 
            User userObj = UnitTestDataGenerator.standardUser;            
          
            Account objAcc = UnitTestDataGenerator.testAccount.buildInsert(new Map<String, Object>{	 
                'Customer_Serno__c'=>'1123123'  
                    });
            
            Contact contactobj	 = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{	   
                'SerNo__c'=>'2123123'
                    });
            Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{            
                'Category__c'=>'Account closure', 
                    'Origin'=>'Web',
                    'status'=>'New',
                    'OwnerId'=>userObj.id
                    });
            
            Product_Custom__c objProd = UnitTestDataGenerator.TestProductCustom.buildInsert();
            Financial_Account__c objFA = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd', 
                    'Customer__c'=>objAcc.Id,
                    'Account_Serno__c'=>'3123123', 
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>objProd.Id,
                    'Product_Serno__c'=>objProd.Pserno__c
                    });
            
            Card__c objCard = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>objFA.Id,
                    'Prod_Serno__c'=>objProd.Pserno__c, 
                    'Product__c'=>objProd.Id
                    });
            
            Case caseObj1 = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{            
                'Category__c'=>'Account closure', 
                    'Origin'=>'Web',
                    'status'=>'Re Open',
                    'OwnerId'=>userObj.id
                    });
            
                      
           
            CaseCloseBySubject.fetchCases(new List<ID>{caseObj.Id});  
            PrimeActionOnClosedStatus.getStatus(caseObj.id);
            PrimeActionOnClosedStatus.updateStatus(caseObj.id);
            PrimeActionOnClosedStatus.StatusUpdate(caseObj.id);
            PrimeActionOnClosedStatus.StatusUpdates(caseObj.id);
            PrimeActionOnClosedStatus.ownerAssign(caseObj.id);
            PrimeActionOnClosedStatus.getPeople(objCard .id);
            PrimeActionOnClosedStatus.getFAdetails(objFA.id);
            System.assertEquals('New',caseObj.status,'The status is not correct');
            Test.stopTest();
        }
    }
}