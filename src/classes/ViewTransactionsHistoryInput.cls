/*
----------------------------------------------------------------------------------------
Author    Date         Description
Shanthi   10/11/2016   Input class to implement SOA service using Integration framework.
----------------------------------------------------------------------------------------
*/
public class ViewTransactionsHistoryInput{

    public Integer InstitutionId;
    public String Reference;
    public Integer PageSize;
    public Integer SkipIndex;
    public Date From_x;
    public Date To;
    public String CardNo;
    public Boolean Include;
    public String Number_x;
    public String TransactionType;
     //To Cover Test Class 100%
    public void test()
    {
        
    }

}