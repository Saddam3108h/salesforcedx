/**********************************************************************
Name: ManageCampaignTrigger
=======================================================================
Purpose: This is helper class of ManageCampaignTrigger

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         	               Initial Version
2.0			Priyanka Singh		    11-Aug-2020	       Trigger Factory Implementation

**********************************************************************/
public class ManageCampaignTriggerHandler implements ITriggerHandler {
    Set<String> finAccSerno = new Set<String>();
    Set<String> productSerno = new Set<String>();
    Set<String> contactSerno = new Set<String>();
    Map<String,Id> getCmpRelatedToIdContact = new Map<String,Id>();
    Map<String,Id> getCmpRelatedToIdFinancialAcc = new Map<String,Id>();
    Map<String,Id> getCmpRelatedToIdProduct = new Map<String,Id>();
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
 
    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
      
        if (TriggerSettings__c.getInstance().ManageCampaignExecution__c 
           &&  TriggerSettings__c.getInstance().OrgExecution__c)
        { 
            return true;
        }
        else
        { 
            return TriggerDisabled;
        }
        
    }
 
    public void BeforeInsert(List<SObject> newItems) {
        UpdateCampaign(newItems, null);
    }    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
          Map<Id, Manage_Campaign__c> newManageCapgnItems = (Map<Id, Manage_Campaign__c>) newItems; 
          Map<Id, Manage_Campaign__c> oldManageCapgnItems = (Map<Id, Manage_Campaign__c>) oldItems; 
          UpdateCampaign(newManageCapgnItems.values(),oldManageCapgnItems.values());
    }
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {
    }
 
    public void AfterInsert(Map<Id, SObject> newItems) {}
 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
     
        
    }     
    public void AfterDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    public ManageCampaignTriggerHandler()
    {
        
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     1-Jul-2020
*   User Story : SFD-1584
*   Param: None
*   Return: Map    
*   Description: In this method we update Product name on manage campaign.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    Public Void UpdateCampaign(List<Manage_Campaign__c> newCmpList, List<Manage_Campaign__c> OldCmpList)
    {
        //for Insert
        if(OldCmpList == null)
        {
            for(Manage_Campaign__c objCmp : newCmpList)
            {
                if(objCmp.People_SerNo__c != '' && objCmp.People_SerNo__c != null)
                {
                    contactSerno.add(objCmp.People_SerNo__c);
                }
                if(objCmp.AccountSerno__c != '' && objCmp.AccountSerno__c != null)
                {
                    finAccSerno.add(objCmp.AccountSerno__c);
                }
                if(objCmp.ProductSerNo__c != '' && objCmp.ProductSerNo__c != null)
                {
                    productSerno.add(objCmp.ProductSerNo__c);
                }
            }
            //Query to get object Id
            if(contactSerno.size() > 0)
            {
                for(Contact objCon: [SELECT id,serno__c from Contact where serno__c IN: contactSerno])
                {
                    getCmpRelatedToIdContact.put(objCon.serno__c, objCon.id);
                }
            }
            if(finAccSerno.size() > 0)
            {
                for(Financial_Account__c objFA: [SELECT id, Account_Serno__c from Financial_Account__c where Account_Serno__c IN: finAccSerno])
                {
                    getCmpRelatedToIdFinancialAcc.put(objFA.Account_Serno__c, objFA.id);
                }
            }
            if(productSerno.size() > 0)
            {
                for(Product_Custom__c objProd: [SELECT id, pserno__c from Product_Custom__c where pserno__c IN: productSerno])
                {
                    getCmpRelatedToIdProduct.put(objProd.pserno__c, objProd.id);
                }
            }
            if(getCmpRelatedToIdContact != null && !getCmpRelatedToIdContact.isEmpty())
            {
                for(Manage_Campaign__c objCmp: newCmpList)
                {
                    objCmp.People__c = getCmpRelatedToIdContact.get(objCmp.People_SerNo__c);
                    //objCmp.FinAccount__c = getCmpRelatedToIdFinancialAcc.get(objCmp.AccountSerno__c);
                    //objCmp.Product_Name__c = getCmpRelatedToIdProduct.get(objCmp.ProductSerNo__c);
                    objCmp.DateTime_Stamp__c = System.now();
                }
            }
            if(getCmpRelatedToIdFinancialAcc != null && !getCmpRelatedToIdFinancialAcc.isEmpty())
            {
                for(Manage_Campaign__c objCmp: newCmpList)
                {
                    //objCmp.People__c = getCmpRelatedToIdContact.get(objCmp.People_SerNo__c);
                    objCmp.FinAccount__c = getCmpRelatedToIdFinancialAcc.get(objCmp.AccountSerno__c);
                    //objCmp.Product_Name__c = getCmpRelatedToIdProduct.get(objCmp.ProductSerNo__c);
                    objCmp.DateTime_Stamp__c = System.now();
                }
            }
            if(getCmpRelatedToIdProduct != null && !getCmpRelatedToIdProduct.isEmpty())
            {
                for(Manage_Campaign__c objCmp: newCmpList)
                {
                    //objCmp.People__c = getCmpRelatedToIdContact.get(objCmp.People_SerNo__c);
                    //objCmp.FinAccount__c = getCmpRelatedToIdFinancialAcc.get(objCmp.AccountSerno__c);
                    objCmp.Product_Name__c = getCmpRelatedToIdProduct.get(objCmp.ProductSerNo__c);
                    objCmp.DateTime_Stamp__c = System.now();
                }
            }
        }
        //for Update
        if(oldCmpList != null)
        {
            for(integer i=0;i<newCmpList.size();i++)
            {
                for(integer j=0;j<oldCmpList.size();j++)
                {
                    //For customer serno
                    if(newCmpList[i].People_SerNo__c != null && newCmpList[i].People_SerNo__c != '')
                    {
                        if(oldCmpList[j].People_SerNo__c != null && oldCmpList[j].People_SerNo__c != '')
                        {
                            if(oldCmpList[j].People_SerNo__c != newCmpList[i].People_SerNo__c)
                            {
                                contactSerno.add(newCmpList[i].People_SerNo__c);
                            }
                        }
                    }
                    //For FA serno
                    if(newCmpList[i].AccountSerno__c != null && newCmpList[i].AccountSerno__c != '')
                    {
                        if(oldCmpList[j].AccountSerno__c != null && oldCmpList[j].AccountSerno__c != '')
                        {
                            if(oldCmpList[j].AccountSerno__c != newCmpList[i].AccountSerno__c)
                            {
                                finAccSerno.add(newCmpList[i].AccountSerno__c);
                            }
                        }
                    }
                    //For Product serno
                    if(newCmpList[i].ProductSerNo__c != null && newCmpList[i].ProductSerNo__c != '')
                    {
                        if(oldCmpList[j].ProductSerNo__c != null && oldCmpList[j].ProductSerNo__c != '')
                        {
                            if(oldCmpList[j].ProductSerNo__c != newCmpList[i].ProductSerNo__c)
                            {
                                productSerno.add(newCmpList[i].ProductSerNo__c);
                            }
                        }
                    }
                }
            }
            //Query to get object Id
            if(contactSerno.size() > 0)
            {
                for(Contact objCon: [SELECT id,serno__c from Contact where serno__c IN: contactSerno])
                {
                    getCmpRelatedToIdContact.put(objCon.serno__c, objCon.id);
                }
            }
            if(finAccSerno.size() > 0)
            {
                for(Financial_Account__c objFA: [SELECT id, Account_Serno__c from Financial_Account__c where Account_Serno__c IN: finAccSerno])
                {
                    getCmpRelatedToIdFinancialAcc.put(objFA.Account_Serno__c, objFA.id);
                }
            }
            if(productSerno.size() > 0)
            {
                for(Product_Custom__c objProd: [SELECT id, pserno__c from Product_Custom__c where pserno__c IN: productSerno])
                {
                    getCmpRelatedToIdProduct.put(objProd.pserno__c, objProd.id);
                }
            }
            if(getCmpRelatedToIdContact != null && !getCmpRelatedToIdContact.isEmpty())
            {
                for(Manage_Campaign__c objCmp: newCmpList)
                {
                    objCmp.People__c = getCmpRelatedToIdContact.get(objCmp.People_SerNo__c);
                    //objCmp.FinAccount__c = getCmpRelatedToIdFinancialAcc.get(objCmp.AccountSerno__c);
                    //objCmp.Product_Name__c = getCmpRelatedToIdProduct.get(objCmp.ProductSerNo__c);
                    objCmp.DateTime_Stamp__c = System.now();
                }
            }
            if(getCmpRelatedToIdFinancialAcc != null && !getCmpRelatedToIdFinancialAcc.isEmpty())
            {
                for(Manage_Campaign__c objCmp: newCmpList)
                {
                    //objCmp.People__c = getCmpRelatedToIdContact.get(objCmp.People_SerNo__c);
                    objCmp.FinAccount__c = getCmpRelatedToIdFinancialAcc.get(objCmp.AccountSerno__c);
                    //objCmp.Product_Name__c = getCmpRelatedToIdProduct.get(objCmp.ProductSerNo__c);
                    objCmp.DateTime_Stamp__c = System.now();
                }
            }
            if(getCmpRelatedToIdProduct != null && !getCmpRelatedToIdProduct.isEmpty())
            {
                for(Manage_Campaign__c objCmp: newCmpList)
                {
                    //objCmp.People__c = getCmpRelatedToIdContact.get(objCmp.People_SerNo__c);
                    //objCmp.FinAccount__c = getCmpRelatedToIdFinancialAcc.get(objCmp.AccountSerno__c);
                    objCmp.Product_Name__c = getCmpRelatedToIdProduct.get(objCmp.ProductSerNo__c);
                    objCmp.DateTime_Stamp__c = System.now();
                }
            }
            
            
        }
    }
}