@isTest
public class ShadowContactsMCQualificationSch_Test {
    public static testMethod void testshadowContactMCQualificationschedule() {
        System.runAs(UnitTestDataGenerator.adminUser){
            
            Test.StartTest();
            ShadowContactsMCQualificationSchedule sh1 = new ShadowContactsMCQualificationSchedule();
            String sch = '0 0 20 * * ?'; 
            System.schedule('Shadow Contact Check', sch, sh1); 
            Test.stopTest(); 
        }
    }
}