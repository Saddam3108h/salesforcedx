/********************************************************************************************
Name: DeleteManageCampaignTypeBatch_Test
=============================================================================================
Purpose: This test class is covering 'DeleteManageCampaignTypeBatch_Test' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date             Created for Jira     Detail
1.0		    Kakasaheb Ekshinge 	       07-October-2020	SFD -1771		     Intial version
********************************************************************************************/
@isTest
public class DeleteManageCampaignTypeBatch_Test {
    public static testMethod void deleteManageCampaignBatchTest(){
        
        System.runAs(UnitTestDataGenerator.adminUser) {
            
            List<DeleteCampaignType__c>  lstDe = new List<DeleteCampaignType__c>();
            DeleteCampaignType__c DC1 = new DeleteCampaignType__c();
            DC1.Name = 'Information';
            
            DeleteCampaignType__c DC2 = new DeleteCampaignType__c();
            DC2.Name = 'News Letter';
            
            DeleteCampaignType__c DC3 = new DeleteCampaignType__c();
            DC3.Name = 'Retention Group';
            
            DeleteCampaignType__c DC4 = new DeleteCampaignType__c();
            DC4.Name = 'Welcome';
            lstDe.add(DC1);	lstDe.add(DC2);	lstDe.add(DC3);	lstDe.add(DC4);	
            
            insert lstDe;   
            
            
            Manage_Campaign__c camp = UnitTestDataGenerator.testManageCampaign.buildInsert(new Map<String, Object>{
                'Campaign_Type__c'=>'Welcome'
            });
            //system.debug('==>'+camplist);
            SchedulableContext sc = null;
            test.setCreatedDate(camp.Id,date.today()-30);
            DeleteManageCampaignTypeBatch d=new DeleteManageCampaignTypeBatch();
            Database.executeBatch(d,200) ;
            
        }
    }
}