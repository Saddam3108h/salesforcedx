public class PayBackCreditBalanceOutput{
    public String MsgId;
    public String CorrelationId;
    public String RequestorId;
    public String SystemId;
    public String InstitutionId;
    //public Integer Code;
    public String Code;
    public String Description;
    
    public ErrorResult[] ErrorResultList;
    
    public class ErrorResult {
        public Integer Code;
        public String Description;
    } 
    //To Cover Test Class 100%
    public void test()
    {
        
    } 
}