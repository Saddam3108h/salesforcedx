/********************************************************************************************
Name: DeactivateUsersSchedular_Test
=============================================================================================
Purpose: This test class is covering 'DeactivateUsersSchedular' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         	Created for Jira     Detail
1.0         Hari            	   10-AUG-2020       SFD-1422   		Intial version
********************************************************************************************/
@isTest(SeeAllData=false)
private class DeactivateUsersSchedular_Test{
    static testmethod void testschedule(){
        System.runAs(UnitTestDataGenerator.adminUser) {  
            DeactivateUsersSchedular.lastLoginDate = Date.today();   
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            ServiceSettings__c serviceSettings = UnitTestDataGenerator.serviceSettings.buildInsert(new Map<String, Object>{            
            });
            
            SalesforceTeam__c emailAdd = UnitTestDataGenerator.testSalesforceTeam.buildInsert(new Map<String, Object>{            
            });
            
            set<Id> setAccId = new Set<ID>();
            setAccId.add(UnitTestDataGenerator.adminUser.id);                

            Test.StartTest();
            DeactivateUsersSchedular schJob = new DeactivateUsersSchedular();             
            schJob.execute(null);  
            Test.stopTest();  
            System.assertEquals(emailAdd.Name, 'Test User');
        }   
    }
}