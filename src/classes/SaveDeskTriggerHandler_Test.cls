/**********************************************************************
Name: SaveDeskTriggerHandler_Test
=======================================================================
Purpose: This Test class is used for test coverage of the SaveDeskTriggerHandler.User Story -SFD-1430

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         20-May-2020       Initial Version

**********************************************************************/
@isTest(seeAllData = false)
public class SaveDeskTriggerHandler_Test {
     /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Create SaveDesk and other related data to coverage the code for the
	updateSDeskCustomerAndFAccount, updateAttemptField,updateTaskRecord,createSaveDeskListView methods.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testMethod void updateSDeskCustomerAndFAccountTest() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            
          
		    TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            
            SaveDeskQueue__c sdObj = UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Callback Later',
                    'Calling_Attempts__c'=>'Inbound Call',
                    'Task_Count__c'=>0
                    });
            SaveDeskQueue__c sdObjs = UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Callback Later',
                    'Calling_Attempts__c'=>'Inbound Call',
                    'Task_Count__c'=>1
                    });
            SaveDeskQueue__c sdObjects = UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Callback Later',
                    'Calling_Attempts__c'=>'Inbound Call',
                    'Task_Count__c'=>2
                    });
            SaveDeskQueue__c sdObject = UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Callback Later',
                    'Calling_Attempts__c'=>'Inbound Call',
                    'Task_Count__c'=>3
                    });
            SaveDeskQueue__c sdObjec = UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Callback Later',
                    'Calling_Attempts__c'=>'Inbound Call',
                    'Task_Count__c'=>4
                    });
            List<SaveDeskQueue__c> saveDeskList = new List<SaveDeskQueue__c>();
            saveDeskList.add(sdObj);
            saveDeskList.add(sdObjs);
            saveDeskList.add(sdObjects);
            saveDeskList.add(sdObject);
            saveDeskList.add(sdObjec);
            List<String>  sDeskAttr = new List<String> {'Status__c','Response__c','Calling_Attempts__c','Task_Count__c'};
                List<SaveDeskQueue__c> saveDeskReload = UnitTestDataGenerator.TestSaveDesk.reloadList(saveDeskList, sDeskAttr);
            
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN
                    });
            system.debug('-contactObj--'+contactObj.SSN__c);
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            Card__c cardObj = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Product__c'=>prodobj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c
                    });
            List<Card__c> cardList = new List<Card__c>();
            cardList.add(cardObj);
            List<String>  cardAttr = new List<String> {'People__c','Financial_Account__c','Product__c','Prod_Serno__c'};
                List<Card__c> cardReload = UnitTestDataGenerator.TestCard.reloadList(cardList,cardAttr);
            system.debug('--cardReload--'+cardReload);
            
            
            Task taskObj = UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'WhoId'=>contactObj.Id,
                    'CreatedById'=>UserInfo.getUserId()
                    });
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            List<String>  taskAttr = new List<String> {'WhoId','CreatedById'};
                List<Task> taskReload = UnitTestDataGenerator.TestTask.reloadList(taskList,taskAttr);
            
            SaveDeskTriggerHandler.updateSDeskCustomerAndFAccount(saveDeskList);
            SaveDeskTriggerHandler.updateAttemptField(saveDeskList);
            SaveDeskTriggerHandler.updateTaskRecord(saveDeskList);
            SaveDeskTriggerHandler.createSaveDeskListView(saveDeskList);
			system.assertEquals('Callback Later', sdObjec.Response__c, 'Task Response is not correct');            
        }
    }
    
      /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Create SaveDesk if condition data to coverage the code for the
    updateAttemptField methods.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    static testMethod void updateAttemptFieldIfTest() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            
            SaveDeskQueue__c sdObj = UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Call not answered',
                    'Task_Count__c'=>1
                    });
            List<SaveDeskQueue__c> saveDeskList = new List<SaveDeskQueue__c>();
            saveDeskList.add(sdObj);
            List<String>  sDeskAttr = new List<String> {'Status__c','Response__c','Task_Count__c'};
                List<SaveDeskQueue__c> saveDeskReload = UnitTestDataGenerator.TestSaveDesk.reloadList(saveDeskList, sDeskAttr);
            
            Task taskObj = UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'CreatedById'=>UserInfo.getUserId()
                    });
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            List<String>  taskAttr = new List<String> {'CreatedById'};
                List<Task> taskReload = UnitTestDataGenerator.TestTask.reloadList(taskList,taskAttr);
            SaveDeskTriggerHandler.updateAttemptField(saveDeskList);
        	system.assertEquals(1, sdObj.Task_Count__c, 'Task count is not correct');
        }
    }
    
          /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Create SaveDesk else condition data to coverage the code for the
    updateAttemptField methods.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    static testMethod void updateAttemptFieldElseTest() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            SaveDeskQueue__c sdObj = UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Call not answered',
                    'Task_Count__c'=>2
                    });
            List<SaveDeskQueue__c> saveDeskList = new List<SaveDeskQueue__c>();
            saveDeskList.add(sdObj);
            List<String>  sDeskAttr = new List<String> {'Status__c','Response__c','Task_Count__c'};
                List<SaveDeskQueue__c> saveDeskReload = UnitTestDataGenerator.TestSaveDesk.reloadList(saveDeskList, sDeskAttr);
            
            Task taskObj = UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'Savedesk__c'=>saveDeskList[0].Id,
                    'CreatedById'=>UserInfo.getUserId()
                    });
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            List<String>  taskAttr = new List<String> {'Savedesk__c','CreatedById'};
                List<Task> taskReload = UnitTestDataGenerator.TestTask.reloadList(taskList,taskAttr);
            SaveDeskTriggerHandler.updateAttemptField(saveDeskList);
            SaveDeskTriggerHandler.updateLatestTaskRecord(saveDeskList);
            system.assertEquals('Call not answered', sdObj.Response__c, 'Response is not correct');
        }
    }  
}