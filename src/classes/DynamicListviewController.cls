/**********************************************************************
Name: DynamicListviewController
=======================================================================
Purpose: This class is trying to create the dynamic listview in SaveDesk Object.User Story -SFD-1489

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         20-May-2020       Initial Version

**********************************************************************/
global class DynamicListviewController {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-
*   Param: None
*   Return: None    
*   Description: In this method we are trying to used for make the callout to interanally to create the Listview in Savedesk Object 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    @future (callout=true)
    public static void createListView(List<String> listViewName) {
        try {
            system.debug('--listViewName-'+listViewName.size());
            MetadataService.MetadataPort service =  createService();
            List<MetadataService.ListView> metaDatalistViewList = new List<MetadataService.ListView>();
            
            
            for(String str : listViewName) {
                
                MetadataService.ListView listView = new MetadataService.ListView();
                List<MetadataService.ListViewFilter> filterListToAdd = new List<MetadataService.ListViewFilter>();
                
                listView.fullName = StaticConstant.SAVEDESK+str.replace(' ','_');
                system.debug('---fullname---'+listView.fullName);
                listView.label = str;
                system.debug('---fullname---'+listView.label);
                MetadataService.ListViewFilter filter = new MetadataService.ListViewFilter();
                filter.field = StaticConstant.SAVEDESK_CAMPAIGN_TYPE;
                filter.operation = 'equals';
                filter.value = str;
                filterListToAdd.add(filter);
                MetadataService.ListViewFilter filter1 = new MetadataService.ListViewFilter();
                filter1.field = StaticConstant.SAVEDESK_STATUS;
                filter1.operation = 'notEqual';
                filter1.value = StaticConstant.SAVEDESK_CLOSED_STATUS;
                filterListToAdd.add(filter1);                
                System.debug('----filterListToAdd--'+filterListToAdd.size());
                listView.filters = filterListToAdd;
                listView.filterScope = StaticConstant.SAVEDESK_FILTERSCOPE;
                
                listView.columns = new List<String> {'NAME','Institution_Id__c','Customer_Name__c','Financial_Account__c','Interest_Rate__c',
                    'Product_Name__c','Deadline_Date_Campaign__c','Offer_Description__c','Phone_No__c','Calling_Attempts__c','Call_Time_Stamp__c',
                    'Extra_Comment__c','Comments__c','Response__c','Agent_ID__c','Status__c'
                    };
                        metaDatalistViewList.add(listView);
            }
            
            List<MetadataService.SaveResult> results = service.createMetadata(metaDatalistViewList);
            system.debug('--results--'+results.size());
            handleSaveResults(results);
        } catch(Exception exp) {
            StatusLogHelper.logSalesforceError('DynamicListviewController', 'E005', 'createListView', exp, false); // Class,method
        }
        
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-
*   Param: None
*   Return: Service    
*   Description: In this method we are trying to create the service.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    
    public static MetadataService.MetadataPort createService()
    {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;
    }
    
    public class MetadataServiceExamplesException extends Exception { }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-
*   Param: None
*   Return: Service    
*   Description: In this method we are trying to capture the error result of the service.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    public static void handleSaveResults(List<MetadataService.SaveResult> saveResult)
    {
        // Nothing to see?
        for(MetadataService.SaveResult response : saveResult) {
            if(response==null 
               || response.success
              )
                return;
            // Construct error message and throw an exception
            if(response.errors!=null) {
                List<String> messages = new List<String>();
                messages.add(
                    (response.errors.size()==1 ? 'Error ' : 'Errors ') +
                    'occured processing component ' + response.fullName + '.');
                for(MetadataService.Error error : response.errors)
                    messages.add(
                        error.message + ' (' + error.statusCode + ').' +
                        ( error.fields!=null && error.fields.size()>0 ?
                         ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );                
            }
        }
    }
    
}