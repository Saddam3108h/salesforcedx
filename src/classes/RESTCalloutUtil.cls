/**********************************************************************
Name: RESTCalloutUtil
=======================================================================
Purpose: This apex is used for RESTCalloutUtil

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                 Date              Detail
1.0         Priyanka Singh         29-Sept-2020      Initial Version

**********************************************************************/
public class RESTCalloutUtil {
    /**********************************************************************
*   Author: Priyanka Singh
*   Date:     
*   User Story : 
*   Param: None
*   Return: None    
*   Description: In this method We check for existing logged in User record from Custom setting
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/    
    
    public static ManageCurityAccessToken__c findUser() {        
        ManageCurityAccessToken__c userCS = new ManageCurityAccessToken__c();
        if(ManageCurityAccessToken__c.getValues(userinfo.getUserId()) != null){
            userCS = ManageCurityAccessToken__c.getValues(userinfo.getUserId()); 
        }       
        return userCS;         
    }
    public static Boolean calculateExpiryofAccTkn(ManageCurityAccessToken__c accTokn) {  
        Long createdDateTime = accTokn.Created_Date__c.getTime();       
        Long currentTime = DateTime.now().getTime();        
        Long milliseconds = currentTime - createdDateTime;       
        Long seconds = milliseconds / 1000;
        Long minutes = (seconds / 60)+3;      
        return ((minutes > accTokn.AccessTokenExpriy__c) ? true : false);// token expired in case of true        
    }
    
    /**********************************************************************
*   Author: Kakasaheb Ekshinge
*   Date:     
*   User Story : 
*   Param: None
*   Return: None    
*   Description: In this method we create the Custom setting record for latest Token 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/    
    public static void createUser(String accessToken,DateTime createdDateTime,Integer tokenExpiry,Blob key){        
        List<ManageCurityAccessToken__c> userCreationList = new List<ManageCurityAccessToken__c>();
        ManageCurityAccessToken__c userCreation = new ManageCurityAccessToken__c();
        userCreation.Name = Userinfo.getName();          
        userCreation.Access_Token__c = accessToken;       
        userCreation.AccessTokenExpriy__c = (tokenExpiry/60);      
        userCreation.Created_Date__c = createdDateTime;         
        userCreation.SetupOwnerId = userinfo.getUserId();       
        userCreation.Key__c = EncodingUtil.base64Encode(key);       
        userCreationList.add(userCreation);
        try{
            insert userCreationList;
            system.debug('Record created ManageCurityAccessToken ==>'+userCreationList);
        }
        Catch(Exception Ex){
            system.debug('Exception in ManageCurityAccessToken ==>'+Ex);
            StatusLogHelper.logSalesforceError('RESTCalloutUtil', 'createUser', 'Error in creating user in acces token custom setting', ex, false);
        }
    }
    
    /**********************************************************************
*   Author: Kakasaheb Ekshinge
*   Date:     
*   User Story : 
*   Param: None
*   Return: None    
*   Description: In this method we update the Custom setting record for latest Token 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/ 
   // @future
    public static void updateUser(String accessToken,DateTime createdDateTime,Integer tokenExpiry,Blob key) {        
        ManageCurityAccessToken__c userUpdate = [select Id,Access_Token__c,AccessTokenExpriy__c,Created_Date__c,SetupOwnerId 
                                                 FROM ManageCurityAccessToken__c WHERE SetupOwnerId = :Userinfo.getUserId()];
        userUpdate.Access_Token__c = accessToken;
        userUpdate.AccessTokenExpriy__c = (tokenExpiry/60);
        userUpdate.Created_Date__c = createdDateTime;         
        userUpdate.SetupOwnerId = userinfo.getUserId();
        userUpdate.Key__c = EncodingUtil.base64Encode(key);
        try{
            update userUpdate;
        }
        catch(Exception Ex){
            system.debug('Exception in update ManageCurityAccessToken ==>'+Ex);  
            StatusLogHelper.logSalesforceError('RESTCalloutUtil', 'updateUser', 'Error in creating user in acces token custom setting', ex, false);

        }
    }
    /**********************************************************************
*   Author: Priyanka Singh
*   Date:     
*   User Story : 
*   Param: None
*   Return: None    
*   Description: In this method we check whether logged in user has valid access token from custom setting
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    @AuraEnabled
    public static ResponseWrapper fetchDataByMap(Map<String,String> attributeMap)
    { 
        try{
            ResponseWrapper responseWrapper = new ResponseWrapper();
            if(attributeMap != null && attributeMap.size()>0)
            {
                HttpRequest req = new HttpRequest();
                req.setTimeout(4000); // 4 seconds               
                if(attributeMap.containsKey('accessToken')){
                    req.setHeader('Authorization', 'Bearer '+attributeMap.get('accessToken'));   
                }
                if(attributeMap.containsKey('certificate')){
                    req.setClientCertificateName(attributeMap.get('certificate'));   
                }
                if(attributeMap.containsKey('endpoint')){
                    req.setEndpoint(attributeMap.get('endpoint'));      
                }
                if(attributeMap.containsKey('httpMethod')){
                    req.setMethod(attributeMap.get('httpMethod'));   
                }
                if(attributeMap.containsKey('ContentType')){
                    req.setHeader('Content-Type', attributeMap.get('ContentType')); 
                }          
                if(attributeMap.containsKey('inputJson')){
                   req.setBody(attributeMap.get('inputJson'));
                } 
                Http http = new Http();
                HTTPResponse res = http.send(req); 
                responseWrapper.RequestBody = ''+req;
                responseWrapper.StatusCode = res.getStatusCode();
                responseWrapper.Status = res.getStatus();
                responseWrapper.ResponseBody = res.getBody();
            }  
            return responseWrapper;    
        }catch(exception ex){
            system.debug('@@Exception--'+ex);
            StatusLogHelper.logSalesforceError('RESTCalloutUtil', 'fetchDataByMap', 'Error while sending request to Microservice', ex, false);
            return null;
        }    
    }
    
    /**********************************************************************
*   Author: Priyanka Singh
*   Date:     
*   User Story : 
*   Param: None
*   Return: access token    
*   Description: In this method we check whether logged in user has valid access token from custom setting
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    @AuraEnabled
    public static String validateUserAccTkn() // main function to be call from each module
    {
        system.debug('-----validateUserAccTkn called-----');   
        String accessToken ='';
        accToknWrapObj = null;
        Boolean responseStats = true;
        AccessTokenOnOff__c onOfFObj = AccessTokenOnOff__c.getValues('TokenOnOff'); 
       	if(onOfFObj.active__c){        
            
            Blob privateKey;
            ManageCurityAccessToken__c userCS = findUser(); 
            if(userCS.Access_Token__c != null && !calculateExpiryofAccTkn(userCS)){ // for exsiting access token 
                accessToken = userCS.Access_Token__c;
                privateKey = EncodingUtil.base64Decode(userCS.key__c);   
            }else { //for new access token          
                accToknWrapObj= findAccessTokenJwtCurity();
                if(accToknWrapObj!=null){
                    privateKey = Crypto.generateAesKey(128);   
                    accessToken = encriptString(accToknWrapObj.access_token,privateKey);                
                    if(userCS.Access_Token__c == null)
                        createUser(accessToken,DateTime.now(),accToknWrapObj.expires_in,privateKey);
                    else 
                        updateUser(accessToken,DateTime.now(),accToknWrapObj.expires_in,privateKey); 
                }else{
                    responseStats = false;
                }         
            }              
            if(responseStats)        
                return  decryptString(accessToken, privateKey);
            else{                 
                return 'NO TOKEN';            
            }
   
        } 
        else {
            system.debug('===>No encryption & Decryption ====>');
            accToknWrapObj= findAccessTokenJwtCurity();            
            if(accToknWrapObj== null){
             StatusLogHelper.logSFErrors('NoAccessTokenAtCurityEnd','NoAccessToken','C', 'NO TOKEN', 'No Response');
             return 'NO TOKEN';
            } 
            else 
               return accToknWrapObj.access_token;            
        }
    }
    /**********************************************************************
*   Author: Priyanka Singh
*   Date:     
*   User Story : 
*   Param: None
*   Return: None    
*   Description: In this method , we are making callout to get access token from Curity
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description] 
**********************************************************************/
    public static AccessTokenWrapper findAccessTokenJwtCurity(){ 
        ManageRESTServices__c curityEndPoint = ManageRESTServices__c.getValues('CurityEndPoint');
        if(curityEndPoint != null){   
            DateTime exp = DateTime.now().addMinutes(30); 
            long mill = exp.getTime();
          
            Map<String,String> extraClaims = new Map<String,String>(); 
            extraClaims.put('nbf', DateTime.now()+'');
            extraClaims.put('iat', DateTime.now()+'');     
            extraClaims.put('exp', mill+'');            
            User useObj = [select Id, UserRole.Name,FederationIdentifier from user WHERE id =: UserInfo.getUserId()];        
            
            if(String.isNotBlank(useObj.FederationIdentifier) && useObj.FederationIdentifier!=null && useObj.UserRole.Name!=null) {
                Auth.Jwt jwt = new Auth.Jwt();
                jwt.setIss(curityEndPoint.ClientId__c); 
                jwt.setAud(curityEndPoint.Endpoint__c);
                jwt.setSub(useObj.FederationIdentifier+' '+useObj.UserRole.Name); 
                jwt.setAdditionalClaims(extraClaims);
                //  Imp code to set the access Token validity 
                jwt.setValidityLength(1200); // 60X20 = 1200= 20 min
                
                Auth.JWS jws = new Auth.JWS(jwt, curityEndPoint.CertificateName__c);
                
                String assertion = jws.getCompactSerialization();
                String payload = 'assertion=' + assertion+'&grant_type=' + System.EncodingUtil.urlEncode(curityEndPoint.GrantType__c, 'UTF-8')+  
                    '&client_id='+curityEndPoint.ClientId__c+'&client_secret='+curityEndPoint.ClientSecret__c+'&scope='+curityEndPoint.Scope__c;
                
                Http httpObj = new Http();
                HttpRequest req = new HttpRequest();
                HttpResponse res;
                req.setEndpoint(curityEndPoint.Endpoint__c);
                req.setClientCertificateName(curityEndPoint.CertificateName__c); 
                req.setMethod('POST');
                req.setTimeout(2000);// means 2 second = (2X1000 ms)
                req.setHeader('Content-Type', curityEndPoint.ContentType__c);
                req.setBody(payload);
                res = httpObj.send(req);
                if (res.getStatusCode() != 200) { //Make sure about the Status code with Dev team
                    System.debug('The status code returned was not expected: ' +
                                 res.getStatusCode() + ' ' + res.getStatus());                                  
                } 
                else { // Success block
                       accToknWrapObj = (AccessTokenWrapper)Json.deserialize(res.getbody(),AccessTokenWrapper.class); // List<permissionDataWrapper>.class 
                    }               
            }else{
               StatusLogHelper.logSFErrors('NoFedIDandRoleSet','NO FedAndRole found','400', 'Request body empty because of FedrationId and Role', 'No Response');
            }
        }        
        return accToknWrapObj;
    }
    
    /**********************************************************************
*   Author: Kakasaheb Ekshinge
*   Date:     
*   User Story : 
*   Param: None
*   Return: None    
*   Description: Below two methods will get used to encode the access token
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public static String encriptString(String data,Blob key){
        try{
            system.debug('==>encriptString function called');
            Blob bdata = Blob.valueOf(data);          
            Blob encrypted = Crypto.encryptWithManagedIV('AES128', key, bdata);    
            return  EncodingUtil.base64Encode(encrypted); 
        }catch(Exception e){
            system.debug('exception'+e.getMessage());   
            return '';
        }
        
    }
    
      /**********************************************************************
*   Author: Kakasaheb Ekshinge
*   Date:     
*   User Story : 
*   Param: None
*   Return: None    
*   Description: Below two methods will get used to decode the access token
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    public static String decryptString(String decryptString,Blob key){
        try{
            system.debug('==>decryptString function called');
            Blob DecodedEncryptedBlob = EncodingUtil.base64Decode(decryptString);
            Blob decryptedB = Crypto.decryptWithManagedIV('AES128',key, DecodedEncryptedBlob);
            return decryptedB.toString();   
        }catch(Exception e){
            system.debug('exception'+e.getMessage());
            return '';
        }
        
    }
    
    public static AccessTokenWrapper accToknWrapObj {get; set;}
    public class AccessTokenWrapper{ 
        String access_token;        
        Integer expires_in;
        String refresh_expires_in;
        String refresh_token;
        String token_type;
        String scope;
        String session_state;
    }
    public static ResponseWrapper responseWrapper {get; set;}
    public class ResponseWrapper {       
        public Integer StatusCode ;
        public String Status;
        public String RequestBody;
        public String ResponseBody;
        public String ResponseMoreInfo;
        public String calloutHappened;
    }
}