@isTest
private class PrimeActionRetryBatch_Test
{
    static Prime_Action_Log__c objPrimeLog;
    @isTest
    static void test() 
    {
       
         System.runAs(UnitTestDataGenerator.adminUser){
          TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
        
        List<Prime_Action_Log__c> inserPrimeActionLst = new List<Prime_Action_Log__c>();
        //Account Creation for case
        Account accountObj=new Account
        (
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='2342323'
        );
        insert accountObj;
        
        
        
        //Contact creation for case
        Contact contactObj =new Contact
        (
            LastName='Test Contact', 
            FirstName='First Test Contact',
            SerNo__c='2312233', 
            Institution_Id__c=2, 
            SSN__c = '12589',
            AccountId=accountObj.Id,
            Phone='123123123', 
            Fax='98789879', 
            MobilePhone='98789879', 
            HomePhone='123123123', 
            Email='testemail@test.com'          
        );
        insert contactObj;
        //Financial Account Creation for case
        /*Financial_Account__c finAccObj=new Financial_Account__c
        (
            Account_Number__c='Test5700192', 
            Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2
        );
        insert finAccObj;*/
        
        //Case creation
        Case objCase = new Case
        (
            //AccountId = accountObj.Id,
            //ContactId = contactObj.id,
            Status = 'New',
            Category__c = 'Payment Free Holiday'
            //Financial_Account__c = finAccObj.id
        );
        insert objCase;
        
        Prime_Action_Log__c objPrimeLog1 = new Prime_Action_Log__c();
        objPrimeLog1.Number_of_Attempt__c = 3;
        objPrimeLog1.Case__c = objCase.id;
        objPrimeLog1.Prime_Action_Type__c = 'Payback Credit Balance';
        objPrimeLog1.Response_Status__c = 'Failure';
        objPrimeLog1.SOA_service_Inbound_Message__c = '{"ServiceType" : "mapa","ServiceIndicator" : "AccountServices ", "Reference" : "Account","PaybackAmount" : "90", "LogAction" : "manual","InstitutionID" : null,"ActionDate" : "2017-01-09","Action" : "Add", "AccountNumber" : "5438730000000614 " }';
        insert objPrimeLog1;
        
        objPrimeLog = new Prime_Action_Log__c();
        objPrimeLog.Number_of_Attempt__c = 3;
        objPrimeLog.Case__c = objCase.id;
        //objPrimeLog.Financial_Account__c = finAccObj.id;
        objPrimeLog.Prime_Action_Type__c = 'Close Account';
        objPrimeLog.Response_Status__c = 'Failure';
        objPrimeLog.SOA_service_Inbound_Message__c = '{"setPPIReq" : null,"replaceCardReq" : null,"PaymentHolidaysReq" : null,"MobileReq" : null,"InstitutionId" : 2,"InstalmentReq" : null,"EmailReq" : null,"CloseAccReq" : {"ActionDate" : "2016-11-24","AccountNo" : "5435600034776114"},"cardReissueReq" : null,"addExtraCardReq" : null}';
        insert objPrimeLog;
        
        Prime_Action_Log__c objPrimeLog2 = new Prime_Action_Log__c();
        objPrimeLog2.Number_of_Attempt__c = 3;
        objPrimeLog2.Case__c = objCase.id;
        //objPrimeLog.Financial_Account__c = finAccObj.id;
        objPrimeLog2.Prime_Action_Type__c = 'Credit Limit Increase';
        objPrimeLog2.Response_Status__c = 'Failure';
        objPrimeLog2.SOA_service_Inbound_Message__c = '{"setPPIReq" : null,"replaceCardReq" : null,"PaymentHolidaysReq" : null,"MobileReq" : null,"InstitutionId" : 2,"InstalmentReq" : null,"EmailReq" : null,"CloseAccReq" : {"ActionDate" : "2016-11-24","AccountNo" : "5435600034776114"},"cardReissueReq" : null,"addExtraCardReq" : null}';
        insert objPrimeLog2;
        
      
        
        //Insert Custom Settings for call outs
        
        //GlobalSettings__c
        List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.Name = 'ServiceStrategies'; 
        objGlobal.Value__c = 'SOAPService';
        GlobalSettingsList.add(objGlobal);
        
        insert GlobalSettingsList;
        
        //common settings
        Common_Settings__c objcommn = new Common_Settings__c();
        objcommn .Name = 'PrimeActionRetryBatch'; 
        objcommn .Retry_batch_interval_in_hours__c = 4;
        insert(objcommn );
        
        //ServiceHeaders__c
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'SOAPHTTP';
        insert objServiceHeaders;
        
        //ServiceSettings__c
        ServiceSettings__c objServiceSettings = new ServiceSettings__c();
        objServiceSettings.Name = 'UpdateCustomerData';
        objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings.HeaderName__c = 'SOAPHTTP';
        objServiceSettings.OutputClass__c = 'UpdateCustomerDataOutput';
        objServiceSettings.ProcessingClass__c = 'UpdateCustomerDatacallout'; 
        objServiceSettings.Strategy__c = 'SOAPService';
        objServiceSettings.Input_Class__c = 'UpdateCustomerDataInput'; 
        
        insert objServiceSettings;
        
        //ServiceSettings__c
        ServiceSettings__c objServiceSettings1 = new ServiceSettings__c();
        objServiceSettings1.Name = 'PayBackCreditBalance';
        objServiceSettings1.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings1.HeaderName__c = 'SOAPHTTP';
        objServiceSettings1.OutputClass__c = 'PayBackCreditBalanceOutput';
        objServiceSettings1.ProcessingClass__c = 'PayBackCreditBalancecallout'; 
        objServiceSettings1.Strategy__c = 'SOAPService';
        objServiceSettings1.Input_Class__c = 'PayBackCreditBalanceInput'; 
        
        insert objServiceSettings1;
        
        Test.startTest();
            Test.setMock(WebServiceMock.class, new PaybackCreditbalanceImpl ());
            PrimeActionRetryBatch c = new PrimeActionRetryBatch();
            Database.executeBatch(c, 3);
            //c.cronschedule();
        Test.stopTest();
         }
    }
    static testmethod void test1() 
    {
         //common settings
        Common_Settings__c objcommn = new Common_Settings__c();
        objcommn .Name = 'PrimeActionRetryBatch'; 
        objcommn .Retry_batch_interval_in_hours__c = 4;
        insert(objcommn );
        Test.startTest();
        SchedulableContext sc = null;
        PrimeActionRetryBatch tsc = new PrimeActionRetryBatch();
        //tsc.execute(sc); 
        tsc .cronschedule();
        Test.stopTest();
    }
}