/**********************************************************************
Name: PostChatController
=======================================================================
Purpose: This class is used update the customer feedback in chat transcript record.  User Story -SFD-1397

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         1-Jul-2020       Initial Version

**********************************************************************/
public class PostChatController {
    
    Public Boolean flag {set;get;}
    Public String chatKey {set;get;}
    Public String feedBack {set;get;}
    
    Public PostChatController(){
        flag = false;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     1-Jul-2020
*   User Story : SFD-1397
*   Param: None
*   Return: Map    
*   Description: In this method we are trying to update the Customer_Feedback__c field value in chat transcript object.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    public PageReference updateCustFeedBack() {
        try {
            chatKey = ApexPages.currentPage().getParameters().get('chatkey');
            feedBack = ApexPages.currentPage().getParameters().get('feedback');
            system.debug('---chatky--'+chatKey+'---'+feedBack);
            
            List<LiveChatTranscript> chatSctipts = [SELECT Id, Customer_Feedback__c
                                                    FROM LiveChatTranscript 
                                                    WHERE chatkey =: chatKey];
            List<LiveChatTranscript> updateChatList = new List<LiveChatTranscript>();
            if(chatSctipts.size() > 0) {
                for(LiveChatTranscript chatObj : chatSctipts) {
                    chatObj.Customer_Feedback__c = feedBack;
                    updateChatList.add(chatObj);
                }
            }
            if(updateChatList.size() > 0) {
                update updateChatList;
                flag = true;
            }
        }catch(Exception exp){
            StatusLogHelper.logSalesforceError('PostChatController', 
                                               'E005', 'updateCustFeedBack', exp, false);
        }
        return null;
    }
}