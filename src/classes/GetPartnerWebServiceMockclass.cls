global class GetPartnerWebServiceMockclass implements WebServiceMock
{
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) 
    {
        NonCustomerServiceResponse.GetPartnersNameResponse_element objpartEle = new NonCustomerServiceResponse.GetPartnersNameResponse_element();
        NonCustomerServiceResponse.GetPartnersNameResponseRecordType[] objpartRecType = new List<NonCustomerServiceResponse.GetPartnersNameResponseRecordType>();
        NonCustomerServiceResponse.GetPartnersNameResponseRecordType  partRecType = new NonCustomerServiceResponse.GetPartnersNameResponseRecordType();
        partRecType.Institution_ID ='1';
        partRecType.PartnerID='123456';
        partRecType.PartnerName='Testing';
        objpartRecType.add(partRecType);
        objpartEle.GetPartnersNameResponseRecord = objpartRecType;
        response.put('response_x', objpartEle);
    }
}