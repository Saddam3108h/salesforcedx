/**********************************************************************
Name: LogActivityController_Test
=======================================================================
Purpose: Purpose: This is the test class for LogActivityController User Story -SFD-1584, SFD-1357

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         1-Sep-2020       Initial Version

**********************************************************************/
@isTest(seeAllData = false)
public class LogActivityController_Test {
    static testMethod void doCaseUpdateTest() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN
                    });
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            Card__c cardObj = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Product__c'=>prodobj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c
                    });
            List<Card__c> cardList = new List<Card__c>();
            cardList.add(cardObj);
            List<String>  cardAttr = new List<String> {'People__c','Financial_Account__c','Product__c','Prod_Serno__c'};
                List<Card__c> cardReload = UnitTestDataGenerator.TestCard.reloadList(cardList,cardAttr);
            Task taskObj = UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'CreatedById'=>UserInfo.getUserId(),
                    'WhoId'=>contactObj.Id,
                    'CallType'=>'Inbound'
                    });
            
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            List<String>  taskAttr = new List<String> {'subject','status','Category__c','Comments__c','type','CallType',
                'WhoId','CreatedById'};
                    List<Task> taskReload = UnitTestDataGenerator.TestTask.reloadList(taskList,taskAttr);
            
            LiveChatVisitor chatVisitorObj = UnitTestDataGenerator.TestLiveChatVisitor.buildInsert();
           
            LiveChatTranscript liveChatObj = UnitTestDataGenerator.TestLiveChatTranscript.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Category__c'=>'Test',
                    'LiveChatVisitorId'=>chatVisitorObj.Id,
                    'ContactId'=>contactObj.Id,
                    'Card__c'=>cardObj.id
                    }); 
            Department_wise_Category__mdt departmentObj = [SELECT Id, Category__c, Department__c FROM Department_wise_Category__mdt 
                                                          WHERE Department__c = 'Collection' LIMIT 1];
            
            LogActivityController.getFinancialAccounts(contactObj.Id);
            LogActivityController.getCards(String.valueOf(finAccObj.Id), contactObj.Id);
            LogActivityController.updateTask(taskObj.WhoId,contactObj.Id);
            LogActivityController.doCaseUpdate(contactObj.Id,finAccObj.Id,cardObj.Id, '',StaticConstant.CATEGORY_ACCOUNT_CLOSURE,
                                               '','','Yes','Yes','Test','Test', '','','',false,'','','','Test'); 
            LogActivityController.redirctcustomer(contactObj.SSN__C);  
            LogActivityController.updateActivity('Test','Test','Test'); 
            LogActivityController.fetchChatData(liveChatObj.Id);  
            LogActivityController.updateChat(liveChatObj.id,String.valueOf(liveChatObj.Financial_Account__c),
                                            String.valueOf(liveChatObj.ContactId), String.valueOf(liveChatObj.Card__c), liveChatObj.Category__c);
            LogActivityController.fetchCategories(departmentObj.Department__c);
            test.stopTest();
            System.assert(taskList!=null);
        }
    }
    
    
    static testMethod void doCaseUpdateIfTest() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN
                    });
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            Card__c cardObj = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Product__c'=>prodobj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c
                    });
            List<Card__c> cardList = new List<Card__c>();
            cardList.add(cardObj);
            List<String>  cardAttr = new List<String> {'People__c','Financial_Account__c','Product__c','Prod_Serno__c'};
                List<Card__c> cardReload = UnitTestDataGenerator.TestCard.reloadList(cardList,cardAttr);
            Task taskObj = UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'CreatedById'=>UserInfo.getUserId(),
                    'WhoId'=>'',
                    'CallType'=>'Inbound',
                    'Comments__c'=>''
                    });
            
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            List<String>  taskAttr = new List<String> {'subject','status','Category__c','Comments__c','type','CallType',
                'WhoId','CreatedById'};
                    List<Task> taskReload = UnitTestDataGenerator.TestTask.reloadList(taskList,taskAttr); 
            
            LiveChatVisitor chatVisitorObj = UnitTestDataGenerator.TestLiveChatVisitor.buildInsert();
           
            LiveChatTranscript liveChatObj = UnitTestDataGenerator.TestLiveChatTranscript.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Category__c'=>'Test',
                    'LiveChatVisitorId'=>chatVisitorObj.Id,
                    'ContactId'=>contactObj.Id,
                    'Card__c'=>cardObj.id
                    }); 
            
            LogActivityController.getFinancialAccounts(contactObj.Id);
            LogActivityController.getCards(String.valueOf(finAccObj.Id), contactObj.Id);
            LogActivityController.updateTask(taskObj.WhoId,contactObj.Id);
            LogActivityController.doCaseUpdate(contactObj.Id,finAccObj.Id,cardObj.Id, 'Test','Payment Holiday',
                                               '','','','','','', String.valueOf(Date.parse('12/15/15')),'Test','',true,'','','','Test');
            LogActivityController.doCaseUpdate(contactObj.Id,finAccObj.Id,cardObj.Id, 'Test','CrossSell Response',
                                               '','','','','','', '','','Test',false,'Test','Test','Test','Test');
            LogActivityController.redirctcustomer(contactObj.SSN__C);  
            LogActivityController.updateActivity('','Test','Test');
            LogActivityController.updateChat(liveChatObj.id,String.valueOf('Other'),
                                            String.valueOf(liveChatObj.ContactId), String.valueOf(liveChatObj.Card__c), liveChatObj.Category__c);  
            test.stopTest();
            System.assert(cardList!=null);
        }
    }
}