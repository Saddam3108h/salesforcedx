/**********************************************************************
Name: ViewActivityHistoryController_Test
=======================================================================
Purpose: This is the test class for ViewActivityHistoryController SFD-1679
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         18-Sep-2020       Initial Version
**********************************************************************/
@isTest
public class ViewActivityHistoryController_Test {
    static testMethod void getTaskDataTest(){        
        
        System.runAs(UnitTestDataGenerator.adminUser){  
            Test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            
            Account objAcc = UnitTestDataGenerator.testAccount.buildInsert(new Map<String, Object>{	
                'Customer_Serno__c'=>'1123123'  
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            
            Contact contactobj	 = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{	
                'Encrypted_SSN__c'=>'encryptSSN',
                    'AccountId'=>objAcc.Id,
                    'SerNo__c'=>'2123123'
                    });
            
            ViewActivityHistoryController.getTaskData(contactobj.Id);    
            system.assertEquals(objAcc.Id, contactobj.AccountId);
            Test.stopTest();   
        }
    }
    
    static testMethod void getTaskDataIfTest(){                 
        System.runAs(UnitTestDataGenerator.adminUser){ 
            Test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account objAcc = UnitTestDataGenerator.testAccount.buildInsert(new Map<String, Object>{	
                'Customer_Serno__c'=>'1123123'  
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            
            Contact contactobj	 = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{	
                'Encrypted_SSN__c'=>'encryptSSN',
                    'AccountId'=>objAcc.Id,
                    'SerNo__c'=>'2123123'
                    });
            
            Case objcase = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{ 
                'ContactId'=>contactObj.Id
                    });
            
            ViewActivityHistoryController.getTaskData(objcase.Id);    
            system.assertEquals(contactobj.Id, objcase.ContactId);
            Test.stopTest();   
        }
    }
}