public with sharing class StatusLogHelper {
  
  public static void logStatus(StatusLog__c statusLog, boolean logAtFuture, boolean logWithCallout){
    System.debug('StatusLogHelper - logStatus');
    if(statusLog != null){ 
      if(logAtFuture)
        logStatusByDBCall(JSON.serializePretty(statusLog));
      else{
        if(logWithCallout)
            logStatusByRESTServiceCall(JSON.serializePretty(statusLog));
        else
            insert statusLog;
      }
    }
    
  }
  
  @future
  public static void logStatusByDBCall(String jsonString){
    System.debug('StatusLogHelper - logStatusByDBCall');
    StatusLog__c  statusLog;
        
        try {
            statusLog = (StatusLog__c) JSON.deserialize(jsonString, StatusLog__c.class);
        } catch (Exception e) {
            System.debug('Error in JSON deserialization');
        }
        
        if(statusLog != null) {
            try {
                insert statusLog;
            } catch (Exception e) {
                System.debug('Error upserting SObjects');
            }            
        } 
  }  
  
  public static void logStatusByRESTServiceCall(String jsonString){    
        System.debug('StatusLogHelper - logStatusByRESTServiceCall');
        Service service = new Service(ServiceHelper.statusLogService);        
        Object status = service.callout(jsonString);
        system.debug('@@logStatusByRESTServiceCall - status' + status); 
  }
  
  public static void logSalesforceError(string Name, string ErrorCode, string RequestBody, Exception ex, boolean logWithCallout)
  {
        StatusLog__c objLog = new StatusLog__c();    
        objLog.Name = Name ;      	
        objLog.Log_Type__c = 'Salesforce Log';
      	objLog.ErrorCode__c = ErrorCode;
        objLog.EndTime__c = System.now();
        objLog.StartTime__c = System.now();
        objLog.Status__c = 'E';
        objLog.RequestBody__c = RequestBody;
        objLog.ErrorDescription__c = ex.getLineNumber() + ' ' + ex.getMessage() + ' ' + ex.getstacktraceString();
        StatusLogHelper.logStatus(objLog, false, logWithCallout); // update
  }
   /**********************************************************************
*   Author: Kakasaheb Ekshinge
*   Date:   16-Oct-2020  
*   User Story : 
*   Param: None
*   Return: None    
*   Description: In this method we are creating record in status log with success & Fail response of Get service of MS.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    public static void logSFErrors(string Name,string statusCode,String status, 
                                   string RequestBody,String ResponseBody 
                                   ){ // service , SF issue or Integration issue
                                       
        StatusLog__c objLog = new StatusLog__c();    
        objLog.Name = Name ;
        if(status.startsWith('5'))
        	objLog.Log_Type__c = 'MS Log';
        else if(status.startsWith('C')) 
         	objLog.Log_Type__c = 'Curity Log';
       else 
          objLog.Log_Type__c = 'Salesforce Log';
        
        objLog.ErrorCode__c = statusCode;
        objLog.EndTime__c = System.now();
        objLog.StartTime__c = System.now();
        objLog.Status__c = status ;
        objLog.RequestBody__c = RequestBody;
        objLog.ResponseBody__c = ResponseBody; 
        system.debug('@@objLog==>'+objLog);
        try{
             insert objLog;    
        }catch(Exception e){
            system.debug('Exception in status log :'+e.getMessage());
        }
                                         
        }

}