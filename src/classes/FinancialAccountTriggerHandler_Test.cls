/**********************************************************************
Name: FinancialAccountTriggerHandler_Test
=======================================================================
Purpose: This Test class is used for test coverage of the FinancialAccountTriggerHandler.User Story -SFD-1602

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         29-July-2020       Initial Version

**********************************************************************/
@isTest(seeAllData = false)
public class FinancialAccountTriggerHandler_Test {
    static testmethod void updateAccountNumberTest() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
            });            
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
            string encryptaccNumber=EncodingUtil.convertToHex(hash);
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>accountObj.Customer_Serno__c, 
                    'Product__c'=>prodobj.Id,
                    'Encrypted_Account_Number__c'=>encryptaccNumber,
                    'Product_Serno__c'=>prodobj.Pserno__c,
                    'FA_Status__c'=> true,
                    'Fa_ClosedDate__c'=>'12-12-2020'
                    });
            List<Financial_Account__c> finAccountList = new List<Financial_Account__c>();
            finAccountList.add(finAccObj);
            List<String>  financialAttr = new List<String> {'Customer__c','Customer_Serno__c','Encrypted_Account_Number__c',
                											'Product__c','Product_Serno__c','Account_Number__c','FA_Status__c',
                											'Fa_ClosedDate__c'};
            List<Financial_Account__c> financialReload = UnitTestDataGenerator.TestFinancialAccount.reloadList(finAccountList,
                                                                                                               financialAttr);
            Map<Id, Financial_Account__c> finAccountOldMap = new Map<Id, Financial_Account__c>();
            finAccountOldMap.put(finAccObj.Id, finAccObj);
            Test.startTest();
            FinancialAccountTriggerHandler.encryptAccountNumber(financialReload, finAccountOldMap, true);
            FinancialAccountTriggerHandler.updateFields(financialReload);
            system.assertEquals(true, finAccObj.FA_Status__c, 'FA status is not correct');
            Test.stopTest();
        }
    }
}