/********************************************************************************************
Name: DeactivateUsers
=============================================================================================
Purpose: This Class is used for deactivating the users who are last login is today - 60 days
User Story - SFD-1422
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                          Date              Detail
1.0        Harisivaramakrishna          02/04/2020     Initial Version
2.0        Priyanka Singh               05/02/2021     

*********************************************************************************************/

global class DeactivateUsersSchedular implements Schedulable {
    global static DateTime lastLoginDate = date.today().addDays(-60); 
    
    /*********************************************************************************************************
*   Author: Harisivaramakrishna
*   Date:     08/04/2020
*   User Story : SFD-1422
*   Param: None
*   Return: None    
*   Description: In this method we are trying to deactivate the users who doesn't logged in since 60 days. 

**********************************************************************************************************/
    global void execute(SchedulableContext SC) {
        Set<Id> userIdSet = new Set<Id>();
        Id profileId = [SELECT Id FROM Profile WHERE name=: StaticConstant.PROFILE_NAME].Id;
        //DateTime lastLoginDate = date.today().addDays(-60); 
        List<User> userList = [SELECT ID, Name, LastLoginDate, IsActive, Profile.Name 
                               FROM User 
                               WHERE IsActive = TRUE 
                               AND LastLoginDate <: lastLoginDate
                               AND Name !=: StaticConstant.USER_SALESFORCE_SUPPORT
                               AND ProfileId !=:profileId ];
        System.debug('*****userList***** '+userList.size());
        List<User> deactivateUserList = new List<User>();
        if(userList.size() > 0 ){ 
            for (User usr : userList){
                usr.IsActive= FALSE;  // This is used to deactivating user
                deactivateUserList.add(usr);                    
            }
        } 
        System.debug('*****Deactivated users***** '+deactivateUserList);
        
        if(deactivateUserList.size() > 0) {
            
            List<Database.SaveResult> uResults = Database.update(deactivateUserList,false);
            for (Database.SaveResult sr : uResults) {
                if (sr.isSuccess()) {                   
                    System.debug('Successfully updated Invoice. Invoice ID is : ' + sr.getId());
                } else {
                    // This condition will be executed for failed records
                    for(Database.Error objErr : sr.getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(objErr.getStatusCode() + ': ' + objErr.getMessage());
                        System.debug('Invoice oject field which are affected by the error:' 
                                     + objErr.getFields());
                    }
                }
            }                
        }
    }
}