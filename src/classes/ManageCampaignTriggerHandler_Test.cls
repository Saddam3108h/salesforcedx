/********************************************************************************************
Name: ManageCampaignTriggerHandler_Test
=============================================================================================
Purpose: To cover the 'ManageCampaignTriggerHandler' apex code coverage 
============================================================================================= 
History 
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0        Srinivas D             4th-April-2019    SFD-984   		    Intial version
2.0		   Kakasaheb Ekshinge 	  22-July-2020		SFD-1596			Code coverage has increases
********************************************************************************************/
@isTest
public class ManageCampaignTriggerHandler_Test {
    
    static testmethod void  testManageCampaignTriggerHandlerMethod(){
        Test.startTest();
        try{
            System.runAs(UnitTestDataGenerator.adminUser){
                Product_Custom__c  objProd = UnitTestDataGenerator.testProductCustom.default();       
                Insert objProd;
                Product_Custom__c objProd2 = UnitTestDataGenerator.testProductCustom.buildInsert(new Map<String, Object>{
                    'Name'=>'Test Product', 
                        'Product_Description__c'=>'Test Desc',
                        'Pserno__c'=>'805'
                        }); 
                Account objAcc = UnitTestDataGenerator.testAccount.buildInsert(new Map<String, Object>{
                    'Name'=>'Account for Test Class', 
                        'Customer_Serno__c'=>'123654'
                        });
                
                Contact  objCon = UnitTestDataGenerator.testContact.buildInsert(new Map<String, Object>{
                });
                
                Contact  objCon2 = UnitTestDataGenerator.testContact.buildInsert(new Map<String, Object>{
                    'FirstName'=>'TestContact2',
                        'SerNo__c' => '125895',
                        'SSN__c' =>'78958741',
                        'Email'=>'xyz@ab1c.com'
                        });
                
                
                Financial_Account__c objFA = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                    'Account_Number__c'=>'4141144', 
                        'Customer__c'=>objAcc.Id,
                        'Account_Serno__c'=>'11166',
                        'Customer_Serno__c'=>objAcc.Customer_Serno__c,
                        'Product__c'=>objProd.Id,
                        'Product_Serno__c'=>objProd.Pserno__c
                        });
                
                Financial_Account__c objFA2 = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                    'Account_Number__c'=>'4141144', 
                        'Customer__c'=>objAcc.Id,
                        'Account_Serno__c'=>'1116666',
                        'Customer_Serno__c'=>objAcc.Customer_Serno__c,
                        'Product__c'=>objProd.Id,
                        'Product_Serno__c'=>objProd.Pserno__c
                        }); 
                
                Card__c objCard = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                    'People__c'=>objCon.Id,
                        'Financial_Account__c'=>objFA.Id, 
                        'Financial_Account_Serno__c'=>objFA.Account_Serno__c,
                        'People__c'=>objCon.Id,
                        'People_Serno__c'=>objCon.SerNo__c,
                        'Product__c'=>objProd.Id,
                        'Prod_Serno__c'=>objProd.Pserno__c  
                        });
                
                Manage_Campaign__c objCmpgn = UnitTestDataGenerator.testManageCampaign.buildInsert(new Map<String, Object>{
                });
                
                objCmpgn.People_SerNo__c = '12589';
                objCmpgn.AccountSerno__c = '11166';
                objCmpgn.ProductSerNo__c = '85';
                update objCmpgn;
                
                system.assertEquals(true, objCard.PrimaryCardFlag__c,'PrimaryCardFlag is not correct');
            }
            
        }catch(Exception exp){
        }
        Test.stopTest();
    }
}