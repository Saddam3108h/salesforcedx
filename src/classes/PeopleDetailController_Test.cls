@istest
Public class PeopleDetailController_Test{
    
    @testSetup
    public static void testInsertData() 
    {
       System.runAs(UnitTestDataGenerator.adminUser){
        TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
        Account accountObj=new Account(
        Name='Test Account', 
        Institution_Id__c=2, 
        Customer_Serno__c='2342323'
        );
        insert accountObj;

        Contact contactObj =new Contact(
        LastName='Test Contact', 
        FirstName='First Test Contact',
        SerNo__c='2312233', 
        Institution_Id__c=2,
        SSN__c='hsgvjfhg',
        AccountId=accountObj.Id,
        Phone='123123123', 
        Fax='98789879', 
        MobilePhone='98789879', 
        HomePhone='123123123', 
        Email='testemail@test.com'
        );
        insert contactObj;
        
        Product_Custom__c prodobj=new Product_Custom__c(
        Name='Test Product',
        Pserno__c='56789'
        );
        insert prodobj;

        Financial_Account__c finAccObj=new Financial_Account__c(
        Account_Number__c='Test5700192', 
        Customer__c=accountObj.Id, 
        Account_Serno__c='3123123', 
        Institution_Id__c=2,
        Segment__c='a',
        Customer_Serno__c=contactObj.Id,
        Product__c=prodobj.Id,
        Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;
        
        Card__c cardObj =new Card__c(
        Card_Serno__c='2231236', 
        Card_Number_Truncated__c='5456654565665', 
        People__c=contactObj.Id, 
        Institution_Id__c=2, 
        Financial_Account__c=finAccObj.Id,
        Financial_Account_Serno__c=finAccObj.Id, 
        People_Serno__c=contactObj.Id,
        Prod_Serno__c=prodobj.Pserno__c, 
        Product__c=prodobj.Id
        );
        insert cardObj;
        
        Alert__c alertObj = new Alert__c(
        Active__c = true,
        People__c = contactObj.id,
        DueDate__c = null
        );
        insert alertObj;
        
         List<ServiceHeaders__c> serviceHeadersList =new List<ServiceHeaders__c>();
        //ServiceHeaders
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'GetCustomerDetailsHeader';
        serviceHeadersList.add(objServiceHeaders);
        insert serviceHeadersList;
        
        List<ServiceSettings__c> serviceSettingList = new List<ServiceSettings__c>();
         //ServiceSettings__c
        ServiceSettings__c objServiceSettings = new ServiceSettings__c();
        objServiceSettings.Name = 'GetCustomerDetails';
        objServiceSettings.HeaderName__c = 'GetCustomerDetailsHeader'; 
        objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings.OutputClass__c = 'GetCustomerOutput';
        objServiceSettings.ProcessingClass__c = 'GetCustomerCallout'; 
        objServiceSettings.Strategy__c = 'SOAPService';
        objServiceSettings.Input_Class__c = 'GetCustomerInput'; 
        objServiceSettings.Username__c='Username';
        objServiceSettings.Password__c='Password';
        serviceSettingList.add(objServiceSettings);
        insert serviceSettingList;
        //Insert Service Header
        ServiceHeaders__c objServHeader = new ServiceHeaders__c();
        objServHeader.Name = 'SOAPHTTP';
        objServHeader.ContentType__c = 'application/xml';
        Insert objServHeader;
        
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.name = 'ServiceStrategies';
        objGlobal.Value__c = 'SOAPService,RESTService';
        Insert objGlobal;
        
        List<Common_Settings__c> commonSettingList =new List<Common_Settings__c>();
        Common_Settings__c commonSetting =new Common_Settings__c(
        Name='CorrelationId',
        Common_Value__c='1'
        );
        commonSettingList.add(commonSetting);
        Common_Settings__c commonSetting1 =new Common_Settings__c(
        Name='RequestorId',
        Common_Value__c='Salesforce'
        );
        commonSettingList.add(commonSetting1);
        Common_Settings__c commonSetting2 =new Common_Settings__c(
        Name='SystemId',
        Common_Value__c='1'
        );
        commonSettingList.add(commonSetting2);
        insert commonSettingList;
       }
    }
    
    static testMethod void testfinancialAccount() {
        
        test.startTest();
        
        Contact contactObj =[select Id, Name, Institution_Id__c,SSN__c from Contact limit 1];
        Test.setCurrentPageReference(new PageReference('Page.ViewFinancialAccount')); 
        System.currentPageReference().getParameters().put('Id', contactObj.Id);
        Test.setMock(WebServiceMock.class, new GetCustomerWebServiceMockclass());
        ApexPages.StandardController sc = new ApexPages.standardController(contactObj);
        PeopleDetailController contlr = new PeopleDetailController();
        PeopleDetailController stdCntlr = new PeopleDetailController(sc);
        stdCntlr.contactObj=contactObj;
        stdCntlr.getFinancialAccountsByPeopleId();
        stdCntlr.serNoToFinancialAccountMap=new Map<String,PeopleDetailController.FinancialAccountWrapper>();
        stdCntlr.contactObj=null;
        stdCntlr.getFinancialAccountsByPeopleId();
        test.stopTest();
        
    }
    
}