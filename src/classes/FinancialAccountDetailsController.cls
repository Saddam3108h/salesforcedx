public with sharing class FinancialAccountDetailsController{ 

  public Financial_Account__c financialAccount {get;set;}
  public List<ViewPaymentWrapper> paymentWrapperList {get;set;}
  public List<ViewPaymentWrapper> transactionWrapperList {get;set;}
  public List<ViewLimitChangesWrapper> limitChangesWrapperList {get;set;}
  
  public string paymentMessage{get;set;}
  public string transMessage{get;set;}
  public string limitMessage{get;set;}
   public boolean isShowingDetailList{get;set;}
   
  // constructor
  public FinancialAccountDetailsController(ApexPages.StandardController sc){
      
    paymentWrapperList =new List<ViewPaymentWrapper>();
    transactionWrapperList =new List<ViewPaymentWrapper>();
    limitChangesWrapperList=new List<ViewLimitChangesWrapper>();
    
    If(!Test.isRunningTest()) { 
    sc.AddFields(new List<String>{'Account_Number__c'});
    sc.AddFields(new List<String>{'Institution_Id__c'});
    }
    this.financialAccount =(Financial_Account__c)sc.getRecord();
    paymentMessage='';
    transMessage='';
    limitMessage='';
    isShowingDetailList=false;
    system.debug('<<---std controller--->>'+financialAccount);
  }
  
  public FinancialAccountDetailsController(){}
 
 
    public void viewlastFivePayments()
    {
        try{
        
            if(financialAccount !=null && financialAccount.Institution_Id__c!=null){
                
                String fromdatestr ='1990-01-01';
                Date fromdate   = Date.valueOf(fromdatestr);
                Date todate  = system.today();
            
                ViewTransactionsHistoryOutput  output= CSSServiceHelper.viewTransactionDetails(integer.valueof(financialAccount.Institution_Id__c),'ACCOUNT',5,0,fromdate,todate,NULL,false,financialAccount.Account_Number__c,'PAYMT');
                paymentWrapperList=new List<ViewPaymentWrapper>();
                    
                    system.debug('<<---output 0--->>'+output);
                    
                    if(output==null)
                    {
                      paymentMessage='Something went wrong, data cannot be displayed';
                    }
                    else if(output.Transaction_x!=null)
                    {
                        system.debug('<<---output.Transaction_x 0--->>'+output.Transaction_x);
                        
                        for(ViewTransactionsHistoryOutput.viewTransactionType  payment : output.Transaction_x)
                        {
                           paymentWrapperList.add(new ViewPaymentWrapper(payment.TxSerno,payment.Sequence,payment.PostDate,payment.ReasonCode
                           ,payment.TxAmount,payment.TxDate,payment.TxCurrency,payment.BillingAmount,payment.BillingCurrency,payment.TruncatedCardNo,payment.CanBeSplitted,payment.OrigMsgType,payment.Text));
                           
                        }
                    }
                    else
                    {
                        paymentMessage='No records to display';
                    }
               
            } 
        } 
        catch(dmlException e)   {
            
        system.debug('***Exception*** '+e.getMessage()+' ***Line Number*** '+e.getLineNumber());
        paymentMessage='Something went wrong, data cannot be displayed';
        StatusLogHelper.logSalesforceError('viewlastFivePayments', 'E005', 'financialAccount: ' + financialAccount, e, false);
        
        }
        catch(calloutException e)   {
            
        system.debug('***Exception*** '+e.getMessage()+' ***Line Number*** '+e.getLineNumber());
        paymentMessage='Something went wrong, data cannot be displayed';
        StatusLogHelper.logSalesforceError('viewlastFivePayments', 'E001', 'financialAccount: ' + financialAccount, e, false);
        
        }
        catch(Exception e)   {
            
        system.debug('***Exception*** '+e.getMessage()+' ***Line Number*** '+e.getLineNumber());
        paymentMessage='Something went wrong, data cannot be displayed';
        StatusLogHelper.logSalesforceError('viewlastFivePayments', 'E004', 'financialAccount: ' + financialAccount, e, false);
        
        }
        
        system.debug('<<---paymentMessage--->>'+paymentMessage);
    }

    
    public void viewlastFiveLimitChanges()
    {
        try{
        
            if(financialAccount !=null && financialAccount.Institution_Id__c!=null){
                
                GetLimitChangesServiceInput InputObj = new GetLimitChangesServiceInput();
                date fromdate = date.parse('1900-01-01');
                InputObj.AccountNo=financialAccount.Account_Number__c;//'2500010000';//4581990000005919, 5299360000024774
                InputObj.From_x=fromdate;
                InputObj.InstitutionId=integer.valueof(financialAccount.Institution_Id__c);
                
                GetLimitChangesServiceOutput  output= CSSServiceHelper.GetLimitChanges(InputObj);
               
                    system.debug('<<---output 0--->>'+output);
                    
                    if(output==null)
                    {
                        limitMessage='Something went wrong, data cannot be displayed';
                    }
                    else if(output.GLCR!=null && output.GLCR.limitChange!=null)
                    {
                        system.debug('<<---output.GLCR--->>'+output.GLCR);
                        
                        for(GetLimitChangesServiceOutput.LimitChange  lim : output.GLCR.limitChange)
                        {
                            if(limitChangesWrapperList.size()<5)
                            limitChangesWrapperList.add(new ViewLimitChangesWrapper(lim.Date_x,lim.OldLimit,lim.NewLimit));
                            else
                            break;
        
                        }
                    }
                    else
                    {
                        limitMessage='No records to display';
                    }
               
            } 
        } 
        catch(dmlException e)   {
            
        system.debug('***Exception*** '+e.getMessage()+' ***Line Number*** '+e.getLineNumber());
        limitMessage='Something went wrong, data cannot be displayed';
        StatusLogHelper.logSalesforceError('viewlastFiveLimitChanges', 'E005', 'financialAccount: ' + financialAccount, e, false);
        }
        catch(calloutException e)   {
            
        system.debug('***Exception*** '+e.getMessage()+' ***Line Number*** '+e.getLineNumber());
        limitMessage='Something went wrong, data cannot be displayed';
        StatusLogHelper.logSalesforceError('viewlastFiveLimitChanges', 'E001', 'financialAccount: ' + financialAccount, e, false);
        }
        catch(Exception e)   {
            
        system.debug('***Exception*** '+e.getMessage()+' ***Line Number*** '+e.getLineNumber());
        limitMessage='Something went wrong, data cannot be displayed';
        StatusLogHelper.logSalesforceError('viewlastFiveLimitChanges', 'E004', 'financialAccount: ' + financialAccount, e, false);
        }
        
        system.debug('<<---limitMessage--->>'+limitMessage);
    }
    
    public void viewlastFiveTransactions()
    {
        
        try{
        
            if(financialAccount !=null && financialAccount.Institution_Id__c!=null){
               
                String fromdatestr ='1990-01-01';
                Date fromdate   = Date.valueOf(fromdatestr);
                Date todate  = system.today();
                
                ViewTransactionsHistoryOutput  transoutput =new ViewTransactionsHistoryOutput();
                transoutput.Transaction_x=new List<ViewTransactionsHistoryOutput.viewTransactionType>();
                system.debug('<<---transoutput.Transaction_x 1--->>'+transoutput.Transaction_x);
                system.debug('<<---transoutput 2--->>'+transoutput);
                transoutput= CSSServiceHelper.viewTransactionDetails(integer.valueof(financialAccount.Institution_Id__c),'ACCOUNT',5,0,fromdate,todate,NULL,false,financialAccount.Account_Number__c,'ALL');
                transactionWrapperList=new List<ViewPaymentWrapper>();
                
                    if(transoutput==null)
                    {
                      transMessage='Something went wrong, data cannot be displayed';    
                    }
                    else if(transoutput.Transaction_x!=null)
                    {
                        system.debug('<<---transoutput.Transaction_x 2--->>'+transoutput.Transaction_x);
                        for(ViewTransactionsHistoryOutput.viewTransactionType  viewtrans : transoutput.Transaction_x)
                        {
                            transactionWrapperList.add(new ViewPaymentWrapper(viewtrans.TxSerno,viewtrans.Sequence,viewtrans.PostDate,viewtrans.ReasonCode
                           ,viewtrans.TxAmount,viewtrans.TxDate,viewtrans.TxCurrency,viewtrans.BillingAmount,viewtrans.BillingCurrency,viewtrans.TruncatedCardNo,viewtrans.CanBeSplitted,viewtrans.OrigMsgType,viewtrans.Text));
                           
                      
                        }
                    }
                    else
                    {
                       transMessage='No records to display';
                    }
                
            } 
        } 
        catch(Exception e)   {
        system.debug('***Exception*** '+e.getMessage()+' ***Line Number*** '+e.getLineNumber());
                transMessage='Something went wrong, data cannot be displayed';
                StatusLogHelper.logSalesforceError('viewTransactionDetails', 'E001', 'viewTransactionDetails: ' + integer.valueof(financialAccount.Institution_Id__c) +financialAccount.Account_Number__c, e, false);

        
        }
        
       
    }
    
    // ViewLimitChangesWrapper 
    public class ViewLimitChangesWrapper 
    {
     
     public ViewLimitChangesWrapper(Date Date_x,string OldLimit,string NewLimit)
     {
         this.Date_x=Date_x;
         this.OldLimit=OldLimit;
         this.NewLimit=NewLimit;
         
     }
        public Date Date_x {get;set;}
        public String OldLimit{get;set;}
        public String NewLimit {get;set;}
    }
    
    // ViewPaymentWrapper 
    public class ViewPaymentWrapper 
    {
      public ViewPaymentWrapper(Long TxSerno,Integer Sequence,Date PostDate,String ReasonCode,Decimal TxAmount,
      Date TxDate,String TxCurrency,Decimal BillingAmount,String BillingCurrency,String TruncatedCardNo,Boolean CanBeSplitted,string OrigMsgType,string Text)
      {
     
      this.TxSerno=TxSerno;
      this.Sequence=Sequence;
      this.PostDate=PostDate;
      this.ReasonCode=ReasonCode;
      this.TxAmount=TxAmount;
      this.TxDate=TxDate;
      this.TxCurrency=TxCurrency;
      this.BillingAmount=BillingAmount;
      this.BillingCurrency=BillingCurrency;
      this.TruncatedCardNo=TruncatedCardNo;
      this.CanBeSplitted=CanBeSplitted;
      this.Text=Text;
      this.OrigMsgType=OrigMsgType;
      }
       
        public Long TxSerno{get;set;}
        public Integer Sequence{get;set;}
        public Date PostDate{get;set;}
        public String ReasonCode{get;set;}
        public Decimal TxAmount{get;set;}
        public Date TxDate{get;set;}
        public String TxCurrency{get;set;}
        public Decimal BillingAmount{get;set;}
        public String BillingCurrency{get;set;}
        public String Text{get;set;}
        public String OrigMsgType{get;set;}
        public String TruncatedCardNo{get;set;}
        public Boolean CanBeSplitted{get;set;}
        
      }
      
      public pagereference showDetailList(){
  
      viewlastFivePayments();
      isShowingDetailList = true;
      return null;
      }
  
  public pagereference showDetailListLimitChange(){
  
      viewlastFiveLimitChanges();
      isShowingDetailList = true;
      return null;
      }
  public pagereference showDetailListTransaction(){
  
      viewlastFiveTransactions();
      isShowingDetailList = true;
      return null;
      }    
  
  
}