public with sharing class EditPeopleController{
    public Contact con{get;set;}
    public Contact oldcon;
    public string custSerNo;
    public Integer respstatuscode;
    Map<String,object> mapEmp = new Map<String,object>();
    UpdateCustomerDataOutput ucdo;
    UpdateCustomerDataInput ucdi;
    //HttpRequest req;
    public EditPeopleController(ApexPages.StandardController sc) {                       
        if(!test.isRunningTest()){
            sc.addfields(new List<String>{'FirstName','LastName','Name','Email','MobilePhone','AccountId','SSN__c','Account.Customer_Serno__c','Account.Name','SerNo__c','Mother_Name__c','Marital_Status__c','Institution_Id__c'});
        }
        this.con = (Contact)sc.getrecord(); 
        oldcon = con.clone(); 
        oldcon.Id = con.Id;  
        custSerNo = this.con.Account.Customer_Serno__c;                  
    }
    
    public string updateContactByRest(Contact con)
    {
        Datetime startTime = System.Now();
        HttpRequest req;
        HTTPResponse res;
        try
        {
            System.debug('CSS_UpdateContactHelper Contact: ' +con);
            ServiceSettings__c serviceSettings = ServiceSettings__c.getValues('UpdatePeople');
            System.debug('serviceSettings: ' +serviceSettings);    
            Map<string, string> requestHeader = ServiceHelper.getServiceHeaders(serviceSettings.HeaderName__c);   
            req = new HttpRequest();
            string strEndpoint;            
            strEndpoint = serviceSettings.EndPoint__c + con.id + '?_HttpMethod=PATCH';           
            req.setEndpoint(strEndpoint);
            System.debug('CSS_UpdateContactHelper strEndpoint : ' +strEndpoint);            
            if(serviceSettings.Timeout__c != null) req.setTimeout(integer.ValueOf(serviceSettings.Timeout__c));
            if(serviceSettings.Operation__c != null) req.setMethod(serviceSettings.Operation__c);
            if(serviceSettings.Compressed__c) req.setCompressed(serviceSettings.Compressed__c);           
            System.debug('@@UserInfo.getSessionId(): ' +UserInfo.getSessionId());
            //requestHeader.put('Authorization', requestHeader.get('Authorization') + ' '+ UserInfo.getSessionId());            
            if(requestHeader != null && !requestHeader.isEmpty())
                
            {
                Set<string> keys = requestHeader.keySet();
                for(string key : keys){
                    req.setHeader(key, requestHeader.get(key));
                }         
            }           
            req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
            req.setHeader('Content-Type', 'application/json');            
            
            //mapEmp.put('Deactivation_TimeStamp__c',Date.today());
            //mapEmp.put('AccountId',con.FirstName);
            mapEmp.put('FirstName', con.FirstName);
            mapEmp.put('LastName', con.LastName);            
            mapEmp.put('MobilePhone', con.MobilePhone);            
            mapEmp.put('Email', con.Email);
            if(oldcon.Email!=null || con.Email!=null)
                mapEmp.put('Customer_Email__c', con.Email);//Custom Email to sync with 
            mapEmp.put('Mother_Name__c', con.Mother_Name__c); 
            mapEmp.put('Marital_Status__c', con.Marital_Status__c);                                    
            String JSONString = JSON.serialize(mapEmp);
            System.debug('CSS_UpdateContactHelper JSONString : ' +JSONString );           
            req.setBody(JSONString);
            System.debug('CSS_UpdateContactHelper req: ' +req);            
            Http http = new Http();
            res = http.send(req);  
            respstatuscode = res.getStatusCode();           
            System.debug('CSS_UpdateContactHelper res : ' +res);
            //if(res.getStatusCode()==204)
            
        }
        catch(dmlException ex)
        {
            
            System.debug('UpdateContactHelper Exception: ' + ex.getMessage() + '  ' + ex.getstacktraceString());
            StatusLogHelper.logSalesforceError('updateContactByRest', 'E004', 'req: ' + req, ex, false);
            
            //RemoteHelper.logStatus(RemoteHelper.CSS_UpdateContactRest, startTime, System.Now(), JSON.serialize(objContact),  'Exception:--' + ex.getMessage() + '  ' + ex.getstacktraceString());
            
            //throw ex;
            
        }
        catch(calloutException ex)
        {
            
            System.debug('UpdateContactHelper Exception: ' + ex.getMessage() + '  ' + ex.getstacktraceString());
            StatusLogHelper.logSalesforceError('updateContactByRest', 'E001', 'req: ' + req, ex, false);
            
            //RemoteHelper.logStatus(RemoteHelper.CSS_UpdateContactRest, startTime, System.Now(), JSON.serialize(objContact),  'Exception:--' + ex.getMessage() + '  ' + ex.getstacktraceString());
            
            //throw ex;
            
        }
        catch(Exception ex)
        {
            
            System.debug('UpdateContactHelper Exception: ' + ex.getMessage() + '  ' + ex.getstacktraceString());
            StatusLogHelper.logSalesforceError('updateContactByRest', 'E005', 'req: ' + req, ex, false);
            
            //RemoteHelper.logStatus(RemoteHelper.CSS_UpdateContactRest, startTime, System.Now(), JSON.serialize(objContact),  'Exception:--' + ex.getMessage() + '  ' + ex.getstacktraceString());
            
            //throw ex;
            
        }
        return res.getBody();
        
    }
    Public String accname;
    Public String conname;
    
    public pagereference savepeople(){
        
        string resp = updateContactByRest(con); 
        if(respstatuscode==204)//update in salesforce succeeded
        {
            //callout part
            try{
                Account acc = [Select id,name,customer_serno__c from Account where id = :oldcon.AccountId];
                system.debug('-----accname----'+acc.name);
                accname = oldcon.Account.name.deleteWhitespace();
                conname = oldcon.name.deleteWhitespace();
                system.debug('------mahname---'+accname +'------contact name-----'+conname);
                Integer instid;
                UpdateCustomerAddressOutput ucao;
                if(con.Institution_Id__c!=null)
                    instid = Integer.valueOf(con.Institution_Id__c);
                if((oldcon.FirstName!=con.FirstName || oldcon.LastName!=con.LastName || oldcon.Marital_Status__c!=con.Marital_Status__c) && (accname.contains(oldcon.FirstName) || accname.contains(oldcon.LastName))){
                    ucao = CSSServiceHelper.UpdateCustomerAddress(instid,con,con.SSN__c);
                    system.debug('------ucao-----'+ucao);
                    UpdateMAHResponse outmah = CSSServiceHelper.UpdateMAHName(instid, Con, acc.Customer_Serno__c); // Add by saddam on 10 June 2018 to update ccustomer in Prime.
                    system.debug('-----mah-----'+outmah); 
                    
                    acc.Name = con.LastName +' '+ con.FirstName;
                    update acc;
                    system.debug('-----after-accname----'+acc.name);
                    
                } else {
                    ucao = CSSServiceHelper.UpdateCustomerAddress(instid,con,con.SSN__c);
                    system.debug('-------people-----'+ucao);
                }
                if(ucao!=null && ucao.code!='0'){
                    //oldcon.account.name = con.FirstName +' '+ Con.LastName;
                    update oldcon;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,CommonHelper.displayMessage(ucao.code));
                    ApexPages.addMessage(myMsg);      
                    return null;
                }
                if(oldcon.Email!=con.Email || oldcon.MobilePhone!=con.MobilePhone)//perform updatecustomerdata soa callout
                {  
                    ucdi = new UpdateCustomerDataInput();
                    ucdi.InstitutionId = instid;
                    ucdi.EmailReq = new UpdateCustomerDataInput.setEmailRequest();
                    ucdi.EmailReq.SSN = con.SSN__c;
                    ucdi.EmailReq.Email = con.Email;
                    ucdi.MobileReq = new UpdateCustomerDataInput.setMobileRequest();
                    ucdi.MobileReq.SSN = con.SSN__c;
                    ucdi.MobileReq.mobile = con.MobilePhone;
                    ucdo = CSSServiceHelper.UpdateCustomerData(ucdi);
                    if(ucdo!=null && ucdo.UCDR.Code!='0'){
                        system.debug('oldcon-->'+oldcon);
                        system.debug('con-->'+con);
                        this.con.MobilePhone=oldcon.MobilePhone;
                        this.con.Email=oldcon.Email;
                        this.con.Customer_Email__c = oldcon.Customer_Email__c;
                        update this.con;
                        ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,'Mobile and Email could not be updated because '+CommonHelper.displayMessage(ucdo.UCDR.Code));
                        ApexPages.addMessage(myMsg1);           
                        return null;
                    }
                } 
            }
            catch(dmlException e){
                system.debug('Exception happened-->'+e.getMessage());
                system.debug('Exception happened at line-->'+e.getLineNumber());
                StatusLogHelper.logSalesforceError('savepeople', 'E005', 'UpdateCustomerData: ' + ucdi, e, false);
            }
            catch(calloutException e){
                system.debug('Exception happened-->'+e.getMessage());
                system.debug('Exception happened at line-->'+e.getLineNumber());
                StatusLogHelper.logSalesforceError('savepeople', 'E001', 'UpdateCustomerData: ' + ucdi, e, false);
            }
            catch(Exception e){
                system.debug('Exception happened-->'+e.getMessage());
                system.debug('Exception happened at line-->'+e.getLineNumber());
                StatusLogHelper.logSalesforceError('savepeople', 'E005', 'UpdateCustomerData: ' + ucdi, e, false);
            }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record saved successfully');
            ApexPages.addMessage(myMsg);
            return null;
        } 
        else{      //problem occured saving record in salesforce
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'There was a problem saving People record in Salesforce.Please try again later');
            ApexPages.addMessage(myMsg);
            return null;
        }    
        PageReference nextPage = new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
        return nextPage;       
        
        
    }  
}