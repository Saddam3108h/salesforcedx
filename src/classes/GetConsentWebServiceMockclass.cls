global class GetConsentWebServiceMockclass implements WebServiceMock
{
     global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) 
        {
            SOAConsentResponse.GetConsentDataResponse_element objResEle = new SOAConsentResponse.GetConsentDataResponse_element();
            SOAConsentResponse.GetConsentDataResponseRecordType[] objgetConsentRecType = new List<SOAConsentResponse.GetConsentDataResponseRecordType>();
            SOAConsentResponse.GetConsentDataResponseRecordType objRecType = new SOAConsentResponse.GetConsentDataResponseRecordType();
            objRecType.Institution_ID = '2';
            objRecType.ConsentEntityID = '123123';
            objRecType.ConsentEntityID_Type = 'Account';
            objRecType.Consent_Code = 'Accept Nemkonto';
            objRecType.Value = 'Y';
            objRecType.DateTimeStamp = system.now();
            objRecType.UpdatedBy = 'UpdatedBy';
            objRecType.Source = 'Source';
            objgetConsentRecType.add(objRecType);
            objResEle.GetConsentDataResponseRecord = objgetConsentRecType;
            response.put('response_x', objResEle);
        }
}