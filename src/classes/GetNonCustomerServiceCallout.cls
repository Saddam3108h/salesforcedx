Public class GetNonCustomerServiceCallout extends SoapIO {
    NonCustomerServiceRequest.NonCustomerUpdateConsentRequestBodyType getNonCustomerRequestbody;
    GetNonCustomerInputs inputs;
    
    Public  void convertInputToRequest(){
        getNonCustomerRequestbody = new NonCustomerServiceRequest.NonCustomerUpdateConsentRequestBodyType();
        inputs = (GetNonCustomerInputs)ServiceInput;
        
        getNonCustomerRequestbody.Institution_ID = inputs.InstitutionId;
        getNonCustomerRequestbody.FirstName = inputs.FirstName;
        getNonCustomerRequestbody.LastName = inputs.LastName;
        getNonCustomerRequestbody.Address1 = inputs.Address1;
        getNonCustomerRequestbody.City = inputs.City;
        getNonCustomerRequestbody.Zip = inputs.Zip;
        getNonCustomerRequestbody.PartnerID = inputs.PartnerId;
        getNonCustomerRequestbody.IdentifierType = inputs.IdentifierType;
        getNonCustomerRequestbody.IdentifierValue = inputs.IdentifierValue;
        getNonCustomerRequestbody.UnionName = inputs.UnionName;
        getNonCustomerRequestbody.DepartmentNumber = inputs.DepartmentNumber;
        system.debug('-----getNonCustomerRequestbody----'+getNonCustomerRequestbody);
    }
    Public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        NonCustomerTypelib10.headerType HeadInstance = new NonCustomerTypelib10.headerType();
        HeadInstance.MsgId = 'SFDC consuming Get Consent SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=inputs.InstitutionId;
        
        NonCustomerCssserviceInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance = new NonCustomerCssserviceInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x = 30000;
        
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('GetPartnerDetails');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
            invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        String username = serviceObj.Username__c;
        String password = serviceObj.Password__c;
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        NonCustomerOasis20.UsernameToken creds=new NonCustomerOasis20.UsernameToken();
        creds.Username=serviceObj.Username__c;
        creds.Password=serviceObj.Password__c;
        
        NonCustomerOasis20.Security_element security_ele = new NonCustomerOasis20.Security_element();
        security_ele.UsernameToken = creds;
        invokeInstance.SecurityPart = security_ele;
        
        system.debug('----security_ele--'+security_ele);
        
        NonCustomerServiceResponse.NonCustomerUpdateConsentResponse_element getNonCustomerResponse_element;
        getNonCustomerResponse_element = invokeInstance.nonCustomerUpdateConsent(HeadInstance, getNonCustomerRequestbody);
        System.debug('---getNonCustomerRequestbody----->'+getNonCustomerResponse_element);
        return getNonCustomerResponse_element;
        //return null;
        
    }
    Public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
        if(response == null)
            return null;
        
        NonCustomerServiceResponse.NonCustomerUpdateConsentResponse_element getNonCustomerResponse_elementInstance = (NonCustomerServiceResponse.NonCustomerUpdateConsentResponse_element)response;
        NonCustomerServiceResponse.NonCustomerUpdateConsentResponseRecordType NonCustomerUpdateConsentResponseInstance = getNonCustomerResponse_elementInstance.NonCustomerUpdateConsentDataResponse;
        
        GetNonCustomerOutputs getnonoutputs = new GetNonCustomerOutputs();
        
        if(NonCustomerUpdateConsentResponseInstance != null){
            getnonoutputs.Code = NonCustomerUpdateConsentResponseInstance.Code;
            getnonoutputs.Description = NonCustomerUpdateConsentResponseInstance.Description;  
        }
        system.debug('----getnonoutputs--'+getnonoutputs);
        return getnonoutputs;
    }
    
}