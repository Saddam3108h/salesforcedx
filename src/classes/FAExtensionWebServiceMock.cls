@isTest
global class FAExtensionWebServiceMock implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       SOAPCSSServiceResponse.GetCustomerResponse_element  respElement= new SOAPCSSServiceResponse.GetCustomerResponse_element ();
       SOAPCSSServiceResponse.getCustomerResponseRecordType  stshis = new SOAPCSSServiceResponse.getCustomerResponseRecordType ();
       SOAPCSSServiceResponse.customerType customerTypeinstance = new SOAPCSSServiceResponse.customerType();
       SOAPCSSServiceResponse.accountCustResType accountCustResTypeinstance=new SOAPCSSServiceResponse.accountCustResType();
       SOAPCSSServiceResponse.accountStatusType accountStatusTypeinstance=new SOAPCSSServiceResponse.accountStatusType();
       SOAPschemasEntercardComTypelib10.directDebitType directDebitTypeinstance=new SOAPschemasEntercardComTypelib10.directDebitType();
       List<SOAPCSSServiceResponse.accountCustResType> accountCustResTypeinstanceList=new List<SOAPCSSServiceResponse.accountCustResType>();
       customerTypeinstance.Serno = 112; 
       customerTypeinstance.SSN = 'SSN';
       customerTypeinstance.FirstName = 'FirstName';
       customerTypeinstance.LastName = 'LastName';
       customerTypeinstance.Mobile = 'Mobile';
       customerTypeinstance.Email = 'Email';
       customerTypeinstance.TOTB = 100.000;
       customerTypeinstance.TotalLimit = 10.00;
       customerTypeinstance.TotalPossibleLimitIncrease = 100.00;
       accountStatusTypeinstance.StGen='stGen';
       accountStatusTypeinstance.StFin='stfin';
       accountStatusTypeinstance.StAuth='stauth';
       accountCustResTypeinstance.Serno=456;
       accountCustResTypeinstance.AccountNo='12w23eeeeedd';
       accountCustResTypeinstance.AccountStatus=accountStatusTypeinstance;
       directDebitTypeinstance.Enabled='enabled';
       accountCustResTypeinstance.DirectDebit=directDebitTypeinstance;
       accountCustResTypeinstance.AccountRating='bajkbck';
       accountCustResTypeinstance.Balance=5644;
       accountCustResTypeinstance.MTP=1;
       accountCustResTypeinstanceList.add(accountCustResTypeinstance);
       //customerTypeinstance.CustomerAddress = '';
       customerTypeinstance.Account = accountCustResTypeinstanceList;
       respElement.GetCustomerResponseRecord=stshis;
       respElement.GetCustomerResponseRecord.Customer=customerTypeinstance;
       response.put('response_x', respElement);
    }
    }