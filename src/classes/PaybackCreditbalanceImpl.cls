@isTest
global class PaybackCreditbalanceImpl implements WebServiceMock {
   global void doInvoke(Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

       CSSServiceHelper.updatePayBackCreditBalance(2,'Add','Account','5435600034776114','AccountServices','mapa','manual','2007-01-10','10');
       
       UpdateCustomerDataInput UpdateCustomerDataUserInput=new UpdateCustomerDataInput();
       UpdateCustomerDataUserInput.InstitutionId=2;
       UpdateCustomerDataInput.closeAccountRequest closeAccReq=new UpdateCustomerDataInput.closeAccountRequest();
       closeAccReq.ActionDate=date.today();
       closeAccReq.AccountNo='5435600034776114';
       UpdateCustomerDataUserInput.CloseAccReq=closeAccReq;
       
       UpdateCustomerDataOutput output = CSSServiceHelper.UpdateCustomerData(UpdateCustomerDataUserInput);
       
       response.put('response_x', output );

   }
}