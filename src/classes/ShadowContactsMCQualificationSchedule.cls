public class ShadowContactsMCQualificationSchedule implements Schedulable {
   public void execute(SchedulableContext SC) {
      Database.executeBatch(new ShadowContactsMCDisqualificationBatch('full'), Integer.valueOf(System.Label.ShadowContactsMCQualificationBatch));
   }
}