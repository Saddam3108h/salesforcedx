global class ServiceHeader{

    public String userId;
    public DateTime systemDateTime;
    public String systemName;
    public String applicationType;
    public String messageReference;
    public String functionName;
    public String legalEntity;
    public String userLanguage;
    public DateTime transactionStartDateTime;
    
    public String customerId;
    public String loopback;  
    public String timeZone;
    
     //To Cover Test Class 100%
    public void test()
    {
        
    }
    

}