@isTest
public class PayBackCreditBalanceInput_test {
     @isTest static void test()
     {
          
         PayBackCreditBalanceInput pb= new PayBackCreditBalanceInput();
         pb.InstitutionID= 123;
         pb.Action='save';
         pb.Reference='asdf';
         pb.AccountNumber='sb0005';
         pb.ServiceIndicator='asx';
         pb.ServiceType='free';
         pb.LogAction='xyz';
         pb.ActionDate='awx';
         pb.PaybackAmount='1233';
         pb.test();
         test.startTest();
         system.assertEquals(123,  pb.InstitutionID);
          system.assertEquals('save',  pb.Action);
          system.assertEquals('asdf',  pb.Reference);
          system.assertEquals('sb0005',  pb.AccountNumber);
          system.assertEquals('asx',  pb.ServiceIndicator);
          system.assertEquals('free',  pb.ServiceType);
          system.assertEquals('xyz',  pb.LogAction);
          system.assertEquals('awx',  pb.ActionDate);
          system.assertEquals('1233',  pb.PaybackAmount);
         test.stopTest();    
    } 
    

}