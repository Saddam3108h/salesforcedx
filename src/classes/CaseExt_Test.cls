/********************************************************************************************
Name: CaseExt_Test
=============================================================================================
Purpose: This test class is covering 'CaseExt' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0        Srinivas D             4th-April-2019    SFD-984   		    Intial version
2.0		   Kakasaheb Ekshinge 	  22-July-2020		SFD-1596			Code coverage has increases
********************************************************************************************/
@isTest
public class CaseExt_Test {
    public static testMethod void myUnitTest() {
        Test.startTest();
        System.runAs(UnitTestDataGenerator.adminUser){
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            List<Case> cslist = new List<Case>();
            Case cs1 = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
            'Origin' =>'Web',
            'Status'=>'New',
            'Category__c' => 'None'
            });
            cslist.add(cs1);
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(cslist);
            stdSetController.setSelected(cslist);
            CaseExt obj = new CaseExt(stdSetController);
            obj.assignCases();
            
            Test.stopTest();
        }
    }
}