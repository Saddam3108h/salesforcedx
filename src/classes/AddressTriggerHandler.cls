/**********************************************************************
Name: AddressTriggerHandler
=======================================================================
Purpose: This is helper class of AddressTriggerHandler

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author              Date              Detail
1.0         HSR Krishna         29-Jul-2020       Initial Version
2.0			Priyanka Singh		11-Aug-2020	   Trigger Factory Implementation

**********************************************************************/
public class AddressTriggerHandler implements ITriggerHandler {
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
 
    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
      
        if (TriggerSettings__c.getInstance().AddressExecution__c 
            &&  TriggerSettings__c.getInstance().OrgExecution__c)
        { 
            return true;
        }
        else
        { 
            return TriggerDisabled;
        }
        
    }
 
    public void BeforeInsert(List<SObject> newItems) {
        
        populateAddress(newItems);
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        
        Map<Id, Address__c> addressItems = (Map<Id, Address__c>) newItems; 
        populateAddress(addressItems.values());
    }
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterInsert(Map<Id, SObject> newItems) {}
 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
     
    public void AfterDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}

    
    /**********************************************************************
*   Author: HSR Krishna
*   Date:   29-Jul-2020
*   User Story : 
*   Param: None
*   Return: Map    
*   Description: This method works for formating the address and mobile while loading the data.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public Void populateAddress(List<Address__c> addressLst){
        Map<id,string>ContidVSHomeaddress=new Map<id,string>();
        Map<id,string>ContidVSHomephone=new Map<id,string>();
        Map<string,String>CountryCodeVSCountryMap=new Map<string,string>();
        
        for(Country_Codes__c code : Country_Codes__c.getall().values())
        {
            CountryCodeVSCountryMap.put(code.Country_Code__c,code.name);
        }
        
        for(Address__c addr: addressLst)
        {
            
            if(addr.addresstype__c!=null)
            {
                if(string.valueof(addr.addresstype__c).length()==5)
                {
                    
                    for(integer j=0; j<string.valueof(addr.addresstype__c).length();j++)
                    {
                        
                        string strVal=string.valueof(addr.addresstype__c).substring(j,j+1);
                        // cardpin
                        if(j==0)
                        {
                            addr.CardPin__c=strVal=='1'?true:false;
                            
                        }
                        //statement
                        if(j==1)
                        {
                            addr.Statement__c=strVal=='1'?true:false;
                            
                        }
                        
                        //mailer
                        if(j==2)
                        {
                            addr.Mailer__c=strVal=='1'?true:false;
                            
                        }
                        
                        //invoice
                        if(j==3)
                        {
                            addr.Invoice__c=strVal=='1'?true:false;
                            
                        }
                        
                        //other
                        if(j==4)
                        {
                            addr.Other__c=strVal=='1'?true:false;
                            
                        }
                        
                    } 
                    
                }
                
            }
            
            if(addr.Country_Code__c!=null && CountryCodeVSCountryMap.containskey(addr.Country_Code__c))
            {
                addr.Country__c = CountryCodeVSCountryMap.get(addr.Country_Code__c);    
            }
            
            if(addr.Institution_Id__c == 2 && addr.Country_Code__c != null && addr.Country_Code__c != '' && CountryCodeVSCountryMap.size()>0 && CountryCodeVSCountryMap.containskey(addr.Country_Code__c) && CountryCodeVSCountryMap.get(addr.Country_Code__c).equalsignorecase('se'))
            {    
                
                if(addr.Tel1__c != null && addr.Tel1__c != '')
                {
                    if(addr.Tel1__c.trim().startsWith('0'))
                        addr.Tel1__c = addr.Tel1__c.trim().replaceFirst('^0+', '');
                    
                    if(!addr.Tel1__c.trim().startsWith('46'))
                        addr.Tel1__c = '46' + addr.Tel1__c;
                }
                if(addr.Tel2__c != null && addr.Tel2__c != '')
                {
                    if(addr.Tel2__c.trim().startsWith('0'))
                        addr.Tel2__c = addr.Tel2__c.trim().replaceFirst('^0+', '');
                    
                    if(!addr.Tel2__c.trim().startsWith('46'))
                        addr.Tel2__c = '46' + addr.Tel2__c;
                }
            }
            
            if(addr.Location__c!=null && addr.Location__c.equalsignorecase('Home'))
            {
                system.debug('!!!!');
                ContidVSHomeaddress.put(addr.People__c,addr.Street_Address__c +(addr.Street_Address_2__c!=null?','+addr.Street_Address_2__c:'')+(addr.Street_Address_3__c!=null?','+addr.Street_Address_3__c:'')+(addr.Street_Address_4__c!=null?','+addr.Street_Address_4__c:'')+(addr.Street_Address_5__c!=null?','+addr.Street_Address_5__c:'')+','+addr.ZIP__c+','+addr.City__c +','+addr.Country__c); 
                ContidVSHomephone.put(addr.People__c,addr.Tel1__c);   
            }
            
            if(addr.ZIP__c==null || addr.ZIP__c=='')
            {
                addr.zip__c='00000';
            }
            
        }
        List<Contact>UpdateCont=new List<contact>();
        for(contact cnt : [select id,Home_Address__c,HomePhone from contact where id in : ContidVSHomeaddress.keyset()])
        {
            if(ContidVSHomeaddress.containskey(cnt.id) && (ContidVSHomeaddress.get(cnt.id)!=cnt.Home_Address__c || ContidVSHomephone.get(cnt.id)!=cnt.HomePhone))
            {
                cnt.Home_Address__c= ContidVSHomeaddress.get(cnt.id);
                cnt.HomePhone= ContidVSHomephone.get(cnt.id);
                UpdateCont.add(cnt);
            }
        }
        if(UpdateCont!=null && UpdateCont.size()>0)
        {
            ContactTriggerControl.ContactTriggerFlag = FALSE;
            update(UpdateCont);
            //ContactTriggerControl.ContactTriggerFlag = TRUE;
        } 
    }
}