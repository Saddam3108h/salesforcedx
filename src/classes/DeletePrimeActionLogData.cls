global class DeletePrimeActionLogData implements Database.Batchable<sObject>, schedulable{
    
    global String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        query = 'SELECT Id,name FROM Prime_Action_Log__c Where CreatedDate < N_DAYS_AGO:90' ;  
      
        return Database.getQueryLocator(query);
        
    }  
    
    global void execute(Database.BatchableContext BC, List<Prime_Action_Log__c> scope)
    {
            
            try{
                
                delete scope;
            }
            catch(DmlException e)
            {
                System.debug('Unexpected error occurred: ' + e.getMessage());
                StatusLogHelper.logSalesforceError('DeletePrimeActionLogData', 'E005', 'scope: ' + scope, e, true);
            }
            catch(calloutException e)
            {
                System.debug('Unexpected error occurred: ' + e.getMessage());
                StatusLogHelper.logSalesforceError('DeletePrimeActionLogData', 'E001', 'scope: ' + scope, e, true);
            }
            catch(Exception e)
            {
                System.debug('Unexpected error occurred: ' + e.getMessage());
                StatusLogHelper.logSalesforceError('DeletePrimeActionLogData', 'E004', 'scope: ' + scope, e, true);
            }
       

    } 
    
    global void execute(SchedulableContext sc)
    {
        
        DeletePrimeActionLogData d=new DeletePrimeActionLogData();
        Database.executeBatch(d, 200);
        
    }
    
    global void finish(Database.BatchableContext BC)
    {

    }
    

}