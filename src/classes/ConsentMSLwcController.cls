/**********************************************************************
Name: ConsentMSLwcController
=======================================================================
Purpose: This apex class is used to send outbound request for consents
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                 Date              Detail
1.0         Kakasaheb Ekshinge     20-Jan-2021      Initial Version
**********************************************************************/
public with sharing class ConsentMSLwcController {  
    
    /**********************************************************************************************
* @Author: Kakasaheb Ekshinge        
* @Date: 04-01-2021      
* @Description: This method is used to fetch Consents of FA and display correct true/false at UI
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/ 
    
    @AuraEnabled(cacheable=true)
    public static List<ConsentWrapper> getConsentList(string faId) {     
        
        List<ConsentWrapper> consWrapperList = new List<ConsentWrapper>();   
       for(ContactPointTypeConsent conPtc: [SELECT ID,EC_Consent_Type__c,Name,Contact_SerNo__c,PrivacyConsentStatus,Financial_Account__c
                                             FROM ContactPointTypeConsent 
                                             WHERE Financial_Account__c=:faId]){
            ConsentWrapper ow = new ConsentWrapper();
            ow.Name= conPtc.Name;
            ow.Id = conPtc.Id;
            Switch on conPtc.PrivacyConsentStatus {
                when 'OptIn' {
                    ow.consentStatus= TRUE;
                }
                when 'OptOut' {
                    ow.consentStatus= FALSE;
                }
                when else {
                    ow.consentStatus= FALSE;
                }
            }
            consWrapperList.add(ow);
        }    
        system.debug('Test==>'+consWrapperList);
        return consWrapperList;    
    }
    
   /**********************************************************************************************
* @Author: Kakasaheb Ekshinge        
* @Date: 14-01-2021      
* @Description: This method will pick the correct consents record and will send to different MS endpoints
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/ 
   // public static Boolean isAllSuccess = TRUE;
    @AuraEnabled
    public static String sendCallout(String jsonParamData){
        Set<Id> consentIdFalseSet = new set<Id>(); //OPtIn to OptOut
        Set<Id> consentIdTrueSet = new set<Id>(); // OptOut to OPtIn
       // Boolean isAllSuccess = TRUE;
        List<String> successFailList = new List<String>();
        String finalMessage;
        JsonToApex loopdataList = (JsonToApex)JSON.deserialize(jsonParamData,JsonToApex.class);
        String accessToken = loopdataList.accessToken;
        system.debug('accessToken : ' +accessToken);
        system.debug('loopdataList: ' + loopdataList);
        if(loopdataList.loopdata.size() > 0 ){
            for(Loopdata keyVal: loopdataList.loopdata){
                if(keyVal.value == false){
                   consentIdFalseSet.add(keyVal.key); 
                }else {
                    consentIdTrueSet.add(keyVal.key);
                }
            }
        }
       
        if(!consentIdFalseSet.isEmpty()){
            for(Id ids : consentIdFalseSet){
              String successFail =  sendCalloutToMS(ids,accessToken,false);
               successFailList.add(successFail);    
            }
        }
        
        if(!consentIdTrueSet.isEmpty()){
              for(Id ids : consentIdTrueSet){
                String successFail =  sendCalloutToMS(ids,accessToken,true);
                successFailList.add(successFail); 
            }
         }
        boolean isAllSuccess = true;
        if(!successFailList.isEmpty()){
            finalMessage = System.label.Fail_Label+' : ';
            for(String ss : successFailList){ 
                if(ss.contains('Fail')){
                   isAllSuccess = false; 
                   finalMessage +=ss;
                }                             
            }
        }
        if(isAllSuccess == true && successFailList.size()> 1) 
            return System.label.Success_Lbl_for_all_consents; 
        else if(isAllSuccess == true && successFailList.size() == 1) {
                return System.label.Success_Lbl_for_one_consent; 
        }
        else 
            return finalMessage;
       
    }  
	
     /**********************************************************************************************
* @Author: Kakasaheb Ekshinge        
* @Date: 14-01-2021      
* @Description: This method will send the actual callout request to MS
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
     public static String sendCalloutToMS(Id consentId,String accessToken,boolean msapi){
        String jsonData;
        ContactPointTypeConsent consents = [SELECT Id, Name,Institution_Id__c,EC_Consent_Type__c,
                                                     Financial_Account__r.Account_Number__c,External_Id__c,Contact_SerNo__c
                                                     FROM ContactPointTypeConsent WHERE Id =: consentId LIMIT 1];
        
         Contact contactObj = [SELECT Id, SSN__c,SerNo__c from Contact where SerNo__c = : consents.Contact_SerNo__c LIMIT 1];
         
          if(consents != null){
             JSONGenerator jsonGen = JSON.createGenerator(true);
                jsonGen.writeStartObject();
                jsonGen.writeStringField('ssn',contactObj.SSN__c); //''contactObj.SSN__c''
               jsonGen.writeStringField('accountNumber',consents.Financial_Account__r.Account_Number__c);
                jsonGen.writeStringField('consentCode','Accept ' + consents.EC_Consent_Type__c);
                jsonGen.writeStringField('institutionId',consents.Institution_Id__c);
                jsonGen.writeEndObject();
            jsonData = jsonGen.getAsString();
        }
         
         try{ 
            
             String metaDataApiName;
             if(msapi){
                 metaDataApiName = 'ConsentListOptouttoOptIn'; // TRUE
             } else {
                  metaDataApiName = 'ConsentListOptInToOptout'; // FALSE
             }
            
            ManageRESTServices__c last5Trans = ManageRESTServices__c.getValues(metaDataApiName);
            Map<String, String> attributeMap = new Map<String, String>(); 
            attributeMap.put('accessToken',accessToken);
            attributeMap.put('certificate',last5Trans.CertificateName__c);
            attributeMap.put('endpoint',last5Trans.Endpoint__c);
            attributeMap.put('httpMethod',last5Trans.Method__c);
            attributeMap.put('ContentType',last5Trans.ContentType__c);
            attributeMap.put('inputJson',jsonData);  
            system.debug('@@'+attributeMap);
            RESTCalloutUtil.responseWrapper = RESTCalloutUtil.fetchDataByMap(attributeMap);
            String successFail = '';   
            system.debug('@ RESTCalloutUtil.responseWrapper.StatusCode==>'+RESTCalloutUtil.responseWrapper.StatusCode);
             
             if(RESTCalloutUtil.responseWrapper.StatusCode == 204 ){ 
                  successFail = ' '+consents.EC_Consent_Type__c+':'+'Success'; 
             } else if(RESTCalloutUtil.responseWrapper.StatusCode == 400 || RESTCalloutUtil.responseWrapper.StatusCode == 401 || RESTCalloutUtil.responseWrapper.StatusCode == 500 ){ 
                 successFail = ' '+consents.EC_Consent_Type__c+':'+'Fail';
                 System.debug('Test==>'+successFail);
             }
           
            return successFail;
           }catch(Exception ex){
              logConsentsException('Connsent Exception:','500',jsonData,'Empty response', 'Exception');   
              return 'No Response';         
        }
    }
    
    @future
    public static void logConsentsException(String apiName,String statusCode,String request,String Response,String ex){        
        StatusLog__c objLog = new StatusLog__c();    
        objLog.Name = apiName ;       
        objLog.ErrorCode__c = statusCode;
        objLog.EndTime__c = System.now();
        objLog.StartTime__c = System.now();
        //objLog.Status__c = status;
        objLog.RequestBody__c = request;
        objLog.ResponseBody__c = Response;
        objLog.ErrorDescription__c = ex;
        insert objLog;
    }
    
    public class ConsentWrapper{  
        public ConsentWrapper(){
        } 
        @AuraEnabled
        public String Id {get; set;} 
        @AuraEnabled
        public String Name {get; set;}
        @AuraEnabled
        public Boolean consentStatus {get; set;} 
    } 
    
    
    public class JsonToApex {
        @AuraEnabled 
        public String accessToken;
        @AuraEnabled 
        public List<Loopdata> loopdata;
    }
    
    public class Loopdata {
        @AuraEnabled 
        public Boolean value;
        @AuraEnabled 
        public String key;
    }
        
}