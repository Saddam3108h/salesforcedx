/********************************************************************************************
Name: CaseCloseBySubject 
=============================================================================================
Purpose: This Class is called by Process Builder (Case_Close) and it changes the staut to Closed
when coditons are met after created the case.The conditions mentioned on Subject, To emailid,
Category, Department and status.
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                          Date              Detail
1.0        Harisivaramakrishna             13/07/2020       Initial Version
*********************************************************************************************/
public class CaseCloseBySubject {
    /********************************************************************************************
Function Name: fetchCases
=============================================================================================
Purpose: It fetches the list of cases and existing the custom metadata(Automatic_Case_Close__mdt),
Then it compare and close the cases.
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                          Date              Detail
1.0        Harisivaramakrishna             13/07/2020       Initial Version
*********************************************************************************************/
    @InvocableMethod
    public static void fetchCases(List<ID> CaseIds ){
        try {
            System.debug('*****CaseList***** '+CaseIds);
            List<Case> caseList = [SELECT id, 
                                   Status, 
                                   To_Address__c, 
                                   Category__c, 
                                   Department__c,
                                   Subject
                                   FROM Case
                                   WHERE Case.id in :CaseIds]; 
            
            List<Automatic_Case_Close__mdt> mdList = new List<Automatic_Case_Close__mdt>([SELECT Subject__c, 
                                                                                          Category__c, 
                                                                                          Department__c,
                                                                                          ToEmail__c,
                                                                                          Status__c
                                                                                          FROM Automatic_Case_Close__mdt]); 
            System.debug('*****allMetadataSet ***** '+mdList);

            List<case> updatecaseLst = new list<Case>();
            
            for(case c :caseList){
                if(c.Subject!= NULL && c.To_Address__c !=NULL && c.Category__c!=NULL && c.Department__c!=NULL) {
                    for(Automatic_Case_Close__mdt mdt :mdList){
                        System.debug('*****c.Subject *****:'+c.Subject);
                        If(c.Subject.contains(mdt.Subject__c)
                           && c.To_Address__c.contains(mdt.ToEmail__c) 
                           && c.Status.contains(mdt.Status__c)
                           && c.Category__c.contains(mdt.Category__c)
                           && c.Department__c.contains(mdt.Department__c)){
                               c.Status='Closed';
                               updatecaseLst.add(c);   
                               System.debug('*****Inside if ***** '+c.Subject);
                           }
                    }
                }
            }
            System.debug('*****Case to be Closed ***** '+updatecaseLst);
            if(!updatecaseLst.isEmpty())
                update updatecaseLst;
        } catch(Exception exp) {
            StatusLogHelper.logSalesforceError('CaseCloseBySubject', 'E005', 'fetchCases', exp, false); // Class,method
        }
    }
}