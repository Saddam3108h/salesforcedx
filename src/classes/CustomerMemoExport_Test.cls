@isTest 
private class CustomerMemoExport_Test{
    
    @testSetup
    public static void testInsertData8() 
    {
        TriggerSettings__c triggerObj = new TriggerSettings__c();
        triggerObj.Name = 'Execution';
        triggerObj.CustomerExecution__c = false;
        triggerObj.OrgExecution__c = false;
        insert triggerObj;
        Account accountObj=new Account(
            Name='Test Account', 
            Institution_Id__c=2, 
            Customer_Serno__c='1123123'
        );
        insert accountObj;
        
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        Contact contactObj =new Contact(
            LastName='Test Contact', 
            FirstName='First Test Contact',
            SerNo__c='2123123', 
            Institution_Id__c=2, 
            SSN__c='0000000061',
            Encrypted_SSN__c=encryptSSN,
            AccountId=accountObj.Id,
            Phone='123123123', 
            Fax='98789879', 
            MobilePhone='98789879', 
            HomePhone='123123123', 
            Email='testemail@test.com'
        );
        insert contactObj;
        
        Product_Custom__c prodobj=new Product_Custom__c(
            Name='Test Product',
            Pserno__c='56789'
        );
        insert prodobj;
        
        Financial_Account__c finAccObj=new Financial_Account__c(
            Account_Number__c='12w23eeeeedd', 
            Customer__c=accountObj.Id, 
            Account_Serno__c='3123123', 
            Institution_Id__c=2,
            Customer_Serno__c=contactObj.SerNo__c, 
            Product__c=prodobj.Id,
            Product_Serno__c=prodobj.Pserno__c
        );
        insert finAccObj;
        
        Card__c cardObj=new Card__c(
            People__c=contactObj.Id,
            PrimaryCardFlag__c=true,
            Financial_Account__c=finAccObj.Id, 
            Card_Serno__c='524858519', 
            Card_Number_Truncated__c='7136785481583561', 
            Financial_Account_Serno__c=finAccObj.Id, 
            People_Serno__c=contactObj.Id,
            Prod_Serno__c=prodobj.Pserno__c, 
            Product__c=prodobj.Id,
            Institution_Id__c=2
        );
        insert cardObj;
        
        Case caseObj=new Case(
            ContactId=contactObj.Id, 
            AccountId=accountObj.Id, 
            Card__c=cardObj.Id,
            Financial_Account__c=finAccObj.Id, 
            Category__c='Account closure', 
            Origin='Web'
        );
        insert caseObj;
        
        
        //GlobalSettings__c
        List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.Name = 'ServiceStrategies'; 
        objGlobal.Value__c = 'SOAPService';
        GlobalSettingsList.add(objGlobal);
        
        insert GlobalSettingsList;
        
        List<ServiceHeaders__c> serviceHeadersList =new List<ServiceHeaders__c>();
        //ServiceHeaders
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'CustomerMemoExportHeader';
        serviceHeadersList.add(objServiceHeaders);
        
        insert serviceHeadersList;
        
        List<ServiceSettings__c> serviceSettingList = new List<ServiceSettings__c>();
        //ServiceSettings__c
        ServiceSettings__c objServiceSettings = new ServiceSettings__c();
        objServiceSettings.Name = 'CustomerMemoExportData';
        objServiceSettings.HeaderName__c = 'CustomerMemoExportHeader'; 
        objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings.OutputClass__c = 'CustomerMemoExportDataOutput';
        objServiceSettings.ProcessingClass__c = 'CustomerMemoExportDataCallout'; 
        objServiceSettings.Strategy__c = 'SOAPService';
        objServiceSettings.Input_Class__c = 'CustomerMemoExportDataInput'; 
        objServiceSettings.Username__c='Username';
        objServiceSettings.Password__c='Password';
        serviceSettingList.add(objServiceSettings);
        insert serviceSettingList;
        
        List<Common_Settings__c> commonSettingList =new List<Common_Settings__c>();
        Common_Settings__c commonSetting =new Common_Settings__c(
            Name='CorrelationId',
            Common_Value__c='1'
        );
        commonSettingList.add(commonSetting);
        Common_Settings__c commonSetting1 =new Common_Settings__c(
            Name='RequestorId',
            Common_Value__c='Salesforce'
        );
        commonSettingList.add(commonSetting1);
        Common_Settings__c commonSetting2 =new Common_Settings__c(
            Name='SystemId',
            Common_Value__c='1'
        );
        commonSettingList.add(commonSetting2);
        insert commonSettingList;
        
        ErrorCodes__c errorCodes =new ErrorCodes__c();
        errorCodes.Name='0';
        errorCodes.ErrorDescription__c='ErrorDescription';
        errorCodes.UIDisplayMessage__c='UIDisplayMessage';
        insert errorCodes;
        
        ErrorCodes__c errorCodes1 =new ErrorCodes__c();
        errorCodes1.Name='defaultError';
        errorCodes1.ErrorDescription__c='ErrorDescription';
        errorCodes1.UIDisplayMessage__c='UIDisplayMessage';
        insert errorCodes1;
        
        
    }
    
    public static testmethod void testMethod1() {
        
        test.startTest();
        Test.setMock(WebServiceMock.class, new MockResponseClass());
        CustomerMemoExportDataInput customerMemoExportDataInputInstance = new CustomerMemoExportDataInput();
        customerMemoExportDataInputInstance.Action = 'NewMemo';
        customerMemoExportDataInputInstance.Reference = 'Customer';
        customerMemoExportDataInputInstance.Number_x = '431';
        customerMemoExportDataInputInstance.Type_x = 'Test';
        customerMemoExportDataInputInstance.Message = 'This is for testing';
        Service svc = new Service(ServiceHelper.CustomerMemoExportData);
        CustomerMemoExportDataOutput output = (CustomerMemoExportDataOutput)svc.callout(customerMemoExportDataInputInstance);
        
        test.stopTest();
    }
    
    public static testmethod void testMethod2() {
        
        test.startTest();
        
        CustomerMemoExportDataInput customerMemoExportDataInputInstance = new CustomerMemoExportDataInput();
        customerMemoExportDataInputInstance.Action = 'NewMemo';
        customerMemoExportDataInputInstance.Reference = 'Customer';
        customerMemoExportDataInputInstance.Number_x = '431';
        customerMemoExportDataInputInstance.Type_x = 'Test';
        customerMemoExportDataInputInstance.Message = 'This is for testing';
        Service svc = new Service(ServiceHelper.CustomerMemoExportData);
        CustomerMemoExportDataOutput output = (CustomerMemoExportDataOutput)svc.callout(customerMemoExportDataInputInstance);
        
        test.stopTest();
    }
}