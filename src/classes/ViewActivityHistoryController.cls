/**********************************************************************
Name: ViewActivityHistoryController
=======================================================================
Purpose: This class is used retrieve the Activity history reccords SFD-1679

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         18-Sep-2020       Initial Version

**********************************************************************/
public with sharing class ViewActivityHistoryController {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     18-Sep-2020
*   User Story : SFD-1679
*   Param: None
*   Return: List    
*   Description: In this method we are trying to get the Activity history records for customer on both
Case and customer level.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    @AuraEnabled(cacheable=true)
    public static List<Task> getTaskData(Id customerId){
        system.debug('@Apex class :'+customerId);
        if(customerId.getSobjectType() == Schema.Case.SObjectType){
            Case caseObj = [SELECT Id, ContactId FROM Case WHERE Id =: customerId];
            customerId = caseObj.ContactId;
        }
        system.debug('--customerId--'+customerId);
        if(customerId != null){
            return [SELECT Id, Subject, Owner.Name, CallType, Description, createdDate, CreatedBy.Name,Comments__c,Status 
                    FROM Task 
                    WHERE WhoId =: customerId 
                    AND Status = 'Completed' 
                    ORDER BY CreatedDate DESC];
        }
        return null;                      
    }
}