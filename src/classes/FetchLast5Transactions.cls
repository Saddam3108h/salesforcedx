/**********************************************************************
Name: FetchLast5Transactions
=======================================================================
Purpose: This apex is used to capture last 5 Transaction List of Particular financial account on the base on AccountNumber
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                 Date              Detail
1.0         Kakasaheb Ekshinge     16-Oct-2020      Initial Version
2.0         Priyanka Singh		   11-Feb-2021      EasyVista ticket ECINC0019817 Duplicate transaction bug fix SFD-1937
**********************************************************************/
public class FetchLast5Transactions {
   	
    /**********************************************************************
    *   Author: Kakasaheb Ekshinge
    *   Date:     
    *   User Story : 
    *   Param: None
    *   Return: None    
    *   Description: This method is fetching the Account Number of Customer to send for callout input
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    
    **********************************************************************/
    
    @AuraEnabled(cacheable=true)
    public static String fetchFinAccountNumber(String recordId) { 
        system.debug('@@ recordId==='+recordId);    
        List<Financial_Account__c> listFin;
        if(String.isNotBlank(recordId))	
       		 listFin = [select Id, Account_Number__c From Financial_Account__c WHERE Id = :recordId LIMIT 1];
        If(!listFin.isEmpty()){
            return  listFin[0].Account_Number__c;
        }
        else {
           return null; 
        }
     }
    
     /**********************************************************************
    *   Author: Kakasaheb Ekshinge
    *   Date:     
    *   User Story : 
    *   Param: None
    *   Return: None    
    *   Description: This method will return the last 5 Transaction List.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    
    **********************************************************************/    
    @AuraEnabled
    public static String fetchTransactionList(String accessToken,String accountNumber) { 
       try{  
        	system.debug('Test==>'+accessToken+accountNumber);
            ManageRESTServices__c last5Trans = ManageRESTServices__c.getValues('Last5Transactions'); 
        	Map<String, String> attributeMap = new Map<String, String>();            
            String jsonInput = '{"accountNumber":'+accountNumber+',"startAtRowNum": 1,"stopAfterRowNum": 5,"amountThreshold": null,"includeAccountMovements": true,"includeCardMovements": true,"includePrimaryCardOnly": false,"includeTransactions": true,"includeAuthorizations": true,"includeMatchedAuthorizations": false,"includeCancelledAuthorizations": false,"orderNewestOnTop": true,"orderMovementsPerCard": false}';
            attributeMap.put('accessToken',accessToken);
            attributeMap.put('certificate',last5Trans.CertificateName__c);
            attributeMap.put('endpoint',last5Trans.Endpoint__c);
            attributeMap.put('httpMethod',last5Trans.Method__c);
            attributeMap.put('ContentType',last5Trans.ContentType__c);
            attributeMap.put('inputJson',jsonInput);  
            system.debug('@@'+attributeMap);
            RESTCalloutUtil.responseWrapper = RESTCalloutUtil.fetchDataByMap(attributeMap);           
            If(RESTCalloutUtil.responseWrapper.StatusCode != 200){
                System.debug('The status code returned was not expected: ' + RESTCalloutUtil.responseWrapper.Status + ' ' + RESTCalloutUtil.responseWrapper.StatusCode); 
            	
            }             
            StatusLogHelper.logSFErrors('Last5Transactions',
                                            RESTCalloutUtil.responseWrapper.Status,
                                            RESTCalloutUtil.responseWrapper.StatusCode+'',
                                            RESTCalloutUtil.responseWrapper.RequestBody+'inputJson==>'+jsonInput,
                                            RESTCalloutUtil.responseWrapper.ResponseBody);
            
            return RESTCalloutUtil.responseWrapper.ResponseBody;
           }catch(Exception ex){
              StatusLogHelper.logSalesforceError('FetchLast5Transactions', 'fetchTransactionList', 'Error while sending fetchTransactionList request to MS', ex, false);
              return 'No Response'; 
        }
     }
    
     /**********************************************************************
    *   Author: Kakasaheb Ekshinge
    *   Date:     
    *   User Story : 
    *   Param: None
    *   Return: None    
    *   Description: This method will return the all Transaction List between the two dates.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    
    **********************************************************************/
    
    @AuraEnabled
    public static String fetchTxnBetDate(String accountNumber,String accessToken, Date fromdate,Date todate) { 
        
 		Datetime fromDate1 = datetime.newInstance(fromdate.year(), fromdate.month(),fromdate.day());
		Datetime toDate1 = datetime.newInstance(todate.year(), todate.month(),todate.day());
        String fromDateformatted = String.valueOf(fromDate1.format('yyyy-MM-dd\'T\'hh:mm:ss'));
        String toDateformatted = String.valueOf(toDate1.format('yyyy-MM-dd\'T\'hh:mm:ss'));
       
        try{  
        	ManageRESTServices__c last5Trans = ManageRESTServices__c.getValues('Last5Transactions'); 
        	Map<String, String> attributeMap = new Map<String, String>();            
            String jsonMSG;
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('accountNumber',accountNumber);
            gen.writeStringField('fromDateTime',fromDateformatted);
            gen.writeStringField('toDateTime',toDateformatted); 
            //gen.writeStringField('startAtRowNum','1');
            //gen.writeStringField('stopAfterRowNum','5');  
            gen.writeStringField('amountThreshold','');
            gen.writeStringField('includeAccountMovements','true');
            gen.writeStringField('includeCardMovements','true');
            gen.writeStringField('includePrimaryCardOnly','false');
            gen.writeStringField('includeTransactions','true');
            gen.writeStringField('includeAuthorizations','true');
			gen.writeStringField('includeMatchedAuthorizations','false');
			gen.writeStringField('includeCancelledAuthorizations','false');
			gen.writeStringField('orderNewestOnTop','true');
			gen.writeStringField('orderMovementsPerCard','false'); 
            gen.writeStringField('institutionID','1'); 
          
            /*     "amountThreshold": null,
                    "includeAccountMovements": true,
                    "includeCardMovements": true,
                    "includePrimaryCardOnly": false,
                    "includeTransactions": true,
                    "includeAuthorizations": true,
                    "includeMatchedAuthorizations": false,
                    "includeCancelledAuthorizations": false,
                    "orderNewestOnTop": true,
                    "orderMovementsPerCard": false,
                    "institutionID": 1*/

            gen.writeEndObject();
            jsonMSG = gen.getAsString();
            
            attributeMap.put('accessToken',accessToken);
            attributeMap.put('certificate',last5Trans.CertificateName__c);
            attributeMap.put('endpoint',last5Trans.Endpoint__c);
            attributeMap.put('httpMethod',last5Trans.Method__c);
            attributeMap.put('ContentType',last5Trans.ContentType__c);
            attributeMap.put('inputJson',jsonMSG); 
            RESTCalloutUtil.responseWrapper = RESTCalloutUtil.fetchDataByMap(attributeMap);
            If(RESTCalloutUtil.responseWrapper.StatusCode != 200){
                System.debug('The status code returned was not expected: ' + RESTCalloutUtil.responseWrapper.Status + ' ' + RESTCalloutUtil.responseWrapper.StatusCode); 
            }             
            
            StatusLogHelper.logSFErrors('Last5Transactions - Between two date',
                                        RESTCalloutUtil.responseWrapper.Status,
                                        RESTCalloutUtil.responseWrapper.StatusCode+'',
                                        RESTCalloutUtil.responseWrapper.RequestBody+' jsonMSG=>'+jsonMSG,
                                        RESTCalloutUtil.responseWrapper.ResponseBody);
            
            
            return RESTCalloutUtil.responseWrapper.ResponseBody;
           }catch(Exception ex){
             StatusLogHelper.logSalesforceError('FetchLast5Transactions', 'fetchTxnBetDate', 'Error while sending fetchTxnBetDate request to MS', ex, false);
             return 'No Response';
        }
     }
     
    
}