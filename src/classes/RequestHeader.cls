global class RequestHeader{

    public String userId;
    public String password;
    public DateTime systemDateTime;
    public String systemName;
    public String applicationType;
    public String messageReference;
    public String functionName;
    public String legalEntity;
    public String userLanguage;
    public DateTime transactionStartDateTime;
    //To Cover Test Class 100%
    public void test()
    {
        
    }

}