public class CSSServiceHelper{     
    //method added by Swarnava Bhattacharyya on 20 OCT 2016, for Get Statement History SOA service consume
    public static GetInstallInfoOutput outputinstallInfo;
    public static GetStatementHistoryOutput[] getTransactionHistory(integer InstitutionId,string serNo)
    {  
        GetStatementHistoryOutput[] output;
        
        if(serNo==null || serNo=='' || InstitutionId==null)
            return null;          
        GetStatementHistoryInput g = new GetStatementHistoryInput();
        //g.serno = '35916';
        g.serno = serNo;
        g.institutionId = String.valueOf(InstitutionId);
        Service svc = new Service(ServiceHelper.GetStatementHistory);
        output = (GetStatementHistoryOutput[])svc.callout(g); 
        system.debug('statement output list form-->'+output);
        
        
        return output;              
        
    }
    // method added by Subhajit pal on 24 OCT 2016, for GetCustomer SOA service consume
    public static GetCustomerOutput getCustomerDetails(integer InstitutionId,string SSN,string RetrieveAccounts,string RetrieveCards)
    {  
        if(InstitutionId==null || SSN==null || SSN=='' || RetrieveAccounts==null || RetrieveAccounts==''|| RetrieveCards==null || RetrieveCards=='' )
            return null;          
        GetCustomerInput GetCustomerInputInstance = new GetCustomerInput();
        GetCustomerInputInstance.InstitutionId=InstitutionId;
        GetCustomerInputInstance.SSN  = SSN;//0000000061
        GetCustomerInputInstance.RetrieveAccounts=RetrieveAccounts;
        GetCustomerInputInstance.RetrieveCards=RetrieveCards;
        Service svc = new Service(ServiceHelper.GetCustomerDetails);
        System.debug('success');
        GetCustomerOutput output = (GetCustomerOutput)svc.callout(GetCustomerInputInstance); 
        system.debug('getCustomer output form-->'+output);
        return output;
    }
    
    
    // method added by Shanthi on 17 NOV 2016, for viewTransactionhistory from SOA service
    public static ViewTransactionsHistoryOutput viewTransactionDetails(Integer InstitutionId  , string Reference, Integer PageSize,Integer SkipIndex,Date From_x,Date To,String CardNo,Boolean Include,String Number_x,String TransactionType)
    {  
        if(Reference==null || Reference=='' || PageSize==0 /*|| SkipIndex==0*/ || TransactionType==null || TransactionType =='' || Number_x==null || Number_x=='')  
            return null;          
        ViewTransactionsHistoryInput  viewTransInputInstance = new ViewTransactionsHistoryInput ();
        viewTransInputInstance.InstitutionId  = InstitutionId ;
        viewTransInputInstance.Reference=Reference;
        viewTransInputInstance.PageSize=PageSize;
        viewTransInputInstance.SkipIndex=SkipIndex;
        viewTransInputInstance.From_x=From_x;
        viewTransInputInstance.To=To;
        //viewTransInputInstance.CardNo=CardNo;
        //viewTransInputInstance.Include=Include;
        viewTransInputInstance.Number_x=Number_x;
        viewTransInputInstance.TransactionType=TransactionType;
        Service svc = new Service(ServiceHelper.viewTransactionDetails);
        ViewTransactionsHistoryOutput output = (ViewTransactionsHistoryOutput)svc.callout(viewTransInputInstance ); 
        system.debug('gettrans output form-->'+output);
        return output;
    }
    // method added by Subhajit pal on 16 NOV 2016, for Update Customer Data SOA service consume
    public static UpdateCustomerDataOutput UpdateCustomerData(UpdateCustomerDataInput UpdateCustomerDataUserInput)
    {  
        if(UpdateCustomerDataUserInput==null )
            return null;          
        UpdateCustomerDataInput UpdateCustomerDataInputInstance = UpdateCustomerDataUserInput;
        Service svc = new Service(ServiceHelper.UpdateCustomerData);
        UpdateCustomerDataOutput output = (UpdateCustomerDataOutput)svc.callout(UpdateCustomerDataInputInstance); 
        system.debug('UpdateCustomer output form-->'+output);
      
        return output;
    }
    
    // method added by Shameel on 28 NOV 2016, for Get Limit Changes SOA service consume
    public static GetLimitChangesServiceOutput GetLimitChanges(GetLimitChangesServiceInput GetLimitChangesServiceInput)
    {  
        if(GetLimitChangesServiceInput == null)
            return null;   
        
        GetLimitChangesServiceInput GetLimitChangesServiceInputInstance = GetLimitChangesServiceInput;
        Service svc = new Service(ServiceHelper.GetLimitChanges);
        GetLimitChangesServiceOutput output = (GetLimitChangesServiceOutput)svc.callout(GetLimitChangesServiceInputInstance); 
        system.debug('GetLimitChanges output form-->'+output);
        return output;
    }
    
    // method added by Shameel on 19 DEC 2016, for view insurance SOA service consume
    public static ViewInsuranceServiceOutput ViewInsurance(ViewInsuranceServiceInput ViewInsuranceServiceInput)
    {  
        if(ViewInsuranceServiceInput == null)
            return null;   
        
        ViewInsuranceServiceInput ViewInsuranceServiceInputInstance = ViewInsuranceServiceInput;
        Service svc = new Service(ServiceHelper.ViewInsurance);
        ViewInsuranceServiceOutput output = (ViewInsuranceServiceOutput)svc.callout(ViewInsuranceServiceInputInstance); 
        system.debug('ViewInsurance output form-->'+output);
        return output;
    }
    
    
    
    //method added by Swarnava Bhattacharyya on 22 NOV 2016, for Update Customer Address SOA service consume
    public static UpdateCustomerAddressOutput UpdateCustomerAddress(integer InstitutionId, Address__c addr, String SSN)
    {  
        if(addr==null)
            return null;          
        UpdateCustomerAddressInput UpdateCustomerAddressInputInstance = new UpdateCustomerAddressInput();
        UpdateCustomerAddressInputInstance.isOnlyPeopleUpd = false;
        //UpdateCustomerAddressInputInstance.CustomerID = CustomerId;
        UpdateCustomerAddressInputInstance.SSN = SSN;
        UpdateCustomerAddressInputInstance.Reference = 'S';
        UpdateCustomerAddressInputInstance.NewIndicator = 'N';
        UpdateCustomerAddressInputInstance.City = addr.City__c;
        UpdateCustomerAddressInputInstance.CountyCode = addr.Country_Code__c;
        UpdateCustomerAddressInputInstance.Country = addr.Country__c;        
        UpdateCustomerAddressInputInstance.Location = addr.Location__c;
        UpdateCustomerAddressInputInstance.Address1 = addr.Street_Address__c;
        UpdateCustomerAddressInputInstance.Address2 = addr.Street_Address_2__c;
        UpdateCustomerAddressInputInstance.Address3 = addr.Street_Address_3__c;
        UpdateCustomerAddressInputInstance.Address4 = addr.Street_Address_4__c;
        UpdateCustomerAddressInputInstance.Address5 = addr.Street_Address_5__c;
        UpdateCustomerAddressInputInstance.ZIP = addr.ZIP__c;
        
        // Added by shameel 24-04-2017
        UpdateCustomerAddressInputInstance.Phone1 = addr.Tel1__c;
        UpdateCustomerAddressInputInstance.Phone2 = addr.Tel2__c;
        // end
        
        
        if(InstitutionId!=null)
            UpdateCustomerAddressInputInstance.InstitutionId = string.valueof(InstitutionId);
        Service svc = new Service(ServiceHelper.updateCustomerNameAddress);
        UpdateCustomerAddressOutput output = (UpdateCustomerAddressOutput)svc.callout(UpdateCustomerAddressInputInstance); 
        system.debug('UpdateCustomerAddress output list form-->'+output);
       return output;              
        
    }
    
    //method added by Swarnava Bhattacharyya on 22 NOV 2016, for Update Customer Name,Email,Mobile SOA service consume
    public static UpdateCustomerAddressOutput UpdateCustomerAddress(integer InstitutionId, Contact con, String SSN)
    {  
        if(con==null)
            return null;          
        UpdateCustomerAddressInput UpdateCustomerAddressInputInstance = new UpdateCustomerAddressInput();
        UpdateCustomerAddressInputInstance.isOnlyPeopleUpd = true;
        //UpdateCustomerAddressInputInstance.CustomerID = CustomerId;
        UpdateCustomerAddressInputInstance.SSN = SSN;
        UpdateCustomerAddressInputInstance.Reference = 'S';
        UpdateCustomerAddressInputInstance.NewIndicator = 'N';
        UpdateCustomerAddressInputInstance.FirstName = con.FirstName;
        UpdateCustomerAddressInputInstance.LastName = con.LastName; 
        UpdateCustomerAddressInputInstance.MaritalStatus = con.Marital_Status__c; 
        UpdateCustomerAddressInputInstance.MotherName = con.Mother_Name__c;      
        UpdateCustomerAddressInputInstance.Mobile = con.MobilePhone;
        UpdateCustomerAddressInputInstance.Email = con.Email;       
        if(InstitutionId!=null)
            UpdateCustomerAddressInputInstance.InstitutionId = string.valueof(InstitutionId);
        Service svc = new Service(ServiceHelper.updateCustomerNameAddress);
        UpdateCustomerAddressOutput output = (UpdateCustomerAddressOutput)svc.callout(UpdateCustomerAddressInputInstance); 
        system.debug('UpdateCustomerAddress output list form-->'+output);
        return output;              
        
    }
    
    /*===============================================================
Modified Date   Description                                             DeveloperName       Jira No
6th, June 2018  To update the Customer name in Prime from Salesforce     Saddam             SFD-194
======================================================================================= */
    public static UpdateMAHResponse UpdateMAHName(integer InstId, Contact con, String Custserno)
    {
        if(Custserno == null)
            return null;
        UpdateMAHRequest iputs = new UpdateMAHRequest();
        system.debug('---conName----'+con.Name);
        iputs.CustomerID = Custserno;
        iputs.CustomerName = con.FirstName +' '+ con.LastName;
        iputs.Reference = 'U';
        iputs.NewIndicator = 'N';
        //iputs.SSN = SSN;
        
        if(InstId!=null)
            iputs.InstitutionId = string.valueof(InstId); 
        system.debug('------iputs----'+iputs);  
        Service svc = new Service(ServiceHelper.updateMAHName);
        UpdateMAHResponse outputs = (UpdateMAHResponse)svc.callout(iputs); 
        system.debug('UpdateCustomerAddress output list form-->'+outputs);
        return outputs;
        
    }
    
    //method added by Subhajit on 02 Jan 2017, for Payback credit balance SOA service consume
    public static PayBackCreditBalanceOutput updatePayBackCreditBalance(integer InstitutionId, string Action,String Reference,String AccountNumber,String ServiceIndicator,String ServiceType,String LogAction,String ActionDate,String PaybackAmount)
    {  
        if(InstitutionId==null ||Action==null||Reference==null||AccountNumber==null||ServiceIndicator==null||ServiceType==null||LogAction==null|| PaybackAmount==null)
            return null;          
        PayBackCreditBalanceInput PayBackCreditBalanceInputInstance = new PayBackCreditBalanceInput();
        PayBackCreditBalanceInputInstance.InstitutionID= InstitutionID;
        PayBackCreditBalanceInputInstance.Action= Action;
        PayBackCreditBalanceInputInstance.Reference= Reference;
        PayBackCreditBalanceInputInstance.AccountNumber= AccountNumber;
        PayBackCreditBalanceInputInstance.ServiceIndicator= ServiceIndicator;
        PayBackCreditBalanceInputInstance.ServiceType= ServiceType; 
        PayBackCreditBalanceInputInstance.LogAction= LogAction; 
        PayBackCreditBalanceInputInstance.ActionDate= ActionDate;      
        PayBackCreditBalanceInputInstance.PaybackAmount= PaybackAmount;
        
        Service svc = new Service(ServiceHelper.PayBackCreditBalance);
        PayBackCreditBalanceOutput output = (PayBackCreditBalanceOutput)svc.callout(PayBackCreditBalanceInputInstance); 
        system.debug('payback credit balance output-->'+output);
        return output;              
        
    }  
    
    // method added by Shameel on 17 JAN 2017, for get consent SOA service consume
    public static GetConsentServiceOutput GetConsent(GetConsentServiceInput GetConsentServiceInput)
    {  
        if(GetConsentServiceInput== null)
            return null;   
        
        GetConsentServiceInput getConsentServiceInputInstance = GetConsentServiceInput;
        Service svc = new Service(ServiceHelper.GetConsent);
        GetConsentServiceOutput output = (GetConsentServiceOutput)svc.callout(getConsentServiceInputInstance); 
        system.debug('GetConsent output -->'+output);
        return output;
    }
    
    // method added by Shameel on 17 JAN 2017, for Update consent SOA service consume
    public static UpdateConsentServiceOutput UpdateConsent(UpdateConsentServiceInput UpdateConsentServiceInput)
    {  
        if(UpdateConsentServiceInput == null)
            return null;   
        
        UpdateConsentServiceInput updateConsentServiceInputInstance = UpdateConsentServiceInput;
        Service svc = new Service(ServiceHelper.UpdateConsent);
        UpdateConsentServiceOutput output = (UpdateConsentServiceOutput)svc.callout(updateConsentServiceInputInstance); 
        system.debug('UpdateConsent output -->'+output);
                return output;
    }
    
    // method added by Shameel on 30 JAN 2017, for CLI Application SOA service consume
    public static CLIApplicationServiceOutput submitCLIApplication(CLIApplicationServiceInput cLIApplicationServiceInput)
    {  
        if(CLIApplicationServiceInput == null)
            return null;   
        
        CLIApplicationServiceInput cLIApplicationServiceInputInstance= cLIApplicationServiceInput;
        Service svc = new Service(ServiceHelper.CLIApplication);
        CLIApplicationServiceOutput output = (CLIApplicationServiceOutput)svc.callout(cLIApplicationServiceInputInstance); 
        system.debug('CLI Application output -->'+output);
        
      
        return output;
    }
    
    //method added by Ranendu on 27 Jan 2017, for installment info SOA service consume
    public static GetInstallInfoOutput getInstallmentInfo(integer Serno, String InstitutionId)
    {  
        if(Serno==null ||InstitutionId==null)
            return null;          
        GetInstallInfoInput GetInstallInfoInputInputInstance = new GetInstallInfoInput();
        GetInstallInfoInputInputInstance.Serno= Serno;
        /*GetInstallInfoInputInputInstance.MsgId= MsgId;
GetInstallInfoInputInputInstance.CorrelationId= CorrelationId;
GetInstallInfoInputInputInstance.RequestorId= RequestorId;
GetInstallInfoInputInputInstance.SystemId= SystemId;*/
        GetInstallInfoInputInputInstance.InstitutionId= InstitutionId;
        
        Service svc = new Service(ServiceHelper.GetInstallment);
        outputinstallInfo = (GetInstallInfoOutput)svc.callout(GetInstallInfoInputInputInstance); 
        system.debug('getinstallment output-->'+outputinstallInfo);       
        return outputinstallInfo;              
        
    }
    
    //method added by Swarnava on 15 Feb 2017, for installment info SOA service consume
    public static CustomerMemoExportDataOutput updateCustomerMemo(String Action, String Reference,String Number_x,String Type_x,String Message,String InstitutionId)
    {  
        if(Action==null ||Reference==null||Number_x==null||Type_x==null||Message==null)
            return null;          
        CustomerMemoExportDataInput customerMemoExportDataInputInstance = new CustomerMemoExportDataInput();
        customerMemoExportDataInputInstance.Action = Action;
        customerMemoExportDataInputInstance.Reference = Reference;
        customerMemoExportDataInputInstance.Number_x = Number_x;
        customerMemoExportDataInputInstance.Type_x = Type_x;
        customerMemoExportDataInputInstance.Message = Message;
        customerMemoExportDataInputInstance.InstitutionId = InstitutionId;
        Service svc = new Service(ServiceHelper.CustomerMemoExportData);
        CustomerMemoExportDataOutput outputmemoexportdata = (CustomerMemoExportDataOutput)svc.callout(customerMemoExportDataInputInstance); 
        system.debug('CustomerMemoExportDataOutput output-->'+outputmemoexportdata);
        return outputmemoexportdata;              
        
    }
    
    
    // method added by Shameel on 31 March 2017, for GetLoans SOA service consume
    public static GetLoanServiceOutput GetLoans(GetLoanServiceInput getLoanServiceInput)
    {  
        if(getLoanServiceInput== null)
            return null;   
        
        GetLoanServiceInput getLoanServiceInputInstance= getLoanServiceInput;
        Service svc = new Service(ServiceHelper.GetLoans);
        GetLoanServiceOutput output = (GetLoanServiceOutput)svc.callout(getLoanServiceInputInstance); 
        system.debug('Get Loans output -->'+output);
        
        return output;
    }
    // method added by Subhajit Pal on 12 Sept 2017, for ReadMembershipData SOA service consume
    public static ReadMembershipDataOutput ReadMembershipData(ReadMembershipDataInput ReadMembershipDataInputArg)
    {  
        if(ReadMembershipDataInputArg== null)
            return null;   
        
        ReadMembershipDataInput ReadMembershipDataInputInstance= ReadMembershipDataInputArg;
        Service svc = new Service(ServiceHelper.ReadMembershipData);
        ReadMembershipDataOutput output = (ReadMembershipDataOutput)svc.callout(ReadMembershipDataInputInstance); 
        system.debug('Get MembershipData output -->'+output);
        return output;
    }
    // method added by Subhajit Pal on 21 Sept 2017, for UpdateMembershipData SOA service consume
    public static UpdateMembershipDataOutput UpdateMembershipData(UpdateMembershipDataInput UpdateMembershipDataInputArg)
    {  
        if(UpdateMembershipDataInputArg== null)
            return null;   
        
        UpdateMembershipDataInput UpdateMembershipDataInputInstance= UpdateMembershipDataInputArg;
        Service svc = new Service(ServiceHelper.UpdateMembershipData);
        UpdateMembershipDataOutput output = (UpdateMembershipDataOutput)svc.callout(UpdateMembershipDataInputInstance); 
        system.debug('Update MembershipData output -->'+output);
        return output;
    }
    
    // method added by saddam on 12 FEB 2018, for GetPartner SOA service consume
    Public static GetNonCustomerOutputs getNonCustomer(GetNonCustomerInputs getNonCustomerInput){
        If(getNonCustomerInput == null)
            return null;
        
        GetNonCustomerInputs getNoncustomerInputInstance = getNonCustomerInput;
        system.debug('-----getNoncustomerInputInstance--'+getNoncustomerInputInstance);
        Service svc = new Service(ServiceHelper.GetNonCustomerDetails);
        System.debug('*******success******'+svc);
        GetNonCustomerOutputs outputs = (GetNonCustomerOutputs)svc.callout(getNoncustomerInputInstance);
        system.debug('------getCustomer output form----->'+outputs);
        return outputs;
    }
    
    // method added by saddam on 9 FEB 2018, for GetPartner SOA service consume
    Public static GetPartnerOutputs getPartnerDetails(GetPartnerInputs getPartnerInputs){
        If(getPartnerInputs == null)
            return null;
        GetPartnerInputs getPartnerInputInstance = getPartnerInputs;
        system.debug('--------getPartnerInputInstance----'+getPartnerInputInstance);
        Service svc = new Service(ServiceHelper.GetPartnerDetails);
        System.debug('*******success******'+svc);
        GetPartnerOutputs outputs = (GetPartnerOutputs)svc.callout(getPartnerInputInstance);
        system.debug('getPartner output form-->'+outputs);
        return outputs;
    }
     // Method added by Shauryanaditya 3rd May 2018, for CustomerRights OASIS Service consume
    Public static GetCustomerRightsOutputs getCustomerRights(GetCustomerRightsInputs CustomerRightsInput)
    {
        IF (CustomerRightsInput == null)
            return null;
        
        GetCustomerRightsInputs getCustomerRightsInputsInstance = CustomerRightsInput;
        system.debug('-----GetCustomerRightsInputsInstance--'+getCustomerRightsInputsInstance);
        Service svc = new Service(ServiceHelper.CustomerRights);
        System.debug('*******success******'+svc);
        GetCustomerRightsOutputs outputs = (GetCustomerRightsOutputs)svc.callout(getCustomerRightsInputsInstance);
        system.debug('------getCustomer output form----->'+outputs);
        return outputs;  
    }
}