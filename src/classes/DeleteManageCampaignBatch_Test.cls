/********************************************************************************************
Name: DeleteManageCampaignBatch_Test
=============================================================================================
Purpose: This test class is covering 'DeleteManageCampaignBatch_Test' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date             Created for Jira     Detail
1.0		    Gagan Pathak 	           07-October-2020	SFD-1692		     Intial version
********************************************************************************************/
@isTest
public class DeleteManageCampaignBatch_Test {
    Public static testMethod void deleteManageCampaignBatchTest(){
        System.runAs(UnitTestDataGenerator.adminUser) { 
            TriggerSettings__c triggerObj = UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Date today = System.today();
            Date lastmonth = today.addMonths(-2);
            List<Manage_Campaign__c> camplist = new List<Manage_Campaign__c>();
            Manage_Campaign__c camp = UnitTestDataGenerator.testManageCampaign.buildInsert(new Map<String, Object>{
                     'End_Date__c' => lastmonth,
                     'IsActive__c' => false
                    });
             Manage_Campaign__c camp1 = UnitTestDataGenerator.testManageCampaign.buildInsert(new Map<String, Object>{
                    'AccountSerno__c' => '1234564',
                    'Campaign_Code__c' => '12451',
                    'End_Date__c' => lastmonth,
                    'IsActive__c' => false
                    });
            camplist.add(camp);
            camplist.add(camp1);
            
            Test.StartTest();
            DeleteManageCampaignBatch d=new DeleteManageCampaignBatch();
            Database.executeBatch(d, 200);
            
            Delete camp;
            DeleteManageCampaignBatch dmc = new DeleteManageCampaignBatch();
            String sch = '0 0 20 * * ?';
            System.schedule('test job', sch, dmc);
            Test.StopTest(); 
        }
    }
}