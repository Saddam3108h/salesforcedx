public with sharing class ConsentDataController{ 
    
    public ConsentDataController(){
        
        system.debug('***default constructor***');
        initialize();
        
    }
    
    public string serNo {get;set;}
    public string consentEntityType{get;set;}
    public string[] consentList{get;set;}
    public string fromDate{get;set;}
    public string toDate{get;set;}
    public string institutionId{get;set;}
    public string idstr{get;set;}
    public List<ConsentWrapper> consentWrapperList{get;set;}
    public set<string> selectedConsentCodeSet = new set<string>();
    public boolean isSuccess {get;set;}
    
    public Case caseObj{get;set;}
    public boolean isShowingDetailList{get;set;}
    
    public ConsentDataController(ApexPages.StandardController sc){
        
        system.debug('***standard constructor***');
        initialize(); 
        isShowingDetailList = false;
        
    }
    
    //to pass the current controller instance to the component
    public ConsentDataController consentDataConInstance
    {
        get{
            return this;
        }
        set;
    }
    
    public void initialize()
    {
        consentList=new List<string>();
        consentWrapperList=new List<ConsentWrapper>();
        selectedConsentCodeSet = new set<string>();
        isSuccess =false; 
        idstr=apexpages.currentpage().getparameters().get('Id');
        
        if(idstr!=null)
        {
            if(idstr.startsWith('003'))
            {
                List<Contact> contactList=[SELECT Id, SerNo__c, Institution_Id__c FROM Contact where Id=:idstr];
                if(contactList.size()>0)
                {
                    this.serNo=contactList[0].SerNo__c;
                    this.consentEntityType='People';
                    this.institutionId=contactList[0].Institution_Id__c!=null?string.valueof(contactList[0].Institution_Id__c):'';
                }
                
            }
            else
            {
                
                List<Financial_Account__c> financialAccList= [SELECT Id, Account_Serno__c, Institution_Id__c FROM Financial_Account__c where id =:idstr];
                if(financialAccList.size()>0)
                {
                    this.serNo=financialAccList[0].Account_Serno__c;
                    this.consentEntityType='Account';
                    this.institutionId=financialAccList[0].Institution_Id__c!=null?string.valueof(financialAccList[0].Institution_Id__c):'';
                }
            }
        }
        
        system.debug('***idstr***'+idstr);
        system.debug('***serNo***'+serNo);
        system.debug('***consentEntityType***'+consentEntityType);
        system.debug('***institutionId***'+institutionId);
        
    }
    
    public PageReference loadConsents()
    {
        selectedConsentCodeSet =new set<string>();
        consentList=new List<string>();
        GetConsentServiceInput getConsentServiceInput = new GetConsentServiceInput();
        
        try{
            
            getConsentServiceInput.InstitutionId=institutionId;
            getConsentServiceInput.ConsentEntityId=serNo;
            getConsentServiceInput.ConsentEntityIdType=consentEntityType;
            
            GetConsentServiceOutput getConsentServiceOutput = CSSServiceHelper.GetConsent(getConsentServiceInput);
            
            if(getConsentServiceOutput!=null && getConsentServiceOutput.GetConsentDataResponseRecord!=null)
            {
                for(GetConsentServiceOutput.GetConsentDataResponseRecordType resp:getConsentServiceOutput.GetConsentDataResponseRecord)
                {
                    // As per input provided by barkha
                    if(resp.ConsentCode!=null && resp.Value=='Y')
                    {
                        consentList.add(resp.ConsentCode); 
                        selectedConsentCodeSet.add(resp.ConsentCode);
                    }
                    
                }   
                
            }
            if(isSuccess==true)
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Record saved successfully'));
            
            system.debug('***consentList***'+consentList);
            system.debug('***selectedConsentCodeSet***'+selectedConsentCodeSet);
            
        }
        
        Catch(dmlException e)
        {
            StatusLogHelper.logSalesforceError('loadConsents', 'E005', 'loadConsents: ' + getConsentServiceInput, e, false);
        }
        Catch(calloutException e)
        {
            StatusLogHelper.logSalesforceError('loadConsents', 'E001', 'loadConsents: ' + getConsentServiceInput, e, false);
        }
        Catch(Exception e)
        {
            StatusLogHelper.logSalesforceError('loadConsents', 'E004', 'loadConsents: ' + getConsentServiceInput, e, false);
        }
        
        return null;
    }
    
    public PageReference getHistoricalConsents()
    {
        system.debug('***1***');
        GetConsentServiceInput getConsentServiceInput = new GetConsentServiceInput();
        try{
            
            if(fromDate==null || fromDate==''|| toDate==null || toDate==''||!isDateValid(fromDate)||!isDateValid(toDate))
            {
                system.debug('***2***');
                if(fromDate ==''|| fromDate == null)
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select fromdate'));
                else if(!isDateValid(fromDate))
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid fromdate'));
                
                if(toDate ==''||toDate == null)
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select todate'));
                else if(!isDateValid(toDate))
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid todate'));
                
                system.debug('***!isDateValid(toDate)***'+!isDateValid(toDate));
                system.debug('***!isDateValid(fromDate)***'+!isDateValid(fromDate));
                system.debug('***3***');
                
                return null;
            }
            system.debug('***4***');
            Date fromDateTemp = date.valueOf(fromDate);
            Date toDateTemp = date.valueOf(toDate);
            
            system.debug('***fromDateTemp***'+fromDateTemp);
            system.debug('***toDateTemp***'+toDateTemp);
            
            if((fromDateTemp > toDateTemp) ||(fromDateTemp.daysBetween(toDateTemp)> 365))
            {
                system.debug('***5***');
                if((fromDateTemp > toDateTemp))
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'fromdate has to be less than or equal to todate'));
                
                if((fromDateTemp.daysBetween(toDateTemp)> 365))
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'date difference can not be greater than 365 days'));
                
                return null;
            }
            
            system.debug('***fromDateTemp 1***'+fromDateTemp);
            system.debug('***toDateTemp 1***'+toDateTemp);
            
            consentWrapperList= new List<ConsentWrapper>();
            
            getConsentServiceInput.InstitutionId=institutionId;
            getConsentServiceInput.ConsentEntityId=serNo;
            getConsentServiceInput.ConsentEntityIdType=consentEntityType;
            
            
            getConsentServiceInput.FromDate=DateTime.newInstance(fromDateTemp.year(),fromDateTemp.month(),fromDateTemp.day(), 01, 00, 00);
            getConsentServiceInput.ToDate=DateTime.newInstance(toDateTemp.year(),toDateTemp.month(),toDateTemp.day(), 24, 59, 59);
            
            GetConsentServiceOutput getConsentServiceOutput = CSSServiceHelper.GetConsent(getConsentServiceInput);
            
            if(getConsentServiceOutput!=null && getConsentServiceOutput.GetConsentDataResponseRecord!=null)
            {
                
                for(GetConsentServiceOutput.GetConsentDataResponseRecordType resp:getConsentServiceOutput.GetConsentDataResponseRecord)
                {     
                    
                    if(consentWrapperList.size()<500)
                        consentWrapperList.add(new ConsentWrapper(resp.InstitutionId,resp.ConsentEntityId,resp.ConsentEntityIdType,resp.ConsentCode,resp.Value,resp.DateTimeStamp,resp.UpdatedBy,resp.Source));
                    
                }   
                
            }
            system.debug('***consentWrapperList***'+consentWrapperList);
        }
        
        Catch(dmlException e)
        {
            StatusLogHelper.logSalesforceError('getHistoricalConsents', 'E005', 'getHistoricalConsents: ' + getConsentServiceInput, e, false);
        }
        Catch(calloutException e)
        {
            StatusLogHelper.logSalesforceError('getHistoricalConsents', 'E001', 'getHistoricalConsents: ' + getConsentServiceInput, e, false);
        }
        Catch(Exception e)
        {
            StatusLogHelper.logSalesforceError('getHistoricalConsents', 'E004', 'getHistoricalConsents: ' + getConsentServiceInput, e, false);
        }
        
        return null;
    }
    
    public pagereference updateConsentData()
    {
        UpdateConsentServiceInput updateConsentServiceInput=new UpdateConsentServiceInput();
        try{
            
            List<string> tempCodeYList = new List<string>();
            List<string> tempCodeNList = new List<string>();
            set<string> tempCodeSet = new set<string>();
            isSuccess=false;
            
            for(integer i=0;i<consentList.size();i++)
            {
                if(!selectedConsentCodeSet.contains(consentList[i]))
                {
                    tempCodeYList.add(consentList[i]);
                    
                }
                tempCodeSet.add(consentList[i]);
            }
            
            List<string> tempList =new List<string>();
            
            if(selectedConsentCodeSet.size()>0)
                tempList.addall(selectedConsentCodeSet);
            
            for(integer i=0;i<tempList.size();i++)
            {
                if(!tempCodeSet.contains(tempList[i]))
                {
                    tempCodeNList.add(tempList[i]);
                }
                
            }
            
            if(tempCodeYList.size()==0 && tempCodeNList.size()==0){
                
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select the consents to update'));
                
                return null;
            }  
            
            
            updateConsentServiceInput.InstitutionId=institutionId;
            updateConsentServiceInput.ConsentEntityId=serNo;
            updateConsentServiceInput.ConsentEntityIdType=consentEntityType;
            
            updateConsentServiceInput.ConsentCodeValue=new List<UpdateConsentServiceInput.ConsentCodeType>();
            
            for(integer i=0;i<tempCodeYList.size();i++)
            {
                UpdateConsentServiceInput.ConsentCodeType consentCodeType = new UpdateConsentServiceInput.ConsentCodeType();
                consentCodeType.code=tempCodeYList[i];
                consentCodeType.value ='Y';
                updateConsentServiceInput.ConsentCodeValue.add(consentCodeType);                
            }
            
            for(integer i=0;i<tempCodeNList.size();i++)
            {
                UpdateConsentServiceInput.ConsentCodeType consentCodeType = new UpdateConsentServiceInput.ConsentCodeType();
                consentCodeType.code=tempCodeNList[i];
                consentCodeType.value ='N';   
                updateConsentServiceInput.ConsentCodeValue.add(consentCodeType);                
            }
            
            updateConsentServiceInput.DateTimeStamp=system.now();
            updateConsentServiceInput.UpdatedBy=UserInfo.getUserId();//UserInfo.getName();
            updateConsentServiceInput.Source='Salesforce';
            
            UpdateConsentServiceOutput updateConsentServiceOutput =CSSServiceHelper.UpdateConsent(updateConsentServiceInput);
            
            if(updateConsentServiceOutput!= null && updateConsentServiceOutput.Code == '0')
            {
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Record saved successfully'));
                isSuccess=true;
                
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,CommonHelper.displayMessage(updateConsentServiceOutput!= null?updateConsentServiceOutput.Code:'')));
                //SOAServiceRetryLogOpsController.insertDataSOAServiceRetryLog();//insert retry logs if any
                
            }
        }
        
        Catch(dmlException e)
        {
            StatusLogHelper.logSalesforceError('updateConsentData', 'E005', 'updateConsentData: ' + updateConsentServiceInput, e, false);
        }
        Catch(calloutException e)
        {
            StatusLogHelper.logSalesforceError('updateConsentData', 'E001', 'updateConsentData: ' + updateConsentServiceInput, e, false);
        }
        Catch(Exception e)
        {
            StatusLogHelper.logSalesforceError('updateConsentData', 'E004', 'updateConsentData: ' + updateConsentServiceInput, e, false);
        }
        
        return null;
    }
    
    // to check if date is valid
    public boolean isDateValid(string strDate)
    {
        // try parsing date
        try
        {
            date formattedDate = date.parse(strDate);
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }
    }
    
    // Wrapper Class
    
    public class ConsentWrapper
    {
        public ConsentWrapper(String InstitutionId,String ConsentEntityId,String ConsentEntityIdType,String ConsentCode,String Value,DateTime DateTimeStamp,String UpdatedBy,String Source){
            this.InstitutionId=InstitutionId;
            this.ConsentEntityId=ConsentEntityId;
            if(ConsentEntityIdType=='1')
                this.ConsentEntityIdType='Account';
            else if(ConsentEntityIdType=='2')
                this.ConsentEntityIdType='People';
            
            this.ConsentCode=ConsentCode;
            this.Value=Value;
            this.DateTimeStamp=DateTimeStamp;
            this.UpdatedBy=UpdatedBy;
            this.Source=Source;
        }
        public ConsentWrapper(){}  
        public String InstitutionId{get;set;}
        public String ConsentEntityId{get;set;}
        public String ConsentEntityIdType{get;set;}
        public String ConsentCode{get;set;}
        public String Value{get;set;}
        public DateTime DateTimeStamp{get;set;}
        public String UpdatedBy{get;set;}
        public String Source{get;set;}
        
    }
    
    public pagereference showDetailList(){
  
      loadConsents();
      isShowingDetailList = true;
      return null;
  }
    
}