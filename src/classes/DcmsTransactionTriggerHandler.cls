/**********************************************************************
Name: DcmsTransactionTriggerHandler
=======================================================================
Purpose: This trigger handler class is created on DCMS_Transaction object to handle all the logics. User Story SFD-984

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Srinivas D           5th-Feb-2019       Initial Version
1.1         Srinivas D			 27th-Feb-2019
**********************************************************************/
public with sharing class DcmsTransactionTriggerHandler implements ITriggerHandler {
    public static Boolean TriggerDisabled = false;
    /*
Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {
        if (TriggerSettings__c.getInstance('Execution').DcmsTransactionExecution__c // false
            &&  TriggerSettings__c.getInstance('Execution').OrgExecution__c)
        { 
            return true;
        }
        else
        { 
            return TriggerDisabled;
        }
        
    }
    
    public void BeforeInsert(List<SObject> newItems) {
        updateTransactionDate(newItems);
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterInsert(Map<Id, SObject> newItems) {
        Map<Id, DCMS_Transaction__c> newCaseItems = (Map<Id, DCMS_Transaction__c>) newItems;
        createDisputeCases(newCaseItems.values()); 
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, DCMS_Transaction__c> newCaseItems = (Map<Id, DCMS_Transaction__c>) newItems;
        Map<Id, DCMS_Transaction__c> oldCaseItems = (Map<Id, DCMS_Transaction__c>) oldItems;
        if(checkRecursive.runOnce()){
            DCMSCalculationLogicController.updateTotalAmounts(newCaseItems.values(), oldCaseItems);
        }
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems) {
        Map<Id, DCMS_Transaction__c> oldCaseItems = (Map<Id, DCMS_Transaction__c>) oldItems;
        updateGrossAmount(oldCaseItems.values());
    }
    
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    // caseListToCreate list is to store new Cases to insert
    public List<Case> caseListToCreate = new List<Case>();
    // Below caseMap is store Dispute Cases created on Today. CardserNo as key and Case as Value
    public Map<String,Case> caseMap = new Map<String,Case>();
    public Map<String,Case> caseDispMap = new Map<String,Case>();
    public Map<id,Case> caseIdMap = new Map<Id,Case>();
    public List<String> cardSerNolist = new List<String>();
    public Map<String,Card__c> cardMap = new Map<String,Card__c>();
    // Below Map is to hold Queue Data
    public Map<String,Id> queueMap = new Map<String,Id>();
    public List<Case> newCasesList = new List<Case>();
    public List<DCMS_Transaction__c> updateTrans = new List<DCMS_Transaction__c>();
    // Below fraudCaseList to hold new FraudCases to insert
    public List<DCMS_FraudCase__c> fraudCaseList = new List<DCMS_FraudCase__c>(); 
    public Map<String,String> qcsMap = new Map<String,String>();
    public Map<String,String> qcsMap1 = new Map<String,String>();
    
    public List<DCMS_Decision__c> decisionStatusCS; 
    public List<DCMS_CaseStatus__c> caseStatusCS;
    public List<DCMS_FraudRing__c> fraudRingCS;
    
    public Map<String,Decimal> decisionStatusMap = new Map<String,Decimal>();
    public Map<String,Decimal> caseStatusMap = new Map<String,Decimal>();
    public Map<String,Decimal> fraudRingMap = new Map<String,Decimal>();
    
    //Constructor 
    public void DcmsTransactionTriggerHandler(){
        
        //Getting Custom Settings Data
        decisionStatusCS=DCMS_Decision__c.getall().values();
        caseStatusCS=DCMS_CaseStatus__c.getall().values();
        fraudRingCS=DCMS_FraudRing__c.getall().values();  
        
        for(DCMS_Decision__c desCS : decisionStatusCS){
            decisionStatusMap.put(desCS.Name,desCS.DecisionId__c);
        }
        
        for(DCMS_CaseStatus__c stsCS : caseStatusCS){
            caseStatusMap.put(stsCS.Name,stsCS.CaseStatusId__c);
        }
        
        for(DCMS_FraudRing__c frdCS : fraudRingCS){
            fraudRingMap.put(frdCS.Name,frdCS.FraudRingId__c);
        }
        for(DCMS_Queue_Settings__c qcset : DCMS_Queue_Settings__c.getall().values()){
            qcsMap.put(qcset.Name,qcset.Queue_Name__c);
        }
        System.debug('qcsMap with out SOQL'+qcsMap1);
        
        // Below code is to get DCMS_Queue_Settings__c data
        /*  for(DCMS_Queue_Settings__c qcset : [Select Name,Dispute_Type__c,Institutionid__c,Queue_Name__c from DCMS_Queue_Settings__c]){
qcsMap.put(qcset.Name,qcset.Queue_Name__c);
} */
        System.debug('qcsMap with SOQL'+qcsMap);
        // Below for loop is to store Queue details into Map
        for(Group grpqueue : [select Id,Name,DeveloperName from Group where Type = 'Queue']){
            queueMap.put(grpqueue.DeveloperName,grpqueue.Id);
        }
    }
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     05-Feb-2019
*   User Story : SFD-984
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Case records based on Dispute Type field on DCMS_Transaction__c Object.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public void createDisputeCases (List<DCMS_Transaction__c> transList) {
        try{
            system.debug('---transList-'+transList.size());
            String keyval;
            Id caseId;
            DcmsTransactionTriggerHandler();
            System.debug('transList============>'+transList);
            List<DCMS_Transaction__c> transactionsList = [Select id,Txn_CardSerno__c,DisputeType__c,MerchName__c,PCI_Dss_Token__c,BillingAmount__c,Institutionid__c,CaseNumber__c,CaseNumber__r.Card_SerNo__c,CaseNumber__r.Dispute_Type__c from DCMS_Transaction__c where Id IN: transList];
            List<DCMS_FraudCase__c> fCaseList = new List<DCMS_FraudCase__c>();
            List<Decimal> tranCardSernosList = new List<Decimal>();
            // Below for loop is to get all cardserNo from DCMS Transaction Object
            for(DCMS_Transaction__c tran : transList){
                system.debug('--card Seno---'+tran.Txn_CardSerno__c);
                if(tran.Txn_CardSerno__c != null){
                    String cardSNo = String.valueOf(tran.Txn_CardSerno__c.longValue());
                    System.debug('cardSNo============>'+cardSNo); 
                    
                    if(cardSNo != null){
                        String ss=cardSNo.removeEnd('.0');
                        System.debug('sss============>'+ss);
                        cardSerNolist.add(ss);
                        tranCardSernosList.add(Decimal.valueOf(ss));
                        System.debug('tranCardSernosList============>'+tranCardSernosList);
                    }
                } 
            }
            system.debug('cardSerNolist=====>>>>>>'+cardSerNolist);
            Map<Id,DCMS_Transaction__c> oldTransList = new  Map<Id,DCMS_Transaction__c>([Select id,Txn_CardSerno__c,MerchName__c,DisputeType__c,PCI_Dss_Token__c,BillingAmount__c,Institutionid__c,CaseNumber__c,CaseNumber__r.Card_SerNo__c,CaseNumber__r.Dispute_Type__c,CaseNumber__r.Status from DCMS_Transaction__c where Txn_CardSerno__c IN: tranCardSernosList Limit 50000]);
            
            Map<String,DCMS_Transaction__c> tranMerchMap = new Map<String,DCMS_Transaction__c>();
            for(DCMS_Transaction__c oldtxn : oldTransList.values()){
                String temp =oldtxn.Txn_CardSerno__c+oldtxn.MerchName__c;
                tranMerchMap.put(temp,oldtxn);
            }
            
            
            // Below for loop is store Dispute Cases created on Today. CardserNo as key and Case as Value
            for(Case cas : [Select id,Description,Card_SerNo__c,Status,Card__r.Financial_Account__r.Account_Serno__c,Fraud_Ring__c,Decision_Status__c,Dispute_Type__c,Institutionid__c,Gross_Amount__c,Card_Number_Masked__c,Pci_Dss_Token__c,CreatedDate,Contact.SerNo__c,Contact.Institution_Id__c,(Select id,Institutionid__c from DCMS_Transactios__r),(Select id,TotalFraudAmt__c,TotalAtRisk__c,NumberOfTransactions__c from DCMS_FraudCases__r) from Case where Dispute_Type__c!='' AND (Status='In Progress' OR Status='New') limit 50000]){
                caseMap.put(cas.Card_SerNo__c,cas);
                caseIdMap.put(cas.id,cas);
                String keyParam= cas.Dispute_Type__c+cas.Card_SerNo__c;
                caseDispMap.put(keyParam,cas);
            }
            system.debug('====caseMap=====>>>>>>'+caseMap);
            
            // Below for loop is to get Card details into Map<CardSerNo,Card>
            for(Card__c crd : [select id,Card_Serno__c,People__c,People__r.Institution_Id__c from Card__c where Card_Serno__c IN: cardSerNolist]){
                cardMap.put(crd.Card_Serno__c,crd);
            }
            system.debug('cardMap=====>>>>>>'+cardMap);
            // Below code is to create new Dispute cases if all conditions met
            for(DCMS_Transaction__c tran : transactionsList){
                System.debug('--- case number----> '+tran.CaseNumber__c);
                Case cs = new Case();
                if(tran.CaseNumber__c != null){
                    if(caseMap.get(tran.CaseNumber__r.Card_SerNo__c) != null){
                        system.debug('----institution---**'+tran.Institutionid__c);
                        tran.DisputeType__c = caseIdMap.get(tran.CaseNumber__r.Id).Dispute_Type__c;
                        System.debug('account serno '+caseIdMap.get(tran.CaseNumber__r.Id).Card__r.Financial_Account__r.Account_Serno__c);
                        if(caseIdMap.get(tran.CaseNumber__r.Id).Card__r.Financial_Account__r.Account_Serno__c != null){
                            tran.Account_Serno__c = Decimal.valueOf(caseIdMap.get(tran.CaseNumber__r.Id).Card__r.Financial_Account__r.Account_Serno__c);
                        }
                        System.debug('tran Case Dispute_Type__c ====>> ' +caseIdMap.get(tran.CaseNumber__r.Id).Dispute_Type__c);   
                        if(caseIdMap.get(tran.CaseNumber__r.Id).Card_SerNo__c != null){
                            tran.Txn_CardSerno__c = Decimal.valueOf(caseIdMap.get(tran.CaseNumber__r.Id).Card_SerNo__c);
                            tran.Truncated_Card_Number__c = caseIdMap.get(tran.CaseNumber__r.Id).Card_Number_Masked__c;
                        }
                        cs.Gross_Amount__c = caseIdMap.get(tran.CaseNumber__r.Id).Gross_Amount__c+tran.BillingAmount__c;
                        System.debug('tran Case 111 Dispute_Type__c ====>>'+caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r.size());
                        if(caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r.size() > 0){
                            System.debug('tran Case Dispute_Type__c ====>> '+caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r.size());
                            caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r[0].TotalAtRisk__c= caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r[0].TotalAtRisk__c+tran.BillingAmount__c;
                            caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r[0].TotalFraudAmt__c= caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r[0].TotalFraudAmt__c+tran.BillingAmount__c;
                            caseIdMap.get(tran.CaseNumber__r.Id).DCMS_FraudCases__r[0].NumberOfTransactions__c= caseIdMap.get(tran.CaseNumber__r.Id).DCMS_Transactios__r.size();
                            fCaseList.add(caseIdMap.get(tran.CaseNumber__r.id).DCMS_FraudCases__r[0]);
                        }
                        updateTrans.add(tran);
                        system.debug('-------205----'+updateTrans);
                    }
                } else {
                    System.debug('--- Dispute Type----> '+tran.DisputeType__c);
                    if(tran.DisputeType__c != null) {
                        String cardSerNo = String.valueOf(tran.Txn_CardSerno__c);
                        System.debug('--cardNum-----> '+cardSerNo);
                        System.debug('--caseMap---cardNum-----> '+caseMap.get(cardSerNo));
                        if(caseMap.get(cardSerNo) != null && tran.CaseNumber__c == null){
                            keyval=tran.DisputeType__c+tran.Txn_CardSerno__c;
                            //system.debug('Case Disp Type -->'+caseDispMap.get(keyval).Dispute_Type__c);
                            system.debug('Case cardSerNo -->'+caseMap.get(cardSerNo).Card_SerNo__c);
                            system.debug('tran.DisputeType__c -->'+tran.DisputeType__c);
                            system.debug('Tran cardSerNo -->'+tran.Txn_CardSerno__c);
                            system.debug('Case Status -->'+caseMap.get(cardSerNo).Status);
                            
                            if(tran.DisputeType__c == '10: credit'){
                                System.debug('new Tran Disp Type== '+tranMerchMap.get(tran.MerchName__c));
                                String temp =tran.Txn_CardSerno__c+tran.MerchName__c;
                                System.debug('temp== '+temp);
                                System.debug('temp1== '+tranMerchMap.get(temp));
                                System.debug('temp2== '+(tranMerchMap.get(temp).CaseNumber__r.Status == 'In Progress' || tranMerchMap.get(temp).CaseNumber__r.Status == 'New'));
                                if(tranMerchMap.get(temp) != null && (tranMerchMap.get(temp).CaseNumber__r.Status == 'In Progress' || tranMerchMap.get(temp).CaseNumber__r.Status == 'New')){
                                    System.debug('new Tran Case based on MerchName== '+tranMerchMap.get(temp).CaseNumber__c);
                                    tran.CaseNumber__c = tranMerchMap.get(temp).CaseNumber__c;
                                    updateTrans.add(tran);
                                } else {
                                    System.debug('===MerchName not Matched---'+caseMap.get(cardSerNo).Id);
                                    tran.CaseNumber__c = caseMap.get(cardSerNo).Id;
                                    updateTrans.add(tran);
                                }
                            }
                            else if(caseDispMap.get(keyval) != null 
                                    && (tran.DisputeType__c == caseDispMap.get(keyval).Dispute_Type__c) 
                                    && (tran.Txn_CardSerno__c == Decimal.valueOf(caseDispMap.get(keyval).Card_SerNo__c)) 
                                    && (cardSerNo == caseMap.get(cardSerNo).Card_SerNo__c) 
                                    && (caseMap.get(cardSerNo).Status == 'In Progress' || caseMap.get(cardSerNo).Status == 'New')
                                   ){
                                       System.debug('--inside- if----> '); 
                                       // Here we need to update the Case Id in Transaction Record.
                                       System.debug('keyval===>>'+keyval);
                                       tran.CaseNumber__c = caseDispMap.get(keyval).Id;
                                       tran.Institutionid__c = String.valueOf(caseDispMap.get(keyval).Institutionid__c);
                                       updateTrans.add(tran);
                                       if(caseDispMap.get(keyval).DCMS_FraudCases__r.size() > 0){
                                           caseDispMap.get(keyval).DCMS_FraudCases__r[0].TotalAtRisk__c= caseDispMap.get(keyval).DCMS_FraudCases__r[0].TotalAtRisk__c+tran.BillingAmount__c;
                                           caseDispMap.get(keyval).DCMS_FraudCases__r[0].TotalFraudAmt__c= caseDispMap.get(keyval).DCMS_FraudCases__r[0].TotalFraudAmt__c+tran.BillingAmount__c;
                                           caseDispMap.get(keyval).DCMS_FraudCases__r[0].NumberOfTransactions__c= caseDispMap.get(keyval).DCMS_Transactios__r.size()+1;
                                           fCaseList.add(caseDispMap.get(keyval).DCMS_FraudCases__r[0]);
                                       }
                                   } else if(tran.CaseNumber__c == null){
                                       System.debug(' ---inner If else-->>>>>> '+tran.CaseNumber__c);
                                       cs.Department__c='Fraud and Chargeback';
                                       cs.Category__c='Dispute';
                                       cs.Dispute_Type__c=tran.DisputeType__c;
                                       cs.Card__c = cardMap.get(cardSerNo).Id;
                                       cs.ContactId = cardMap.get(cardSerNo).People__c;
                                       cs.Institutionid__c = Decimal.valueOf(tran.Institutionid__c);
                                       cs.Pci_Dss_Token__c = String.valueOf(tran.PCI_Dss_Token__c);
                                       System.debug('BillingAmount===>>>> '+tran.BillingAmount__c);
                                       cs.Origin = 'SOA';
                                       if(tran.Institutionid__c == '1')
                                           cs.Country__c ='NO';
                                       if(tran.Institutionid__c == '2')
                                           cs.Country__c ='SE';
                                       if(tran.Institutionid__c == '3')
                                           cs.Country__c ='DK';
                                       if(tran.DisputeType__c != null && tran.Institutionid__c != null){
                                           String temp= tran.DisputeType__c+tran.Institutionid__c;
                                           if(queueMap.get(qcsMap.get(temp)) != null){
                                               cs.OwnerId = queueMap.get(qcsMap.get(temp));
                                           }
                                       }
                                       caseListToCreate.add(cs);
                                   }
                        } else {
                            System.debug('-- inside else -----> '); 
                            cs.Department__c='Fraud and Chargeback';
                            cs.Category__c='Dispute';
                            cs.Dispute_Type__c=tran.DisputeType__c;
                            System.debug('======cardMap1111====='+cardMap.get(cardSerNo));
                            if(cardMap.get(cardSerNo) != Null){
                                System.debug('======cardMap1111222');
                                cs.Card__c = cardMap.get(cardSerNo).Id;
                                cs.ContactId = cardMap.get(cardSerNo).People__c;
                            }
                            cs.Institutionid__c = Decimal.valueOf(tran.Institutionid__c);
                            System.debug('===============BillingAmount===>>>> '+tran.BillingAmount__c);
                            cs.Pci_Dss_Token__c = String.valueOf(tran.PCI_Dss_Token__c);
                            cs.Origin = 'SOA';
                            
                            if(tran.Institutionid__c == '1')
                                cs.Country__c ='NO';
                            if(tran.Institutionid__c == '2')
                                cs.Country__c ='SE';
                            if(tran.Institutionid__c == '3')
                                cs.Country__c ='DK';
                            
                            if(tran.DisputeType__c != null && tran.Institutionid__c != null){
                                String temp= tran.DisputeType__c+tran.Institutionid__c;
                                if(queueMap.get(qcsMap.get(temp)) != null){
                                    cs.OwnerId = queueMap.get(qcsMap.get(temp));
                                }
                            }
                            caseListToCreate.add(cs);
                        }
                    } 
                }
            }
            System.debug(' ---caseListToCreate-->>>> '+caseListToCreate.size()+'--list--'+caseListToCreate);
            // Below code to insert new Case records if list size is not null.
            if(caseListToCreate.size() >0 && cardMap.size() > 0){
                insert caseListToCreate;
                System.debug(' ---caseListToCreate-->>>> '+caseListToCreate);
            }
            //Below Code is to link Case on DCMS_Transaction object  
            if(caseListToCreate.size() > 0 && cardMap.size() > 0){
                newCasesList =[Select Id,Description,Card_SerNo__c,Dispute_Type__c,Institutionid__c,
                               Gross_Amount__c,Card_Number_Masked__c,Pci_Dss_Token__c,CreatedDate,Contact.SerNo__c,Contact.Institution_Id__c,
                               (Select Id,Institutionid__c,BillingAmount__c from DCMS_Transactios__r),
                               (Select Id,TotalFraudAmt__c,TotalAtRisk__c, NumberOfTransactions__c from DCMS_FraudCases__r) 
                               FROM Case WHERE Id IN:caseListToCreate];
                for(DCMS_Transaction__c tran : transactionsList){
                    for(Case cas : newCasesList){                        
                        String cardSerNo = String.valueOf(tran.Txn_CardSerno__c);
                        //  System.debug(' ---cardSerNo-->>>> '+cardSerNo);
                        System.debug(' ---case serNo -->>>> '+cas.Card_SerNo__c);
                        if(tran.DisputeType__c == cas.Dispute_Type__c && cardSerNo == cas.Card_SerNo__c){
                            tran.CaseNumber__c = cas.Id;
                            System.debug(' ---tran.CaseNumber-->>>> '+tran.CaseNumber__c);
                            updateTrans.add(tran);
                        }
                    }
                }
            }
            system.debug('-----updateTrans---*'+updateTrans);
            system.debug('--updateTrans.size--'+updateTrans.size());
            // Update Transaction Records if Transaction records are there in list. 
            if(updateTrans.size() > 0){
                update updateTrans;
            }
            List<DCMS_Transaction__c> transactionCountList = [SELECT Id FROM DCMS_Transaction__c WHERE CaseNumber__c IN : caseListToCreate]; 
            system.debug('---trans--size---'+transactionCountList.size());
           /* if(caseDispMap.get(keyval).Id != null || caseId != null){
                DCMS_FraudCase__c fraudCaseObj = new DCMS_FraudCase__c();
                fraudCaseObj.Id = fCaseList[0].Id;
                fraudCaseObj.NumberOfTransactions__c =transactionCountList.size();
                if(fraudCaseObj != null){
                    update fraudCaseObj;
                }
                /*if(fraudCaseObj != null){
                    fraudCaseObj.NumberOfTransactions__c = transactionCountList.size();
                    fCaseList.add(fraudCaseObj);
                }
            }*/
            for(DCMS_Transaction__c tran : transactionsList){
                for(Case cas : newCasesList){
                    if(cas.DCMS_FraudCases__r.size() > 0){
                        system.debug('-----fraud-case---'+cas.DCMS_FraudCases__r.size());
                        cas.DCMS_FraudCases__r[0].TotalAtRisk__c = tran.BillingAmount__c;
                        cas.DCMS_FraudCases__r[0].NumberOfTransactions__c = transactionCountList.size();
                        system.debug('-----number of---'+cas.DCMS_FraudCases__r[0].NumberOfTransactions__c);
                        fCaseList.add(cas.DCMS_FraudCases__r[0]);
                    }
                }
            }
            system.debug('---fCaseList-'+fCaseList);
            if(fCaseList.size() > 0){
                update fCaseList;
            }
        }catch(Exception exp){
            StatusLogHelper.logSalesforceError('disputeTypeCaseHandler', 'E004', 'Error in createDisputeCases method ' , exp, false);
        }
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     05-Feb-2019
*   User Story : SFD-984
*   Param: None
*   Return: None    
*   Description:  In this method we are trying to update the gross amount in DCMS Transaction record.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public void updateGrossAmount(List<DCMS_Transaction__c> delTransList){
        try{
            Set<Id> caseIdSet = new Set<Id>();
            //List<Id> caseIdSet = new List<Id>();
            for(DCMS_Transaction__c trans : delTransList){
                caseIdSet.add(trans.CaseNumber__c);
            } 
            system.debug('---caseIdSet--'+caseIdSet);
            Map<id,Case> caseMap = new Map<id,Case>([Select id, Gross_Amount__c,(Select id,Institutionid__c,BillingAmount__c from DCMS_Transactios__r),(Select id,TotalFraudAmt__c,TotalAtRisk__c from DCMS_FraudCases__r) from Case where Id IN: caseIdSet]);
            system.debug('--------caseMap-----'+caseMap);
            Set<Case> casesToUpdate = new Set<Case>();
            Set<DCMS_FraudCase__c> fcasesToUpdate = new Set<DCMS_FraudCase__c>();
            List<Case> casesToUpdate1 = new List<Case>();
            List<DCMS_FraudCase__c> fcasesToUpdate1 = new List<DCMS_FraudCase__c>();
            for(DCMS_Transaction__c trans1 : delTransList){
                if(caseMap.get(trans1.CaseNumber__c) !=null){
                    caseMap.get(trans1.CaseNumber__c).Gross_Amount__c = caseMap.get(trans1.CaseNumber__c).Gross_Amount__c-trans1.BillingAmount__c;
                    casesToUpdate1.add(caseMap.get(trans1.CaseNumber__c));
                    System.debug('fCase Size '+caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r.size());
                    if(caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r.size() > 0){
                        caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0].TotalAtRisk__c = caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0].TotalAtRisk__c - trans1.BillingAmount__c;
                        caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0].TotalFraudAmt__c = caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0].TotalFraudAmt__c - trans1.BillingAmount__c;
                        caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0].NumberOfTransactions__c = caseMap.get(trans1.CaseNumber__c).DCMS_Transactios__r.size();
                        fcasesToUpdate1.add(caseMap.get(trans1.CaseNumber__c).DCMS_FraudCases__r[0]);
                    }
                }
            }
            
            casesToUpdate.addAll(casesToUpdate1);
            casesToUpdate1.clear();
            casesToUpdate1.addAll(casesToUpdate);
            if(casesToUpdate1.size() > 0){
                update casesToUpdate1;
            }
            fcasesToUpdate.addAll(fcasesToUpdate1);
            fcasesToUpdate1.clear();
            fcasesToUpdate1.addAll(fcasesToUpdate);
            if(fcasesToUpdate.size() > 0){
                update fcasesToUpdate1;
            }
        }catch(Exception exp){
            StatusLogHelper.logSalesforceError('disputeTypeCaseHandler', 'E004', 'Error in updateGrossAmount method ' , exp, false);
        }
    }    
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     12-Jan-2021
*   User Story : SFD-1903
*   Param: None
*   Return: None    
*   Description:  In this method we are trying to update transaction date in dcms transaction record before the transaction creation.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    Public void updateTransactionDate(List<DCMS_Transaction__c> transactionList){
        for(DCMS_Transaction__c tranObj : transactionList){
            tranObj.Transaction_Date__c = String.valueOf(tranObj.AuthDateTime__c);
        }
    }     
}