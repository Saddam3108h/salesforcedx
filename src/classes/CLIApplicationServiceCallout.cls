public class CLIApplicationServiceCallout extends SoapIO{
    
    SOACLI.submitCLIApplicationRequestBodyType  submitCLIApplicationRequest;
    CLIApplicationServiceInput input;
    
    public  void convertInputToRequest(){
        
        input = (CLIApplicationServiceInput)serviceInput;
        submitCLIApplicationRequest = new SOACLI.submitCLIApplicationRequestBodyType();
        submitCLIApplicationRequest.InitialCLIApplicationData=new SOACLI.initialCLIApplicationDataType();
        
        //Initial CLI
        submitCLIApplicationRequest.InitialCLIApplicationData.Product=((input.Product!=null && input.Product=='0')? null:input.Product);
        submitCLIApplicationRequest.InitialCLIApplicationData.SourceCode=((input.SourceCode!=null && input.SourceCode=='0')?null:input.SourceCode);
        submitCLIApplicationRequest.InitialCLIApplicationData.CampaignCode=((input.CampaignCode!=null && input.CampaignCode=='0')?null:input.CampaignCode);
        submitCLIApplicationRequest.InitialCLIApplicationData.UniqueCode=((input.UniqueCode!=null && input.UniqueCode=='0')?null:input.UniqueCode);
        submitCLIApplicationRequest.InitialCLIApplicationData.CryptId=((input.CryptId!=null && input.CryptId=='0')?null:input.CryptId);
        submitCLIApplicationRequest.InitialCLIApplicationData.AccountNumber=((input.AccountNumber!=null && input.AccountNumber=='0')?null:input.AccountNumber);
        submitCLIApplicationRequest.InitialCLIApplicationData.AccountSerno=((input.AccountSerno!=null && input.AccountSerno=='0')?null:input.AccountSerno);
        submitCLIApplicationRequest.InitialCLIApplicationData.CardNumber=((input.CardNumber!=null && input.CardNumber=='0')?null:input.CardNumber);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicationID=((input.ApplicationID!=null && input.ApplicationID=='0')?null:input.ApplicationID);
        submitCLIApplicationRequest.InitialCLIApplicationData.BankName=((input.BankName!=null && input.BankName=='0')?null:input.BankName);
        submitCLIApplicationRequest.InitialCLIApplicationData.BankID=((input.BankID!=null && input.BankID=='0')?null:input.BankID);
        submitCLIApplicationRequest.InitialCLIApplicationData.DesiredCreditLimit=((input.DesiredCreditLimit!=null && input.DesiredCreditLimit==0)?null:input.DesiredCreditLimit);
        submitCLIApplicationRequest.InitialCLIApplicationData.UTMSource=((input.UTMSource!=null && input.UTMSource=='0')?null:input.UTMSource);
        //submitCLIApplicationRequest.InitialCLIApplicationData.BankID=input.BankID;
        submitCLIApplicationRequest.InitialCLIApplicationData.UTMMedium=((input.UTMMedium!=null && input.UTMMedium=='0')?null:input.UTMMedium);
        submitCLIApplicationRequest.InitialCLIApplicationData.UTMCampaign=((input.UTMCampaign!=null && input.UTMCampaign=='0')?null:input.UTMCampaign);
        submitCLIApplicationRequest.InitialCLIApplicationData.UTMTerm=((input.UTMTerm!=null && input.UTMTerm=='0')?null:input.UTMTerm);
        submitCLIApplicationRequest.InitialCLIApplicationData.UTMContent=((input.UTMContent!=null && input.UTMContent=='0')?null:input.UTMContent);
        submitCLIApplicationRequest.InitialCLIApplicationData.UTMGclid=((input.UTMGclid!=null && input.UTMGclid=='0')?null:input.UTMGclid);
        submitCLIApplicationRequest.InitialCLIApplicationData.UTMPrisfakta=((input.UTMPrisfakta!=null && input.UTMPrisfakta=='0')?null:input.UTMPrisfakta);
        
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails=new SOACLI.applicantCLIDataType();
        
        //Applicant CLI
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.SSN=((input.ApplicantCLIDetails.SSN!=null && input.ApplicantCLIDetails.SSN=='0')?null:input.ApplicantCLIDetails.SSN);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.EmploymentType=((input.ApplicantCLIDetails.EmploymentType!=null && input.ApplicantCLIDetails.EmploymentType=='0')?null:input.ApplicantCLIDetails.EmploymentType);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.GrossYearlyIncome=((input.ApplicantCLIDetails.GrossYearlyIncome!=null && input.ApplicantCLIDetails.GrossYearlyIncome==0)?null:input.ApplicantCLIDetails.GrossYearlyIncome);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.GrossMonthlyIncome=((input.ApplicantCLIDetails.GrossMonthlyIncome!=null && input.ApplicantCLIDetails.GrossMonthlyIncome==0)?null: input.ApplicantCLIDetails.GrossMonthlyIncome);
        
        //SFD-1196 -Start
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.CreditCardDebt = input.ApplicantCLIDetails.CreditCardDebt;
        // submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.MortgageDebt= input.ApplicantCLIDetails.MortgageDebt;
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.ConsumerOtherLoans = input.ApplicantCLIDetails.ConsumerOtherLoans;
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.TypeOfHousing=((input.ApplicantCLIDetails.TypeOfHousing!=null && input.ApplicantCLIDetails.TypeOfHousing=='0')?null:input.ApplicantCLIDetails.TypeOfHousing);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.MaritalStatus=((input.ApplicantCLIDetails.MaritalStatus!=null && input.ApplicantCLIDetails.MaritalStatus=='0')?null:input.ApplicantCLIDetails.MaritalStatus); 
        //sfd 1252 
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.SharingHouseholdPercentage= input.ApplicantCLIDetails.SharingHouseholdPercentage;
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.HaveCar=((input.ApplicantCLIDetails.HaveCar!=null && input.ApplicantCLIDetails.HaveCar=='0')?null:input.ApplicantCLIDetails.HaveCar);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.Rent= input.ApplicantCLIDetails.Rent;
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.OtherIncome= input.ApplicantCLIDetails.OtherIncome;
        
        
        If(input.ApplicantCLIDetails.NetMonthlyIncome!=null && input.ApplicantCLIDetails.NetMonthlyIncome!=0)
            submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.NetMonthlyIncome= input.ApplicantCLIDetails.NetMonthlyIncome;
        
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.Employer=((input.ApplicantCLIDetails.Employer!=null && input.ApplicantCLIDetails.Employer=='0')?null:input.ApplicantCLIDetails.Employer);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.EmployedSince=((input.ApplicantCLIDetails.EmployedSince!=null && input.ApplicantCLIDetails.EmployedSince=='0')?null:input.ApplicantCLIDetails.EmployedSince);
        //submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.MortgageDebt=((input.ApplicantCLIDetails.MortgageDebt!=null && input.ApplicantCLIDetails.MortgageDebt==0)?null:input.ApplicantCLIDetails.MortgageDebt);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.MonthlyCostOfHousing=((input.ApplicantCLIDetails.MonthlyCostOfHousing!=null && input.ApplicantCLIDetails.MonthlyCostOfHousing==0)?null:input.ApplicantCLIDetails.MonthlyCostOfHousing);
        //submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.NumberOfChildrenUnder18=((input.ApplicantCLIDetails.NumberOfChildrenUnder18!=null && input.ApplicantCLIDetails.NumberOfChildrenUnder18==0)?null:input.ApplicantCLIDetails.NumberOfChildrenUnder18);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.NumberOfChildrenUnder18= input.ApplicantCLIDetails.NumberOfChildrenUnder18;
        
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.NumberOfAdults=((input.ApplicantCLIDetails.NumberOfAdults!=null && input.ApplicantCLIDetails.NumberOfAdults==0)?null:input.ApplicantCLIDetails.NumberOfAdults);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.OtherLoans=((input.ApplicantCLIDetails.OtherLoans!=null && input.ApplicantCLIDetails.OtherLoans==0)?null:input.ApplicantCLIDetails.OtherLoans);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.Age=((input.ApplicantCLIDetails.Age!=null && input.ApplicantCLIDetails.Age==0)?null:input.ApplicantCLIDetails.Age);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.DateOfBirth=input.ApplicantCLIDetails.DateOfBirth;
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.HousingSocieties=((input.ApplicantCLIDetails.HousingSocieties!=null && input.ApplicantCLIDetails.HousingSocieties=='0')?null:input.ApplicantCLIDetails.HousingSocieties);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.DesiredDueDate=((input.ApplicantCLIDetails.DesiredDueDate!=null && input.ApplicantCLIDetails.DesiredDueDate=='0')?null:input.ApplicantCLIDetails.DesiredDueDate);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.MarketingConsent=((input.ApplicantCLIDetails.MarketingConsent!=null && input.ApplicantCLIDetails.MarketingConsent=='0')?null:input.ApplicantCLIDetails.MarketingConsent);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.MembershipNumber=((input.ApplicantCLIDetails.MembershipNumber!=null && input.ApplicantCLIDetails.MembershipNumber==0)?null:input.ApplicantCLIDetails.MembershipNumber);
        
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.TotalDebt=((input.ApplicantCLIDetails.TotalDebt!=null && input.ApplicantCLIDetails.TotalDebt==0)?null:input.ApplicantCLIDetails.TotalDebt);
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.HousingType=((input.ApplicantCLIDetails.HousingType!=null && input.ApplicantCLIDetails.HousingType=='0')?null:input.ApplicantCLIDetails.HousingType);
        
        system.debug('--input.ApplicantCLIDetails--'+input.ApplicantCLIDetails.DebtRegistryDataDetails);
        
        submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.DebtRegistryDataDetails = new List<SOACLI.debtRegistryDataDetailsType>();
        for(integer i=0;i<input.ApplicantCLIDetails.DebtRegistryDataDetails.size();i++){
            system.debug('*************82*****'+input.ApplicantCLIDetails.DebtRegistryDataDetails[i].LoanType);
            SOACLI.debtRegistryDataDetailsType debtregistry = new SOACLI.debtRegistryDataDetailsType();
            
            debtregistry.LoanType = input.ApplicantCLIDetails.DebtRegistryDataDetails[i].LoanType;
            debtregistry.LoanAmountCreditCardLimit = input.ApplicantCLIDetails.DebtRegistryDataDetails[i].LoanAmountCreditCardLimit;
            debtregistry.NominalInterestRate = input.ApplicantCLIDetails.DebtRegistryDataDetails[i].NominalInterestRate;
            debtregistry.RemainingLoanTerm = input.ApplicantCLIDetails.DebtRegistryDataDetails[i].RemainingLoanTerm;
            submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.DebtRegistryDataDetails.add(debtregistry);       
        }
        system.debug('--ApplicantCLIDetails.DebtRegistryDataDetails---'+submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.DebtRegistryDataDetails);
    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        
        SOACLI.headerType HeadInstance=new SOACLI.headerType();
        HeadInstance.MsgId='FinalCAll';
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        // As suggested by pankaj
        HeadInstance.SystemId=input.Channel;//'Salesforce';
        //HeadInstance.UserIP='10.123.134.56';
        HeadInstance.InstitutionId=string.valueof(input.InstitutionId);
        
        SOACLI.CardApplicationServiceBindingPort invokeInstance= new SOACLI.CardApplicationServiceBindingPort ();
        invokeInstance.timeout_x=120000;
        // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('CLIApplication');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
            invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
        
        //new 
        String username = serviceObj.Username__c;//'credit_appn_webapp_access';
        String password = serviceObj.Password__c;//'c@w@1234';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        // Added on 03-29-2017
        SOACLISecurityHeader.UsernameToken creds=new SOACLISecurityHeader.UsernameToken();
        creds.Username=serviceObj.Username__c;//'css_soa';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';
        SOACLISecurityHeader.Security_element security_ele=new SOACLISecurityHeader.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        // Added on 03-29-2017
        
        SOACLI.submitCLIApplicationResponseType  submitCLIApplicationResponse ;
        submitCLIApplicationResponse =  invokeInstance.submitCLIApplication(HeadInstance,submitCLIApplicationRequest);
        system.debug('submitCLIApplicationResponse -->'+submitCLIApplicationResponse);
        system.debug(submitCLIApplicationRequest.InitialCLIApplicationData.ApplicantCLIDetails.MortgageDebt+'submitCLIApplicationRequest -->'+submitCLIApplicationRequest);
        return submitCLIApplicationResponse;
    }  
    
    public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
        if(response==null)
            return null;
        
        SOACLI.submitCLIApplicationResponseType  submitCLIApplicationResponse = (SOACLI.submitCLIApplicationResponseType)response;
        SOACLI.submitCLIApplicationResponseBodyType  submitCLIApplicationResponseBody =submitCLIApplicationResponse.SubmitCLIApplicationResponseBody;
        
        system.debug('***submitCLIApplicationResponseBody***'+submitCLIApplicationResponseBody);
        
        CLIApplicationServiceOutput cLIApplicationServiceOutput = new CLIApplicationServiceOutput();
        
        if(submitCLIApplicationResponseBody!=null)
        {
            cLIApplicationServiceOutput.ApplicationStatus=submitCLIApplicationResponseBody.ApplicationStatus;
        }
        system.debug('***cLIApplicationServiceOutput***'+cLIApplicationServiceOutput);
        
        return cLIApplicationServiceOutput;
    }        
}