@isTest
public class TestCustomerDataController
{
    public static Product_Custom__c objProd;
    public static Account objAcc;
    public static Financial_Account__c objFA;
    public static Contact objCon;
    public static Card__c objCard;
    public static ServiceHeaders__c objServHeader;
    @testSetup
    public static void testInsertData() 
    {
        
        System.runAs(UnitTestDataGenerator.adminUser){ 
            
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
        //Product Insert
        objProd = new Product_Custom__c();
        objProd.Name = 'Test Product';
        objProd.Product_Description__c = 'Test Desc';
        objProd.Pserno__c = '85';
        Insert objProd;
        //Account Insert
        objAcc = new Account();
        objAcc.Customer_Serno__c = '123654';
        objAcc.Name = 'Account for Test Class';
        Insert objAcc;
        //Contact Insert
        objCon = new Contact();
        objCon.FirstName='TestContact1';
        objCon.Lastname = 'Test';
        objCon.SerNo__c = '12589';
        objCon.SSN__c = '7895874';
        objCon.Institution_Id__c = 2;
        objCon.Email='xyz@abc.com';
        objCon.MobilePhone='9876543210';
        objCon.Phone='123123123'; 
        objCon.Fax='98789879';
        objCon.MobilePhone='98789879'; 
        objCon.HomePhone='123123123';
        objCon.Email='testemail@test.com';
        Insert objCon;
        //FA Insert
        objFA = new Financial_Account__c();
        objFA.Account_Number__c = '4141144';
        objFA.Institution_Id__c = 2;
        objFA.Account_Serno__c = '11166';
        objFA.Customer__c = objAcc.id;
        objFA.Customer_Serno__c = objAcc.Customer_Serno__c;
        objFA.Product__c = objProd.id;
        objFA.Product_Serno__c = objProd.Pserno__c;
        Insert objFA;
        //Insert Card
        objCard = new Card__c();
        objCard.Card_Number_Truncated__c = '5588********9966';
        objCard.Card_Serno__c = '8855225588';
        objCard.Financial_Account__c = objFA.id;
        objCard.Financial_Account_Serno__c = objFA.Account_Serno__c;
        objCard.People__c = objCon.id;
        objCard.People_Serno__c = objCon.SerNo__c;
        objCard.Product__c = objProd.id;
        objCard.Prod_Serno__c = objProd.Pserno__c;
        objCard.PrimaryCardFlag__c=true;
        objCard.Institution_Id__c = 2;
        Insert objCard;
        }
    }
    public static testmethod void testCustomerDetails() 
    {
        
        //Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        Financial_Account__c objFAQuery = [Select Id from Financial_Account__c limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(objFAQuery);
        CustomerDataController objController = new CustomerDataController(sc);
        objController.getcontacts();
        objController.getCustDataConInstance();
        

    }
    public static testmethod void testInstallDetails() 
    {
    	/*
        System.runAs(UnitTestDataGenerator.adminUser){ 
            
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
        Common_Settings__c objCmnValue1 = new Common_Settings__c();
        objCmnValue1.Common_Value__c = '1';
        objCmnValue1.Name = 'CorrelationId';
        Common_Settings__c objCmnValue2 = new Common_Settings__c();
        objCmnValue2.Common_Value__c = 'Salesforce';
        objCmnValue2.Name = 'RequestorId';
        Common_Settings__c objCmnValue3 = new Common_Settings__c();
        objCmnValue3.Common_Value__c = '1';
        objCmnValue3.Name = 'SystemId';
        Insert objCmnValue3;
        Insert objCmnValue2;
        Insert objCmnValue1;
        ServiceSettings__c objService = new ServiceSettings__c();
        objService.name = 'GetInstallment';
        objService.HeaderName__c = 'SOAPHTTP'; 
        objService.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objService.OutputClass__c = 'GetInstallInfoOutput';
        objService.ProcessingClass__c = 'GetInstallInfoCallout';
        objService.Strategy__c = 'SOAPService';
        objService.LogRequest__c = false;
        objService.LogResponse__c = false;
        objService.LogWithCallout__c = false;
        objService.Input_Class__c = 'GetInstallInfoInput';
        Insert objService;
        //Insert Service Header
        objServHeader = new ServiceHeaders__c();
        objServHeader.Name = 'SOAPHTTP';
        objServHeader.ContentType__c = 'application/xml';
        Insert objServHeader;
        
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.name = 'ServiceStrategies';
        objGlobal.Value__c = 'SOAPService,RESTService';
        Insert objGlobal;
        
        Test.setMock(WebServiceMock.class, new GetInstallInfoWebServiceMockclass());
        
        Financial_Account__c objFAQuery = [Select Id,Account_Serno__c  from Financial_Account__c limit 1];
        ApexPages.currentPage().getParameters().put('id', objFAQuery.id); //it is getParameters not setParameters
        ApexPages.StandardController sc = new ApexPages.StandardController(objFAQuery);
        CustomerDataController objController = new CustomerDataController(sc);
        Test.startTest();
            objController.getInstallInfo();
        Test.stopTest();
        */
    }
    public static testmethod void testInsuranceDetails() 
    {
    
        Common_Settings__c objCmnValue1 = new Common_Settings__c();
        objCmnValue1.Common_Value__c = '1';
        objCmnValue1.Name = 'CorrelationId';
        Common_Settings__c objCmnValue2 = new Common_Settings__c();
        objCmnValue2.Common_Value__c = 'Salesforce';
        objCmnValue2.Name = 'RequestorId';
        Common_Settings__c objCmnValue3 = new Common_Settings__c();
        objCmnValue3.Common_Value__c = '1';
        objCmnValue3.Name = 'SystemId';
        Insert objCmnValue3;
        Insert objCmnValue2;
        Insert objCmnValue1;
        ServiceSettings__c objService = new ServiceSettings__c();
        objService.name = 'ViewInsurance';
        objService.HeaderName__c = 'SOAPHTTP'; 
        objService.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objService.OutputClass__c = 'ViewInsuranceServiceOutput';
        objService.ProcessingClass__c = 'ViewInsuranceServiceCallout';
        objService.Strategy__c = 'SOAPService';
        objService.LogRequest__c = false;
        objService.LogResponse__c = false;
        objService.LogWithCallout__c = false;
        objService.Input_Class__c = 'ViewInsuranceServiceInput';
        Insert objService;
        //Insert Service Header
        objServHeader = new ServiceHeaders__c();
        objServHeader.Name = 'SOAPHTTP';
        objServHeader.ContentType__c = 'application/xml';
        Insert objServHeader;
        
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.name = 'ServiceStrategies';
        objGlobal.Value__c = 'SOAPService,RESTService';
        Insert objGlobal;
        
        Test.setMock(WebServiceMock.class, new ViewInsuranceWebServiceMockclass());
        
        Financial_Account__c objFAQuery = [Select Id,Account_Serno__c,Account_Number__c,Institution_Id__c  from Financial_Account__c limit 1];
        ApexPages.currentPage().getParameters().put('id', objFAQuery.id); //it is getParameters not setParameters
        ApexPages.StandardController sc = new ApexPages.StandardController(objFAQuery);
        CustomerDataController objController = new CustomerDataController(sc);
        Test.startTest();
            objController.getViewInsurance();
        Test.stopTest();
    }
    public static testmethod void testStatementDetails() 
    {
        
        
        Common_Settings__c objCmnValue1 = new Common_Settings__c();
        objCmnValue1.Common_Value__c = '1';
        objCmnValue1.Name = 'CorrelationId';
        Common_Settings__c objCmnValue2 = new Common_Settings__c();
        objCmnValue2.Common_Value__c = 'Salesforce';
        objCmnValue2.Name = 'RequestorId';
        Common_Settings__c objCmnValue3 = new Common_Settings__c();
        objCmnValue3.Common_Value__c = '1';
        objCmnValue3.Name = 'SystemId';
        Insert objCmnValue3;
        Insert objCmnValue2;
        Insert objCmnValue1;
        ServiceSettings__c objService = new ServiceSettings__c();
        objService.name = 'GetStatementHistory';
        objService.HeaderName__c = 'SOAPHTTP'; 
        objService.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objService.OutputClass__c = 'GetStatementHistoryOutput';
        objService.ProcessingClass__c = 'GetStatementCallout';
        objService.Strategy__c = 'SOAPService';
        objService.LogRequest__c = false;
        objService.LogResponse__c = false;
        objService.LogWithCallout__c = false;
        objService.Input_Class__c = 'GetStatementHistoryInput';
        Insert objService;
        //Insert Service Header
        objServHeader = new ServiceHeaders__c();
        objServHeader.Name = 'SOAPHTTP';
        objServHeader.ContentType__c = 'application/xml';
        Insert objServHeader;
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.name = 'ServiceStrategies';
        objGlobal.Value__c = 'SOAPService,RESTService';
        Insert objGlobal;
        
        Test.setMock(WebServiceMock.class, new GetStatementsHistoryWebServiceMockclass());
        
        Financial_Account__c objFAQuery = [Select Id,Account_Serno__c,Account_Number__c,Institution_Id__c  from Financial_Account__c limit 1];
        ApexPages.currentPage().getParameters().put('id', objFAQuery.id); //it is getParameters not setParameters
        ApexPages.StandardController sc = new ApexPages.StandardController(objFAQuery);
        CustomerDataController objController = new CustomerDataController(sc);
        Test.startTest();
            objController.getStatements();
        Test.stopTest();
    }
}