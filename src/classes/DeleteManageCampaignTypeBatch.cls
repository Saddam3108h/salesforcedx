/**********************************************************************
Name: DeleteManageCampaignTypeBatch
=======================================================================
Purpose: This batch apex is used for to delete Campaign data for specific type  mentioned in custom setting DeleteCampaignType__c SFD-1692

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author          Date              Detail
1.0         Priyanka Singh  28-Oct-2020       

**********************************************************************/
global class DeleteManageCampaignTypeBatch implements Database.Batchable<sObject>,Schedulable{ 
    
    global static String constructWhereClause(String baseQuery, Set<String> whereMap, String key){
        
        if(whereMap.size() > 0){       
            baseQuery += ' WHERE ';
            Integer i = 0;
            for(String value : whereMap){            
                baseQuery += key + ' = \'' + value + '\'';
                if(i < whereMap.size() -1){
                    baseQuery += ' OR ';
                } 
                ++i;
            }
        }
        return baseQuery;
    }
    global Database.QueryLocator start(Database.BatchableContext context)
    {
        
        String baseQuery = 'Select ID from Manage_Campaign__c';
        String mainQuery ='';        
        Set<String > lst = new Set<String>();
        Map<String, DeleteCampaignType__c> deleteCampgnTyp = DeleteCampaignType__c.getAll();
        if(!deleteCampgnTyp.isEmpty()){
            for(String cmtp :deleteCampgnTyp.keyset()){
                lst.add(cmtp);
            }
            mainQuery = constructWhereClause(baseQuery,lst,'Campaign_Type__c');
            
            return Database.getQueryLocator(mainQuery);
        }else {
            return null;
        } 
    }    
    global void execute(Database.BatchableContext context, List<Manage_Campaign__c> scope)
    {
        
        Database.DeleteResult[] drList = Database.delete(scope, false);
        
        // Iterate through each returned result
        for(Database.DeleteResult dr : drList) {
            if (dr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deleted account with ID: ' + dr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : dr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
    global void execute(SchedulableContext ctx) 
    {      
        DeleteManageCampaignTypeBatch d=new DeleteManageCampaignTypeBatch();
        Database.executeBatch(d, 200);
    }
    global void finish(Database.BatchableContext BC){}
}