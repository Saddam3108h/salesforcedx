/**********************************************************************
Name: OpenCaseControllerLwc
=======================================================================
Purpose: This class is used to display open cases alert at Customer record.  
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Kakasaaheb Ekshinge		03-Dec-2020		 Initial Version
**********************************************************************/
public class OpenCaseControllerLwc {
	
    @AuraEnabled(cacheable=true)  
    Public static String openCaseCount(String customerId){
        system.debug('-----customerId------'+customerId);
        List<Contact> conlist = [Select id,name,Open_Cases__c,(Select id,status from cases where status Not IN ('Closed','Cancelled')) from Contact where Id =: customerId];
          if(!conlist.isEmpty()){
            return conlist[0].cases.size()+'';
      } else
          return '0';      
     }
}