/**********************************************************************
Name:DeletePrimeActionLog_Test 
=======================================================================
Purpose: This is the test class for 'DeletePrimeActionLogData' Apex Class
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date            CreatedforJira  Detail
1.0         Kakasaheb Ekshinge       06-Aug-2020     SFD-1538        Initial Version
**********************************************************************/
@isTest
public class DeletePrimeActionLog_Test {
    static testmethod void deletePrimeActionLog(){
        
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Case objCase = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'Category__c' =>'Payment Free Holiday'
                    });
            
            Prime_Action_Log__c objPrimeLog1 = UnitTestDataGenerator.testPrimeAction.buildInsert(new Map<String, Object>{
                'Case__c' => objCase.id,
                    'Prime_Action_Type__c' => 'Payback Credit Balance',
                    'Response_Status__c' => 'Failure',
                    'SOA_service_Inbound_Message__c' => '{"ServiceType" : "mapa","ServiceIndicator" : "AccountServices ", "Reference" : "Account","PaybackAmount" : "90", "LogAction" : "manual","InstitutionID" : null,"ActionDate" : "2017-01-09","Action" : "Add", "AccountNumber" : "5438730000000614 " }'
                    });
            
            Prime_Action_Log__c objPrimeLog = UnitTestDataGenerator.testPrimeAction.buildInsert(new Map<String, Object>{
                'Case__c' => objCase.id,
                    //objPrimeLog.Financial_Account__c = finAccObj.id;
                    'Prime_Action_Type__c' => 'Close Account',
                    'Response_Status__c' => 'Failure',
                    'SOA_service_Inbound_Message__c' => '{"setPPIReq" : null,"replaceCardReq" : null,"PaymentHolidaysReq" : null,"MobileReq" : null,"InstitutionId" : 2,"InstalmentReq" : null,"EmailReq" : null,"CloseAccReq" : {"ActionDate" : "2016-11-24","AccountNo" : "5435600034776114"},"cardReissueReq" : null,"addExtraCardReq" : null}'
                    });
            
            Prime_Action_Log__c objPrimeLog2 = UnitTestDataGenerator.testPrimeAction.buildInsert(new Map<String, Object>{
                'Case__c' => objCase.id,
                    'Prime_Action_Type__c' => 'Credit Limit Increase',
                    'Response_Status__c' => 'Failure',
                    'SOA_service_Inbound_Message__c' => '{"setPPIReq" : null,"replaceCardReq" : null,"PaymentHolidaysReq" : null,"MobileReq" : null,"InstitutionId" : 2,"InstalmentReq" : null,"EmailReq" : null,"CloseAccReq" : {"ActionDate" : "2016-11-24","AccountNo" : "5435600034776114"},"cardReissueReq" : null,"addExtraCardReq" : null}'
                    });
            
            test.setCreatedDate(objPrimeLog.Id,date.today()-100);
            test.setCreatedDate(objPrimeLog1.Id,date.today()-95);
            test.setCreatedDate(objPrimeLog2.Id,date.today()-91);
            System.assertEquals(3, [Select Id from Prime_Action_Log__c where CreatedDate < N_DAYS_AGO:90].size());
            
            test.startTest();
            
            DeletePrimeActionLogData d=new DeletePrimeActionLogData();
            Database.executeBatch(d, 3);
            
            test.stopTest();
            
            System.assertEquals(0, [Select Id from Prime_Action_Log__c where CreatedDate < N_DAYS_AGO:90].size());
            
        } 
    }
    
    
    @isTest
    static void deletePrimeAction(){
        System.runAs(UnitTestDataGenerator.adminUser) {    
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Case objCase = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'Category__c' => 'Payment Free Holiday'
                    });
            
            //Prine Action Log
            Prime_Action_Log__c objPrimeLog1 = UnitTestDataGenerator.testPrimeAction.buildInsert(new Map<String, Object>{
                'Case__c' => objCase.id,
                    'Prime_Action_Type__c' => 'Payback Credit Balance',
                    'Response_Status__c' => 'Failure',
                    'SOA_service_Inbound_Message__c' => '{"ServiceType" : "mapa","ServiceIndicator" : "AccountServices ", "Reference" : "Account","PaybackAmount" : "90", "LogAction" : "manual","InstitutionID" : null,"ActionDate" : "2017-01-09","Action" : "Add", "AccountNumber" : "5438730000000614 " }'
                    });
            
            Prime_Action_Log__c objPrimeLog = UnitTestDataGenerator.testPrimeAction.buildInsert(new Map<String, Object>{
                'Case__c' => objCase.id,
                    //objPrimeLog.Financial_Account__c = finAccObj.id;
                    'Prime_Action_Type__c' => 'Close Account',
                    'Response_Status__c' => 'Failure',
                    'SOA_service_Inbound_Message__c' => '{"setPPIReq" : null,"replaceCardReq" : null,"PaymentHolidaysReq" : null,"MobileReq" : null,"InstitutionId" : 2,"InstalmentReq" : null,"EmailReq" : null,"CloseAccReq" : {"ActionDate" : "2016-11-24","AccountNo" : "5435600034776114"},"cardReissueReq" : null,"addExtraCardReq" : null}'
                    });
            
            Prime_Action_Log__c objPrimeLog2 = UnitTestDataGenerator.testPrimeAction.buildInsert(new Map<String, Object>{
                'Case__c' => objCase.id,
                    'Prime_Action_Type__c' => 'Credit Limit Increase',
                    'Response_Status__c' => 'Failure',
                    'SOA_service_Inbound_Message__c' => '{"setPPIReq" : null,"replaceCardReq" : null,"PaymentHolidaysReq" : null,"MobileReq" : null,"InstitutionId" : 2,"InstalmentReq" : null,"EmailReq" : null,"CloseAccReq" : {"ActionDate" : "2016-11-24","AccountNo" : "5435600034776114"},"cardReissueReq" : null,"addExtraCardReq" : null}'
                    });
            
            test.setCreatedDate(objPrimeLog.Id,date.today()-100);
            test.setCreatedDate(objPrimeLog1.Id,date.today()-90);
            test.setCreatedDate(objPrimeLog2.Id,date.today()-90);
            
            test.startTest();
            Delete objPrimeLog;
            Delete objPrimeLog1; 
            Delete objPrimeLog2;
            
            DeletePrimeActionLogData d=new DeletePrimeActionLogData();
            String sch = '0 0 * * * ?';
            System.schedule('test job', sch, d);
            System.assertEquals(0, [Select Id from Prime_Action_Log__c where CreatedDate < N_DAYS_AGO:90].size());
            test.stopTest();
        }
    }    
    @isTest
    static void deletePrimeActionHandleCatch(){
        System.runAs(UnitTestDataGenerator.adminUser) { 
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Case objCase = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'Category__c' => 'Payment Free Holiday'
                    });
            
            Prime_Action_Log__c objPrimeLog1 = UnitTestDataGenerator.testPrimeAction.buildInsert(new Map<String, Object>{
                'Case__c' => objCase.id,
                    'Prime_Action_Type__c' => 'Payback Credit Balance',
                    'Response_Status__c' => 'Failure',
                    'SOA_service_Inbound_Message__c' => '{"ServiceType" : "mapa","ServiceIndicator" : "AccountServices ", "Reference" : "Account","PaybackAmount" : "90", "LogAction" : "manual","InstitutionID" : null,"ActionDate" : "2017-01-09","Action" : "Add", "AccountNumber" : "5438730000000614 " }'
                    });
            
            test.setCreatedDate(objPrimeLog1.Id,date.today()-100);            
            test.startTest();
            Delete objPrimeLog1;            
            DeletePrimeActionLogData d = new DeletePrimeActionLogData();
            String sch = '0 0 * * * ?';
            System.schedule('test job', sch, d);
            System.assertEquals(0, [Select Id from Prime_Action_Log__c where CreatedDate < N_DAYS_AGO:90].size());
            test.stopTest();
        }
    } 
}