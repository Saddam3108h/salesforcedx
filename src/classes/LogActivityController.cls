/**********************************************************************
Name: LogActivityController
=======================================================================
Purpose: This class is used update the case with related Financial account, customer and card data in salesforce 
User Story -SFD-1584, SFD-1357

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         1-Sep-2020       Initial Version

**********************************************************************/
public with sharing class LogActivityController {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     1-Jul-2020
*   User Story : SFD-1584
*   Param: None
*   Return: Map    
*   Description: In this method we are trying to get the Financial Account record for customer.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    @AuraEnabled(cacheable=true)
    Public Static Map<String,String> getFinancialAccounts(Id customerId){
        System.debug('-----conId---'+customerId);
        Map<String, String> finOpionMap =  new Map<String, String>();
        finOpionMap.put(StaticConstant.NONE_VALUE,StaticConstant.NONE_VALUE);
        finOpionMap.put(StaticConstant.OTHER_VALUE,StaticConstant.OTHER_VALUE);
        Set<Id> financialAccIdSet = new Set<Id>();
        List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c 
                                  FROM Card__c 
                                  WHERE People__c =: customerId 
                                  AND Financial_Account__c!=null 
                                  ORDER BY CreatedDate DESC Limit 50000];
        for(Card__c cardObj:cardList)
        {
            financialAccIdSet.add(cardObj.Financial_Account__c);
        }
        
        List<Financial_Account__c> financialAccList = [SELECT Id, Name, Customer__c, Account_Number__c,Account_Serno__c,
                                                       Product__c,Product__r.name FROM Financial_Account__c 
                                                       WHERE Id IN:financialAccIdSet 
                                                       ORDER BY CreatedDate DESC Limit 50000];
        if(financialAccList.size() > 0){
            for(Financial_Account__c finObj : financialAccList){
                finOpionMap.put(finObj.Id,finObj.Account_Number__c+' : '+(finObj.Product__r.name!=null?finObj.Product__r.name:'N/A'));
            }
        } /*else {
//finOpionMap.put(StaticConstant.OTHER_VALUE,StaticConstant.OTHER_VALUE);
finOpionMap.put(StaticConstant.NONE_VALUE,StaticConstant.NONE_VALUE);
}  */    
        return finOpionMap;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     1-Jul-2020
*   User Story : SFD-1584
*   Param: None
*   Return: Map    
*   Description: In this method we are trying to get the card record related to Financial Account.

**********************************************************************/
    
    @AuraEnabled(cacheable=true)
    Public Static Map<String, String> getCards(String financialId, Id customerId){
        Map<String, String> cardMap = new Map<String, String>();
        List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c,
                                  Card_Number_Truncated__c,Product__r.name,GeneralStatus__c 
                                  FROM Card__c 
                                  WHERE People__c=:customerId 
                                  AND Financial_Account__c=:financialId 
                                  ORDER BY ExpiryDate__C DESC Limit 50000];       
        system.debug('---financialVal-*'+financialId);
        if(financialId == StaticConstant.OTHER_VALUE){
            cardMap.put(StaticConstant.OTHER_VALUE,StaticConstant.OTHER_VALUE); 
        } else if(financialId == StaticConstant.NONE_VALUE) {
            cardMap.put(StaticConstant.NONE_VALUE,StaticConstant.NONE_VALUE);
        }
        
        if(cardList.size() == 1){
            cardMap.put(cardList[0].Id,cardList[0].Card_Number_Truncated__c+' : '
                        +(cardList[0].GeneralStatus__c!=null?cardList[0].GeneralStatus__c:'N/A')
                       );
        } else {
            for(Card__c cardObj:cardList)
            {
                cardMap.put(cardObj.id , cardObj.Card_Number_Truncated__c+' : '
                            +(cardObj.GeneralStatus__c!=null?cardObj.GeneralStatus__c:'N/A'));
            }
        }        
        system.debug('---cardMap-'+cardMap);
        return cardMap;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     9-Jul-19
*   User Story : SFD-1254
*   Param: None
*   Return: boolean    
*   Description: In this method we are trying to update CTI task when change the customer name
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    @AuraEnabled
    Public Static Boolean updateTask(Id whoId, Id customerId) {
        Boolean flag = false;
        try {
            List<Task> taskList = [SELECT Id, WhoId FROM Task 
                                   WHERE (CreatedById = :UserInfo.getUserId() 
                                          AND CreatedDate = Today 
                                          AND WhoId =: customerId) 
                                   AND (Status =: StaticConstant.TASK_COMPLETE_STATUS 
                                        AND Comments__c = ''
                                        AND CallType != null) 
                                   ORDER BY CreatedDate DESC Limit 1];
            system.debug('-----task-----'+taskList.size());
            
            if (taskList.size() > 0) {
                taskList[0].WhoId = whoId;
                upsert taskList;
                flag = true;
            } else {
                flag = false;
            }
            system.debug('---after-update----'+taskList);
        } catch(Exception exp) {
            StatusLogHelper.logSalesforceError('LogACallController', 'E005', 'updateCustomerName', exp, false); // Class,method
        }
        return flag;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     1-Jul-2020
*   User Story : SFD-1584
*   Param: None
*   Return: Sting    
*   Description: In this method we are trying to update the case with customer and related financial account, card
also created the task.
**********************************************************************/
    
    @AuraEnabled
    public static string doCaseUpdate(String customerId, String financialId, 
                                      String cardId, String comment, String category,
                                      String typeVal, String caseId, String saveAttempt, 
                                      String saved, String reason, String offer, 
                                      String dueDate, String target, String alertCategory,
                                      Boolean alertFlag,String country, String crossSellSucess, 
                                      String serviceSold,String federationId) 
    {
        system.debug('---alertFlag--'+alertFlag);
        String message = '';
        try {
            List<Task> taskList = [SELECT Id, Description, WhatId, WhoId,Comments__c, 
                                   CallType, Call_Type__c, Account_Serno__c,Card_Serno__c,
                                   Category__c,Customer_Serno__c,MAH_Serno__c,Type 
                                   FROM Task 
                                   WHERE (CreatedById = :UserInfo.getUserId() 
                                          AND CreatedDate = Today 
                                          AND WhoId =: customerId) 
                                   AND (Status =: StaticConstant.TASK_COMPLETE_STATUS 
                                        AND Comments__c = '' 
                                        AND CallType != null) 
                                   ORDER BY 
                                   CreatedDate DESC Limit 1];
            system.debug('----task---'+taskList.size());
            List<Card__c> cardList = [SELECT Id, Card_Serno__c,People_Serno__c,
                                      Financial_Account_Serno__c,GeneralStatus__c, 
                                      People__r.Account.Customer_Serno__c, 
                                      People__r.SSN__c, People__r.Institution_Id__c 
                                      FROM Card__c 
                                      WHERE People__c =: customerId 
                                      AND Id =: cardId]; 
            List<Contact> contactList = [SELECT id,Institution_Id__c,SSN__c, Serno__C
                                         FROM Contact 
                                         WHERE Id=:customerId];
            if((cardList.size() > 0 
                || cardId == StaticConstant.OTHER_VALUE)
               && taskList.size() > 0 
               && String.isNotBlank(category)
              ) {
                  taskList[0].Description = comment;
                  taskList[0].Comments__c = (comment != null 
                                             && comment.length() > 255 ?
                                             comment.substring(0,254) : comment
                                            );
                  taskList[0].Subject = StaticConstant.TASK_CALL_SUBJECT; 
                  taskList[0].Type = typeVal;
                  taskList[0].Call_Type__c = (taskList[0].CallType != null ?
                                              taskList[0].CallType : 'N/A'
                                             );   
                  taskList[0].Category__c = category;
                  if(contactList.size()>0){
                      taskList[0].Customer_Serno__c = contactList[0].Serno__c;
                  }
                  if (cardId != StaticConstant.OTHER_VALUE) {
                      taskList[0].Account_Serno__c = cardList[0].Financial_Account_Serno__c;                      
                      taskList[0].Card_Serno__c = cardList[0].Card_Serno__c;
                      taskList[0].MAH_Serno__c = cardList[0].People__r.Account.Customer_Serno__c;
                  }
                  if (taskList[0].CallType == StaticConstant.TASK_INBOUND_CALLTYPE 
                      || taskList[0].CallType == StaticConstant.TASK_OUTBOUND_CALLTYPE
                     )
                  {
                      upsert taskList;
                      message = System.Label.Activity_update_msg;
                  }
              } else if(category != null 
                        && String.isNotBlank(comment)
                       ) {
                           Task taskObj = new Task();
                           taskObj.Subject = (caseId.startsWith('570') ? StaticConstant.TASK_CHAT_SUBJECT : StaticConstant.TASK_MEMO_SUBJECT);
                           taskObj.Category__c = category;
                           taskObj.Type = typeVal;
                           taskObj.Status = StaticConstant.TASK_COMPLETE_STATUS;
                           taskObj.Description = comment;
                           system.debug('--caseId-'+caseId);
                           if(caseId.startsWith('500')){
                               taskObj.WhatId = caseId;  
                           }             
                           taskObj.Comments__c = (comment != null 
                                                  && comment.length() > 255 ?
                                                  comment.substring(0,254) : comment);
                           
                           taskObj.WhoId = customerId;
                           if(contactList.size()>0){
                               taskObj.Customer_Serno__c = contactList[0].Serno__c;
                           }                           
                           if(financialId != StaticConstant.OTHER_VALUE){	
                               taskObj.Account_Serno__c = cardList[0].Financial_Account_Serno__c;                               
                               taskObj.Card_Serno__c = cardList[0].Card_Serno__c;
                               taskObj.MAH_Serno__c = cardList[0].People__r.Account.Customer_Serno__c;
                           }
                           
                           insert taskObj;           
                           system.debug('----taskId---'+taskObj.Id);
                           message = StaticConstant.TASK_CREATED_MESSAGE;
                       }
            If(category == StaticConstant.CATEGORY_ACCOUNT_CLOSURE 
               && (String.isNotBlank(saveAttempt) 
                   || String.isNotBlank(saved) 
                   || String.isNotBlank(reason) 
                   || String.isNotBlank(offer))
              ) {
                  Retention_Response__c retentionObj = new Retention_Response__c();
                  retentionObj.Financial_Account__c = financialId;
                  retentionObj.Person__c = customerId;
                  retentionObj.Save_Attempt__c = saveAttempt;
                  retentionObj.Account_Retained__c = saved;
                  retentionObj.Account_Closed__c = (saved == StaticConstant.YES_VALUE ? 
                                                    StaticConstant.NO_VALUE
                                                    :StaticConstant.YES_VALUE
                                                   );
                  retentionObj.Reason_for_Account_Closure__c = reason;
                  retentionObj.Offer__c = offer;
                  retentionObj.Agent_Id__c = federationId;
                  insert retentionObj;
              }
            else if(alertFlag == true){
                Alert__c alertObj = new Alert__c();
                alertObj.People__c = customerId;
                alertObj.Category__c = alertCategory;
                if(String.isNotBlank(dueDate)){
                    alertObj.DueDate__c = Date.valueOf(dueDate); 
                }
                alertObj.Target__c = target;
                alertObj.Comments__c = (comment != null 
                                        && comment.length() > 255 ?
                                        comment.substring(0,254) : comment);
                insert alertObj;
            }
            else if(category == StaticConstant.CATEGORY_CROSSSELL_RESPONSE){
                Cross_Sell_Response__c crossSellObj = new Cross_Sell_Response__c();
                crossSellObj.Person__c = customerId;
                crossSellObj.Financial_Account__c = financialId;
                crossSellObj.Cross_Sell_Success__c = crossSellSucess;
                crossSellObj.Country__c = country;
                crossSellObj.Service_Sold__c = serviceSold;
                crossSellObj.Comments__c = (comment != null 
                                            && comment.length() > 255 ?
                                            comment.substring(0,254) : comment);
                crossSellObj.Agent_Id__c = federationId;
                insert crossSellObj;                    
            }
            system.debug('--customerId---'+customerId);
            // Below method is used to make the callout to Prime to create memo.                        
            system.debug('-conlst--'+contactList);
            if(contactList!= null 
               && contactList.size()>0 
               && contactList[0].Institution_Id__c!=null
              ) {
                  CustomerMemoExportDataOutput outputmemoexportdata = 
                      CSSServiceHelper.updateCustomerMemo('NewMemo','PersonSSN',
                                                          contactList[0].SSN__c,'css',comment,
                                                          String.valueOf(
                                                              contactList[0].Institution_Id__c
                                                          )
                                                         );
                  system.debug('--outputmemoexportdata---'+outputmemoexportdata);
              }       
        }catch(Exception exp) {
            StatusLogHelper.logSalesforceError('LogActivityController', 'E005', 
                                               'doCaseUpdate', exp, false); // Class,method
        }
        return message;        
    }
     /**********************************************************************
*   Author: Saddam Hussain
*   Date:     29-Jul-2021
*   User Story : SFD-1902
*   Param: None
*   Return: None    
*   Description: In this method we are trying to fetch the Department wise category custom metadata.

**********************************************************************/
    
    @AuraEnabled(cacheable=true)
    Public static List<Department_wise_Category__mdt> fetchCategories(String departmentVal) {
        system.debug('---categoryOptions--'+departmentVal);
        List<Department_wise_Category__mdt> departmentList = [SELECT Id,Category__c,Department__c 
                                                      FROM Department_wise_Category__mdt
                                                      WHERE Department__c =: departmentVal];
        departmentList.sort();
        return departmentList;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     9-Jul-19
*   User Story : SFD-1254
*   Param: None
*   Return: None    
*   Description: In this method we are trying to search the customer by SSN and update the created inbound task
with the found customer.

**********************************************************************/
    @AuraEnabled
    Public static Id redirctcustomer(String sSn) {
        List<Task> taskList = [SELECT Id, WhoId, Comments__c, 
                               Category__c, Type 
                               FROM Task 
                               WHERE CreatedById = :UserInfo.getUserId() 
                               AND CreatedDate = Today 
                               AND Comments__c = '' 
                               ORDER BY CreatedDate DESC Limit 1];
        if (ssn != null) {
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(ssn));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            List<Contact> contactList = [SELECT Id,Name FROM Contact WHERE Encrypted_SSN__c = :encryptSSN];
            
            if (contactList.size() > 0 && taskList.size() > 0) {
                taskList[0].WhoId = contactList[0].id;
                update taskList;
                return taskList[0].WhoId;
            } else if(contactList.size() > 0){
                return contactList[0].id;
            }            
        }
        return null;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     9-Jul-19
*   User Story : SFD-1254
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update the created inbound task
with the found customer in the above method.

**********************************************************************/
    @AuraEnabled
    Public Static String updateActivity(String categoryVal, String typeVal, String commentVal) {
         try {
             List<Task> taskList = [SELECT Id, WhoId, Comments__c, 
                                   Category__c, Type, Subject 
                                   FROM Task 
                                   WHERE CreatedById = :UserInfo.getUserId() 
                                   AND CreatedDate = Today 
                                   AND Comments__c = '' 
                                   AND WhoId = ''
                                   ORDER BY CreatedDate DESC Limit 1];
             if(taskList.size() > 0){
                   taskList[0].Category__c = categoryVal;
                   taskList[0].Comments__c = commentVal;
                   taskList[0].Comments__c = (commentVal != null 
                                              && commentVal.length()>255 ?
                                              commentVal.substring(0,254) : commentVal);
                   taskList[0].Type = typeVal;
                   taskList[0].Subject = StaticConstant.TASK_CALL_SUBJECT;
                   update taskList;
                   return System.Label.Activity_update_msg;
             } else {
                 Task tskObj = new Task();
                 tskObj.Category__c = categoryVal;
                 tskObj.Comments__c = commentVal;
                 tskObj.Comments__c = (commentVal != null 
                                             && commentVal.length()>255 ?
                                             commentVal.substring(0,254) : commentVal);
                 tskObj.Type = typeVal;
                 tskObj.Subject = StaticConstant.TASK_CALL_SUBJECT;
                 tskObj.status = 'Completed';
                 insert tskObj;
                 return System.Label.Activity_Insert_msg;
             }
         }catch(Exception exp){
            StatusLogHelper.logSalesforceError('LogActivityController', 
                                               'E005', 'updateActivity', exp, false);
        }
        return null;
    }

    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     18-Dec-2020
*   User Story : SFD-1846
*   Param: None
*   Return: Id    
*   Description: In this method we are trying to get the contact id of the live chat transcript record.
**********************************************************************/
    
    @AuraEnabled(cacheable=true)
    Public Static Id fetchChatData(String chatRecordId){
        return [SELECT Id, ContactId FROM LiveChatTranscript WHERE Id =: chatRecordId].ContactId;
    }

     /**********************************************************************
*   Author: Saddam Hussain
*   Date:     18-Dec-2020
*   User Story : SFD-1846
*   Param: None
*   Return: String    
*   Description: In this method we are trying to get the contact id, financialId, 
AccountId, CardId, Category in the live chat transcript record.
**********************************************************************/

    @AuraEnabled
    Public Static String updateChat(String chatRecordId, String financialId, String customerId, String cardId, String category){
        try{
            List<Contact> contactList = [SELECT Id, AccountId FROM Contact WHERE Id =: customerId];
            LiveChatTranscript chatObj = new LiveChatTranscript(Id=chatRecordId);
            chatObj.ContactId = customerId;
            chatObj.Category__c = category;
            chatObj.AccountId = contactList[0].AccountId;
            if(financialId != StaticConstant.OTHER_VALUE){
                chatObj.Financial_Account__c = financialId;
                chatObj.Card__c = cardId; 
            } else {
                chatObj.Financial_Account__c = null;
                chatObj.Card__c = null;
            }               
            update chatObj;
            return chatObj.Id;
        }catch(Exception exp){
            StatusLogHelper.logSalesforceError('LogActivityController', 
                                               'E005', 'updateChat', exp, false);
        }
        return null;
    }
}