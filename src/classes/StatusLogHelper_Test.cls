/********************************************************************************************
Name: StatusLogHelperTest
=============================================================================================
Purpose: To cover the 'StatusLogHelper' apex class code coverage 
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0		   Kakasaheb Ekshinge 	  22-July-2020		SFD-1596			Code coverage has increases
********************************************************************************************/

@isTest
public class StatusLogHelper_Test {
	        
    public static testMethod void logStatusIF() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            StatusLog__c objStatus = UnitTestDataGenerator.testStatus.buildInsert();
            Test.startTest();
            Try{
            StatusLogHelper.logStatus(objStatus,false,true);  
            }catch(Exception e){
            	Try{
               		 StatusLogHelper.logSalesforceError('Apex Error','200','test request body',e,TRUE);
                }catch(Exception ee){
                 system.debug('Exception logStatusIF '+ee.getMessage());   
                }
             system.debug('Exception logStatusIF '+e.getMessage());
            }
            system.assert(objStatus!=null, 'Error Status logStatusIF');
            Test.stopTest();
        }
    }
     public static testMethod void logStatusIFElse() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            StatusLog__c objStatus = UnitTestDataGenerator.testStatus.buildInsert();
            Test.startTest();
            StatusLogHelper.logStatus(objStatus,true,false);
            system.assert(objStatus!=null, 'Error Status logStatusIFElse');
            Test.stopTest();
        }
    }
    public static testMethod void logStatusNegative() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            StatusLog__c objStatus = UnitTestDataGenerator.testStatus.buildInsert();
            Test.startTest();
            Try{
            StatusLogHelper.logStatus(objStatus,false,false);
            }catch(Exception e){
              system.debug('Exception logStatusNegative '+e.getMessage());  
            }
            system.assert(objStatus!=null, 'Error Status logStatusNegative');
            Test.stopTest();
        }
    }
    
    public static testMethod void logSFErrorsExc() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            StatusLog__c objStatus = UnitTestDataGenerator.testStatus.buildInsert();
            Test.startTest();
            Try{
            StatusLogHelper.logSFErrors('NoFedIDandRoleSet','NO FedAndRole found','500', 'Request body empty because of FedrationId and Role', 'No Response');
            }catch(Exception e){
              system.debug('Exception logStatusNegative '+e.getMessage());  
            }
            system.assert(objStatus!=null, 'Error Status logStatusNegative');
            Test.stopTest();
        }
    }
    public static testMethod void logSFErrorsCurity() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            StatusLog__c objStatus = UnitTestDataGenerator.testStatus.buildInsert();
            Test.startTest();
            Try{
            StatusLogHelper.logSFErrors('NoFedIDandRoleSet','NO FedAndRole found','4', 'Request body empty because of FedrationId and Role', 'No Response');
            }catch(Exception e){
              system.debug('Exception logStatusNegative '+e.getMessage());  
            }
            system.assert(objStatus!=null, 'Error Status logStatusNegative');
            Test.stopTest();
        }
    }
    public static testMethod void logSFErrorsMS() {
        System.runAs(UnitTestDataGenerator.adminUser) {
            StatusLog__c objStatus = UnitTestDataGenerator.testStatus.buildInsert();
            Test.startTest();
            Try{
            StatusLogHelper.logSFErrors('NoFedIDandRoleSet','NO FedAndRole found','Curity', 'Request body empty because of FedrationId and Role', 'No Response');
            }catch(Exception e){
              system.debug('Exception logStatusNegative '+e.getMessage());  
            }
            system.assert(objStatus!=null, 'Error Status logStatusNegative');
            Test.stopTest();
        }
    }
    
    
    
    
}