/**************************************************************************************************
* @Author:      Capgemini  
* @Date:        
* @PageCode:    
* @Description: This is utility class to send email to receipient.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public without sharing class UtilitySendEmail { 
    /**********************************************************************************************
* @Author:  Capgemini      
* @Date:        
* @Description: Responsibile to send email to send email with attachments 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/    
    
    
    public static Boolean sendEmailWithAttachment(List<String> toAddresses,
                                                  String subject,
                                                  String mailBody,
                                                  String senderName,
                                                  List<Messaging.EmailFileAttachment> attachments) 
    { 
        try{
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>(); 
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            mail.setToAddresses(toAddresses); 
            //mail.setReplyTo(replyTo); 
            mail.setSenderDisplayName(senderName); 
            mail.setSubject(subject); 
            mail.setHtmlBody(mailBody); 
            if(attachments.size()>0){
                mail.setFileAttachments(attachments); 
            }            
            mails.add(mail); 
            Messaging.sendEmail(mails); 
            return true;
        } catch(Exception ex){
            StatusLogHelper.logSalesforceError('UtilitySendEmail', 'E005', 'sendEmailWithAttachment', ex, true);
            return false;              
        }
    }
    
    
    /********************************************************************************************************
* @author        Capgemini
* @date          
* @description   Method to send email to admin user
* @Paramters     
* @return        
*********************************************************************************************************/
    
 /*   public static Void sendEmailToAdmin(String email,String firstName,String lastName,String portalName)
    {    
        try 
        {   
            EmailTemplate targetEmailTemp = new EmailTemplate();
            String pname = 'TEST';
            
            List<EmailTemplate> templateList = [select Id,HtmlValue,Body,Subject 
                                                from EmailTemplate 
                                                where name =:pname];  
            if(templateList.size()>0){
                targetEmailTemp = templateList[0];
            }
            if(String.isNotBlank(targetEmailTemp.Subject)){
                String htmlBody = targetEmailTemp.HtmlValue;
                htmlBody = htmlBody.replace('{!email}',email);
                htmlBody = htmlBody.replace('{!name}',firstName+lastName );
                
                String plainBody = targetEmailTemp.Body;
                plainBody = plainBody.replace('{!email}',email);
                plainBody = plainBody.replace('{!name}', firstName+lastName);
                
                String subject = targetEmailTemp.Subject;
                subject = Subject.replace('{!email}',email);
                
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] { Site.getAdminEmail() };
                    message.optOutPolicy = 'FILTER';
                message.ccaddresses = new String[] { Userinfo.getUserEmail()}; 
                    
                    message.subject = subject;
                message.plainTextBody = plainBody;
                message.htmlbody= htmlBody;
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                
            }
        }
        catch(Exception ex){
            StatusLogHelper.logSalesforceError('UtilitySendEmail', 'E005', 'sendEmailToAdmin', ex, true);
            
        }
    } */   
    
}