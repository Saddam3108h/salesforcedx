@isTest
global class CurityCalloutMock implements HttpCalloutMock{
  global HttpResponse respond(HTTPRequest req){
    HttpResponse res = new HttpResponse();
    res.setStatus('OK');
    res.setStatusCode(200);
    res.setBody('{"access_token":"ca454b0b-0794-4133-ad2c-596e3d208894","scope":"um_admin profile email","claims":"ad_dept ad_emailid ad_fedid ad_firstname ad_fullname ad_lastname ad_role client_serial_number email email_verified given_name kid name preferred_username profile static_claim website","token_type":"bearer","expires_in":1200}');
    return res;      
  }
}