/**********************************************************************
Name: CaseLogActivity
=======================================================================
Purpose: This class is used to handle all the logic of Financial Account object. User Story -SFD-1602

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         29-Jul-2020       Initial Version

**********************************************************************/
public with sharing class FinancialAccountTriggerHandler implements ITriggerHandler {
    
      //  // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
 
    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
      
        if (TriggerSettings__c.getInstance().FinancialAccountExecution__c // false
            &&  TriggerSettings__c.getInstance().OrgExecution__c)
        { 
            return true;
        }
        else
        { 
            return TriggerDisabled;
        }
        
    }
 
    public void BeforeInsert(List<SObject> newItems) 
    {
        //Map<Id, Financial_Account__c> newFAItems = (Map<Id, Financial_Account__c>) newItems;
        encryptAccountNumber(newItems,null,true);
    }
 
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        
        Map<Id, Financial_Account__c> newFAItems = (Map<Id, Financial_Account__c>) newItems;                
        Map<Id, Financial_Account__c> oldFAItems = (Map<Id, Financial_Account__c>) oldItems;  
        updateFields( newFAItems.values());
        encryptAccountNumber(newFAItems.values(),oldFAItems,false);
        
    }
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterInsert(Map<Id, SObject> newItems) {}
 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {} 
     
    public void AfterDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}

    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     29-Jul-2020
*   User Story : SFD-1602
*   Param: None
*   Return: Map    
*   Description: In this method we are trying to encrypt the account number before Financial Account record.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public static void encryptAccountNumber(List<Financial_Account__c> finAccountList, 
                                            Map<Id, Financial_Account__c> finAccountOldMap,
                                           Boolean isInsert){
        for(Financial_Account__c finObj : finAccountList){
            if(finObj.Account_Number__c != null 
               && (isInsert == true || finObj.Account_Number__c !=  finAccountOldMap.get(finObj.Id).Account_Number__c)) {
                   Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(finObj.Account_Number__c));
                   finObj.Encrypted_Account_Number__c=EncodingUtil.convertToHex(hash);
               }
        }
    }
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     29-Jul-2020
*   User Story : SFD-1602
*   Param: None
*   Return: Map    
*   Description: In this method we are trying to update the 120month field.
*   @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public static void updateFields(List<Financial_Account__c> finAccountList) {
        for(Financial_Account__c finObj : finAccountList){
            if(finObj.FA_Status__c == true 
               && finObj.Fa_ClosedDate__c != null
              ) {
                  String[] dateSplit = finObj.Fa_ClosedDate__c.split('-',3);
                  Integer dd = Integer.valueOf(dateSplit[1]);
                  Integer mm= Integer.valueOf(dateSplit[0]);
                  Integer yyyy= Integer.valueOf(dateSplit[2]); 
                  Date myDate = date.newInstance(mm, dd, yyyy).addMonths(120);
                  if(test.isRunningTest()) {
                      finObj.X120_Months__c=String.valueof(System.today());
                  } else {
                      finObj.X120_Months__c = String.valueOf(myDate);
                  }
              } else {
                  finObj.X120_Months__c = '';
              }
            if(finObj.Fa_ClosedDate__c == '12-12-9999'){
                finObj.X120_Months__c = '';
                finObj.FA_Status__c = false;
            }
        }
    }
}