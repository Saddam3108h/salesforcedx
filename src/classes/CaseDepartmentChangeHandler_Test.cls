/**********************************************************************
Name: CaseDepartmentChangeHandler_Test
=======================================================================
Purpose: This is the Test class for CaseDepartmentChangeHandler Class
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author               Date              Detail
1.0         Gagan Pathak         15-01-2021	       Initial Version

**********************************************************************/
@isTest(SeeAllData=false)
public class CaseDepartmentChangeHandler_Test {
    /**********************************************************************************************
* @Author: Gagan Pathak       
* @Date: 15/01/2021      
* @Description: Test method to cover the code for fetching the list of cases and existing the custom metadata(Case_Department_Change__mdt),
then it compare and close the cases.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    static testmethod void caseCreationTest(){
        Test.starttest();   
        Group queueName = [SELECT Id,Name from Group where Name ='DK Application Handling' and Type='Queue'];
        System.runAs(UnitTestDataGenerator.adminUser) {	
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'Department__c'=>'Customer Service',
                    'Status'=>'New',
                    'OwnerId'=>queueName.Id
                    });
            CaseDepartmentChangeHandler.fetchCases(new List<ID>{caseObj.Id});    
            System.assertEquals(caseObj.Department__c,'Customer Service');
        }                 
        Test.stopTest();
    }
}