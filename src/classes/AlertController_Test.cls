/********************************************************************************************
Name: AlertController_Test
=============================================================================================
Purpose: This class will cover the High Risk Customer code coverage
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author               Date         Created for Jira     Detail
1.0        Kakasaheb Ekshinge    6-Nov-2020   SFD-1804   		    Intial version
********************************************************************************************/
@isTest
public class AlertController_Test {
	
    static testMethod void alertSchedule(){
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.startTest();
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c' => '1123123' 
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123',
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id              
                    });  
            
             Alert__c AL = UnitTestDataGenerator.testAlert.buildInsert(new Map<String, Object>{
               	 	'People__c'=>contactObj.Id, 
                    'DueDate__c'=>System.today() + 180, 
                    'Category__c'=>'High Risk Customer'	
                    });
          
            AlertController.validHighRiskAlert(contactObj.Id);
            
            system.assertEquals(true,AL.Active__c,'invalid');
            Test.stopTest(); 
        }
    }
    
    
}