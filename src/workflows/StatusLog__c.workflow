<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_on_error</fullName>
        <ccEmails>entercardsalesforceteam.in@capgemini.com</ccEmails>
        <description>Send email on status log error</description>
        <protected>false</protected>
        <recipients>
            <recipient>saddam.hussain@capgemini.com.ec</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Status_Log_Error/Send_Email_on_error</template>
    </alerts>
    <rules>
        <fullName>Status Log Alert</fullName>
        <actions>
            <name>Send_email_on_error</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>StatusLog__c.Status__c</field>
            <operation>equals</operation>
            <value>E</value>
        </criteriaItems>
        <criteriaItems>
            <field>StatusLog__c.Status__c</field>
            <operation>equals</operation>
            <value>I</value>
        </criteriaItems>
        <criteriaItems>
            <field>StatusLog__c.Status__c</field>
            <operation>equals</operation>
            <value>E001</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
