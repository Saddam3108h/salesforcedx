<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Case_Status</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Case Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_status_to_In_Progress</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Change status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DK_Update_due_date</fullName>
        <description>Based on department, country and sub origin populate due date = (createddate +SLA)</description>
        <field>Due_Date__c</field>
        <formula>IF(OR(ISPICKVAL(Sub_Origin__c, &apos;Personal&apos;),ISPICKVAL(Sub_Origin__c, &apos;&apos;)),(CreatedDate + 3),IF(OR(ISPICKVAL(Sub_Origin__c, &apos;Netbank&apos;),ISPICKVAL(Sub_Origin__c, &apos;Contact Sheets&apos;),ISPICKVAL(Sub_Origin__c, &apos;Mobile Apps&apos;)),(CreatedDate + 1),
(CreatedDate + 0.1666666666666667)
))</formula>
        <name>DK Update due date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EC_Case_Status_To_CLosed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>EC Case Status To CLosed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Secured</fullName>
        <field>Secured__c</field>
        <literalValue>1</literalValue>
        <name>Mark Secured</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NO_Update_due_date</fullName>
        <description>Based on department, country and sub origin populate due date = (createddate +SLA)</description>
        <field>Due_Date__c</field>
        <formula>IF(OR(ISPICKVAL(Sub_Origin__c, &apos;Personal&apos;),ISPICKVAL(Sub_Origin__c, &apos;&apos;)),(CreatedDate + 3),IF(OR(ISPICKVAL(Sub_Origin__c, &apos;Netbank&apos;),ISPICKVAL(Sub_Origin__c, &apos;Contact Sheets&apos;),ISPICKVAL(Sub_Origin__c, &apos;Mobile Apps&apos;)),(CreatedDate + 1),
(CreatedDate + 0.1666666666666667)
))</formula>
        <name>NO Update due date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SE_Update_due_date</fullName>
        <description>Based on department, country and sub origin populate due date = (createddate +SLA)</description>
        <field>Due_Date__c</field>
        <formula>IF(OR(ISPICKVAL(Sub_Origin__c, &apos;Personal&apos;),ISPICKVAL(Sub_Origin__c, &apos;&apos;)),(CreatedDate + 3),IF(OR(ISPICKVAL(Sub_Origin__c, &apos;Netbank&apos;),ISPICKVAL(Sub_Origin__c, &apos;Contact Sheets&apos;),ISPICKVAL(Sub_Origin__c, &apos;Mobile Apps&apos;)),(CreatedDate + 1),
(CreatedDate + 0.1666666666666667)
))</formula>
        <name>SE Update due date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Required_From</fullName>
        <field>Require_Approval__c</field>
        <name>Update Approval Required From</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manager_Flag</fullName>
        <field>Manager_Review__c</field>
        <literalValue>1</literalValue>
        <name>Update Manager Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Specialist_Flag</fullName>
        <field>Specialist_Review__c</field>
        <literalValue>1</literalValue>
        <name>Update Specialist Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_sub_origin_for_non_secured_source</fullName>
        <description>For Non secured sources</description>
        <field>Sub_Origin__c</field>
        <literalValue>Personal</literalValue>
        <name>Update sub origin for non secured source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_sub_origin_for_secured_source</fullName>
        <description>For secured sources</description>
        <field>Sub_Origin__c</field>
        <literalValue>Netbank</literalValue>
        <name>Update sub origin for secured source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UserCred</fullName>
        <field>User_Credentials__c</field>
        <formula>$User.FirstName  &amp;  $User.LastName &amp; TEXT(LastModifiedDate)</formula>
        <name>UserCred</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Change Status to In Progress</fullName>
        <actions>
            <name>Change_status_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( OR(TEXT(Status)=&quot;New&quot;,TEXT(Status)=&quot;Re Open&quot;), ISCHANGED(OwnerId), BEGINS(PRIORVALUE(OwnerId), &quot;00G&quot;), BEGINS(OwnerId ,&quot;005&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Status to New</fullName>
        <actions>
            <name>Case_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>when change the &quot;Case owner field&quot; value the status should be changed to New</description>
        <formula>AND( TEXT(Status)=&quot;In Progress&quot;, ISCHANGED(OwnerId), BEGINS(PRIORVALUE(OwnerId), &quot;005&quot;), BEGINS(OwnerId ,&quot;00G&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DK Populate due date</fullName>
        <actions>
            <name>DK_Update_due_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Department__c</field>
            <operation>equals</operation>
            <value>Customer Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Country__c</field>
            <operation>equals</operation>
            <value>DK</value>
        </criteriaItems>
        <description>Based on department, country and sub origin populate due date = (createddate +SLA)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EC_Case_Status_Changed_Closed</fullName>
        <actions>
            <name>EC_Case_Status_To_CLosed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4 AND 5 AND (6 OR 7) AND (8 OR 9)</booleanFilter>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Ny överföring,Automatsvar til kunde Santander,Autoreply,Din forespørsel er mottatt,Automatsvar til kunde Santander,Takk for at du kontakter,CreditPro. Endringsrapport for portefølje: Entercard,Henvendelse mottatt,Henvendelse mottatt,Re: Takk for at du kon</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.EC_Subject__c</field>
            <operation>contains</operation>
            <value>Ny överföring,Automatsvar til kunde Santander,Autoreply,Din forespørsel er mottatt,Automatsvar til kunde Santander,Takk for at du kontakter,CreditPro. Endringsrapport for portefølje: Entercard,Henvendelse mottatt,Henvendelse mottatt,Re: Takk for at du kon</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Category__c</field>
            <operation>equals</operation>
            <value>APH Loan,Fund Transfer/Bill Payment,FT / Bill payment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Department__c</field>
            <operation>equals</operation>
            <value>Customer Service,Application Handling</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.To_Address__c</field>
            <operation>equals</operation>
            <value>tekwkorts@entercard.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.To_Address__c</field>
            <operation>equals</operation>
            <value>soknad@entercard.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>Re:</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.EC_Subject__c</field>
            <operation>notContain</operation>
            <value>Re:</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email to case update fields for non secured source</fullName>
        <actions>
            <name>SE_Update_due_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_sub_origin_for_non_secured_source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notEqual</operation>
            <value>Product Information,Feedback,Fraud/chargeback,Statement,Payments,Login,Other</value>
        </criteriaItems>
        <description>Email-to-case Update Sub Origin=&apos;Personal&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email to case update fields for secured source</fullName>
        <actions>
            <name>Mark_Secured</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SE_Update_due_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_sub_origin_for_secured_source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Product Information,Feedback,Fraud/chargeback,Statement,Payments,Login,Other</value>
        </criteriaItems>
        <description>Email-to-case Update Sub Origin=&apos;Netbank&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NO Populate due date</fullName>
        <actions>
            <name>NO_Update_due_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Department__c</field>
            <operation>equals</operation>
            <value>Customer Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Country__c</field>
            <operation>equals</operation>
            <value>NO</value>
        </criteriaItems>
        <description>Based on department, country and sub origin populate due date = (createddate +SLA)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SE Populate due date</fullName>
        <actions>
            <name>SE_Update_due_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Department__c</field>
            <operation>equals</operation>
            <value>Customer Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Country__c</field>
            <operation>equals</operation>
            <value>SE</value>
        </criteriaItems>
        <description>Based on department, country and sub origin populate due date = (createddate +SLA)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
