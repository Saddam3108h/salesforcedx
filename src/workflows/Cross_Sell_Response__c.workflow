<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Customer_First_Name</fullName>
        <field>First_Name1__c</field>
        <formula>Person__r.FirstName</formula>
        <name>Update Customer First Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Customer_Last_Name</fullName>
        <field>Last_Name1__c</field>
        <formula>Person__r.LastName</formula>
        <name>Update Customer Last Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Financial_Account_Number</fullName>
        <field>Account_Number1__c</field>
        <formula>Financial_Account__r.Account_Number__c</formula>
        <name>Update Financial Account Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Cross Sell Update Customer</fullName>
        <actions>
            <name>Update_Customer_First_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Customer_Last_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Financial_Account_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Cross_Sell_Response__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
