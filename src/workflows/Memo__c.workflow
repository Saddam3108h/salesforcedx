<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Created_by_Id</fullName>
        <field>Updated_By__c</field>
        <formula>CreatedBy.FirstName  + &quot; &quot; + &quot; &quot; +   CreatedBy.LastName</formula>
        <name>Update Created by Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Created_date_time</fullName>
        <field>Updated_On_Date_and_Time__c</field>
        <formula>CreatedDate</formula>
        <name>Update Created date time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Memo_Owner</fullName>
        <description>Update memo owner = ‘Salesforce Support’ when a record is created</description>
        <field>OwnerId</field>
        <lookupValue>salesforce.support@capgemini.com.ec</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Memo Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Memo Owner</fullName>
        <actions>
            <name>Update_Memo_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Memo__c.Source__c</field>
            <operation>equals</operation>
            <value>Prime</value>
        </criteriaItems>
        <description>Update memo owner = ‘Salesforce Support’ when a record is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update updated by in Memo</fullName>
        <actions>
            <name>Update_Created_by_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Created_date_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(AND($Profile.Name&lt;&gt;&apos;EC SOA AnonUser&apos;,$Profile.Name&lt;&gt;&apos;EC SAS AnonUser&apos;), true, false)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
