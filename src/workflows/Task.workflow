<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Comment_field_update</fullName>
        <field>Comments__c</field>
        <formula>Description</formula>
        <name>Comment field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Description_Field_update</fullName>
        <description>Update the Description field based on comment value</description>
        <field>Description</field>
        <formula>Comments__c</formula>
        <name>Description Field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>UpdateComment</fullName>
        <actions>
            <name>Description_Field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Comments__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Used to update the comment field based on Description value</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
