<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Activate_alert</fullName>
        <field>Active__c</field>
        <literalValue>1</literalValue>
        <name>Activate alert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deactivate_the_alert</fullName>
        <field>Active__c</field>
        <literalValue>0</literalValue>
        <name>Deactivate the alert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Deactivate the alert</fullName>
        <active>false</active>
        <description>Time based workflow to deactivate the alert once the due date is passed.</description>
        <formula>And(  NOT(ISNULL( DueDate__c)),(DueDate__c&gt;=today()))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Deactivate_the_alert</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Alert__c.DueDate__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Populate Active field</fullName>
        <actions>
            <name>Activate_alert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>And(  NOT(ISNULL( DueDate__c)),(DueDate__c&gt;=today()), OR(ISCHANGED(DueDate__c), ISNEW() ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
