<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Campaign_Owner</fullName>
        <description>Update campaign owner = ‘Salesforce Support’ when a record is created</description>
        <field>OwnerId</field>
        <lookupValue>salesforce.support@capgemini.com.ec</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Campaign Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Campaign Owner</fullName>
        <actions>
            <name>Update_Campaign_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Manage_Campaign__c.CreatedById</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <description>Update campaign owner = ‘Salesforce Support’ when a record is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
