import { LightningElement,track,api } from 'lwc';
import fetchFinAccountNumber from '@salesforce/apex/FetchLast5Transactions.fetchFinAccountNumber'; 
import validateUserAccTkn from '@salesforce/apex/RESTCalloutUtil.validateUserAccTkn';
import fetchTransactionList from '@salesforce/apex/FetchLast5Transactions.fetchTransactionList';
import fetchTxnBetDate from '@salesforce/apex/FetchLast5Transactions.fetchTxnBetDate';


const columns = [
	{
		label: 'Card Number',
		fieldName: 'maskedCardNo',
        type: 'text',
        initialWidth: 165 
	},  
    /*
    {
		label: 'Transaction ID',
		fieldName: 'transactionID',
        type: 'text',
        initialWidth: 165 
	},
   */
    {
		label: 'Txn Date',
		fieldName: 'timeOfPurchase',
        type: 'date', 
        initialWidth: 115,
        typeAttributes:{timeZone:'UTC', year:'numeric', month:'numeric', day:'numeric'}      
	},	
	
	{
		label: 'Posting Date',
		fieldName: 'transactionPostedDate',
        //type: 'text'  
        type: 'date', 
        initialWidth: 130,
        typeAttributes:{timeZone:'UTC', year:'numeric', month:'numeric', day:'numeric'}  
    },
    {
		label: 'Txn Currency',
		fieldName: 'billingCurrency',
        type: 'text',
        initialWidth: 135 
	},
	{
		label: 'Txn Amount',
		fieldName: 'transactionAmount',
        type: 'text', 
        initialWidth: 125  
    },
    {
		label: 'Billing Amount',
		fieldName: 'billingAmount',
        type: 'text' ,
        initialWidth: 140   
    },
    {
		label: 'Txn Text',
		fieldName: 'accountLevelTransactionDescription',
        type: 'text',
        initialWidth: 120      
    },
    {
		label: 'Merchant Name',
		fieldName: 'merchantName',
        type: 'text',
        initialWidth: 150      
    },    
    {
		label: 'City',
		fieldName: 'merchantCity',
        type: 'text',
        initialWidth: 75      
    },
    {
		label: 'Country',
		fieldName: 'merchantCountry',
        type: 'text',
        initialWidth: 92      
    }
    /*
    {
		label: 'Type',
		fieldName: 'reasonCode',
        type: 'text',
        initialWidth: 70    
	}
	 */
    ];

export default class LastTransactionsLwc extends LightningElement {
   
@api recordId;
@track show = false;
@track columns = columns;
@track restdatatran;
//@track restBetweenDates;
@track error;
@track accessToken;
@track accountNumber;
@track showUserMessage;
@track ressize = false;
@track fromdate;
@track todate;
@track stopCall = 0;
@track dateValidationGr = false;
@track toDayDate;


callValidateAccessToken(){ 

    validateUserAccTkn()
    .then(result => {
		console.log('---1-----');
		console.log('@recordId==>'+this.recordId);
        this.accessToken = result;
        console.log('@@@@ this.data-----'+this.accessToken);        
        if(this.accessToken === 'NO TOKEN'){      
            this.show = true;
            this.showUserMessage = 'Please check correct Fedration Id or Role set, else connect to System Administrator!';      
            console.log('In no token if part:'+this.accessToken);
        }else {  
		   console.log('@recordId==>'+this.recordId);
		   this.fetchFinAccountNumber(this.recordId);
         }      
    })
    .catch(error => {
        console.log('@- Error validateUserAccTkn Response-----'+JSON.stringify(error));
        console.log('---Exception Type-----'+JSON.stringify(error.exceptionType));
       // this.show = true;
        //showUserMessage = 'Please check correct Fedration Id or Role set, else connect to System Administrator!';      
   
       // this.error = error;
    });
} 


connectedCallback(){
    this.callValidateAccessToken(); 
    
    let today = new Date().toISOString().slice(0, 10);
    this.toDayDate = today;
   // console.log(today);
    
}    

fetchFinAccountNumber(recordId){
    console.log('Fin Account Number called ==');
    fetchFinAccountNumber({ recordId: recordId })
        .then((result) => {            
			 this.accountNumber = JSON.parse(result); 
			 console.log('this.accountNumber==>'+this.accountNumber); 
			 console.log('this.accessToken==>'+this.accessToken); 
             if( this.stopCall == 0){
                this.fetchTransactionList(this.accountNumber,this.accessToken);
             }    
        })
        .catch((error) => {          
            this.error = error;           
            console.log('@@ Account Number issue:'+error);  
        });
}


fetchTransactionList(accountNumber,accessToken){
    this.ressize = false;
	console.log('!!!! accountNumber accessToken==>'+accountNumber+accessToken); 
    fetchTransactionList({ accountNumber: accountNumber,accessToken : accessToken})
        .then((result) => { 
            this.restdatatran = JSON.parse(result);
            console.log('=='+this.restdatatran.metadata.total_count);  
            if(this.restdatatran.metadata.total_count === 0 ){
                this.ressize = true;
            } 		
        })
        .catch((error) => {
            this.error = error;
            console.log('Transaction record issue: '+error); 
        });
    }

    fromDate(event) {
        this.fromdate= event.target.value;
    }
    toDate(event) {
        this.todate= event.target.value;
    }

    sendCallout(accountNumber,accessToken,fromdate,todate){
        //alert('!!!! accountNumber accessToken==>'+accountNumber+accessToken+fromdate+todate); 
        //this.ressize = false;
        fetchTxnBetDate({ accountNumber:accountNumber,accessToken : accessToken,fromdate:fromdate,todate : todate})
            .then((result) => { 
                
                if(JSON.parse(result).metadata.total_count === 0 ){
                    this.ressize = true;
                } else {
                    this.restdatatran = JSON.parse(result);
                }
            })
            .catch((error) => {
                this.error = error;
                console.log('Between To date issue: '+error);               
            });
        }
          
    showTransactions(){                
        this.stopCall = 1;
        this.dateValidationGr = false;
        this.ressize = false;
        this.restdatatran = ''; // Empty datatable
        const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')]
                .reduce((validSoFar, inputField) => {
                    inputField.reportValidity();
                    return validSoFar && inputField.checkValidity();
                }, true);
            if (isInputsCorrect) { 
                    if(this.fromdate > this.todate){
                        this.dateValidationGr = true;
                        this.restdatatran = ''; // Empty datatable 
                    }else {
                        this.callValidateAccessToken(); // create or Update access token 
                        this.sendCallout(this.accountNumber,this.accessToken,this.fromdate,this.todate)
                    }
            }
    } 

    allowReset(event) {
        this.template.querySelector('form').reset();
        //this.restdatatran = ''; // Empty datatable 
     }

}