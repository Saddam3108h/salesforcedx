import { LightningElement,track,api } from 'lwc';
import openCaseCount1 from '@salesforce/apex/OpenCaseControllerLwc.openCaseCount';

export default class OpenCaseCountLwc extends LightningElement {

    @api recordId;
    @track openCaseCount="";
    divColor = 'lightred';
    
    caseCount(recordId){
        openCaseCount1({ customerId: recordId })
            .then((result) => {           
                console.log('Test===>'+result);
                this.openCaseCount = result; 
                console.log('this.openCaseCount===>'+this.openCaseCount);
                if(this.openCaseCount === '0'){
                   // alert(this.openCaseCount);
                    this.divColor = 'lightgreen';
                }
           
           
            })
            .catch((error) => {          
                this.error = error;   
            });
         
        } 
        
    connectedCallback(){
        this.caseCount(this.recordId);
    }   
    
    }