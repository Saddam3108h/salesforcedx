/**********************************************************************
Name: ManageCampaignTrigger
=======================================================================
Purpose: This is trigger on ManageCampaign

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain                            Initial Version
2.0			Priyanka Singh		    11-Aug-2020	       Trigger Factory Implementation	

**********************************************************************/
trigger ManageCampaignTrigger on Manage_Campaign__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    TriggerDispatcher.Run(new ManageCampaignTriggerHandler());
}