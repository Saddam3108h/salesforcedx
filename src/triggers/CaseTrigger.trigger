/**********************************************************************
Name: CaseTrigger
=======================================================================
Purpose: This Apex Trigger is used for execute the CaseTriggerHandler methods.  User Story -SFD-984

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Srinivas D           05-Feb-2019       Initial Version

**********************************************************************/
trigger CaseTrigger on Case (before insert, before update, before delete, after insert, after update, after delete, after undelete)  {
     TriggerDispatcher.Run(new CaseTriggerHandler());  
}