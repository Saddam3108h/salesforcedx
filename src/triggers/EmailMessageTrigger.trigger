/**********************************************************************
Name: EmailMessageTrigger
=======================================================================
Purpose: This Task trigger handler class is created on Email Message logics.User Story -SFD-1363

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         03-June-2020       Initial Version
2.0			Priyanka Singh		   12-Aug- 2020		  Trigger Factory Implementation

**********************************************************************/
trigger EmailMessageTrigger on EmailMessage (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatcher.Run(new EmailMessageTriggerHandler());  
}