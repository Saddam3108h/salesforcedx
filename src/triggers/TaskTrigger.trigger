/**********************************************************************
Name: TaskTrigger
=======================================================================
Purpose: This Task trigger handler class is created on Task logics.User Story -SFD-1363

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         03-June-2020       Initial Version
2.0			Priyanka Singh		   11-Aug-2020	      Trigger Factory Implementation

**********************************************************************/
trigger TaskTrigger on Task (before insert, before update, before delete, after insert, after update, after delete, after undelete)  {
    TriggerDispatcher.Run(new TaskTriggerHandler());    
}