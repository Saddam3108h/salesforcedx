/**********************************************************************
Name: SaveDeskTrigger
=======================================================================
Purpose: This Apex Trigger is used for execute the SaveDeskTriggerHandler methods.  User Story -SFD-1430

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddaam Hussain         20-May-2020       Initial Version
2.0			Priyanka Singh		    11-Aug-2020	   Trigger Factory Implementation	
**********************************************************************/
trigger SaveDeskTrigger on SaveDeskQueue__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)  {
    {
        TriggerDispatcher.Run(new SaveDeskTriggerHandler());
    }
}