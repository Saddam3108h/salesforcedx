<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Din forespørsel er mottatt</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">APH Loan</value>
    </values>
    <values>
        <field>Department__c</field>
        <value xsi:type="xsd:string">Application Handling</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">New</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Din forespørsel er mottatt</value>
    </values>
    <values>
        <field>ToEmail__c</field>
        <value xsi:type="xsd:string">soknad@entercard.com</value>
    </values>
    <values>
        <field>WebEmail__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
