<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Ny överföring for FT</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Fund Transfer/Bill Payment</value>
    </values>
    <values>
        <field>Department__c</field>
        <value xsi:type="xsd:string">Test</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">New</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Ny överföring</value>
    </values>
    <values>
        <field>ToEmail__c</field>
        <value xsi:type="xsd:string">test.test@test.com</value>
    </values>
    <values>
        <field>WebEmail__c</field>
        <value xsi:type="xsd:string">harisivaramakrishna.aathmakuri@capgemini.com</value>
    </values>
</CustomMetadata>
