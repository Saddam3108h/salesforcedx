<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SE Fraud/Chargeback</label>
    <protected>false</protected>
    <values>
        <field>Department__c</field>
        <value xsi:type="xsd:string">Fraud and Chargeback</value>
    </values>
    <values>
        <field>Owner__c</field>
        <value xsi:type="xsd:string">SE Fraud/Chargeback</value>
    </values>
</CustomMetadata>
