<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Power of attornety</label>
    <protected>false</protected>
    <values>
        <field>Category__c</field>
        <value xsi:type="xsd:string">Power of attornety</value>
    </values>
    <values>
        <field>Department__c</field>
        <value xsi:type="xsd:string">Customer Service</value>
    </values>
</CustomMetadata>
