public with sharing class FAExtension{
    public string FAgenStatus{get;set;}
    public string FAauthStatus{get;set;}
    public string FAfinStatus{get;set;}
    public string FAEinvoice{get;set;}
    public string NK{get;set;} //sfd 793 added by mani
    public boolean FAEChairty{get;set;}//SFD-651--Added By Ravi Jaroli
    public boolean ProductSernocheck{get;set;}//SFD-651--Added By Ravi Jaroli
    public string FAEKYC{get;set;}// sfd 938 added by Mani
    public boolean FAELPI{get;set;}// sfd 249 added by Mani
    public boolean ProductSernochecks{get;set;} // sfd 249 added by Mani
    public boolean ProductSernocheckNK{get;set;} //sfd 793 added by mani
    
    public decimal balance{get;set;}
    public string AccountRating{get;set;}
    public Decimal MTP{get;set;}
    public boolean isMTPEditable{get;set;}
    public decimal AvailableCredit{get;set;}
    public List<Financial_Account__c>FAlist;
    public boolean isMTPupdated{get;set;}
    public List<LoansWrapper> loansWrapperList{get;set;}
    public String StmtNextDate{get;set;}//Date 
    
    //Added by shameel
     public string errorMessage{get;set;}
     public string finAccId{get;set;}
    //end 
     set<string>LoanProdserno = new set<string>();
     public boolean ClickedFAdetails{get;set;}
     public boolean ClickedMembershipdetails{get;set;}
     public boolean ClickedLoandetails{get;set;}    
    
    public FAExtension(ApexPages.StandardController stdController)
    {
        isMTPEditable = false;
        isMTPupdated=true;
        ClickedFAdetails=false;
        ClickedMembershipdetails=false;
        ClickedLoandetails=false;
        //Added by shameel
        errorMessage='';
        loansWrapperList=new List<LoansWrapper>();
        finAccId='';
        LoanProdserno.addAll(Common_Settings__c.getValues('Loan Products').Common_Value__c.split(','));
        //end 
        
        FAlist=[SELECT id,name,Account_Number__c,Institution_Id__c,Account_Serno__c,Partner_Description__c,FA_Status__C,Product_Serno__c,(select id,name,PrimaryCardFlag__c,people__c,People__r.SSN__c,People__r.SerNo__c from Cards__r where PrimaryCardFlag__c=true) from Financial_Account__c where id=:stdController.getID()];
        System.debug('++++FAlist'+FAlist);
        if(FAlist.size()>0)
        {
        finAccId=FAlist[0].Id;
        }
        System.debug('******'+FAlist[0].Product_Serno__c);
        if(FAlist[0].Product_Serno__c=='310')
        {
        ProductSernocheck=true;
        System.debug('!!!!!!!!!'+ProductSernocheck);
        }
        if(FAlist[0].Product_Serno__c=='330')
        {
        ProductSernochecks=true;
        System.debug('!!!!!!!!!'+ProductSernochecks);
        }
        if(FAlist[0].Product_Serno__c=='58' || FAlist[0].Product_Serno__c=='33'
        || FAlist[0].Product_Serno__c=='121' || FAlist[0].Product_Serno__c=='122'
        ||FAlist[0].Product_Serno__c=='123' || FAlist[0].Product_Serno__c=='124'
        || FAlist[0].Product_Serno__c=='160' || FAlist[0].Product_Serno__c=='163'
        ||FAlist[0].Product_Serno__c=='300' || FAlist[0].Product_Serno__c=='301')
        {
        ProductSernocheckNK=true;
        System.debug('!!!!!!!!!'+ProductSernocheckNK);
        }
        
        
    }
    public void doCall()
    {
        try{
            ClickedFAdetails=true;
            if(FAlist!=null && FAlist.size()>0 && FAlist[0].Cards__r.size()>0 && FAlist[0].Cards__r[0].People__c!=null && FAlist[0].Cards__r[0].People__r.SSN__c!=null && FAlist[0].Cards__r[0].People__r.SSN__c!=''  && FAlist[0].Institution_Id__c!=null){
                GetCustomerOutput output= CSSServiceHelper.getCustomerDetails(integer.valueof(FAlist[0].Institution_Id__c),FAlist[0].Cards__r[0].People__r.SSN__c,'Y','Y');//'0000000061'
                if(output!=null && output.FinancialAccounts!=null){
                    for(GetCustomerOutput.CustomerFinancialAccount eachFA : output.FinancialAccounts)
                    {
                        if(eachFA.AccountNo==FAlist[0].Account_Number__c)
                        {
                            FAgenStatus=eachFA.StGen;
                            FAfinStatus=eachFA.StFin;
                            FAauthStatus=eachFA.StAuth;
                            FAEinvoice=eachFA.Einvoice;
                            balance=eachFA.Balance;
                            AccountRating=eachFA.AccountRating;
                            MTP=eachFA.MTP;
                            AvailableCredit=eachFA.OTB;
                           StmtNextDate=((eachFA.StmtNextDate!=null && eachFA.StmtNextDate!='')?eachFA.StmtNextDate.left(10):eachFA.StmtNextDate); 
                                if(eachFA.CharityFlag=='Y')
                                {                           
                            FAEChairty=True;//SFD-651 Added By Ravi Jaroli
                            }
                            else
                            {
                            FAEChairty=False;
                            }
                            if(eachFA.KYC=='Y'|| eachFA.KYC=='')
                                {                           
                            FAEKYC='Y';//SFD-938 Added By Mani
                            }
                            else
                            {
                            FAEKYC='N';
                            }
                            if(eachFA.LPI=='Y')
                                {                           
                            FAELPI=True;//SFD-249 Added By Mani
                            }
                            else
                            {
                            FAELPI=False;
                            } 
                            if(eachFA.NyckelKundFlag=='AVG00001')
                                {                           
                            NK='PrivateBanking';//SFD-793Added By Mani
                            }
                            else if(eachFA.NyckelKundFlag=='AVG00002')
                                {                           
                            NK='Specialare för Premiumrådgivning ISB';//SFD-793Added By Mani
                            }
                            else if(eachFA.NyckelKundFlag=='AVG00003' || eachFA.NyckelKundFlag=='AVG00004' || eachFA.NyckelKundFlag=='AVG00006')
                                {                           
                            NK='NyckelKund';//SFD-793Added By Mani
                            }
                            else if(eachFA.NyckelKundFlag=='AVG00099')
                                {                           
                            NK='Personal';//SFD-793Added By Mani
                            }
                            
                            break;
                        }
                    }
                    update FAlist;
                }
            } 
        } 
        catch(dmlException e)   {
            StatusLogHelper.logSalesforceError('getCustomerDetails', 'E005', 'getCustomerDetails: ' + integer.valueof(FAlist[0].Institution_Id__c) +FAlist[0].Cards__r[0].People__r.SSN__c, e, true);
        }
        catch(calloutException e)   {
            StatusLogHelper.logSalesforceError('getCustomerDetails', 'E001', 'getCustomerDetails: ' + integer.valueof(FAlist[0].Institution_Id__c) +FAlist[0].Cards__r[0].People__r.SSN__c, e, true);
        }
        catch(Exception e)   {
            StatusLogHelper.logSalesforceError('getCustomerDetails', 'E004', 'getCustomerDetails: ' + integer.valueof(FAlist[0].Institution_Id__c) +FAlist[0].Cards__r[0].People__r.SSN__c, e, true);
        }
    }
    public void updateMTP(){
    
        
        //CSSServiceHelper.setMTP(FAlist[0].Cards__r[0].People__r.SSN__c,FAlist[0].Account_Number__c,MTP,FAlist[0].Institution_Id__c);
        if(FAlist[0].Account_Serno__c!=null && FAlist[0].Account_Serno__c!='' && FAlist[0].Account_Number__c!=null && this.MTP!=null){
        UpdateCustomerDataInput UpdateCustomerDataInputInstance = new UpdateCustomerDataInput();
        UpdateCustomerDataInputInstance.InstitutionId = (Integer)FAlist[0].Institution_Id__c;
        UpdateCustomerDataInputInstance.MTRReq = new UpdateCustomerDataInput.MTPRequest();
        UpdateCustomerDataInputInstance.MTRReq.Serno = Integer.valueOf(FAlist[0].Account_Serno__c);
        UpdateCustomerDataInputInstance.MTRReq.AccountNo = FAlist[0].Account_Number__c;
        UpdateCustomerDataInputInstance.MTRReq.MTP = this.MTP;
        UpdateCustomerDataOutput UpdateCustomerDataOutputinstance = CSSServiceHelper.UpdateCustomerData(UpdateCustomerDataInputInstance); 
        isMTPupdated=true;
        if(UpdateCustomerDataOutputinstance==null || UpdateCustomerDataOutputinstance.UCDR.Code!='0')
        {
            isMTPupdated=false;      
            //Added by shameel  
            errorMessage=CommonHelper.displayMessage(UpdateCustomerDataOutputinstance!= null?UpdateCustomerDataOutputinstance.UCDR.Code:'');
            // end
        }     
        }
        isMTPEditable = false;
        MTP=null;
        doCall();//refresh the page
        system.debug('MTP-->'+MTP);
    }
    
    public void doCancel(){                            
        isMTPEditable = false;
        MTP=null;
        doCall();//refresh the page
    }
    
    // Added by shameel 04-03-2017
    public void getLoanDetails()
    {
        ClickedLoandetails=true; 
        loansWrapperList=new List<LoansWrapper>();
        GetLoanServiceInput getLoanServiceInput =new GetLoanServiceInput();
        //if(LoanProdserno.contains(FAlist[0].Product_Serno__c)){// loan product
        try{
            
            List<Card__c> cardList = [SELECT Id, Name, People__c,People__r.SSN__c, Financial_Account__c,Financial_Account__r.Account_Number__c,Financial_Account__r.Institution_Id__c FROM Card__c where People__c!=null and Financial_Account__c=:finAccId Limit 50000];
            if(cardList.size()>0)
            {
                system.debug('SSN-->'+cardList[0].People__r.SSN__c);
                system.debug('AccountNumber-->'+cardList[0].Financial_Account__r.Account_Number__c);
                system.debug('InstitutionId-->'+cardList[0].Financial_Account__r.Institution_Id__c);
                getLoanServiceInput.SSN=cardList[0].People__r.SSN__c;//'00003014110';
                getLoanServiceInput.AccountNumber=cardList[0].Financial_Account__r.Account_Number__c;//'0000148112';
                getLoanServiceInput.InstitutionId=cardList[0].Financial_Account__r.Institution_Id__c!=null?integer.valueOf(cardList[0].Financial_Account__r.Institution_Id__c):0;//1;
                GetLoanServiceOutput getLoanServiceOutput = CSSServiceHelper.GetLoans(getLoanServiceInput); 
                
                
                if(getLoanServiceOutput!=null && getLoanServiceOutput.LoanAccountDetails!=null && getLoanServiceOutput.LoanAccountDetails.size()>0)
                {
                    for(GetLoanServiceOutput.LoanAccountDetails  loanDtls : getLoanServiceOutput.LoanAccountDetails)
                    {
                         LoansWrapper loansWrapper=new LoansWrapper();
                         loansWrapper.creditLimit=loanDtls.InitialLoanAmount!=null?string.valueOf(loanDtls.InitialLoanAmount):'';
                         loansWrapper.OutStandingBalance=loanDtls.OutStandingBalance!=null?string.valueof(loanDtls.OutStandingBalance):'';
                         loansWrapper.interest=loanDtls.NominalInterestRate!=null?string.valueOf(loanDtls.NominalInterestRate):'';
                         loansWrapper.LoanAccountNumber=loanDtls.LoanAccountNumber!=null?loanDtls.LoanAccountNumber:'';
                         loansWrapper.LoanAccountStatus=loanDtls.LoanAccountStatus!=null?loanDtls.LoanAccountStatus:'';
                         if(loanDtls.PaymentPlanDetails!=null && loanDtls.PaymentPlanDetails.size()>0)
                         {
                             GetLoanServiceOutput.PaymentPlanDetails  paymntDtls = loanDtls.PaymentPlanDetails[0];
                             loansWrapper.monthlyPayment=paymntDtls.Amount!=null?string.valueOf(paymntDtls.Amount):'';
                             loansWrapper.dueDate=paymntDtls.PaymentDate!=null? string.valueOf(paymntDtls.PaymentDate):'';
                         }
                         loansWrapperList.add(loansWrapper);         
                    }
                }
            }
            
        }
        catch(dmlException e)   {
            StatusLogHelper.logSalesforceError('getLoanDetails', 'E005', 'getLoanServiceInput: ' +getLoanServiceInput , e, true);
        }
        catch(calloutException e)   {
            StatusLogHelper.logSalesforceError('getLoanDetails', 'E001', 'getLoanServiceInput: ' + getLoanServiceInput, e, true);
        }
        catch(Exception e)   {
            StatusLogHelper.logSalesforceError('getLoanDetails', 'E004', 'getLoanServiceInput: ' + getLoanServiceInput, e, true);
        }
        // Loan callout
        //}//loan product
    }
   
    //Loans wrapper
    public class LoansWrapper
    {
        public LoansWrapper(string creditLimit,string outstandingBalance,string interest,string monthlyPayment,string dueDate,string LoanAccountNumber,string LoanAccountStatus)
        {
            this.creditLimit=creditLimit;
            this.outstandingBalance=outstandingBalance;
            this.interest=interest;
            this.monthlyPayment=monthlyPayment;
            this.dueDate=dueDate;
            this.LoanAccountNumber=this.LoanAccountNumber;
            this.LoanAccountStatus=LoanAccountStatus;
            
        }
        
        public LoansWrapper(){}
        
        public string creditLimit {get;set;}
        public string outstandingBalance{get;set;}
        public string interest{get;set;}
        public string monthlyPayment{get;set;}
        public string dueDate{get;set;}
        public string LoanAccountNumber{get;set;}
        public string LoanAccountStatus{get;set;}
    
    }
    //end by shameel 04-03-2017
    
    //SUBHAJIT PAL [Start] 25 SEPT 2017
    public List<ReadMembershipDataOutput.MembershipResponseBody> MembershipList=new List<ReadMembershipDataOutput.MembershipResponseBody>();
    public list<List<innerwrap>>MembershipdataList{get;set;}
    set<string>isedit=new set<string>();
    
    public void fetchMembershipData(){
        ClickedMembershipdetails=true;
        MembershipdataList=new list<List<innerwrap>>();
        if(!LoanProdserno.contains(FAlist[0].Product_Serno__c)){//not loan product
        ReadMembershipDataInput inputParam=new ReadMembershipDataInput();
        inputParam.InstitutionId=integer.valueof(FAlist[0].Institution_Id__c);
        //inputParam.PartnerName=FAlist[0].Partner_Description__c;//LOPLUS DENMARK
        inputParam.ProductId=FAlist[0].Product_Serno__c;//FAlist[0].Product_Serno__c;
        for(card__c cc : FAlist[0].Cards__r)
        {
            if(cc.PrimaryCardFlag__c)
            {
                inputParam.EntityId=cc.People__r.SerNo__c;
                break;
            }
        }
        //@@@@Callout ReadMembershipdata
        ReadMembershipDataOutput outputParam= CSSServiceHelper.ReadMembershipData(inputParam);
        system.debug('----outputParam---'+outputParam);
            
                //ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There is no Membership data to display'));
            
        if(outputParam!=null && outputParam.MembershipResponseBodyList!=null && outputParam.MembershipResponseBodyList.size()>0)
        {
            MembershipList = outputParam.MembershipResponseBodyList;
            for(UpdateMembershipFields__c itr: UpdateMembershipFields__c.getall().values())
            {
                if(itr.PartnerName__c==MembershipList[0].PartnerName && itr.IsEditable__c)
                {
                    isedit.add(itr.Field_Name__c);
                }
            }
              
            list<innerwrap>tempList=new list<innerwrap>();
            innerwrap temp=new innerwrap();
            temp.name='PartnerName';
            temp.value=MembershipList[0].PartnerName;
            temp.iseditable=false;
            tempList.add(temp);
            integer x=0;
            for(ReadMembershipDataOutput.MembershipResponseBody each: MembershipList)
            {
                if(each.MemberFieldList!=null && each.MemberFieldList.size()>0)
                {
                    if(tempList!=null && tempList.size()<2)
                    {
                        temp=new innerwrap();
                        temp.name=each.MemberFieldList[0].MemberFieldName.trim();
                        temp.value=each.MemberFieldList[0].MemberFieldvalues.trim();
                        temp.iseditable=isedit.contains(each.MemberFieldList[0].MemberFieldName);
                        tempList.add(temp);
                          
                        x++;
                        if(x==MembershipList.size())
                            MembershipdataList.add(tempList);
                    }
                    else
                    {
                        MembershipdataList.add(tempList);
                        system.debug('--MembershipdataList--2--'+MembershipdataList);
                        tempList=new List<innerwrap>();
                        temp=new innerwrap();
                        temp.name=each.MemberFieldList[0].MemberFieldName.trim();
                        temp.value=each.MemberFieldList[0].MemberFieldvalues.trim();
                        temp.iseditable=isedit.contains(each.MemberFieldList[0].MemberFieldName);
                        tempList.add(temp);
                        
                        x++;
                        if(x==MembershipList.size())
                            MembershipdataList.add(tempList);
                        system.debug('--MembershipdataList--3--'+MembershipdataList);
                    }
                }
            }
        }
    }//not loan product
    }
    public void updateMembership()
    {
        UpdateMembershipDataInput input=new UpdateMembershipDataInput();
        input.Institution_ID=integer.valueof(FAlist[0].Institution_Id__c);
        input.PartnerName=MembershipList[0].PartnerName;
        input.ProductId=MembershipList[0].ProductId;
        input.EntityId=MembershipList[0].EntityId;
        input.UpdatedBy=[select  id,federationidentifier from user where id=:userinfo.getUserId()].federationidentifier;//'d5bffad0-9cd1-4324-8';
        input.DateTimeStamp=string.valueOf(date.today());//'2017-09-20';
        input.Source_System='Salesforce';//'Web';
        input.EntityType=MembershipList[0].EntityType;//'p';
        List<UpdateMembershipDataInput.memberField> UpdateList=new List<UpdateMembershipDataInput.memberField>();
        for(List<innerwrap> eachlist: MembershipdataList)
        {
            for(innerwrap eachinn : eachlist)
            {
                if(eachinn.iseditable)
                {
                    UpdateMembershipDataInput.memberField temp=new UpdateMembershipDataInput.memberField();
                    temp.MemberFieldName=eachinn.name;
                    temp.MemberFieldValues=eachinn.value;
                    UpdateList.add(temp);    
                }
            }
        }
        input.memberFieldList = UpdateList;  
        try{ 
            UpdateMembershipDataOutput output = CSSServiceHelper.UpdateMembershipData(input); 
            system.debug('$%$%$%$'+output);
            if(output.Code=='0')ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Membership data updated successfully'));
            else
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,output.Code+'\n'+output.Description));
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,ex.getmessage()));
        }
    }

    
    public class innerwrap{
        public string name{get;set;}
        public string value{get;set;}
        public boolean iseditable{get;set;}
    }
}