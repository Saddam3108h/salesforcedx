public interface ServiceInterface {
  
  Object performServiceCallout(ServiceSettings__c serviceSettings, Object serviceInput);
  
  //Object getRemoteDetails(ServiceSettings__c serviceSettings, Object remoteInput);
  
}