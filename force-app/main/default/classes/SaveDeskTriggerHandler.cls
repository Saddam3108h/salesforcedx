/**********************************************************************
Name: SaveDeskTriggerHandler
=======================================================================
Purpose: This trigger handler class is created on Savedesk object to handle all the Savedesk related logics.User Story -SFD-1430

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         20-May-2020       Initial Version
2.0			Priyanka Singh		   11-Aug-2020	     Trigger Factory Implementation

**********************************************************************/
public with sharing class SaveDeskTriggerHandler implements ITriggerHandler {
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {        
        if (TriggerSettings__c.getInstance('Execution').SaveDeskQueueExecution__c 
            &&  TriggerSettings__c.getInstance('Execution').OrgExecution__c)
        { 
            return true;
        }
        else
        { 
            return TriggerDisabled;
        }
        
    }
    
    public void BeforeInsert(List<SObject> newItems) {
        
        updateSDeskCustomerAndFAccount(newItems);
        createSaveDeskListView(newItems);
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, SaveDeskQueue__c> newSDItems = (Map<Id, SaveDeskQueue__c>) newItems;  
        updateAttemptField(newSDItems.values());
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterInsert(Map<Id, SObject> newItems) {}
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, SaveDeskQueue__c> newSDItems = (Map<Id, SaveDeskQueue__c>) newItems; 
        SaveDeskTriggerHandler.updateTaskRecord(newSDItems.values());
        SaveDeskTriggerHandler.updateLatestTaskRecord(newSDItems.values());
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update customer, financial account, phone number, Status
fields based on the customer and financial account serno.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    public static void updateSDeskCustomerAndFAccount(List<SaveDeskQueue__c> saveDeskList) {
        try {
            Map<String,Contact> contactMap = new Map<String,Contact>();
            Map<String,Financial_Account__c> financialMap = new Map<String,Financial_Account__c>();
            Set<String> customerSernoSet = new Set<String>();
            Set<String> financialSernoSet = new Set<String>();
            for(SaveDeskQueue__c sdObj : saveDeskList) {
                customerSernoSet.add(sdObj.People_Serno__c);
                financialSernoSet.add(sdObj.Financial_Account_Serno__c);
            }
            for(Contact conObj : [SELECT Id, Name, SerNo__c, MobilePhone 
                                  FROM Contact 
                                  WHERE SerNo__c IN :customerSernoSet]){
                                      contactMap.put(conObj.SerNo__c,conObj);
                                  }
            for(Financial_Account__c finObj: [SELECT Id,Account_Serno__c 
                                              FROM Financial_Account__c 
                                              WHERE Account_Serno__c IN :financialSernoSet]){
                                                  financialMap.put(finObj.Account_Serno__c,finObj);
                                              }
            
            for(SaveDeskQueue__c sdObj : saveDeskList) {
                if(contactMap.containsKey(sdObj.People_Serno__c)) {
                    contact conObj = contactMap.get(sdObj.People_Serno__c);
                    sdObj.Phone_No__c = conObj.MobilePhone;
                    sdObj.Customer_Name__c = conObj.id;
                    sdObj.Customers_Name__c = conObj.Name;
                }
                if(financialMap.containsKey(sdObj.Financial_Account_Serno__c)) {
                    Financial_Account__c finanObj = financialMap.get(sdObj.Financial_Account_Serno__c);
                    sdObj.Financial_Account__c = finanObj.id;
                }
                sdObj.Status__c = (sdObj.Offer_Start_Date__c <= Date.valueOf(system.today().format()) ? 
                                   StaticConstant.SAVEDESK_OPEN_STATUS : StaticConstant.SAVEDESK_TOBESTARTED_STATUS
                                  );
            } 
        } catch(Exception exp) {
            StatusLogHelper.logSalesforceError('SaveDeskTriggerHandler', 'E005', 'updateSDeskCustomerAndFAccount', exp, false);
        }
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to AttemptField based on task list size.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    public static void updateAttemptField(List<SaveDeskQueue__c> saveDeskList) {
        try {
            system.debug('---AttemptField----');
            Set<Id> saveDeskIdSet = new Set<Id>();
            for(SaveDeskQueue__c sdObj : saveDeskList) {
                if(sdObj.Task_Count__c == 1 
                   && sdObj.Calling_Attempts__c != StaticConstant.SAVEDESK_INBOUND) {
                       sdObj.Calling_Attempts__c = StaticConstant.SAVEDESK_ATTEMPT_1;
                   } else if (sdObj.Task_Count__c == 2
                              && sdObj.Calling_Attempts__c != StaticConstant.SAVEDESK_INBOUND) {
                                  sdObj.Calling_Attempts__c = StaticConstant.SAVEDESK_ATTEMPT_2;
                              } else if (sdObj.Task_Count__c == 3
                                         && sdObj.Calling_Attempts__c != StaticConstant.SAVEDESK_INBOUND) {
                                             sdObj.Calling_Attempts__c = StaticConstant.SAVEDESK_ATTEMPT_3;
                                         } else if (sdObj.Task_Count__c > 3
                                                    && sdObj.Calling_Attempts__c != StaticConstant.SAVEDESK_INBOUND){
                                                        sdObj.Calling_Attempts__c = StaticConstant.SAVEDESK_MORE;
                                                    }
                system.debug('--value--'+sdObj.Task_Count__c);
                If(sdObj.Response__c == StaticConstant.SAVEDESK_CALLNOTANSWERED
                   && sdObj.Task_Count__c < 3) {
                       sdObj.Status__c = StaticConstant.SAVEDESK_INPROGRESS_STATUS;
                   } else if (sdObj.Response__c == StaticConstant.SAVEDESK_CALLNOTANSWERED
                              && sdObj.Task_Count__c >= 3) {
                                  sdObj.Status__c = StaticConstant.SAVEDESK_CLOSED_STATUS;
                              } else if(sdObj.Response__c == StaticConstant.SAVEDESK_CALLBACKLATER
                                        && sdObj.Task_Count__c >= 1) {
                                            sdObj.Status__c = StaticConstant.SAVEDESK_INPROGRESS_STATUS;
                                        } else if(sdObj.Task_Count__c != null){
                                            sdObj.Status__c = StaticConstant.SAVEDESK_CLOSED_STATUS;
                                        }
                
                // [Start]Below logic to update the status to closed if calltype is Inbound..
                if(sdObj.Calling_Attempts__c == StaticConstant.SAVEDESK_INBOUND
                   && (sdObj.Response__c == StaticConstant.SAVEDESK_CALLNOTANSWERED
                       || sdObj.Response__c == StaticConstant.SAVEDESK_CALLBACKLATER)
                   && sdObj.Status__c != StaticConstant.SAVEDESK_CLOSED_STATUS) {
                       sdObj.Status__c = StaticConstant.SAVEDESK_CLOSED_STATUS;
                   }
                //[END]
            }
            
        } catch(Exception exp) {
            StatusLogHelper.logSalesforceError('SaveDeskTriggerHandler', 'E005', 'updateAttemptField', exp, false);
        }
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update task related to SaveDesk record.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    public static void updateTaskRecord(List<SaveDeskQueue__c> saveDeskList) {
        try {
            if (!StaticConstant.SAVEDESK_ISRECURSIVE) {
                List<Task> taskList = [SELECT Id, Account_Serno__c, MAH_Serno__c, Customer_Serno__c,
                                       SaveDesk__c, Campaign_Type__c, Offer_Description__c, WhatId,
                                       Attempt__c, Response__c, Description, Comments__c, subject
                                       FROM Task
                                       WHERE CreatedById = :UserInfo.getUserId()
                                       AND CreatedDate = Today
                                       AND CallType != null
                                       AND Comments__c = ''
                                       AND SaveDesk__C = null
                                       ORDER BY 
                                       CreatedDate DESC Limit 1];
                system.debug('---taskList--'+taskList.size());
                
                //Below logic is update the task from Savedesk record.
                List<Task> updateTaskList = new List<Task>();
                if(taskList.size() > 0 ) {
                    for(SaveDeskQueue__c sdObj : saveDeskList) {
                        taskList[0].SaveDesk__c = sdObj.Id;
                        taskList[0].WhoId = sdObj.Customer_Name__c;
                        taskList[0].WhatId = sdObj.Financial_Account__c;
                        taskList[0].Account_Serno__c = sdObj.Financial_Account_Serno__c;
                        taskList[0].Customer_Serno__c = sdObj.People_Serno__c;
                        taskList[0].MAH_Serno__c = sdObj.MAH_Serno__c;
                        taskList[0].Campaign_Type__c = sdObj.Campaign_Type__c;
                        taskList[0].Offer_Description__c = sdObj.Offer_Description__c;
                        system.debug('---Task_Count__c---'+sdObj.Task_Count__c);
                        if(sdObj.Task_Count__c == 0) {
                            taskList[0].Attempt__c = StaticConstant.SAVEDESK_ATTEMPT_1;
                        } else if(sdObj.Task_Count__c == 1){
                            taskList[0].Attempt__c = StaticConstant.SAVEDESK_ATTEMPT_2;
                        } else if(sdObj.Task_Count__c == 2){
                            taskList[0].Attempt__c = StaticConstant.SAVEDESK_ATTEMPT_3;
                        } else if(sdObj.Task_Count__c > 2){
                            taskList[0].Attempt__c = StaticConstant.SAVEDESK_MORE;
                        }
                        taskList[0].Response__c = sdObj.Response__c;
                        taskList[0].Description = sdObj.Comments__c;
                        taskList[0].Comments__c = (sdObj.Comments__c != null 
                                                   && sdObj.Comments__c.length() > 255 ?
                                                   sdObj.Comments__c.substring(0,254) : sdObj.Comments__c
                                                  );
                        taskList[0].subject = StaticConstant.TASK_CALLSAVEDESK_SUBJECT;//'CallSavedesk';
                        updateTaskList.add(taskList[0]);
                    }
                    update updateTaskList;
                }
            }
        } catch(Exception exp) {
            StatusLogHelper.logSalesforceError('SaveDeskTriggerHandler', 'E005', 'updateTaskRecord', exp, false);
        }
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     21-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Create the ListView based on savedesk campaign type before insert the record.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public static void createSaveDeskListView(List<SaveDeskQueue__c> saveDeskList) {
        String sDListViewQuery = 'SELECT Id, Name FROM SaveDeskQueue__c ORDER BY NAME ASC';
        ApexPages.StandardSetController saveDeskSetController = 
            new ApexPages.StandardSetController(Database.getQueryLocator(sDListViewQuery));
        system.debug('----listviews---'+saveDeskSetController.getListViewOptions());
        List<String> listVewLabelList = new List<String>();
        for(SelectOption viewLabel : saveDeskSetController.getListViewOptions()) {
            listVewLabelList.add(viewLabel.getLabel());
        }
        
        List<String> campaignNameList = new List<String>();
        for(SaveDeskQueue__c sdObj : saveDeskList) {
            if (!listVewLabelList.contains(sdObj.Campaign_Type__c)) {
                campaignNameList.add(sdObj.Campaign_Type__c);
            }
        }
        if(campaignNameList.size() > 0) {
            system.debug('-----campaignNameList--'+campaignNameList);
            DynamicListviewController.createListView(campaignNameList);
        }	
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     05-June-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Update Latest task based on savedesk update.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public static void updateLatestTaskRecord(List<SaveDeskQueue__c> saveDeskList) {
        try {
            if (!StaticConstant.SAVEDESK_ISRECURSIVE) {
                system.debug('---inside--if');
                StaticConstant.SAVEDESK_ISRECURSIVE = true;
                Set<Id> saveDeskIdSet = new Set<Id>();
                for(SaveDeskQueue__c sdObj : saveDeskList) {
                    saveDeskIdSet.add(sdObj.Id);
                }
                system.debug('--saveDeskIdSet-'+saveDeskIdSet);
                List<Task> taskList = [SELECT Id, Account_Serno__c, MAH_Serno__c, Customer_Serno__c,
                                       SaveDesk__c, Campaign_Type__c, Offer_Description__c, WhatId,
                                       Attempt__c, Response__c, Description, Comments__c, subject
                                       FROM Task
                                       WHERE SaveDesk__C IN : saveDeskIdSet
                                       ORDER BY 
                                       CreatedDate DESC Limit 1];
                system.debug('---tasklist---'+taskList.size());
                List<Task> updateTaskList = new List<Task>();
                if(taskList.size() > 0 ) {
                    for(SaveDeskQueue__c sdObj : saveDeskList) {
                        taskList[0].SaveDesk__c = sdObj.Id;
                        taskList[0].WhoId = sdObj.Customer_Name__c;
                        taskList[0].WhatId = sdObj.Financial_Account__c;
                        taskList[0].Account_Serno__c = sdObj.Financial_Account_Serno__c;
                        taskList[0].Customer_Serno__c = sdObj.People_Serno__c;
                        taskList[0].MAH_Serno__c = sdObj.MAH_Serno__c;
                        taskList[0].Campaign_Type__c = sdObj.Campaign_Type__c;
                        taskList[0].Offer_Description__c = sdObj.Offer_Description__c;
                        taskList[0].Attempt__c = sdObj.Calling_Attempts__c;
                        taskList[0].Response__c = sdObj.Response__c;
                        taskList[0].Description = sdObj.Comments__c;
                        taskList[0].Comments__c = (sdObj.Comments__c != null 
                                                   && sdObj.Comments__c.length() > 255 ?
                                                   sdObj.Comments__c.substring(0,254) : sdObj.Comments__c
                                                  );
                        taskList[0].subject = StaticConstant.TASK_CALLSAVEDESK_SUBJECT;//'CallSavedesk';
                        updateTaskList.add(taskList[0]);
                    }
                    update updateTaskList;
                }
            }
        } catch(Exception exp) {
            StatusLogHelper.logSalesforceError('SaveDeskTriggerHandler', 'E005', 'updateLatestTaskRecord', exp, false); // Class,method
        }
    }
}