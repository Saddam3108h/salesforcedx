public class PrimeActionOnClosedStatus {
    @AuraEnabled
    Public Static Case getStatus(Id csId){
        Case cse = [Select id, status,ownerId,ContactId from Case where Id =: csId];
        system.debug('------cse----'+cse);
        return cse;
    }
    @AuraEnabled
    Public Static Case updateStatus(Id caseId){
        system.debug('----recordId-----'+caseId);
        Case cs = new Case();
        Case cslist = [Select id, status,ownerId,owner.name from Case where Id =: caseId];
        system.debug('------cslist----'+cslist);
        cs.Id = cslist.Id;
        if((cslist.Status == 'New') && cslist.OwnerId != userInfo.getUserId()){
            System.debug('In if');
            cs.OwnerId = userInfo.getUserId();   
            cs.Status = 'In Progress'; 
            System.debug('=====before modify======' + cslist);
            update cs;
            System.debug('=====After modify1======' + cslist);
            
        } 
        /*if(cslist.Status == 'In Progress' && cslist.OwnerId != userInfo.getUserId()){
system.debug('-----owner----'+cslist.owner.name);
cs.OwnerId = userInfo.getUserId();
system.debug('-----owner-after----'+cs.owner.name);
update cs;
}*/
        
        //update cslist;
        return cslist;
    }
    
    @AuraEnabled
    Public Static Case ownerAssign(Id caseId){
        system.debug('----recordId-----'+caseId);
        Case cs = new Case();
        Case cslist = [SELECT id, status,ownerId,owner.name FROM Case WHERE Id =: caseId];
        system.debug('------cslist----'+cslist);
        cs.Id = cslist.Id;
        if((cslist.Status == 'In Progress' || cslist.Status == 'Closed' || cslist.Status == 'Re Open') && cslist.OwnerId != userInfo.getUserId()){
            system.debug('-----owner----'+cslist.owner.name);
            cs.OwnerId = userInfo.getUserId();
            cs.Status = 'In Progress'; 
            system.debug('-----owner-after----'+cs.owner.name);
            update cs;
        }
        return cslist;
    }
    
    @AuraEnabled
    Public Static Case StatusUpdate(Id caseId){
        system.debug('----recordId-----'+caseId);
        Case sclist = [SELECT Id, status,ownerId,Category__c,ContactId,Financial_Account__c,NonExistingFA__c 
                       FROM Case WHERE Id =: caseId];
        system.debug('------sclist----'+sclist);
        /* Condition of field NonExistingFA__c made as true for SFD-1985. When financial account exists nonexistingfa
will reamin false hence changed third condition */
        if((sclist.Status == 'New' 
            || sclist.Status == 'Re Open' 
            || sclist.Status =='cancelled' 
            || sclist.Status =='In Progress') 
           && sclist.OwnerId == userInfo.getUserId()          
           && (sclist.Category__c != 'None' 
               && ((sclist.Financial_Account__c != null
                    && sclist.ContactId != null
                    && sclist.NonExistingFA__c == false) 
                   || (sclist.Financial_Account__c != null
                    && sclist.ContactId == null
                    && sclist.NonExistingFA__c == false)
                   || (sclist.Financial_Account__c == null
                    && sclist.ContactId == null
                    && (sclist.NonExistingFA__c == false || sclist.NonExistingFA__c == true))
                   || (sclist.ContactId != null 
                       && sclist.Financial_Account__c == null 
                       && sclist.NonExistingFA__c == true)))
          ) {
              Case sc = new Case();
              sc.Id = sclist.Id;
              sc.OwnerId = userInfo.getUserId();
              sc.Status = 'Closed';
              update sc;
          }
        return sclist;
    }
    @AuraEnabled
    Public Static Case StatusUpdates(Id caseId){
        system.debug('----recordId-----'+caseId);
        Case sclist = [Select id, status,ownerId from Case where Id =: caseId];
        system.debug('------sclist----'+sclist);
        if((sclist.Status == 'Closed' || sclist.Status =='Cancelled') && sclist.OwnerId == userInfo.getUserId())
        {
            Case sc = new Case();
            sc.Id = sclist.Id;
            sc.OwnerId = userInfo.getUserId();
            sc.Status = 'Re Open';
            update sc;
        }
        return sclist;
    }
    // Card level prime action
    
    @AuraEnabled
    Public Static Card__C getPeople(Id cardId){
        Card__C crd = [Select id,People__c from Card__c where Id =: cardId];
        system.debug('------fa----'+crd);
        return crd;
    }
    
    // Card level prime action
    
    @AuraEnabled
    Public Static Card__C getFAdetails(Id finId){
        Card__C  crd = [Select id, People__C from Card__C where Financial_Account__c =: finId];
        
        
        system.debug('-----card-----'+crd);
        return crd;
    }
}