/********************************************************************************************
Name: CaseDepartmentChangeHandler
=============================================================================================
Purpose: This Class is called by Process Builder (Case_Close) and it changes the staut to Closed
when coditons are met after created the case.The conditions mentioned on Subject, To emailid,
Category, Department and status.
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                          Date              Detail
1.0        Harisivaramakrishna             06/01/2021       Initial Version
*********************************************************************************************/
public class CaseDepartmentChangeHandler {
    /********************************************************************************************
Function Name: fetchCases
=============================================================================================
Purpose: It fetches the list of cases and existing the custom metadata(Case_Department_Change__mdt),
Then it compare and close the cases.
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                          Date              Detail
1.0        Harisivaramakrishna             06/01/2021       Initial Version
*********************************************************************************************/
    @InvocableMethod
    public static void fetchCases(List<ID> CaseIds ){
        System.debug('*****CaseList***** '+CaseIds);
        List<Case> caseList = [SELECT id,
                               Department__c,
                               Owner_Name__c
                               FROM Case
                               WHERE Case.id in :CaseIds]; 
        
        List<Case_Department_Change__mdt> mdList = new List<Case_Department_Change__mdt>([SELECT Department__c,
                                                                                          Owner__c
                                                                                          FROM Case_Department_Change__mdt]); 
        System.debug('*****allMetadataSet ***** '+mdList);
        List<case> updatecaseLst = new list<Case>();
        
        for(case caseObj :caseList){
            if(caseObj.Owner_Name__c!= NULL){
                for(Case_Department_Change__mdt mdl :mdList){
                    System.debug('*****c.Owner_Name__c *****:'+caseObj.Owner_Name__c);
                    If(mdl.Owner__c == caseObj.Owner_Name__c){
                        System.debug('---deparment--'+mdl.Department__c);
                        caseObj.Department__c=mdl.Department__c;
                        updatecaseLst.add(caseObj);   
                        System.debug('*****Inside if ***** '+caseObj.Owner_Name__c);
                    }
                }
            }
        }
        System.debug('*****Case to be Closed ***** '+updatecaseLst);
        if(!updatecaseLst.isEmpty())
            update updatecaseLst;        
    }
}