public class NewEditAddressController{
    public boolean isEditPage{get;set;}
    public Address__c address{get;set;}
    public Address__c addressold;
    public string peopleId{get;set;}
    public string custserNo{get;set;}
    public string pplssn{get;set;}
    public Integer instId{get;set;}
    public Integer statusCode;
    public NewEditAddressController(ApexPages.StandardController sc) {
        if(ApexPages.currentPage().getParameters().get('id')!=null && ApexPages.currentPage().getParameters().get('id')!='')
        isEditPage = true;
        else
        isEditPage = false;        
        if(isEditPage){
            if(!test.isRunningTest()){
                sc.addfields(new List<String>{'Address_Id__c','City__c','Country__c','Country_Code__c','Location__c','People__c','Street_Address__c','Street_Address_2__c','Street_Address_3__c','Street_Address_4__c','Street_Address_5__c','ZIP__c','people__r.SSN__c','people__r.Institution_Id__c','people__r.Account.Customer_Serno__c','From_Date__c','Until_Date__c','Tel1__c','Tel2__c','people__r.SerNo__c'});
           }
        this.address = (Address__c)sc.getrecord();
        addressold = this.address.clone();
        addressold.id = this.address.id;
        custserNo = this.address.people__r.SerNo__c;
        pplssn = this.address.people__r.ssn__c;
        instId =  (Integer)this.address.people__r.Institution_Id__c;   
        peopleId =  this.address.people__c;
            System.debug('MypeopleId'+peopleId);
        }
        else
        this.address = new Address__c();       
        if(!isEditPage)
        {
            string url = ApexPages.currentPage().getURL();
            string addidp = url.substring(url.indexOf('lkid=')+5);
            peopleId = addidp.substring(0,addidp.indexOf('&'));
            system.debug('peopleId-->'+peopleId);
            this.address.People__c = Id.valueOf(peopleId);
            Contact con = [select Account.Customer_Serno__c,SSN__C,Institution_Id__c,SerNo__c from Contact where Id = :peopleId];
            custserNo = con.SerNo__c;
            pplssn = con.ssn__c;
            instId = (Integer)con.Institution_Id__c;
            if(con.Institution_Id__c != null)
            {
                country_codes__c[] counc = [select id,name,Institution_Id__c from country_codes__c where Institution_Id__c=:con.Institution_Id__c];
                if(counc.size()>0)
                address.Country__c = counc[0].name.touppercase();
            }
            else
            {
                address.Country__c = 'SE';
            }
        }
    }
    
    public string updateAddressByRest(Address__c objAddress)
    {
        Datetime startTime = System.Now();
        HTTPResponse res;
        try
        {

            System.debug('CSS_UpdateContactHelper Contact: ' +objAddress);

            ServiceSettings__c serviceSettings = ServiceSettings__c.getValues('UpdateAddress');

            System.debug('serviceSettings: ' +serviceSettings);
    
            Map<string, string> requestHeader = ServiceHelper.getServiceHeaders(serviceSettings.HeaderName__c);
   
            HttpRequest req = new HttpRequest();
            string strEndpoint;
            if(isEditPage)
            strEndpoint = serviceSettings.EndPoint__c + objAddress.id + '?_HttpMethod=PATCH';
            else
            strEndpoint = serviceSettings.EndPoint__c;
            req.setEndpoint(strEndpoint);

            System.debug('CSS_UpdateContactHelper strEndpoint : ' +strEndpoint);
            
            if(serviceSettings.Timeout__c != null) req.setTimeout(integer.ValueOf(serviceSettings.Timeout__c));

            if(serviceSettings.Operation__c != null) req.setMethod(serviceSettings.Operation__c);

            if(serviceSettings.Compressed__c) req.setCompressed(serviceSettings.Compressed__c);
           
            System.debug('@@UserInfo.getSessionId(): ' +UserInfo.getSessionId());

            //requestHeader.put('Authorization', requestHeader.get('Authorization') + ' '+ UserInfo.getSessionId());


            
            if(requestHeader != null && !requestHeader.isEmpty())

            {
                Set<string> keys = requestHeader.keySet();

                for(string key : keys){

                    req.setHeader(key, requestHeader.get(key));

                }
          
  }
            
        req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());

            req.setHeader('Content-Type', 'application/json');
            /*string addid;
            if(!isEditPage){
            string url = ApexPages.currentPage().getURL();
            string addidp = url.substring(url.indexOf('lkid=')+5);
            addid = addidp.substring(0,addidp.indexOf('&'));
            system.debug('addid-->'+addid);
            }*/
            Map<String,object> mapEmp = new Map<String,object>();

            //mapEmp.put('Deactivation_TimeStamp__c',Date.today());

            mapEmp.put('City__c', objAddress.City__c);
            mapEmp.put('Country__c', objAddress.Country__c);           
            if(objAddress.Country__c=='NO')
            objAddress.Country_Code__c = '578';
            if(objAddress.Country__c=='DK')
            objAddress.Country_Code__c = '208';
            if(objAddress.Country__c=='SE')
            objAddress.Country_Code__c = '752';
            mapEmp.put('Country_Code__c', objAddress.Country_Code__c);            
            mapEmp.put('Location__c', objAddress.Location__c);
            mapEmp.put('Street_Address__c', objAddress.Street_Address__c); 
            mapEmp.put('Street_Address_2__c', objAddress.Street_Address_2__c);
            mapEmp.put('Street_Address_3__c', objAddress.Street_Address_3__c);
            mapEmp.put('Street_Address_4__c', objAddress.Street_Address_4__c);
            mapEmp.put('Street_Address_5__c', objAddress.Street_Address_5__c);           
            mapEmp.put('From_Date__c', objAddress.From_Date__c);
            mapEmp.put('Until_Date__c', objAddress.Until_Date__c);
            mapEmp.put('ZIP__c', objAddress.ZIP__c);
            mapEmp.put('Institution_Id__c', instId);
            // Added by Shameel 24-04-2017
            mapEmp.put('Tel1__c', objAddress.Tel1__c);
            mapEmp.put('Tel2__c', objAddress.Tel2__c);
            // end
            if(!isEditPage)
            mapEmp.put('People__c', peopleId);
                        
            String JSONString = JSON.serialize(mapEmp);

            System.debug('CSS_UpdateContactHelper JSONString : ' +JSONString );

            
            req.setBody(JSONString);

            System.debug('CSS_UpdateContactHelper req: ' +req);

            
             Http http = new Http();

             res = http.send(req);             
             System.debug('CSS_UpdateContactHelper res : ' +res);
             //if(res.getStatusCode()==204)
             
            
        }
        catch(dmlException ex)
        {

            System.debug('UpdateContactHelper Exception: ' + ex.getMessage() + '  ' + ex.getstacktraceString());
            StatusLogHelper.logSalesforceError('updateAddressByRest', 'E005', 'res: ' + res, ex, false);
            //RemoteHelper.logStatus(RemoteHelper.CSS_UpdateContactRest, startTime, System.Now(), JSON.serialize(objContact),  'Exception:--' + ex.getMessage() + '  ' + ex.getstacktraceString());

            //throw ex;

        }
        catch(calloutException ex)
        {

            System.debug('UpdateContactHelper Exception: ' + ex.getMessage() + '  ' + ex.getstacktraceString());
            StatusLogHelper.logSalesforceError('updateAddressByRest', 'E001', 'res: ' + res, ex, false);
            //RemoteHelper.logStatus(RemoteHelper.CSS_UpdateContactRest, startTime, System.Now(), JSON.serialize(objContact),  'Exception:--' + ex.getMessage() + '  ' + ex.getstacktraceString());

            //throw ex;

        }
        catch(Exception ex)
        {

            System.debug('UpdateContactHelper Exception: ' + ex.getMessage() + '  ' + ex.getstacktraceString());
            StatusLogHelper.logSalesforceError('updateAddressByRest', 'E004', 'res: ' + res, ex, false);
            //RemoteHelper.logStatus(RemoteHelper.CSS_UpdateContactRest, startTime, System.Now(), JSON.serialize(objContact),  'Exception:--' + ex.getMessage() + '  ' + ex.getstacktraceString());

            //throw ex;

        }
        statusCode = res.getStatusCode();
        return res.getBody();

    }
    
    public pagereference saveaddress(){
        
       /* if(address.Location__c=='Home')
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Address of type Home cannot be created or edited.');
            ApexPages.addMessage(myMsg);
            return null;
        }*/
        if(address.From_Date__c>address.Until_Date__c)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'From Date cannot be older than Until Date.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        //Location should be unique for each customer : Validation
        /*system.debug('altaf'+isEditpage);
        system.debug('altaf'+address.Location__c);
        System.debug('altaf' +[Select Id,Location__c from Address__c where Location__c =:address.location__c and people__c=:peopleId].size());
        */
        
        // Added by shameel 03-21-2017 to encrypt address type
        set<string> locationSet=new set<string>();
        
        for(Address__c addr:[Select Id,Location__c from Address__c where people__c=:peopleId])
        {   if(addr.Location__c!=null && !isEditPage)
            {
             locationSet.add(addr.Location__c.toLowerCase());
            }
            else if(addr.Location__c!=null && isEditPage && (addr.Id!=address.Id))
            {
             locationSet.add(addr.Location__c.toLowerCase());
            }
        }
        // end
        string locationStr = address.location__c!=null?address.location__c.toLowerCase():'';
        if(!isEditPage && locationSet.contains(locationStr))//[Select Id,Location__c from Address__c where Location__c =:address.location__c and people__c=:peopleId];
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'This customer already has address with this location.');
            ApexPages.addMessage(myMsg);
            return null;
        
        }
        else if(isEditPage && locationSet.contains(locationStr))//[Select Id,Location__c from Address__c where Location__c =:address.location__c and people__c=:peopleId and Id!=:address.Id].size()>0
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'This customer already has address with this location.');
            ApexPages.addMessage(myMsg);
            return null;
        
        }
        string resp = updateAddressByRest(address);
        system.debug('Response updating salesforce-->'+resp);
        NewRecSaveOp nrso;
        try{
            if(!isEditPage || statusCode!=204)//update page and status 204 means request succeeded
            {
                nrso = (NewRecSaveOp)JSON.deserialize(resp,NewRecSaveOp.class);
                this.address.id=nrso.id;
            }    
        }
        catch(dmlException e)
        {
            List<ApiErrorad> err = (List<ApiErrorad>)JSON.deserialize(resp,List<ApiErrorad>.class);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,err[0].message);
            ApexPages.addMessage(myMsg);
            
            StatusLogHelper.logSalesforceError('saveaddress', 'E005', 'resp: ' + resp, e, false);
            return null;
            
        }
        catch(calloutException e)
        {
            List<ApiErrorad> err = (List<ApiErrorad>)JSON.deserialize(resp,List<ApiErrorad>.class);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,err[0].message);
            ApexPages.addMessage(myMsg);
            
            StatusLogHelper.logSalesforceError('saveaddress', 'E001', 'resp: ' + resp, e, false);
            return null;
            
        }
        catch(Exception e)
        {
            List<ApiErrorad> err = (List<ApiErrorad>)JSON.deserialize(resp,List<ApiErrorad>.class);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,err[0].message);
            ApexPages.addMessage(myMsg);
            
            StatusLogHelper.logSalesforceError('saveaddress', 'E004', 'resp: ' + resp, e, false);
            return null;
            
        }
        UpdateCustomerAddressOutput ucso = CSSServiceHelper.UpdateCustomerAddress(instId,address,pplssn);// updatecustomernameaddress service callout to SOA
        if(ucso!=null && ucso.code!='0'){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,CommonHelper.displayMessage(ucso.Code)));
        if(!isEditPage)
        delete new Address__c(id=nrso.Id);
        else
        update addressold;
        return null;
        }
        
        //SOAServiceRetryLogOpsController.insertDataSOAServiceRetryLog();
        if(isEditPage)
        {
            PageReference nextPage = new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
            return nextPage;
        }
        else
        {
            //NewRecSaveOp nrso = (NewRecSaveOp)JSON.deserialize(resp,NewRecSaveOp.class);
            Id i = nrso.Id;
            //update newly created address with Prime serno got from SOA Callout
            if(ucso!=null && ucso.AddressSerno!=null){
            Address__c addrtoupdate = new Address__c(id=i,Address_Id__c=String.valueOf(ucso.AddressSerno));
            update addrtoupdate;
            }
            PageReference nextPage = new PageReference('/' + i);
            return nextPage;
        }
        //return null;
    
    }
    
     private class NewRecSaveOp{
        
        public string Id;
        public string[] errors;
        public string success;
    
    }
    private class ApiErrorad {
        public String errorCode;
        public String message;
        public String[] fields;
    }
        
}