/*
----------------------------------------------------------------------------------------
Author    Date         Description
Shameel   17/01/2017   Output class to implement SOA service using Integration framework.
----------------------------------------------------------------------------------------
*/
public class UpdateConsentServiceOutput{

    public String Code;
    public String Description;
    //To Cover Test Class 100%
    public void test()
    {
        
    }
    
}