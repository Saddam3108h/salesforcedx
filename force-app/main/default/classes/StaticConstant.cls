/********************************************************************************************
Name: EC_StaticConstant
=============================================================================================
Purpose: This Class is used for retrive the Constant values
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                          Date              Detail
1.0        Priyanka Singh              06/05/2020     Initial Version

*********************************************************************************************/

public class StaticConstant {   
    
    public final static String USER_SALESFORCE_SUPPORT = 'Salesforce Support'; // This is to use salesforce Support user
    public final static String ATTACHMENT_USER_COLUMNS = 'Name, Profile Name';
    
    //Below are used for EC_ChangeCaseStatusToClose apex class
    public final static String CASE_STATUS = 'New';
    public final static String CASE_TOADDRESS = 'tekwkorts@entercard.com';
    public final static String CASE_VIEW = 'SE Fund Transfer & Bill Payments';
    public final static String CASE_CATEGORY = 'FT / Bill payment';
    public final static String CASE_SUBJECT = '%Ny överföring%';
    public final static String QUEUE_TYPE ='Queue';
    public final static String QUEUE_NAME = 'SE Customer Service';
    public final static String PROFILE_NAME = 'System Administrator';
    public final static String EMAIL_SUBJECT='Status of chnaging status to closed for fund transfer cases';
    public final static String EMAIL_BODY='The job executed successfully and closed the fund tranfer cases where the case owner is SE Customer Service and older than 30 days.';
    
    public final static String CATEGORY_ACCOUNT_CLOSURE = 'Account closure';
    public final static String CATEGORY_PAYMENT_HOLIDAY = 'Payment Holiday';
    public final static String CATEGORY_CROSSSELL_RESPONSE = 'CrossSell Response';
    public final static String TASK_COMPLETE_STATUS = 'Completed';
    public final static String TASK_CALL_SUBJECT = 'Call';
    public final static String TASK_MEMO_SUBJECT = 'Memo';
    public final static String TASK_CHAT_SUBJECT = 'Chat';
    public final static String TASK_CALLSAVEDESK_SUBJECT = 'CallSavedesk';
    public final static String TASK_INBOUND_CALLTYPE = 'Inbound';
    public final static String TASK_OUTBOUND_CALLTYPE = 'Outbound';
    public final static String TASK_CREATED_MESSAGE = 'Task is created successfully.';
    public final static String CASE_UPDATED_MESSAGE = 'Case is updated Successfully.';
    public final static String TASK_DELETE_MESSAGE = 'You can not delete Task.';
    
    //NONE, OTHER, YES, NO values
    public final static String NONE_VALUE = '--None--';
    public final static String OTHER_VALUE = 'Other';
    public final static String YES_VALUE = 'Yes';
    public final static String NO_VALUE = 'No';
    public final static String NONE_VAL = 'None';
    
    //Savedesk object related values.
    public final static String SAVEDESK_OPEN_STATUS = 'Open';
    public final static String SAVEDESK_CLOSED_STATUS = 'Closed';
    public final static String SAVEDESK_INPROGRESS_STATUS = 'In Progress';
    public final static String SAVEDESK_TOBESTARTED_STATUS = 'To be Started';
    public final static String SAVEDESK_ATTEMPT_1 = 'Attempt-1';
    public final static String SAVEDESK_ATTEMPT_2 = 'Attempt-2';
    public final static String SAVEDESK_ATTEMPT_3 = 'Attempt-3';
    public final static String SAVEDESK_MORE = 'More';
    public final static String SAVEDESK_INBOUND = 'Inbound Call';
    public final static String SAVEDESK_CALLNOTANSWERED = 'Call not answered';
    public final static String SAVEDESK_CALLBACKLATER = 'Callback Later';
    public final static String SAVEDESK = 'SaveDeskQueue__c.';
    public final static String SAVEDESK_CAMPAIGN_TYPE = 'Campaign_Type__c';
    public final static String SAVEDESK_COMMENT = 'Closed by System';
    public final static String SAVEDESK_STATUS = 'status__c';
    public final static String SAVEDESK_FILTERSCOPE = 'Everything';
    public static Boolean SAVEDESK_ISRECURSIVE = false; 
    
    // Zisson
    public static String ZISSON_NAME = 'zissonCallCenter';
    
    // for Integartion
    public static String GRANT_TYPE_CLIENTCRED = 'grant_type=client_credentials';
    public static String GET_METHOD = 'GET';
    public static String AUTHORIZATION = 'Authorization';
    
    
    //Profile names
    public final static String DK_OPERATION = 'DK - Operations';
    public final static String NO_OPERATION = 'NO - Operations';
    public final static String SE_OPERATION = 'SE - Operations';
    public final static String SE_NORWAYEMAILACCESS = 'SE -Operations/NorwayEmailAccess';
    public final static String SE_VBC = 'SE VBC';
    
    //Fundstransfer object 
    public final static String PROCESSED_DELETE_ERROR = 'Processed record can not be deleted';
    public final static String TRANSACTION_DELETE_ERROR = 'This transaction is under process by SAS and cannot be deleted';
    public final static String PROCESSED_UPDATE_ERROR = 'Processed record can not be modified';
    public final static String TRANSACTION_UPDATE_ERROR = 'This transaction is under process by SAS and cannot be modified';
    
}