public class CaseExt {
    public List<Case> caseList {get;set;}
    List<Case> caseToUpdate = new List<Case>();
    public CaseExt(ApexPages.StandardSetController controller) {
        caseList= (List<Case>)controller.getSelected();
        System.debug('@@@@Selected Case size ==> '+caseList.size());
    }
    public PageReference assignCases(){
        if(caseList.size() == 0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select atleast one case to change owner'));
        }else if(caseList.size() > 0){
            for(Case cs : caseList){
                cs.OwnerId = UserInfo.getUserId();
                cs.Status = 'In Progress'; 
                caseToUpdate.add(cs);
            }
        }
        
        if(caseToUpdate.size() > 0){
            update caseToUpdate;
        }
        
        
        Schema.DescribeSObjectResult result = Case.SObjectType.getDescribe();
        PageReference pageRef = new PageReference('/' + result.getKeyPrefix());      
        return pageRef;
        
    }
}