/**********************************************************************
Name: DynamicListviewMockController
=======================================================================
Purpose: This class is created for cover the DynamicListviewController test class .User Story -SFD-1489

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         20-May-2020       Initial Version

**********************************************************************/
global class DynamicListviewMockController implements WebServiceMock {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1489
*   Param: None
*   Return: None    
*   Description: In this method we are trying to get the response after created the listview record. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    global void doInvoke(Object stub, Object request, Map<String, Object> response, 
                         String endpoint, String soapAction, String requestName, 
                         String responseNS,String responseName,String responseType
                        ) {
                            MetadataService.createMetadataResponse_element response_x = new MetadataService.createMetadataResponse_element();
                            List<MetadataService.Error> errList = new List<MetadataService.Error>();
                            MetadataService.Error errObj = new MetadataService.Error();
                            errObj.message = 'Error';
                            errObj.statusCode = 'E006';
                            errList.add(errObj);
                            MetadataService.SaveResult[] resultList = new List<MetadataService.SaveResult>();
                            MetadataService.SaveResult outputResult = new MetadataService.SaveResult();
                            outputResult.fullName = 'Testing';
                            outputResult.success = false;
                            outputResult.errors = errList;
                            resultList.add(outputResult);
                            response_x.result = resultList;
                            response.put('response_x', response_x);
                        }
}