@isTest
public class FAExtension_test {
    
    @testSetup
    public static void testInsertData1() 
    {
         TriggerSettings__c triggerObj = new TriggerSettings__c();
            triggerObj.Name = 'Execution';
            triggerObj.CustomerExecution__c = false;
            triggerObj.OrgExecution__c = false;
            insert triggerObj;
     Account accountObj=new Account(
      Name='Test Account', 
      Institution_Id__c=2, 
      Customer_Serno__c='1123123'
      );
      insert accountObj;
      
      Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
      string encryptSSN=EncodingUtil.convertToHex(hash);
      Contact contactObj =new Contact(
      LastName='Test Contact', 
      FirstName='First Test Contact',
      SerNo__c='2123123', 
      Institution_Id__c=2, 
      SSN__c='0000000061',
      Encrypted_SSN__c=encryptSSN,
      AccountId=accountObj.Id,
      Phone='123123123', 
      Fax='98789879', 
      MobilePhone='98789879', 
      HomePhone='123123123', 
      Email='testemail@test.com'
      );
      insert contactObj;
      
      Blob hash2 = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
      string encryptSSN2=EncodingUtil.convertToHex(hash2);
      Contact contactObj1 =new Contact(
      LastName='1Test Contact', 
      FirstName='1First Test Contact',
      SerNo__c='12123123', 
      Institution_Id__c=2, 
      SSN__c='0000000062',
      Encrypted_SSN__c=encryptSSN2,
      AccountId=accountObj.Id,
      Phone='1123123123', 
      Fax='198789879', 
      MobilePhone='198789879', 
      HomePhone='1123123123', 
      Email='1testemail@test.com'
      );
      insert contactObj1;
        
      Product_Custom__c prodobj=new Product_Custom__c(
      Name='Test Product',
      Pserno__c='56789'
      );
      insert prodobj;
      
      Financial_Account__c finAccObj=new Financial_Account__c(
      Account_Number__c='12w23eeeeedd', 
      Customer__c=accountObj.Id, 
      Account_Serno__c='3123123', 
      Institution_Id__c=2,
      Customer_Serno__c=contactObj.SerNo__c, 
      Product__c=prodobj.Id,
      Product_Serno__c=prodobj.Pserno__c
      );
      insert finAccObj;
        
      Card__c cardObj=new Card__c(
      People__c=contactObj.Id,
      PrimaryCardFlag__c=true,
      Financial_Account__c=finAccObj.Id, 
      Card_Serno__c='524858519', 
      Card_Number_Truncated__c='7136785481583561', 
      Financial_Account_Serno__c=finAccObj.Id, 
      People_Serno__c=contactObj.Id,
      Prod_Serno__c=prodobj.Pserno__c, 
      Product__c=prodobj.Id,
      Institution_Id__c=2
      );
      insert cardObj;
      
      Case caseObj=new Case(
      ContactId=contactObj.Id, 
      AccountId=accountObj.Id, 
      Card__c=cardObj.Id,
      Financial_Account__c=finAccObj.Id, 
      Category__c='Account closure', 
      Origin='Web'
      );
      insert caseObj;
        
        //GlobalSettings__c
        List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.Name = 'ServiceStrategies'; 
        objGlobal.Value__c = 'SOAPService';
        GlobalSettingsList.add(objGlobal);
        
        insert GlobalSettingsList;
        
         List<ServiceHeaders__c> serviceHeadersList =new List<ServiceHeaders__c>();
        //ServiceHeaders
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'GetCustomerDetailsHeader';
        serviceHeadersList.add(objServiceHeaders);
        
        ServiceHeaders__c objServiceHeaders2 = new ServiceHeaders__c();
        objServiceHeaders2.name = 'UpdateCustomerDataHeader';
        serviceHeadersList.add(objServiceHeaders2);
        
        ServiceHeaders__c objServiceHeaders3 = new ServiceHeaders__c();
        objServiceHeaders3.name = 'GetLoanHeader';
        serviceHeadersList.add(objServiceHeaders3);
        
        insert serviceHeadersList;
        
        List<ServiceSettings__c> serviceSettingList = new List<ServiceSettings__c>();
         //ServiceSettings__c
        ServiceSettings__c objServiceSettings = new ServiceSettings__c();
        objServiceSettings.Name = 'GetCustomerDetails';
        objServiceSettings.HeaderName__c = 'GetCustomerDetailsHeader'; 
        objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings.OutputClass__c = 'GetCustomerOutput';
        objServiceSettings.ProcessingClass__c = 'GetCustomerCallout'; 
        objServiceSettings.Strategy__c = 'SOAPService';
        objServiceSettings.Input_Class__c = 'GetCustomerInput'; 
        objServiceSettings.Username__c='Username';
        objServiceSettings.Password__c='Password';
        serviceSettingList.add(objServiceSettings);
        
        
        ServiceSettings__c objServiceSettings2 = new ServiceSettings__c();
        objServiceSettings2.Name = 'UpdateCustomerData';
        objServiceSettings2.HeaderName__c = 'UpdateCustomerDataHeader'; 
        objServiceSettings2.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings2.OutputClass__c = 'UpdateCustomerDataOutput';
        objServiceSettings2.ProcessingClass__c = 'UpdateCustomerDataCallout'; 
        objServiceSettings2.Strategy__c = 'SOAPService';
        objServiceSettings2.Input_Class__c = 'UpdateCustomerDataInput'; 
        objServiceSettings2.Username__c='Username';
        objServiceSettings2.Password__c='Password';
        serviceSettingList.add(objServiceSettings2);
        
        ServiceSettings__c objServiceSettings3 = new ServiceSettings__c();
        objServiceSettings3.Name = 'GetLoans';
        objServiceSettings3.HeaderName__c = 'GetLoanHeader'; 
        objServiceSettings3.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings3.OutputClass__c = 'GetLoanServiceOutput';
        objServiceSettings3.ProcessingClass__c = 'GetLoanServiceCallout'; 
        objServiceSettings3.Strategy__c = 'SOAPService';
        objServiceSettings3.Input_Class__c = 'GetLoanServiceInput'; 
        objServiceSettings3.Username__c='Username';
        objServiceSettings3.Password__c='Password';
        serviceSettingList.add(objServiceSettings3);
        
        insert serviceSettingList;
        
        List<Common_Settings__c> commonSettingList =new List<Common_Settings__c>();
        Common_Settings__c commonSetting =new Common_Settings__c(
        Name='CorrelationId',
        Common_Value__c='1'
        );
        commonSettingList.add(commonSetting);
        Common_Settings__c commonSetting1 =new Common_Settings__c(
        Name='RequestorId',
        Common_Value__c='Salesforce'
        );
        commonSettingList.add(commonSetting1);
        Common_Settings__c commonSetting2 =new Common_Settings__c(
        Name='SystemId',
        Common_Value__c='1'
        );
        commonSettingList.add(commonSetting2);
        Common_Settings__c commonSetting3 =new Common_Settings__c(
        name='Loan Products',Common_Value__c='240,260,280,320'
        );
        commonSettingList.add(commonSetting3);
        
        insert commonSettingList;
        
        ErrorCodes__c errorCodes =new ErrorCodes__c();
        errorCodes.Name='0';
        errorCodes.ErrorDescription__c='ErrorDescription';
        errorCodes.UIDisplayMessage__c='UIDisplayMessage';
        insert errorCodes;
        
        ErrorCodes__c errorCodes1 =new ErrorCodes__c();
        errorCodes1.Name='defaultError';
        errorCodes1.ErrorDescription__c='ErrorDescription';
        errorCodes1.UIDisplayMessage__c='UIDisplayMessage';
        insert errorCodes1;
        
    }
    
    
 public static testmethod void testMethod1() {
    Financial_Account__c fc =[SELECT Id, Name, Account_Number__c,Account_Serno__c,Institution_Id__c FROM Financial_Account__c limit 1];
    ApexPages.StandardController sc = new ApexPages.StandardController(fc);
    test.startTest();
    Test.setMock(WebServiceMock.class, new GetCustomerWebServiceMockclass());
    PageReference pageRef = Page.FA_Status_and_Balance;
    Test.setCurrentPage(pageRef);
    FAExtension fa=new FAExtension(sc);
    fa.MTP=1;
    fa.doCall();
    fa.doCancel();  
    fa.updateMTP(); 
    test.stopTest();
    }
    
    public static testmethod void testMethod2() {
    Financial_Account__c fc =[SELECT Id, Name, Account_Number__c,Account_Serno__c,Institution_Id__c FROM Financial_Account__c limit 1];
    Test.setMock(WebServiceMock.class, new MockResponseClass());
    ApexPages.StandardController sc = new ApexPages.StandardController(fc);
    test.startTest();
    PageReference pageRef = Page.FA_Status_and_Balance;
    Test.setCurrentPage(pageRef);
    FAExtension fa=new FAExtension(sc);
    fa.MTP=1;
   // fa.doCall();
    fa.updateMTP();
    test.stopTest();
    }
    
    public static testmethod void testMethod3() {
    Financial_Account__c fc =[SELECT Id, Name, Account_Number__c,Account_Serno__c,Institution_Id__c FROM Financial_Account__c limit 1];
    Test.setMock(WebServiceMock.class, new MockResponseClass());
    ApexPages.StandardController sc = new ApexPages.StandardController(fc);
    test.startTest();
    PageReference pageRef = Page.FA_Status_and_Balance;
    Test.setCurrentPage(pageRef);
    FAExtension fa=new FAExtension(sc);
    fa.MTP=1;
    fa.getLoanDetails();
    FAExtension.LoansWrapper temp=new FAExtension.LoansWrapper('1000','10000','10','2000','2017-09-09','0000309861','NORM');
    fa.loansWrapperList.add(temp);
    test.stopTest();
    }

}