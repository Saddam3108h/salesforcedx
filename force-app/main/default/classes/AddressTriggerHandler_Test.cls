/**********************************************************************
Name: AddressTriggerHandler_Test
=======================================================================
Purpose: This Test class is used to cover the AddressTriggerHandler class.
User Story: SFD-1610
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author               Date              Detail
1.0         HSR Krishna          29-July-2020      Initial Version

**********************************************************************/
@isTest(SeeAllData = false)
public class AddressTriggerHandler_Test {
    /**********************************************************************
*   Author: HSR Krishna
*   Date:     29-July-2020
*   User Story : SFD-1610
*   Param: None
*   Return: None    
*   Description: In this method we are trying to insert the new contact and address to cover the code.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  
**********************************************************************/
    static testmethod void addressTriggerTest(){
        Test.starttest();   
        try{
            System.runAs(UnitTestDataGenerator.adminUser) {
                Country_Codes__c countryCode = UnitTestDataGenerator.TestCountryCodes.buildInsert(new Map<String, Object>{
                });
                Country_Codes__c countryCode1 = UnitTestDataGenerator.TestCountryCodes.buildInsert(new Map<String, Object>{
                    'Country_Code__c' =>'8',
                        'Name'=>'ALBANIA'
                        });
                
                
                Contact ContactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                    'SSN__c'=>'6309030010'
                        });
                Address__c AddrObj = UnitTestDataGenerator.TestAddress.buildInsert(new Map<String, Object>{
                    'addresstype__c'=>'11111',
                        //'Country__c'=>'se',  
                        // 'Country_Code__c'=>'752',
                        //'ZIP__c'=>'',
                        'Tel1__c'=>'0461188809',
                        'Tel2__c'=>'0461188809',
                        'People__c'=>ContactObj.Id
                        });
                AddressTriggerHandler AddTHadlr = new AddressTriggerHandler();
                AddTHadlr.populateAddress(new List<Address__c>{AddrObj}); 
                System.assertNotEquals(AddrObj.People__c, null);   
            }
        }catch(Exception exp){
            // StatusLogHelper.logSalesforceError('EmailHandler', 'defaultError', 'Error in SSN linking to the Case', exp, false);
        }
        Test.stopTest();
    }
    
    
    static testmethod void addressTriggerUpdate(){  
        Test.starttest();   
        try{
            System.runAs(UnitTestDataGenerator.adminUser) {
                Contact ContactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                    'SSN__c'=>'6309030010'
                        });
                Address__c AddrObj = [select Id,Invoice__c,Other__c,Country__c FROM Address__c WHERE  Other__c = false LIMIT 1];
                AddrObj.Other__c =TRUE;  
                update AddrObj;
                
                Country_Codes__c countryCode = UnitTestDataGenerator.TestCountryCodes.buildInsert(new Map<String, Object>{
                    'Country_Code__c' =>'4',
                        'Name'=>'AFGHANISTAN'	
                        });
                Country_Codes__c countryCode1 = UnitTestDataGenerator.TestCountryCodes.buildInsert(new Map<String, Object>{
                    'Country_Code__c' =>'8',
                        'Name'=>'ALBANIA'
                        });
                AddressTriggerHandler AddTHadlr = new AddressTriggerHandler();
                AddTHadlr.populateAddress(new List<Address__c>{AddrObj}); 
                // System.assertNotEquals(AddrObj.Other__c, null);   
            }
        }catch(Exception exp){
            // StatusLogHelper.logSalesforceError('EmailHandler', 'defaultError', 'Error in SSN linking to the Case', exp, false);
        }
        Test.stopTest();
    }
    
    
}