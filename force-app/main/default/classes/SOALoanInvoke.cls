//Generated by wsdl2apex

public class SOALoanInvoke {
    public class x_xsoap_CSSServiceESB_CSSServicePT {
        public String endpoint_x = 'http://eclvmidapp04t.ectest.local:7032/CSSService/ProxyServices/CSSServicePS';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public SOALoanSecurityHeader.Security_element SecurityPart;
        private String SecurityPart_hns = 'Security=http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        private String[] ns_map_type_info = new String[]{'http://schemas.entercard.com/service/CSSService/1.0', 'SOALoanInvoke', 'http://schemas.entercard.com/service/CSSServiceResponse/1.0', 'SOALoanResponse', 'http://schemas.entercard.com/header/1.0', 'SOALoanHeader', 'http://schemas.entercard.com/TYPELIB/1.0', 'SOALoanTypelib', 'http://schemas.entercard.com/service/CSSServiceRequest/1.0', 'SOALoanRequest', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'SOALoanSecurityHeader'};
        public SOALoanResponse.GetLoanAccountsInfoResponse_element getLoanAccountsInfo(SOALoanTypelib.headerType Header,SOALoanRequest.GetLoanAccountsInfoRequestBodyType GetLoanAccountsInfoRequestBody) {
            SOALoanRequest.GetLoanAccountsInfoRequest_element request_x = new SOALoanRequest.GetLoanAccountsInfoRequest_element();
            request_x.Header = Header;
            request_x.GetLoanAccountsInfoRequestBody = GetLoanAccountsInfoRequestBody;
            SOALoanResponse.GetLoanAccountsInfoResponse_element response_x;
            Map<String, SOALoanResponse.GetLoanAccountsInfoResponse_element> response_map_x = new Map<String, SOALoanResponse.GetLoanAccountsInfoResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'getLoanAccountsInfo',
              'http://schemas.entercard.com/service/CSSServiceRequest/1.0',
              'GetLoanAccountsInfoRequest',
              'http://schemas.entercard.com/service/CSSServiceResponse/1.0',
              'GetLoanAccountsInfoResponse',
              'SOALoanResponse.GetLoanAccountsInfoResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}