public class TransactionDetailsContrl {
    
    @AuraEnabled
    Public Static List<DCMS_Transaction__c> getTrans(Id CasId){
        List<DCMS_Transaction__c> TransList = [Select id,AuthDateTime__c, Name, Truncated_Card_Number__c,TransactionAmount__c,BillingAmount__c,TransactionCurrency__c,Account_Serno__c,MerchTypeMCC__c,MerchName__c,MerchID__c,MerchCountry__c,MerchCity__c,PosEntryMode__c,Chargeback__c,Representment__c,Arbitration__c,WriteOff__c,FraudWriteOff__c,Recovery__c from DCMS_Transaction__c where CaseNumber__c = :CasId];
        return TransList;
    }
    
    @AuraEnabled
    Public Static String deleteTxns(List<String> lstRecordId){
        Id CsownerId;
        List <String> oErrorMsg = new List <String>();
        system.debug('----Txns-Id----'+lstRecordId);
        List<DCMS_Transaction__c> lstDeletRec = [Select id,Chargeback__c, Representment__c,Arbitration__c, WriteOff__c, FraudWriteOff__c,  Recovery__c, CaseNumber__r.OwnerId from DCMS_Transaction__c where Id IN: lstRecordId];
        if(lstDeletRec.size() > 0){
            for(DCMS_Transaction__c txns : lstDeletRec){
                CsownerId = txns.CaseNumber__r.OwnerId;
                if(txns.Chargeback__c == true || txns.Representment__c == true || txns.Arbitration__c == true || txns.WriteOff__c == true || txns.FraudWriteOff__c == true || txns.Recovery__c == true){
                    txns.Chargeback__c = false;
                    txns.Representment__c = false;
                    txns.Arbitration__c = false;
                    txns.WriteOff__c = false;
                    txns.FraudWriteOff__c = false;
                    txns.Recovery__c = false;
                }
            }
        }else{
            oErrorMsg.add(Label.No_Transaction_got_selected);
        }
        if(CsownerId == userInfo.getUserId()){
            update lstDeletRec;
            system.debug('------txns-update--'+lstDeletRec);
            Database.DeleteResult[] DleRec = Database.delete(lstDeletRec, false);
            
            for(Database.DeleteResult dr : DleRec){
                if(dr.isSuccess()){
                    oErrorMsg.add(Label.Del);
                    system.debug('successful delete Txns');
                } else {
                    oErrorMsg.add('');
                    for (Database.Error err: dr.getErrors()) {
                        
                        oErrorMsg.add(err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
            }
        } else {
            oErrorMsg.add(Label.Your_not_owner_of_the_Case);
        }
        return oErrorMsg[0];
    }
    
    @AuraEnabled
    Public Static List<String> updateTxns(Id TxnId, String chekboxval){
        Boolean flag =true;
        Id CsownerId;
        List <String> oErrorMsg = new List <String>();
        system.debug('--TxnId---'+TxnId);
        system.debug('--chekboxval---'+chekboxval);
        
        List<DCMS_Transaction__c> txnlist = [SELECT id,Chargeback__c, Representment__c,Arbitration__c,
                                             WriteOff__c, FraudWriteOff__c,  Recovery__c, 
                                             CaseNumber__r.OwnerId FROM DCMS_Transaction__c 
                                             WHERE Id = :TxnId];
        system.debug('---txnlist--'+txnlist);
        
        for(DCMS_Transaction__c txn : txnlist){
            CsownerId = txn.CaseNumber__r.OwnerId;
            if(CsownerId != userInfo.getUserId()){
                oErrorMsg.add(Label.Sorry_you_are_not_the_owner_of_Case_Please_accept_the_case_and_try_again);
                break;
            }
            
            if(chekboxval == Label.cb 
               && txn.Chargeback__c == false 
               && txn.Recovery__c == false){
                   txn.Chargeback__c = true; 
               } else if(chekboxval == Label.cb
                         && txn.Chargeback__c == true 
                         && txn.Representment__c == false 
                         && txn.Recovery__c == false)
               {
                   txn.Chargeback__c = false;
               }
            system.debug('--Representment__c---'+txn.Representment__c + + '---chekboxval---'+chekboxval);
            if(chekboxval == Label.REP 
               &&  txn.Representment__c == false 
               && txn.Chargeback__c == true 
               && txn.Recovery__c == false)
            {
                txn.Representment__c = true;  
            } else if(chekboxval == Label.REP  
                      &&  txn.Representment__c == true 
                      && txn.Arbitration__c == false 
                      && txn.Recovery__c == false)
            {
                txn.Representment__c = false; 
            } else if(chekboxval == Label.REP  
                      &&  txn.Representment__c == true 
                      && txn.Arbitration__c == true 
                      && txn.Chargeback__c == true 
                      && txn.Recovery__c == false)
            {
                flag = false;
                oErrorMsg.add(Label.Sorry_Not_Possible_to_unselect_REP_if_ARB_is_selected);
            }else if((chekboxval == Label.REP 
                      && txn.Representment__c == false 
                      && txn.Chargeback__c == false) 
                     && txn.Recovery__c == false)
            {
                flag = false;
                oErrorMsg.add(Label.Sorry_you_can_not_select_REP_without_CB);
            } else if(chekboxval == Label.cb 
                      && txn.Chargeback__c == true 
                      && (txn.Representment__c == true 
                          || txn.Arbitration__c == true) 
                      && txn.Recovery__c == false)
            {
                flag = false;
                oErrorMsg.add(Label.Sorry_Not_Possible_to_uncheck_if_REP_ARB_is_checked);
            }
            if(chekboxval == Label.ARB 
               && txn.Arbitration__c == false 
               && txn.Representment__c == true 
               && txn.Chargeback__c == true && 
               txn.Recovery__c == false)
            {
                txn.Arbitration__c = true; 
            } else if(chekboxval == Label.ARB
                      && txn.Arbitration__c == true 
                      && txn.Representment__c == true 
                      && txn.Chargeback__c == true 
                      && txn.Recovery__c == false)
            {
                txn.Arbitration__c = false;
            }
            else if(chekboxval == Label.ARB 
                    && txn.Arbitration__c == false 
                    && (txn.Representment__c == false 
                        || txn.Chargeback__c == false)
                    && txn.Recovery__c == false)
            {
                flag = false;
                oErrorMsg.add(Label.Sorry_You_can_not_select_ARB_without_CB_and_REP);
            }

            if(chekboxval == Label.WO && txn.WriteOff__c == false && (txn.FraudWriteOff__c == false && txn.Recovery__c == false)){
               txn.WriteOff__c = true; 
            } else if(chekboxval == Label.WO && txn.WriteOff__c == true && (txn.FraudWriteOff__c == false && txn.Recovery__c == false)){
                txn.WriteOff__c = false;
            }  else if(chekboxval == Label.WO && (txn.FraudWriteOff__c == true || txn.Recovery__c == true)){
                flag = false;
                oErrorMsg.add(Label.Sorry_FWO_or_Rec_flag_should_be_unchecked_before_check_WO);
            }
            system.debug('----write---'+txn.WriteOff__c);
            if(chekboxval == Label.FWO && txn.FraudWriteOff__c == false && (txn.WriteOff__c == false && txn.Recovery__c == false)){
                txn.FraudWriteOff__c = true;
            } else if(chekboxval == Label.FWO && txn.FraudWriteOff__c == true && (txn.WriteOff__c == false && txn.Recovery__c == false)){
                txn.FraudWriteOff__c = false;
            } else if(chekboxval == Label.FWO && (txn.WriteOff__c == true || txn.Recovery__c == true)){
                flag = false;
                oErrorMsg.add(Label.Sorry_WO_or_Rec_flag_should_be_unchecked_before_check_FWO);
            }
            
            if(chekboxval == Label.REC 
               && txn.Recovery__c == false 
               && (txn.Chargeback__c == false  
                   && txn.Representment__c == false 
                   && txn.Arbitration__c == false 
                   && txn.FraudWriteOff__c == false
                   && txn.WriteOff__c == false))
            {
                txn.Recovery__c = true;
            } else if(chekboxval == Label.REC 
               && txn.Recovery__c == true 
               && (txn.Chargeback__c == false  
                   && txn.Representment__c == false 
                   && txn.Arbitration__c == false 
                   && txn.FraudWriteOff__c == false
                   && txn.WriteOff__c == false)){
                txn.Recovery__c = false;
            } else if(chekboxval == Label.REC 
               && (txn.Chargeback__c == true  
                   || txn.Representment__c == true 
                   || txn.Arbitration__c == true 
                   || txn.FraudWriteOff__c == true
                   || txn.WriteOff__c == true))
            {
                flag = false;
                oErrorMsg.add(Label.Sorry_All_flags_should_be_unchecked_before_check_REC);
            }
        }
        
        if((chekboxval == Label.cb 
            || chekboxval == Label.REP 
            || chekboxval == Label.ARB
            || chekboxval == Label.FWO
            || chekboxval == Label.WO
            || chekboxval == Label.REC  
            || chekboxval == Label.CHL) 
           && flag && CsownerId == userInfo.getUserId())
        {
            Database.SaveResult[] UpdtRec = Database.update(txnlist, false);
            for(Database.SaveResult dr : UpdtRec){
                if(dr.isSuccess()){
                    system.debug('successful update txns');
                } else {
                    oErrorMsg.add('');
                    for (Database.Error err: dr.getErrors()) {
                        
                        oErrorMsg.add(err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
            }
        } 
        system.debug('-----txnlist-after---'+txnlist);
        return oErrorMsg;
    }
    @AuraEnabled
    public Static Decimal getGross(ID CasId)
    {
        DCMS_FraudCase__c fruadlist = [Select id, TotalFraudAmt__c from DCMS_FraudCase__c where Case_Number__c = :CasId];
        return fruadlist.TotalFraudAmt__c;
    }
    
    @AuraEnabled
    public Static List<Decimal> getTotalRisk(ID CasId)
    {
        List<Decimal> amnt = new List<Decimal>();
        DCMS_FraudCase__c res=[Select id, TotalCb__c,FraudWO__c,NonFraudWO__c,TotalFraudAmt__c,TotalRefund__c,TotalToCh__c,TotalAtRisk__c from DCMS_FraudCase__c where Case_Number__c = :CasId];
        system.debug('--res--'+res);
        amnt.add(res.TotalAtRisk__c);
        amnt.add(res.TotalCb__c);
        amnt.add(res.TotalRefund__c);
        amnt.add(res.TotalToCh__c);
        amnt.add(res.NonFraudWO__c);
        amnt.add(res.FraudWO__c);
        system.debug('-----amnt-----'+amnt);
        return amnt;        
    } 
    
    @AuraEnabled
    Public Static String updateChliable(Id caseId, Decimal chLiableValue){
        system.debug('----Case-Id----'+caseId);
        try{
            List<DCMS_FraudCase__c> dcmsFraudCaseList = [Select Id,TotalToCh__c,NonFraudWO__c,FraudWO__c,TotalRefund__c,
                                                         Case_Number__r.ownerId, TotalAtRisk__c, TotalFraudAmt__c, TotalCb__c
                                                         from DCMS_FraudCase__c WHERE Case_Number__c = :caseId];
            system.debug('--dcmsFraudCaseList---'+dcmsFraudCaseList[0].NonFraudWO__c);
            List<DCMS_Transaction__c> tranObjsList = [SELECT Id,BillingAmount__c,WriteOff__c,FraudWriteOff__c, Chargeback__c,Recovery__c
                                                      FROM DCMS_Transaction__c 
                                                      WHERE CaseNumber__c = :caseId
                                                      AND (WriteOff__c = true 
                                                           OR FraudWriteOff__c = true
                                                           OR Chargeback__c = true
                                                           OR Recovery__c = true
                                                          )
                                                     ];
            Decimal totalWriteOffAmount = 0.00;
            Decimal totalFraudWriteOffAmount = 0.00;
            Decimal totalRecoveryAmount = 0.00;
            Decimal recoveryChLiableAmount = 0.00;
            Decimal remainingAmount = 0.00;
            Decimal deductedAmount = 0.00;
            for(DCMS_Transaction__c transObj : tranObjsList){
                if(transObj.WriteOff__c == true){
                    totalWriteOffAmount =  totalWriteOffAmount+transObj.BillingAmount__c;
                }
                if(transObj.FraudWriteOff__c == true){
                    totalFraudWriteOffAmount =  totalFraudWriteOffAmount+transObj.BillingAmount__c;
                }
                if(transObj.Recovery__c == true){
                   totalRecoveryAmount = totalRecoveryAmount+transObj.BillingAmount__c;
                }
            }
            List<DCMS_FraudCase__c> updateFraudCaseChList = new List<DCMS_FraudCase__c>();
            if(dcmsFraudCaseList.size() > 0
               && chLiableValue != null){
                   if(dcmsFraudCaseList[0].Case_Number__r.ownerId != userInfo.getUserId()){
                       return 'Please accept the case before enter the C/H Liable amount';
                   }else if(dcmsFraudCaseList[0].TotalFraudAmt__c < chLiableValue){
                       dcmsFraudCaseList[0].TotalToCh__c = 0.00;
                       updateFraudCaseChList.add(dcmsFraudCaseList[0]);
                       return 'CH Liable amount should be less than or equal to gross amount.';
                   } else {
                       dcmsFraudCaseList[0].TotalToCh__c = chLiableValue;
                       if(chLiableValue != 0){
                           recoveryChLiableAmount = chLiableValue + totalRecoveryAmount;
                           remainingAmount = totalWriteOffAmount - recoveryChLiableAmount;
                           dcmsFraudCaseList[0].NonFraudWO__c = remainingAmount;
                           if(remainingAmount < 0 && totalFraudWriteOffAmount != 0.00){
                               deductedAmount = -1*remainingAmount;
                               if(deductedAmount >= 0){
                                   dcmsFraudCaseList[0].FraudWO__c =  totalFraudWriteOffAmount - deductedAmount;  
                               } else {
                                   dcmsFraudCaseList[0].FraudWO__c = totalFraudWriteOffAmount; 
                               }
                           } else {
                               dcmsFraudCaseList[0].FraudWO__c = totalFraudWriteOffAmount; 
                           }
                       } else if(chLiableValue == 0.00 && totalRecoveryAmount != 0.00) {
                           recoveryChLiableAmount = chLiableValue + totalRecoveryAmount;
                           remainingAmount = totalWriteOffAmount - recoveryChLiableAmount;
                           dcmsFraudCaseList[0].NonFraudWO__c = remainingAmount;
                           if(remainingAmount <= 0 && totalFraudWriteOffAmount != 0.00){
                               deductedAmount = -1*remainingAmount;
                               if(deductedAmount >= 0){
                                   dcmsFraudCaseList[0].FraudWO__c =  totalFraudWriteOffAmount - deductedAmount;
                               } else {
                                   dcmsFraudCaseList[0].FraudWO__c =  totalFraudWriteOffAmount; 
                               }
                           }else{
                               dcmsFraudCaseList[0].FraudWO__c =  totalFraudWriteOffAmount; 
                           }
                       } else if(chLiableValue == 0.00 && totalRecoveryAmount == 0.00){
                           dcmsFraudCaseList[0].NonFraudWO__c = totalWriteOffAmount;
                           dcmsFraudCaseList[0].FraudWO__c = totalFraudWriteOffAmount;
                       }
                       if(dcmsFraudCaseList[0].FraudWO__c < 0){
                           dcmsFraudCaseList[0].FraudWO__c = 0.00;
                       }
                       if(dcmsFraudCaseList[0].NonFraudWO__c < 0){
                           dcmsFraudCaseList[0].NonFraudWO__c = 0.00;
                       }
                       
                       updateFraudCaseChList.add(dcmsFraudCaseList[0]);
                   }
                   if(updateFraudCaseChList.size() > 0){
                       update updateFraudCaseChList; 
                   }
               }
        }catch(Exception e){
            StatusLogHelper.logSalesforceError('updateChLiable', 'E004', 'Error in ChLiableAmount', e, false);
        }
        return null;
    }
    
    @AuraEnabled
    Public Static String getCaseOwnerId(Id CasId){
        String caseOwnerId = [Select id, OwnerId from Case where id = :CasId].OwnerId;
        return caseOwnerId;
    }
}