/**********************************************************************
Name:CardTriggerHandler_Test
=======================================================================
Purpose: This is the test class for 'CardTriggerHandler' apex code coverage
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date             CreatedforJira       Detail
1.0         Gagan Pathak          04-Jan-2021          SFD-1890            Initial Version
**********************************************************************/
@isTest(SeeAllData = false)
public class CardTriggerHandler_Test {
    /**********************************************************************
*   Author: Gagan Pathak
*   Date:   02-Dec-2020
*   User Story : SFD-1826
*   Param: None
*   Return: None    
*   Description:In this method we are trying cover the code coverage to update shadow customer field with shadow customer Id before creating the card.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  
**********************************************************************/   
    static testmethod void updateshadowcustomerTest(){
        
        Test.startTest();
        
        System.runAs(UnitTestDataGenerator.adminUser){
            
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});      
            
            Account accountObj=UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{});
            
            Individual individual =UnitTestDataGenerator.TestIndividual.buildInsert(new Map<String, Object>{});
            
            Contact contactObj =UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id,
                    'IndividualId'=>individual.Id,
                    'RecordTypeId'=>Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Shadow Contact RT').getRecordTypeId()
                    });                       
            
            Product_Custom__c prodobj=UnitTestDataGenerator.TestProductCustom.buildInsert(new Map<String, Object>{});
            
            Financial_Account__c finAccObj=UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id, 
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            
            Card__c cardObj=UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Financial_Account_Serno__c'=>finAccObj.Id, 
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id,
                    'Customer_Shadow__c'=>contactObj.Id
                    });
            
            System.assertEquals(true, cardObj.PrimaryCardFlag__c,'PrimaryCardFlag is not correct');
        }        
        
        Test.stopTest();
    }
    
    
    /**********************************************************************
*   Author: Gagan Pathak
*   Date:   02-Dec-2020
*   User Story : SFD-1826
*   Param: None
*   Return: None    
*   Description:In this method we are trying cover the code coverage for create the shadow customer with recordType "Shadow Customer RT" after 
creating the card. 
**********************************************************************/  
    static testmethod void createshadowcustomerTest(){
        
        Test.startTest();
        
        System.runAs(UnitTestDataGenerator.adminUser){
            
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            
            Account accountObj=UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{});
            
            Individual individual =UnitTestDataGenerator.TestIndividual.buildInsert(new Map<String, Object>{});
            
            Contact contactObj =UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id,
                    'IndividualId'=>individual.Id
                    });
            
            Product_Custom__c prodobj=UnitTestDataGenerator.TestProductCustom.buildInsert(new Map<String, Object>{
                'BrandLev2__c'=>'ISB'// The Product BrandLev2 field value is used for creating the Card with new product. 
                    });
            
            Financial_Account__c finAccObj=UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id, 
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            Card__c cardObj=UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Financial_Account_Serno__c'=>finAccObj.Id, 
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id					                   
                    });
            
            System.assertEquals(true, cardObj.PrimaryCardFlag__c,'PrimaryCardFlag is not correct');
        }
        
        Test.stopTest();
    }
}