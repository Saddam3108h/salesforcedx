//This class provides logic for inbound just-in-time provisioning of single sign-on users in your Salesforce organization.
global class ECSAMLAuthHandler implements Auth.SamlJitHandler 
{
    private class JitException extends Exception{}
    
    private void handleUser(boolean create, User u, Map<String, String> attributes, String federationIdentifier, boolean isStandard) 
    {
        if(attributes != null)
        {
            for(string key : attributes.keyset())
            {
                System.debug('@@Key: ' +key);
                System.debug('@@Value: ' +attributes.get(key));
                
                if(create && key.contains('emailaddress')) 
                {
                    u.Username = attributes.get(key) + '.ec';
                }
                if(create) 
                {
                    if(key.contains('UserPrincipalName')) 
                    {
                        u.FederationIdentifier = attributes.get(key);
                    } else 
                    {
                        u.FederationIdentifier = federationIdentifier;
                    }
                }
                if(key.contains('HomePhone')) {
                    u.Phone = attributes.get(key);
                }
                if(key.contains('emailaddress')) {
                    u.Email = attributes.get(key);
                }
                if(key.contains('givenname')) {
                    u.FirstName = attributes.get(key);
                }
                if(key.contains('surname')) {
                    u.LastName = attributes.get(key);
                }
                if(key.contains('Title')) {
                    u.Title = attributes.get(key);
                }
                if(key.contains('company')) {
                    u.CompanyName = attributes.get(key);
                }
                if(key.contains('StreetAddress')) {
                    u.Street = attributes.get(key);
                }
                if(key.contains('State')) {
                    u.State = attributes.get(key);
                }
                if(key.contains('City')) {
                    u.City = attributes.get(key);
                }
                if(key.contains('PostalCode')) {
                    u.PostalCode = attributes.get(key);
                }
                if(key.contains('Country')) {
                    u.Country = attributes.get(key);
                }
                if(key.contains('MobilePhone')) {
                    u.MobilePhone = attributes.get(key);
                }
                if(key.contains('department')) {
                    u.Department = attributes.get(key);
                }
                if(key.contains('EmployeeNumber')) {
                    u.EmployeeNumber = attributes.get(key);
                }
                if(key.contains('role')) 
                {
                    String userRole = attributes.get(key);
                    if(userRole.contains('Agent')) {
                        Id callCenterId = [SELECT Id, Name, InternalName FROM CallCenter 
                                           WHERE InternalName =: StaticConstant.ZISSON_NAME].Id;
                        u.CallCenterId = callCenterId;
                    }
                    List<UserRole> lstRoles = [SELECT Id, Name, developername FROM UserRole WHERE Name =:userRole];            
                    If(lstRoles != null && lstRoles.size() > 0)
                    {
                        u.UserRoleId = lstRoles[0].Id;
                        System.debug('@@Role: ' +u.UserRoleId);
                        
                        //Role_Profile_Mapping__c objMapping = Role_Profile_Mapping__c.getValues(userRole);
                        RDPC_Mapping__c objMapping = RDPC_Mapping__c.getValues(lstRoles[0].developername);
                        
                        If(objMapping != null && objMapping.Profile__c != null)
                        {
                            boolean mapProfile = true;
                            List<Profile> lstProfileIds = [SELECT Id FROM Profile WHERE Name = :Label.Lbl_SAS_Profile or Name = :Label.Lbl_SOA_Profile or Name = :Label.Lbl_Splunk_Profile or Name = :Label.Lbl_System_Admin];
                            System.debug('@@lstProfileIds : ' +lstProfileIds);
                            System.debug('@@objMapping.Profile__c : ' +objMapping.Profile__c);
                            
                            If(create == false && objMapping.Profile__c == Label.Lbl_BA_Profile)
                            {
                                System.debug('@@create : ' +create );
                                System.debug('@@u : ' +u);
                                
                                for(Profile objProfile : lstProfileIds)
                                {
                                    If(u.ProfileId == objProfile.Id)
                                    {
                                        mapProfile = false;
                                        System.debug('@@mapProfile = false');
                                        break;
                                    }
                                }
                            }
                            
                            If(mapProfile)
                            {
                                string ProfileName = (string)objMapping.Profile__c;
                                List<Profile> lstProfile = [SELECT Id FROM Profile WHERE Name =:ProfileName];
                                If(lstProfile != null && lstProfile.size() > 0)
                                {
                                    u.ProfileId = lstProfile[0].Id;
                                }
                            }
                        }
                    }
                }                
            }
        }
        

        u.ReceivesAdminInfoEmails = false;
        
        u.ReceivesInfoEmails = false;
        
        String uid = UserInfo.getUserId();
        //Profile p1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        //User currentUser = [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE ProfileId = :p1.Id and IsActive = true limit 1];
        User currentUser = [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id = :uid];
        
        u.LocaleSidKey = currentUser.LocaleSidKey;
        
        u.LanguageLocaleKey = currentUser.LanguageLocaleKey;
        
        if(create) 
        {
            String alias = '';
            if(u.FirstName == null) {
                alias = u.LastName;
            } else {
                alias = u.FirstName.charAt(0) + u.LastName;
            }
            if(alias != '' && alias.length() > 5) {
                alias = alias.substring(0, 5);
            }
            u.Alias = alias;
        }
        
        u.TimeZoneSidKey = currentUser.TimeZoneSidKey;
        
        u.EmailEncodingKey = currentUser.EmailEncodingKey;        
        
        u.UserPermissionsKnowledgeUser = true;
        
        u.UserPreferencesPreviewLightning = false;
        
        u.UserPermissionsSupportUser = true;
        
        u.UserPermissionsMobileUser= true;
        
        u.EmailPreferencesAutoBcc = false;
        
        //u.CallCenterId = '';        
        //Handle custom fields here

        if(!create) {
            update(u);
        }
    }

    private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) 
    {
        if(communityId != null || portalId != null) {
            handleUser(create, u, attributes, federationIdentifier, false);
        } else {
            handleUser(create, u, attributes, federationIdentifier, true);
        }
    }

    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) 
    {
        User u = new User();
        System.debug('@samlSsoProviderId: ' +samlSsoProviderId);
        System.debug('@communityId: ' +communityId);
        System.debug('@portalId: ' +portalId);
        System.debug('@federationIdentifier: ' +federationIdentifier);
        System.debug('@attributes: ' +attributes);
        System.debug('@assertion: ' +assertion);
        handleJit(true, u, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        return u;
    }

    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) 
    {
        User u = [SELECT Id, FirstName, LastName, ProfileId, UserRoleId, ContactId FROM User WHERE Id=:userId];
        
        System.debug('@userId: ' +userId);
        System.debug('@samlSsoProviderId: ' +samlSsoProviderId);
        System.debug('@communityId: ' +communityId);
        System.debug('@portalId: ' +portalId);
        System.debug('@federationIdentifier: ' +federationIdentifier);
        System.debug('@attributes: ' +attributes);
        System.debug('@assertion: ' +assertion);
        /*
        If(attributes != null)
        {
            for(string key : attributes.keyset())
            {
                System.debug('@key: ' +key);
                System.debug('@value: ' +attributes.get('emailaddress'));
            }
        }*/
        handleJit(false, u, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
    }
}