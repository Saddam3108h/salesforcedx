/********************************************************************************************
Name: AlertSchedular
=============================================================================================
Purpose: To cover the deselect alert whose due date is passed
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author               Date         Created for Jira     Detail
1.0        Gagan                 SFD-984   		    Intial version

********************************************************************************************/
public class AlertSchedular implements Schedulable {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     
*   User Story : 
*   Param: None
*   Return: None    
*   Description: In this method we deselect alert
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public void execute(SchedulableContext sc) {
        try{
            // list of inactive alerts whose due date is not null 
            List<Alert__c> alertLstToUpdate = new List<Alert__c>();
            for(Alert__c alert: [SELECT Id,createdDate,DueDate__c,Active__c 
                                 FROM Alert__c WHERE DueDate__c !=null AND DueDate__c<=today AND Active__c = true])
            {
                alert.Active__c = false;
                alertLstToUpdate.add(alert);
                
            }
            if(alertLstToUpdate.size()>0){
                update alertLstToUpdate;
            }
            
            
        } catch(Exception exp){
            StatusLogHelper.logSalesforceError('AlertSchedularController', 'execute', 
                                               'Error deactivate', exp, false);
        }
    }
}