/**********************************************************************
Name: DynamicListviewController_Test
=======================================================================
Purpose: This Test class is used to cover the DynamicListviewController class - User Story -SFD-1489

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         20-May-2020       Initial Version

**********************************************************************/
@isTest(SeeAllData = false)
public class DynamicListviewController_Test {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to create the listview data and use the mock class to cover the createListView method. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testMethod void createListViewTest() {
        String sDListViewQuery = 'SELECT Id, Name FROM SaveDeskQueue__c ORDER BY NAME ASC';
        ApexPages.StandardSetController saveDeskSetController = 
            new ApexPages.StandardSetController(Database.getQueryLocator(sDListViewQuery));
        List<String> listVewLabelList = new List<String>();
        for(SelectOption viewLabel : saveDeskSetController.getListViewOptions()) {
            listVewLabelList.add(viewLabel.getLabel());
        }
        
        DynamicListviewController.createListView(listVewLabelList);
        
        Test.StartTest();
        Test.setMock(WebServiceMock.class, new DynamicListviewMockController());
        
        List<MetadataService.ListView> metaDatalistViewList = new List<MetadataService.ListView>();
        MetadataService.ListView listView = new MetadataService.ListView();
        listView.fullName = 'Test';
        listView.label = 'Test';
        List<MetadataService.ListViewFilter> filterListToAdd = new List<MetadataService.ListViewFilter>();
        
        MetadataService.ListViewFilter filter = new MetadataService.ListViewFilter();
        filter.field = 'CAMPAIGN_TYPE';
        filter.operation = 'equals';
        filter.value = 'Test';
        filterListToAdd.add(filter);
        MetadataService.ListViewFilter filter1 = new MetadataService.ListViewFilter();
        filter1.field = 'Status__C';
        filter1.operation = 'notEqual';
        filter1.value = 'Closed';
        filterListToAdd.add(filter1);                
        
        listView.filters = filterListToAdd;
        listView.filterScope = StaticConstant.SAVEDESK_FILTERSCOPE;
        listView.columns = new List<String> {'NAME','Customer_Name__c','Financial_Account__c','Interest_Rate__c',
            'Product_Name__c','Deadline_Date_Campaign__c','Offer_Description__c','Phone_No__c','Calling_Attempts__c',
            'Comments__c','Response__c','Agent_ID__c','Status__c'
            };
                
                metaDatalistViewList.add(listView);
        
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        List<MetadataService.SaveResult> results = service.createMetadata(metaDatalistViewList);
        system.assertEquals('Test', listView.fullName);
        DynamicListviewController.handleSaveResults(results);
        Test.StopTest();
    }        
}