public class ViewTransCallout extends SoapIO{
    
    SOAPCSSServiceRequest.getTransactionRequestRecordType[]  transactionRequestRecordTypeInstlist ;
    SOAPCSSServiceRequest.getTransactionRequestRecordType  transactionRequestRecordTypeInst;
    SOAPCSSServiceRequest.Filter_element  filetrelements;
    ViewTransactionsHistoryInput txinput;
    public  void convertInputToRequest(){
    
         txinput = (ViewTransactionsHistoryInput)serviceInput;
        transactionRequestRecordTypeInstlist = new List<SOAPCSSServiceRequest.getTransactionRequestRecordType>();
        
        filetrelements = new SOAPCSSServiceRequest.Filter_element();
        //filetrelements.CardNo =txinput.CardNo;
        //filetrelements.Include=txinput.Include;
        
        transactionRequestRecordTypeInst = new SOAPCSSServiceRequest.getTransactionRequestRecordType();
        transactionRequestRecordTypeInst.InstitutionId=txinput.InstitutionId;
        transactionRequestRecordTypeInst.Reference=txinput.Reference;
        transactionRequestRecordTypeInst.PageSize=txinput.PageSize;
        transactionRequestRecordTypeInst.SkipIndex=txinput.SkipIndex;
        transactionRequestRecordTypeInst.From_x=txinput.From_x;
        transactionRequestRecordTypeInst.To=txinput.To;
        transactionRequestRecordTypeInst.Number_x=txinput.Number_x;
        transactionRequestRecordTypeInst.TransactionType=txinput.TransactionType;
        //transactionRequestRecordTypeInst.Filter=filetrelements;
        transactionRequestRecordTypeInstlist.add(transactionRequestRecordTypeInst);
        
    }
    
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        SOAPschemasEntercardComTypelib10.headerType HeadInstance=new SOAPschemasEntercardComTypelib10.headerType();
        HeadInstance.MsgId='Consuming gettransaction SOA service';
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=string.valueof(txinput.InstitutionId);//'2';
        SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance= new SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=30000;
        // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('viewTransactionDetails');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
        //String username = 'evry_access';
        //String password = '9oKuwQioQ4';
        
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken creds=new SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken();
        creds.Username=serviceObj.Username__c; //'css_soa';//'evry_access';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';//'9oKuwQioQ4';
        SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element security_ele=new SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOAPCSSServiceResponse.ViewTransactionResponse_element  transactionResponse_elementinstance;
        transactionResponse_elementinstance =invokeInstance.viewTransactions(HeadInstance,transactionRequestRecordTypeInstlist);
        system.debug('transactionResponse_elementinstance -->'+transactionResponse_elementinstance);
        return transactionResponse_elementinstance;
     }  
     
    public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
         if(response==null)
           return null;
        SOAPCSSServiceResponse.ViewTransactionResponse_element  transactionResponse_elementinstance = (SOAPCSSServiceResponse.ViewTransactionResponse_element)response;
        SOAPCSSServiceResponse.viewTransactionResponseRecordType   transactionResp = transactionResponse_elementinstance.ViewTransactionResponseRecord;
        SOAPCSSServiceResponse.viewTransactionType[] transtype = transactionResp.Transaction_x;
        
        // Custom output class initialization, this will be the return value of the method
        ViewTransactionsHistoryOutput transactionHistoryOutput=new ViewTransactionsHistoryOutput();
        transactionHistoryOutput.Number_x =transactionResp.Number_x;
        transactionHistoryOutput.TotalNoOfTx =transactionResp.TotalNoOfTx;
        
        if(transtype!=null){
            transactionHistoryOutput.Transaction_x = new  List<ViewTransactionsHistoryOutput.viewTransactionType>();
            for(SOAPCSSServiceResponse.viewTransactionType transtypelist: transtype)// parsing and storing addresses
            {
               ViewTransactionsHistoryOutput.viewTransactionType transactiontypelist = new ViewTransactionsHistoryOutput.viewTransactionType();
               transactiontypelist.TxSerno=transtypelist.TxSerno;
               transactiontypelist.Sequence=transtypelist.Sequence;
               transactiontypelist.PostDate=transtypelist.PostDate;
               transactiontypelist.ReasonCode=transtypelist.ReasonCode;
               transactiontypelist.TxAmount=transtypelist.TxAmount;
               transactiontypelist.TxDate=transtypelist.TxDate;
               transactiontypelist.TxCurrency=transtypelist.TxCurrency;
               transactiontypelist.BillingAmount=transtypelist.BillingAmount;
               transactiontypelist.BillingCurrency=transtypelist.BillingCurrency;
               transactiontypelist.Text=transtypelist.Text;
               transactiontypelist.OrigMsgType=transtypelist.OrigMsgType;
               transactiontypelist.TruncatedCardNo=transtypelist.TruncatedCardNo;
               transactiontypelist.CanBeSplitted=transtypelist.CanBeSplitted;
               transactionHistoryOutput.Transaction_x.add(transactiontypelist);
            }
       }

      return transactionHistoryOutput;

    }
     
     }