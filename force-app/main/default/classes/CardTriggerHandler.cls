/**********************************************************************
Name: CardTriggerHandler
=======================================================================
Purpose: This trigger handler class is created on Card object to handle all the Card related logics. User Story SFD-1755

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.1         Saddam Hussain       04-August-2020       
**********************************************************************/
public with sharing class CardTriggerHandler implements ITriggerHandler {
    public static Boolean TriggerDisabled = false;
    
    /*
Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {
        
        if (TriggerSettings__c.getInstance('Execution').CardExecution__c // false
            &&  TriggerSettings__c.getInstance('Execution').OrgExecution__c){ 
                return true;
            } else { 
                return TriggerDisabled;
            }
        
    }
    
    public void BeforeInsert(List<SObject> newItems) {
        updateProductOnCard(newItems);
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterInsert(Map<Id, SObject> newItems) {
        Map<Id, Card__C> newCardItems = (Map<Id, Card__C>) newItems;
        //Map<Id, Card__C> oldCardItems = (Map<Id, Card__C>) oldItems; 
        createShadowCustomer(newCardItems.values(), null);
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, Card__C> newCardItems = (Map<Id, Card__C>) newItems;
        Map<Id, Card__C> oldCardItems = (Map<Id, Card__C>) oldItems; 
        createShadowCustomer(newCardItems.values(), oldCardItems);
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems) {}    
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     16-Oct-2020
*   User Story : SFD-1755
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update shadow customer and product fields with shadow customer Id before creating the card. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    Public static void updateProductOnCard(List<Card__C> cardList){
        try{
            system.debug('--cardList---'+cardList);
            Set<Id> finIdSet = new Set<Id>();
            Set<Id> CustIdSet = new Set<Id>();
            for(Card__c crdObj : cardList){
                if(crdObj.Financial_Account__c != null){
                    finIdSet.add(crdObj.Financial_Account__c);
                } 
                if(crdObj.People__c != null){
                    CustIdSet.add(crdObj.People__c);
                }
            }
            List<Contact> mainContactList = [SELECT Id, IndividualId FROM Contact WHERE Id IN : CustIdSet];
            Map<Id, Financial_Account__C> finAccountMap = new Map<Id, Financial_Account__C>([SELECT Id, Product__c, BrandLev2__c FROM Financial_Account__c
                                                                                             WHERE Id IN : finIdSet]);
            List<Contact> shadowCustomerList;
            if(mainContactList.size() > 0){
                shadowCustomerList = [SELECT Id, IndividualId, Product_Group_2__c FROM Contact
                                      WHERE IndividualId =: mainContactList[0].IndividualId
                                      AND RecordType.Name = 'Shadow Contact RT'];
            }
            if(finAccountMap.size() > 0){
                for(Card__c objCard : cardList){
                    objCard.Product__c = finAccountMap.get(objCard.Financial_Account__c).Product__c;
                    if(shadowCustomerList.size() > 0){
                        for(contact shadowCons : shadowCustomerList){
                            if(finAccountMap.get(objCard.Financial_Account__c).BrandLev2__c == shadowCons.Product_Group_2__c){
                                objCard.Customer_Shadow__c =  shadowCons.Id;
                                break;
                            }
                        }
                    }
                }
            }
        } catch(Exception exp){
            StatusLogHelper.logSalesforceError('CardTriggerHandler', 
                                               'E005', 'updateProductOnCard', exp, false);
        }  
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     16-Oct-2020
*   User Story : SFD-1755
*   Param: None
*   Return: None    
*   Description: In this method we are trying to create the shadow customer with recordType "Shadow Customer RT" after 
creat the card. 
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    Public static void createShadowCustomer(List<Card__C> cardList, Map<Id, Card__c> cardOldMap){
        try{
            Set<Id> custIdSet = new Set<Id>();
            for(Card__c crdObj : cardList){
                if(crdObj.People__C != null){
                    custIdSet.add(crdObj.People__C);
                }
            }
            Map<Id, Contact> contactMap;
            if(custIdSet.size() > 0){
                contactMap = new Map<Id, Contact>([SELECT Id, Email, LastName, FirstName,AccountId, SSN__C, 
                                                   Serno__C,MobilePhone,IndividualId,Internal_UID__c,Shadow_Contact_Flag__c
                                                   FROM Contact
                                                   WHERE Id IN: custIdSet
                                                   AND RecordTypeId = null]); 
            }
            system.debug('---conmap---'+contactMap);
            Map<String, Contact> shadowContactsToInsertMap = new Map<String, Contact>();
            for(Card__c objCard : cardList){
                if(contactMap.size() > 0){
                    if(objCard.Customer_Shadow__c == null
                       && objCard.BrandLev2__c != null
                       && (objCard.Product__c != null 
                           || objCard.Product__c != cardOldMap.get(objCard.Id).Product__c)
                      ){
                          system.debug('----if condition---');
                          String UID = contactMap.get(objCard.People__c).Serno__C +' '+ objCard.BrandLev2__c;
                          if(!shadowContactsToInsertMap.containsKey(UID)){
                              Contact con = new Contact();
                              con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Shadow Contact RT').getRecordTypeId();
                              con.LastName = contactMap.get(objCard.People__c).LastName;
                              con.FirstName = contactMap.get(objCard.People__c).FirstName;
                              con.AccountId =  contactMap.get(objCard.People__c).AccountId;
                              con.Email =  contactMap.get(objCard.People__c).Email;
                              con.MobilePhone =  contactMap.get(objCard.People__c).MobilePhone;
                              con.IndividualId =  contactMap.get(objCard.People__c).IndividualId;
                              con.Product_Group_2__c = objCard.BrandLev2__c;
                              con.Internal_UID__c = UID;
                              con.Shadow_Contact_Flag__c = true;
                              con.Main_Customer_record__c = contactMap.get(objCard.People__c).Id;
                              
                              ShadowContactsToInsertMap.put(con.Internal_UID__c, con);
                          }
                      }
                }
            }
            if(ShadowContactsToInsertMap.size() > 0){
                Schema.SObjectField ExtFieldName = Contact.Fields.Internal_UID__c;
                database.upsert(ShadowContactsToInsertMap.values(), ExtFieldName, false);
                
                List<Card__c> shadowCustomerLinkToCardList = new List<Card__C>();
                for(Card__c procCrds : cardList){
                    
                    Card__c crds = new Card__c(Id = procCrds.Id);
                    String UID = contactMap.get(procCrds.People__c).SerNo__c + ' ' + procCrds.BrandLev2__c;
                    system.debug('----UID---'+UID);
                    if(ShadowContactsToInsertMap.containsKey(UID)){
                        crds.Customer_Shadow__c = ShadowContactsToInsertMap.get(UID).Id;
                        shadowCustomerLinkToCardList.add(crds);
                    }  
                }
                if(shadowCustomerLinkToCardList.size() > 0){
                    update shadowCustomerLinkToCardList;
                }
            }
        }catch(Exception exp){
            StatusLogHelper.logSalesforceError('CardTriggerHandler', 
                                               'E005', 'createShadowCustomer', exp, false);
        } 
    }
}