/**********************************************************************
Name:CaseTriggerHandler_Test
=======================================================================
Purpose: This is the test class for 'CaseTriggerHandler' apex code coverage
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date            CreatedforJira  Detail
1.0         Kakasaheb Ekshinge       13-Aug-2020     SFD-1596        Initial Version
**********************************************************************/
@IsTest(seeallData=false)
public class CaseTriggerHandler_Test{
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     12-OCT-2020
*   User Story : SFD-1596
*   Param: None
*   Return: None    
*   Description:In this method we are trying to linkFinAccountCustomer, before case inserted.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]  
**********************************************************************/    
    static testmethod void  createFraudCasesTest(){
        
        System.runAs(UnitTestDataGenerator.adminUser){
            test.startTest();            
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            DCMS_CaseStatus__c cstatus = UnitTestDataGenerator.testDCMS_Case.buildInsert();
            List<DCMS_CaseStatus__c> dcmsCaseStatusList = new List<DCMS_CaseStatus__c>();
            dcmsCaseStatusList.add(cstatus);
            List<String>  dcmsCaseStatusAttr = new List<String> {};
                List<DCMS_CaseStatus__c> caseStatusReload = UnitTestDataGenerator.testDCMS_Case.reloadList(dcmsCaseStatusList, dcmsCaseStatusAttr);   
            DCMS_Decision__c desObj = UnitTestDataGenerator.testDCMSDecision.buildInsert();
            List<DCMS_Decision__c> dcmsDecisionList = new List<DCMS_Decision__c>();
            dcmsDecisionList.add(desObj);
            List<String>  dcmsDecisionAttr = new List<String> {};
                List<DCMS_Decision__c> dcmsDecisionReload = UnitTestDataGenerator.testDCMSDecision.reloadList(dcmsDecisionList, dcmsDecisionAttr);
            
            DCMS_FraudRing__c fring = UnitTestDataGenerator.testFraudRing.buildInsert(); 
            List<DCMS_FraudRing__c> dcmsfringList = new List<DCMS_FraudRing__c>();
            dcmsfringList.add(fring);
            List<String>  dcmsfringAttr = new List<String> {};
                List<DCMS_FraudRing__c> dcmsfringReload = UnitTestDataGenerator.testFraudRing.reloadList(dcmsfringList, dcmsfringAttr);
            
            DCMS_Queue_Settings__c qsetting = UnitTestDataGenerator.testQueueSettings.buildInsert();
            List<DCMS_Queue_Settings__c> dcmsQueueList = new List<DCMS_Queue_Settings__c>();
            dcmsQueueList.add(qsetting);
            List<String>  dcmsQueueAttr = new List<String> {};
                List<DCMS_Queue_Settings__c> dcmsQueueReload = UnitTestDataGenerator.testQueueSettings.reloadList(dcmsQueueList, dcmsQueueAttr);
            Account accountObj= UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c'=>'1123123'
                    });
            
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj =UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123', 
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id
                    });
            
            
            Product_Custom__c prodobj=UnitTestDataGenerator.TestProductCustom.buildInsert(new Map<String, Object>{      
            });
            Financial_Account__c finAccObj=UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd',
                    'Customer__c'=>accountObj.Id, 
                    'Account_Serno__c'=>'31231', 
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            
            Card__c cardObj=UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Financial_Account_Serno__c'=>'31231', 
                    'People_Serno__c'=>'2123123',
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id
                    });
            
            
            Case caseObj=UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{ 
                'ContactId'=>contactObj.Id, 
                    'AccountId'=>accountObj.Id, 
                    'Card__c'=>cardObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Category__c'=>'Account closure', 
                    'Origin'=>'Web',
                    'status'=>'Closed',
                    'Dispute_Type__c'=>'0:lost',
                    'Description'=>'Kundens personnummer (10 siffror utan streck) 0000000061'
                    });           
            System.assertEquals('Closed',caseObj.status,'The Status is not correct'); 
            test.stopTest();
        }
    }
    
    Static testmethod void updateFraudCasesTest(){        
        System.runAs(UnitTestDataGenerator.adminUser){
            test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account accountObj=UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c'=>'1123123'
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj =UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123', 
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id
                    });
            
            Product_Custom__c prodobj=UnitTestDataGenerator.TestProductCustom.buildInsert(new Map<String, Object>{      
            });
            
            Financial_Account__c finAccObj=UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd', 
                    'Customer__c'=>accountObj.Id, 
                    'Account_Serno__c'=>'3163123', 
                    'Customer_Serno__c'=>accountObj.Customer_Serno__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            Card__c cardObj=UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Financial_Account_Serno__c'=>finAccObj.Account_Serno__c, 
                    'People_Serno__c'=>contactObj.SerNo__c,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id
                    });
            
            
            Case caseObj=UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{ 
                'ContactId'=>contactObj.Id, 
                    'AccountId'=>accountObj.Id, 
                    'Card__c'=>cardObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Category__c'=>'Account closure', 
                    'Origin'=>'SOA',
                    'status'=>'New',
                    'Dispute_Type__c'=>'0:lost',
                    'Description'=>'Kundens personnummer (10 siffror utan streck) 0000000061'                    
                    });
            
            DCMS_FraudCase__c dcmsFraud = UnitTestDataGenerator.TestDCMSFraudCase.buildInsert(new Map<String, Object>{ 
                'Case_Number__c' => caseObj.Id
                    });
            System.assertEquals('New',caseObj.status,'The Status is not correct');
            test.stopTest();
        }        
    }
    
    Static testmethod void testMethodforCaseTriggers(){          
        System.runAs(UnitTestDataGenerator.adminUser){
            test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account accountObj=UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c'=>'1123123'
                    });
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj =UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123', 
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id
                    });
            
            Product_Custom__c prodobj=UnitTestDataGenerator.TestProductCustom.buildInsert(new Map<String, Object>{      
            });
            
            Financial_Account__c finAccObj=UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd', 
                    'Customer__c'=>accountObj.Id, 
                    'Account_Serno__c'=>'3123023', 
                    'Institution_Id__c'=>2,
                    'Customer_Serno__c'=>accountObj.Customer_Serno__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            
            Card__c cardObj=UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Financial_Account_Serno__c'=>finAccObj.Id, 
                    'People_Serno__c'=>contactObj.SerNo__c,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id
                    });            
            
            Case caseObj=UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{ 
                'ContactId'=>null, 
                    'AccountId'=>accountObj.Id, 
                    'Card__c'=>cardObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Category__c'=>'Account closure', 
                    'Origin'=>'Web',
                    'status'=>'In Progress'                    
                    //ParentId=caseObj1.id
                    });
            
            
            caseObj.status='Closed';
            update caseObj;
            
            DCMS_FraudCase__c dcmsFraud = UnitTestDataGenerator.TestDCMSFraudCase.buildInsert(new Map<String, Object>{ 
                'Case_Number__c' => caseObj.Id
                    });
            
            System.assertEquals('Closed',caseObj.status,'The Status is not correct');
            test.stopTest();
        }
    }
    
    Static testmethod void testMethodforCases(){
        
        System.runAs(UnitTestDataGenerator.adminUser){
            test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account accountObj=UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c'=>'1123123'
                    });
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj =UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123', 
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id
                    });
            
            Product_Custom__c prodobj=UnitTestDataGenerator.TestProductCustom.buildInsert(new Map<String, Object>{      
            });
            
            Financial_Account__c finAccObj=UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd', 
                    'Customer__c'=>accountObj.Id, 
                    'Account_Serno__c'=>'3923123', 
                    'Customer_Serno__c'=>accountObj.Customer_Serno__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            
            Card__c cardObj=UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Financial_Account_Serno__c'=>finAccObj.Id, 
                    'People_Serno__c'=>contactObj.SerNo__c,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id
                    });
            
            Case caseObj=UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{ 
                'ContactId'=>contactObj.id, 
                    'AccountId'=>accountObj.Id, 
                    'Card__c'=>cardObj.Id,
                    'Financial_Account__c'=>null,
                    'Category__c'=>'Account closure',
                    'Dispute_Type__c'=>'0: lost',
                    'Origin'=>'Web',
                    'status'=>'New',
                    'Description'=>'Kundens personnummer (10 siffror utan streck)000000061'                    
                    //ParentId=caseObj1.id
                    });
            DCMS_FraudCase__c dcmsFraud = UnitTestDataGenerator.TestDCMSFraudCase.buildInsert(new Map<String, Object>{ 
                'Case_Number__c' => caseObj.Id
                    });
            
            System.assertEquals('New',caseObj.status,'The Status is not correct');
            test.stopTest();
        }
    }   
}