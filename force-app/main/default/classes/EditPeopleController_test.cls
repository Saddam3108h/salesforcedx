/********************************************************************************************
Name: EditPeopleController_test
=============================================================================================
Purpose: To cover the 'EditPeopleController' VF controller code coverage 
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0        Srinivas D             4th-April-2019    SFD-984   		    Intial version
2.0		   Kakasaheb Ekshinge 	  22-July-2020		SFD-1596			Code coverage has increases
********************************************************************************************/
@isTest
public class EditPeopleController_test {
    
    @isTest
    static void test()
    {
        //Account
        Account account=new Account();
        account.Name='test Account';
        account.Customer_Serno__c='qwertyuiop';
        account.Institution_Id__c=5;
        insert account;
        
        //Contact
        Contact contact=new Contact();
        contact.FirstName='Test Contact1';
        contact.LastName='Test Contact';
        contact.AccountId=account.Id;
        contact.SSN__c='vgvavvvjq5464';
        contact.SerNo__c='qwertyuiop';
        contact.Institution_Id__c=5;
        contact.Email='xyz@abc.com';
        contact.MobilePhone='9876543210';
        contact.Phone='123123123'; 
        contact.Fax='98789879';
        contact.MobilePhone='98789879'; 
        contact.HomePhone='123123123';
        contact.Email='testemail@test.com';
        insert contact;
        
        System.debug(contact);
        
        
        //ServiceHeaders
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'UpdatePeopleHeader';
        objServiceHeaders.ContentType__c='SampleContentType';
        objServiceHeaders.Authorization__c='SampleAuthorization';
        insert objServiceHeaders;
        
        //ServiceSettings__c
        ServiceSettings__c objServiceSettings = new ServiceSettings__c();
        objServiceSettings.Name = 'UpdatePeople';
        objServiceSettings.HeaderName__c = 'UpdatePeopleHeader'; 
        objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings.OutputClass__c = 'UpdateCustomerDataOutput';
        objServiceSettings.ProcessingClass__c = 'UpdateCustomerDatacallout'; 
        objServiceSettings.Strategy__c = 'SOAPService';
        objServiceSettings.Input_Class__c = 'UpdateCustomerDataInput'; 
        insert objServiceSettings;
        
        String expectedvalue='{"animals": ["majestic badger", "fluffy bunny", "scary bear", "chicken", "mighty moose"]}';
        Apexpages.StandardController sc= new ApexPages.standardController(contact);
        
        test.startTest();
        
        PageReference pageRef = Page.EditPeople;
        pageRef.getParameters().put('Id', contact.Id);
        Test.setCurrentPage(pageRef);
        Test.setMock(HttpCalloutMock.class, new EditPeopleControllerCalloutMock());
        EditPeopleController controller=new EditPeopleController(sc);
        String actualvalue=controller.updateContactByRest(contact);
       
        
        
        System.assertEquals(actualValue, expectedValue);
        controller.savepeople();
        
        test.stopTest();

    }
    
    @isTest
    static void test1()
    {
        //Account
        Account account=new Account();
        account.Name='Test Account';
        account.Customer_Serno__c='qwertyuiop';
        account.Institution_Id__c=5;
        insert account;
        
        //Contact
        Contact contact=new Contact();
        contact.FirstName='Test';
        contact.LastName='Test Contact';
        contact.AccountId=account.Id;
        contact.SSN__c='vgvavvvjq5464';
        contact.SerNo__c='qwertyuiop';
        contact.Institution_Id__c=5;
        contact.Email='xyz@abc.com';
        contact.MobilePhone='9876543210';
        contact.Phone='123123123'; 
        contact.Fax='98789879';
        contact.MobilePhone='98789879'; 
        contact.HomePhone='123123123';
        contact.Email='testemail@test.com';
        insert contact;
        
        System.debug(contact);
        
        
        //ServiceHeaders
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'UpdatePeopleHeader';
        objServiceHeaders.ContentType__c='SampleContentType';
        objServiceHeaders.Authorization__c='SampleAuthorization';
        insert objServiceHeaders;
        
        //ServiceSettings__c
        ServiceSettings__c objServiceSettings = new ServiceSettings__c();
        objServiceSettings.Name = 'UpdatePeople';
        objServiceSettings.HeaderName__c = 'UpdatePeopleHeader'; 
        objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings.OutputClass__c = 'UpdateCustomerDataOutput';
        objServiceSettings.ProcessingClass__c = 'UpdateCustomerDatacallout'; 
        objServiceSettings.Strategy__c = 'SOAPService';
        objServiceSettings.Input_Class__c = 'UpdateCustomerDataInput'; 
        insert objServiceSettings;
        
        
        Apexpages.StandardController sc= new ApexPages.standardController(contact);
        EditPeopleController controller=new EditPeopleController(sc);
        controller.con = contact;
        contact.LastName='testcontact1';
        update contact;
        
        test.startTest();
        
        PageReference pageRef = Page.EditPeople;
        pageRef.getParameters().put('Id', contact.Id);
       // EditPeopleControllerCalloutMock.
        Test.setCurrentPage(pageRef);
        Test.setMock(HttpCalloutMock.class, new EditPeopleControllerCalloutMock());
        controller.updateContactByRest(contact);
        controller.oldcon = contact;
        controller.respstatuscode=300; // edited by m 
        controller.savepeople();
        
        test.stopTest();

    }
    
    @isTest
    static void test2()
    {
        //Account
        Account account=new Account();
        account.Name='Test Account';
        account.Customer_Serno__c='qwertyuiop';
        account.Institution_Id__c=5;
        insert account;
        
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('vgvavvvjq5464'));
        string encryptSSN=EncodingUtil.convertToHex(hash);
        //Contact
        Contact contact=new Contact();
        contact.FirstName='Test';
        contact.LastName='Test Contact';
        contact.AccountId=account.Id;
        contact.SSN__c='7895874'; //'vgvavvvjq5464'; edited by m
        contact.SerNo__c='qwertyuiop';
        contact.Institution_Id__c=2 ; //5; edited by m 
        contact.Email='xyz@abc.com';
        contact.MobilePhone='9876543210';
        contact.Encrypted_SSN__c=encryptSSN;
        contact.Phone='123123123'; 
        contact.Fax='98789879';
        contact.MobilePhone='98789879'; 
        contact.HomePhone='123123123';
        contact.Email='testemail@test.com';

        insert contact;
        
        System.debug(contact);
        
        
        //ServiceHeaders
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'UpdatePeopleHeader';
        objServiceHeaders.ContentType__c='SampleContentType';
        objServiceHeaders.Authorization__c='SampleAuthorization';
        insert objServiceHeaders;
        
        //ServiceSettings__c
        ServiceSettings__c objServiceSettings = new ServiceSettings__c();
        objServiceSettings.Name = 'UpdatePeople';
        objServiceSettings.HeaderName__c = 'UpdatePeopleHeader'; 
        objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings.OutputClass__c = 'UpdateCustomerDataOutput';
        objServiceSettings.ProcessingClass__c = 'UpdateCustomerDatacallout'; 
        objServiceSettings.Strategy__c = 'SOAPService';
        objServiceSettings.Input_Class__c = 'UpdateCustomerDataInput'; 
        insert objServiceSettings;
        
        
        Apexpages.StandardController sc= new ApexPages.standardController(contact);
        EditPeopleController controller=new EditPeopleController(sc);
        
        contact.Email='abc@xyz.com';
        contact.MobilePhone='1234567890';
        //update contact;
        
        test.startTest();
        
        PageReference pageRef = Page.EditPeople;
        pageRef.getParameters().put('Id', contact.Id);
        Test.setCurrentPage(pageRef);
        Test.setMock(HttpCalloutMock.class, new EditPeopleControllerCalloutMock());
        //Test.setMock(WebServiceMock.class, new UpdCustomerAddressResponseWebServiceMock());
        //contact.Email = 'test@testcap.com';
       // contact.FirstName='Account';    
        //controller.updateContactByRest(contact); 
        controller.oldcon.Email ='kk@testcap.com' ;
                        system.debug('**oldcon Email='+controller.oldcon.Email);
                
                system.debug('**Con Email='+controller.con.Email); 
        controller.savepeople();
        
        // contact.FirstName='Test1';   // Testing for line 135 -> oldcon.FirstName!=con.FirstName
        
     //   controller.updateContactByRest(contact);
        
     //   controller.savepeople();
        
        test.stopTest();
        
        // testing one more scenario below
     //   test.startTest();
        
       
        
      //  test.stopTest();
        

    }
    
   /* @isTest
    static void test5()
    {
        Account accountObj=new Account(
      Name='Test Account', 
      Institution_Id__c=2, 
      Customer_Serno__c='1123123'
      );
      insert accountObj;
      
      Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
      string encryptSSN=EncodingUtil.convertToHex(hash);
      Contact contactObj =new Contact(
      LastName='Test Contact', 
      FirstName='First Test Contact',
      SerNo__c='2123123', 
      Institution_Id__c=2, 
      SSN__c='0000000061',
      Encrypted_SSN__c=encryptSSN,
      AccountId=accountObj.Id,
      Phone='123123123', 
      Fax='98789879', 
      MobilePhone='98789879', 
      HomePhone='123123123', 
      Email='testemail@test.com'
      );
      insert contactObj;
        
      Product_Custom__c prodobj=new Product_Custom__c(
      Name='Test Product',
      Pserno__c='56789'
      );
      insert prodobj;
      
      Financial_Account__c finAccObj=new Financial_Account__c(
      Account_Number__c='12w23eeeeedd', 
      Customer__c=accountObj.Id, 
      Account_Serno__c='3123123', 
      Institution_Id__c=2,
      Customer_Serno__c=contactObj.SerNo__c, 
      Product__c=prodobj.Id,
      Product_Serno__c=prodobj.Pserno__c
      );
      insert finAccObj;
        
      Card__c cardObj=new Card__c(
      People__c=contactObj.Id,
      PrimaryCardFlag__c=true,
      Financial_Account__c=finAccObj.Id, 
      Card_Serno__c='524858519', 
      Card_Number_Truncated__c='7136785481583561', 
      Financial_Account_Serno__c=finAccObj.Id, 
      People_Serno__c=contactObj.Id,
      Prod_Serno__c=prodobj.Pserno__c, 
      Product__c=prodobj.Id,
      Institution_Id__c=2
      );
      insert cardObj;
      
      Case caseObj=new Case(
      ContactId=contactObj.Id, 
      AccountId=accountObj.Id, 
      Card__c=cardObj.Id,
      Financial_Account__c=finAccObj.Id, 
      Category__c='Account closure', 
      Origin='Web'
      );
      insert caseObj;
        
      
        //GlobalSettings__c
        List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.Name = 'ServiceStrategies'; 
        objGlobal.Value__c = 'SOAPService';
        GlobalSettingsList.add(objGlobal);
        
        insert GlobalSettingsList;
        
        List<ServiceHeaders__c> serviceHeadersList =new List<ServiceHeaders__c>();
        //ServiceHeaders
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'GetCustomerDetailsHeader';
        serviceHeadersList.add(objServiceHeaders);
        
        ServiceHeaders__c objServiceHeaders2 = new ServiceHeaders__c();
        objServiceHeaders2.name = 'UpdateCustomerDataHeader';
        serviceHeadersList.add(objServiceHeaders2);
        
        insert serviceHeadersList;
        
        List<ServiceSettings__c> serviceSettingList = new List<ServiceSettings__c>();
         //ServiceSettings__c
        ServiceSettings__c objServiceSettings = new ServiceSettings__c();
        objServiceSettings.Name = 'GetCustomerDetails';
        objServiceSettings.HeaderName__c = 'GetCustomerDetailsHeader'; 
        objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings.OutputClass__c = 'GetCustomerOutput';
        objServiceSettings.ProcessingClass__c = 'GetCustomerCallout'; 
        objServiceSettings.Strategy__c = 'SOAPService';
        objServiceSettings.Input_Class__c = 'GetCustomerInput'; 
        objServiceSettings.Username__c='Username';
        objServiceSettings.Password__c='Password';
        serviceSettingList.add(objServiceSettings);
        
        ServiceSettings__c objServiceSettings2 = new ServiceSettings__c();
        objServiceSettings2.Name = 'UpdateCustomerData';
        objServiceSettings2.HeaderName__c = 'UpdateCustomerDataHeader'; 
        objServiceSettings2.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
        objServiceSettings2.OutputClass__c = 'UpdateCustomerDataOutput';
        objServiceSettings2.ProcessingClass__c = 'UpdateCustomerDataCallout'; 
        objServiceSettings2.Strategy__c = 'SOAPService';
        objServiceSettings2.Input_Class__c = 'UpdateCustomerDataInput'; 
        objServiceSettings2.Username__c='Username';
        objServiceSettings2.Password__c='Password';
        serviceSettingList.add(objServiceSettings2);
        
        insert serviceSettingList;
        
        List<Common_Settings__c> commonSettingList =new List<Common_Settings__c>();
        Common_Settings__c commonSetting =new Common_Settings__c(
        Name='CorrelationId',
        Common_Value__c='1'
        );
        commonSettingList.add(commonSetting);
        Common_Settings__c commonSetting1 =new Common_Settings__c(
        Name='RequestorId',
        Common_Value__c='Salesforce'
        );
        commonSettingList.add(commonSetting1);
        Common_Settings__c commonSetting2 =new Common_Settings__c(
        Name='SystemId',
        Common_Value__c='1'
        );
        commonSettingList.add(commonSetting2);
        insert commonSettingList;
        
        ErrorCodes__c errorCodes =new ErrorCodes__c();
        errorCodes.Name='0';
        errorCodes.ErrorDescription__c='ErrorDescription';
        errorCodes.UIDisplayMessage__c='UIDisplayMessage';
        insert errorCodes;
        
        ErrorCodes__c errorCodes1 =new ErrorCodes__c();
        errorCodes1.Name='defaultError';
        errorCodes1.ErrorDescription__c='ErrorDescription';
        errorCodes1.UIDisplayMessage__c='UIDisplayMessage';
        insert errorCodes1;
        
        Apexpages.StandardController sc= new ApexPages.standardController(contactObj);
        EditPeopleController controller=new EditPeopleController(sc);
        
        contactObj.Email='abc@xyz.com';
        contactObj.MobilePhone='1234567890';
        update contactObj;
        
        test.startTest();
        
        PageReference pageRef = Page.EditPeople;
        pageRef.getParameters().put('Id', contactObj.Id);
        Test.setCurrentPage(pageRef);
        Test.setMock(HttpCalloutMock.class, new EditPeopleControllerCalloutMock());
        Test.setMock(WebServiceMock.class, new MockResponseClass());
        //controller.updateContactByRest(contactObj);
        controller.savepeople();
        
        test.stopTest();

    }*/
    
}