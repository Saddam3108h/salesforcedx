public class GetCustomerCallout extends SoapIO{
    SOAPCSSServiceRequest.getCustomerRequestRecordType CustomerRequestRecordTypeInstance;
    GetCustomerInput input;
    public  void convertInputToRequest(){
    
        /*GetCustomerInput*/ input = (GetCustomerInput)serviceInput;
        CustomerRequestRecordTypeInstance = new SOAPCSSServiceRequest.getCustomerRequestRecordType();
        
        CustomerRequestRecordTypeInstance .InstitutionId=input.InstitutionId;//2;
        CustomerRequestRecordTypeInstance .SSN=input.SSN;//'0000000061';
        CustomerRequestRecordTypeInstance .RetrieveAccounts=input.RetrieveAccounts;//'Y'
        CustomerRequestRecordTypeInstance .RetrieveCards=input.RetrieveCards;//'N'
        
    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        SOAPschemasEntercardComTypelib10.headerType HeadInstance=new SOAPschemasEntercardComTypelib10.headerType();
        HeadInstance.MsgId='SFDC consuming get customer SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=string.valueof(input.InstitutionId);//'2';
        SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance= new SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=120000;
        // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('GetCustomerDetails');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
        //String username = 'evry_access';
        //String password = '9oKuwQioQ4';
        
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken creds=new SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken();
        creds.Username=serviceObj.Username__c; //'css_soa';//'evry_access';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';//'9oKuwQioQ4';
        SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element security_ele=new SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOAPCSSServiceResponse.GetCustomerResponse_element CustomerResponse_elementinstance;
         CustomerResponse_elementinstance =  invokeInstance.getCustomer(HeadInstance,CustomerRequestRecordTypeInstance);
        system.debug('CustomerResponse_elementinstance -->'+CustomerResponse_elementinstance);
        return CustomerResponse_elementinstance;
     }  
        
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
       if(response==null)
           return null;
       SOAPCSSServiceResponse.GetCustomerResponse_element CustomerResponse_elementinstance = (SOAPCSSServiceResponse.GetCustomerResponse_element)response;
       SOAPCSSServiceResponse.getCustomerResponseRecordType CustomerResponseRecordInstance=CustomerResponse_elementinstance.GetCustomerResponseRecord;
       SOAPCSSServiceResponse.customerType customerTypeinstance=CustomerResponseRecordInstance.Customer;
       //SOAPCSSServiceResponse.serviceType serviceTypeInstance = CustomerResponseRecordInstance.
       
       GetCustomerOutput CustomerOutput=new GetCustomerOutput();// Custom output class initialization, this will be the return value of the method
       CustomerOutput.Serno=customerTypeinstance.Serno;
       CustomerOutput.SSN=customerTypeinstance.SSN;
       CustomerOutput.FirstName=customerTypeinstance.FirstName;
       CustomerOutput.LastName=customerTypeinstance.LastName;
       CustomerOutput.Mobile=customerTypeinstance.Mobile;
       CustomerOutput.Email=customerTypeinstance.Email;
       CustomerOutput.TOTB=customerTypeinstance.TOTB;
       CustomerOutput.TotalLimit=customerTypeinstance.TotalLimit;
       CustomerOutput.TotalPossibleLimitIncrease=customerTypeinstance.TotalPossibleLimitIncrease;
         
       if(customerTypeinstance.CustomerAddress!=null)
       {
           CustomerOutput.Addresses =new List<GetCustomerOutput.CustomerAddress>();
           for(SOAPCSSServiceResponse.customerAddressType EachAddress: customerTypeinstance.CustomerAddress)// parsing and storing addresses
           {
               GetCustomerOutput.CustomerAddress EachCustomerAddress = new GetCustomerOutput.CustomerAddress();
               EachCustomerAddress.AddressType=EachAddress.AddressType;
               EachCustomerAddress.Address1=EachAddress.Address1;
               EachCustomerAddress.Zip=EachAddress.Zip;
               EachCustomerAddress.City=EachAddress.City;
               EachCustomerAddress.Country=EachAddress.Country;
               EachCustomerAddress.Telephone=EachAddress.Telephone;
               CustomerOutput.Addresses.add(EachCustomerAddress);
           }
       }
       if(customerTypeinstance.Account!=null)
       {
           CustomerOutput.FinancialAccounts =new List<GetCustomerOutput.CustomerFinancialAccount>();
           for(SOAPCSSServiceResponse.accountCustResType EachAccount : customerTypeinstance.Account)// parsing and storing Financial Accounts
           {
               GetCustomerOutput.CustomerFinancialAccount EachFA = new GetCustomerOutput.CustomerFinancialAccount();
               EachFA.Serno=EachAccount.Serno;
               EachFA.AccountNo=EachAccount.AccountNo;
               EachFA.StGen=EachAccount.AccountStatus.StGen;
               EachFA.StGenDesc=EachAccount.AccountStatus.StGenDesc;
               EachFA.StFin=EachAccount.AccountStatus.StFin;
               EachFA.StFinDesc=EachAccount.AccountStatus.StFinDesc;
               EachFA.StAuth=EachAccount.AccountStatus.StAuth;
               EachFA.StAuthDesc=EachAccount.AccountStatus.StAuthDesc;
               EachFA.AccountRating=EachAccount.AccountRating;
               EachFA.CreateDate=EachAccount.CreateDate;
               EachFA.RiskIndicator=EachAccount.RiskIndicator;
               EachFA.StmtNextDate=EachAccount.StmtNextDate;
               EachFA.OTB=EachAccount.OTB;
               EachFA.NyckelKundFlag=EachAccount.NyckelKundFlag; //sfd-793 Added by Mani
               EachFA.CharityFlag=EachAccount.CharityFlag;//SFD-651 Added By Ravi Jaroli
               EachFA.KYC=EachAccount.KYC;//SFD-938 Added By Mani
               EachFA.LPI=EachAccount.LPI;//SFD-249 Added By Mani
               EachFA.ProductChanged=EachAccount.ProductChanged;
               EachFA.Balance=EachAccount.Balance;
               EachFA.Limit_x=EachAccount.Limit_x;
               EachFA.PossibleLimitIncrease=EachAccount.PossibleLimitIncrease;
               EachFA.Einvoice=EachAccount.Einvoice;
               EachFA.DirectDebitEnabled=EachAccount.DirectDebit.Enabled;
               EachFA.DirectDebitAmount=EachAccount.DirectDebit.Amount;
               EachFA.DirectDebitPercentage=EachAccount.DirectDebit.Percentage;
               EachFA.DirectDebitCombination=EachAccount.DirectDebit.Combination;
               EachFA.DirectDebitClearingNo=EachAccount.DirectDebit.ClearingNo;
               EachFA.DirectDebitAccountNo=EachAccount.DirectDebit.AccountNo;
               EachFA.PPI=EachAccount.PPI;
               EachFA.PickupCode=EachAccount.PickupCode;
               EachFA.MTP=EachAccount.MTP;
    
               if(EachAccount.Membership!=null)
               {
                   EachFA.Membership=new list<string>();
                   for(string eachinsurance : EachAccount.Membership)// adding membership
                   {
                       EachFA.Membership.add(eachinsurance);
                   }
               }
               if(EachAccount.Product!=null)
               {
                   EachFA.FinancialAccountProducts =new list<GetCustomerOutput.ProductType>();
                   for(SOAPCSSServiceResponse.productType EachproductType : EachAccount.Product)// parsing and storing product Type for each FA
                   {
                       GetCustomerOutput.ProductType EachProdType= new GetCustomerOutput.ProductType();
                       EachProdType.PSerno=EachproductType.PSerno;
                       EachProdType.InstitutionId=EachproductType.InstitutionId;
                       EachProdType.Name=EachproductType.Name;
                       EachProdType.Description=EachproductType.Description;
                       
                       EachFA.FinancialAccountProducts.add(EachProdType);
                   }
               }
               
               if(EachAccount.Card!=null)
               {
                   EachFA.Cards=new List<GetCustomerOutput.CardType>();
                   for(SOAPCSSServiceResponse.cardType EachCard :EachAccount.Card)// adding cards of each financial account
                   {
                       GetCustomerOutput.CardType EachCardInstance = new GetCustomerOutput.CardType();
                       EachCardInstance.CardSerno=EachCard.CardSerno;
                       EachCardInstance.CardNo=EachCard.TruncatedCardNo;//CardNo;
                       EachCardInstance.WrongPinAttempts=EachCard.WrongPinAttempts;
                       EachCardInstance.StGen=EachCard.CardStatus.StGen;
                       EachCardInstance.StGenDesc=EachCard.CardStatus.StGenDesc;
                       EachCardInstance.StFin=EachCard.CardStatus.StFin;
                       EachCardInstance.StFinDesc=EachCard.CardStatus.StFinDesc;
                       EachCardInstance.StAuth=EachCard.CardStatus.StAuth;
                       EachCardInstance.StAuthDesc=EachCard.CardStatus.StAuthDesc;
                       EachCardInstance.StEmb=EachCard.CardStatus.StEmb;
                       EachCardInstance.StEmbDesc=EachCard.CardStatus.StEmbDesc;
                       EachCardInstance.LastName=EachCard.LastName;
                       EachCardInstance.FirstName=EachCard.FirstName;
                       EachCardInstance.EmbName=EachCard.EmbName;
                       EachCardInstance.EmbDate=EachCard.EmbDate;
                       EachCardInstance.PrimaryCard=EachCard.PrimaryCard;
                       EachCardInstance.ExpiryDate=EachCard.ExpiryDate;
                       EachCardInstance.SecureCodeInfo=EachCard.SecureCodeInfo;
                       //EachCardInstance.AnnualFeeDate = SOAPCSSServiceResponse.serviceType.AnnualFeeDate;
                           
                       if(EachCard.Services.Service !=null){
                           EachCardInstance.cardService = new List<GetCustomerOutput.ServiceType>();
                           for(SOAPCSSServiceResponse.serviceType EachServiceType : EachCard.Services.Service)
                           {
                               GetCustomerOutput.ServiceType EachcSerType = new GetCustomerOutput.ServiceType();
                              
                               EachcSerType.AnnualFeeDate = EachServiceType.AnnualFeeActionDate;
                               
                               EachcSerType.status = EachServiceType.status;
                               EachCardInstance.cardService.add(EachcSerType);
                           }
                           System.debug('---------Annual----fee----'+EachCardInstance.cardService);
                       }

  
                       
                       if(EachCard.Product!=null)
                       {
                           EachCardInstance.CardProducts=new List<GetCustomerOutput.ProductType>();
                           for(SOAPCSSServiceResponse.productType EachproductTypeCard : EachCard.Product)// parsing and storing product Type for each card
                           {
                               GetCustomerOutput.ProductType EachProdTypeCard = new GetCustomerOutput.ProductType();
                               EachProdTypeCard.PSerno=EachproductTypeCard.PSerno;
                               EachProdTypeCard.InstitutionId=EachproductTypeCard.InstitutionId;
                               EachProdTypeCard.Name=EachproductTypeCard.Name;
                               EachProdTypeCard.Description=EachproductTypeCard.Description;
                               
                               EachCardInstance.CardProducts.add(EachProdTypeCard );
                           }
                       }
                       if(EachCard.InsurenceList.RememberInsurenceList!=null)
                       {
                           EachCardInstance.RememberInsurenceList=new List<string>();
                           for(SOAPCSSServiceResponse.rememberInsurenceListType EachRememberInsurenceListType : EachCard.InsurenceList.RememberInsurenceList)// Adding RememberInsurence
                           {
                               if(EachRememberInsurenceListType.RememberInsurence!=null)
                               {
                                   for(string EachRememberInsurence : EachRememberInsurenceListType.RememberInsurence)
                                   {
                                       EachCardInstance.RememberInsurenceList.add(EachRememberInsurence);
                                   }
                               }
                           }
                       }
                       if(EachCard.InsurenceList.FreeInsurenceList!=null)
                       {
                           EachCardInstance.FreeInsurenceList=new List<string>();
                           for(SOAPCSSServiceResponse.freeInsurenceListType EachFreeInsurenceListType : EachCard.InsurenceList.FreeInsurenceList)// Adding RememberInsurence
                           {
                               if(EachFreeInsurenceListType.FreeInsurence!=null)
                               {
                                   for(string EachFreeInsurence : EachFreeInsurenceListType.FreeInsurence)
                                   {
                                       EachCardInstance.FreeInsurenceList.add(EachFreeInsurence);
                                   }
                               }
                           }
                       }
                       EachFA.Cards.add(EachCardInstance);
                   }
               }
               
               CustomerOutput.FinancialAccounts.add(EachFA);
           }
       } 
       return CustomerOutput;
    }        
}