/********************************************************************************************
Name: AddIndividualToContactBatch_Test
=============================================================================================
Purpose: This test class is covering 'AddIndividualToContact_Batch' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date             Created for Jira     Detail
1.0         Gagan Pathak               29-October-2020  SFD-1878                     Intial version
********************************************************************************************/
@isTest
public class AddIndividualToContactBatch_Test {
    Public static testMethod void addIndividualToContact_Batch_Test(){
        
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});            
            List<Contact> conlist = new List<Contact>();
            Contact con = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'RecordTypeId'=>Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Shadow Contact RT').getRecordTypeId()
            });
            conlist.add(con);           
            Test.startTest();
            AddIndividualToContactBatch adtc = new AddIndividualToContactBatch();
            Database.executeBatch(adtc);
            Test.stopTest();
        }
    }
    
}