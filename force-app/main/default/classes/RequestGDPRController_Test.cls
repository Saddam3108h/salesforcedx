/********************************************************************************************
Name: RequestGDPRController_Test
=============================================================================================
Purpose: To cover the 'RequestGDPRController' apex class code coverage 
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0        Srinivas D             4th-April-2019    SFD-984   		    Intial version
2.0		   Kakasaheb Ekshinge 	  22-July-2020		SFD-1596			Code coverage has increases
********************************************************************************************/

@isTest
public class RequestGDPRController_Test {
    @testSetup
    public static void testInsertData() 
    {
        System.runAs(UnitTestDataGenerator.adminUser){
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account accountObj=new Account(
                Name='Test Account', 
                Institution_Id__c=2, 
                Customer_Serno__c='1123123'
            );
            insert accountObj;
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj =new Contact(
                LastName='Test Contact', 
                FirstName='First Test Contact',
                SerNo__c='2123123', 
                Institution_Id__c=2, 
                SSN__c='0000000061',
                Encrypted_SSN__c=encryptSSN,
                AccountId=accountObj.Id,
                Phone='123123123', 
                Fax='98789879', 
                MobilePhone='98789879', 
                HomePhone='123123123', 
                Email='testemail@test.com'
            );
            insert contactObj;
            
            Blob hash2 = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
            string encryptSSN2=EncodingUtil.convertToHex(hash2);
            Contact contactObj1 =new Contact(
                LastName='1Test Contact', 
                FirstName='1First Test Contact',
                SerNo__c='12123123', 
                Institution_Id__c=2, 
                SSN__c='0000000062',
                Encrypted_SSN__c=encryptSSN2,
                AccountId=accountObj.Id,
                Phone='1123123123', 
                Fax='198789879', 
                MobilePhone='198789879', 
                HomePhone='1123123123', 
                Email='1testemail@test.com'
            );
            insert contactObj1;
            
            Product_Custom__c prodobj=new Product_Custom__c(
                Name='Test Product',
                Pserno__c='56789'
            );
            insert prodobj;
            
            Financial_Account__c finAccObj=new Financial_Account__c(
                Account_Number__c='12w23eeeeedd', 
                Customer__c=accountObj.Id, 
                Account_Serno__c='3123123', 
                Institution_Id__c=2,
                Customer_Serno__c=contactObj.SerNo__c, 
                Product__c=prodobj.Id,
                Product_Serno__c=prodobj.Pserno__c
            );
            insert finAccObj;
            
            Card__c cardObj=new Card__c(
                People__c=contactObj.Id,
                PrimaryCardFlag__c=true,
                Financial_Account__c=finAccObj.Id, 
                Card_Serno__c='524858519', 
                Card_Number_Truncated__c='7136785481583561', 
                Financial_Account_Serno__c=finAccObj.Id, 
                People_Serno__c=contactObj.Id,
                Prod_Serno__c=prodobj.Pserno__c, 
                Product__c=prodobj.Id,
                Institution_Id__c=2
            );
            insert cardObj;
            
            Case caseObj=new Case(
                ContactId=contactObj.Id, 
                AccountId=accountObj.Id, 
                Card__c=cardObj.Id,
                Financial_Account__c=finAccObj.Id, 
                Category__c='Account closure', 
                Origin='Web'
            );
            insert caseObj;
            
            
            //GlobalSettings__c
            List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
            GlobalSettings__c objGlobal = new GlobalSettings__c();
            objGlobal.Name = 'ServiceStrategies'; 
            objGlobal.Value__c = 'SOAPService';
            GlobalSettingsList.add(objGlobal);
            
            insert GlobalSettingsList;
            
            List<ServiceHeaders__c> serviceHeadersList =new List<ServiceHeaders__c>();
            //ServiceHeaders
            ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
            objServiceHeaders.name = 'CustomerRights';
            serviceHeadersList.add(objServiceHeaders);
            
            insert serviceHeadersList;
            
            //ServiceSettings
            List<ServiceSettings__c> serviceSettingList = new List<ServiceSettings__c>();
            
            ServiceSettings__c objService = new ServiceSettings__c();
            objService.name = 'CustomerRights';
            objService.HeaderName__c = 'CustomerRightsHeader'; 
            objService.EndPoint__c = 'https://services-staging.entercard.com/CustomerRightsService/ProxyService/CustomerRightsServiceSalesforceInPS?WSDL';
            objService.OutputClass__c = 'GetCustomerRightsOutputs';
            objService.ProcessingClass__c = 'GetCustomerRightsCallout';
            objService.Strategy__c = 'SOAPService';
            objService.LogRequest__c = false;
            objService.LogResponse__c = false;
            objService.LogWithCallout__c = false;
            objService.Input_Class__c = 'GetCustomerRightsInputs';
            objService.Username__c='Username';
            objService.Password__c='Password';
            serviceSettingList.add(objService);
            
            insert serviceSettingList;
            
            ServiceSettings__c objService2 = new ServiceSettings__c();
            objService2.name = 'StatusLogService';
            objService2.HeaderName__c = 'RESTJSONOAuth'; 
            objService2.EndPoint__c = 'https://cs82.salesforce.com/services/apexrest/StatusLogRecord/';
            objService2.OutputClass__c = '';
            objService2.Operation__c = 'POST';
            objService2.ProcessingClass__c = 'StatusLogServiceIO';
            objService2.Strategy__c = 'RESTService';
            objService2.LogRequest__c = false;
            objService2.LogResponse__c = false;
            objService2.LogWithCallout__c = false;
            objService2.Input_Class__c = '';
            objService2.Username__c='';
            objService2.Password__c='';
            serviceSettingList.add(objService2);
            
            //Common_Settings__c
            List<Common_Settings__c> commonSettingList =new List<Common_Settings__c>();
            Common_Settings__c commonSetting =new Common_Settings__c(
                Name='CorrelationId',
                Common_Value__c='1'
            );
            commonSettingList.add(commonSetting);
            Common_Settings__c commonSetting1 =new Common_Settings__c(
                Name='RequestorId',
                Common_Value__c='Salesforce'
            );
            commonSettingList.add(commonSetting1);
            Common_Settings__c commonSetting2 =new Common_Settings__c(
                Name='SystemId',
                Common_Value__c='1'
            );
            commonSettingList.add(commonSetting2);
            insert commonSettingList;
            
            ErrorCodes__c errorCodes =new ErrorCodes__c();
            errorCodes.Name='0';
            errorCodes.ErrorDescription__c='ErrorDescription';
            errorCodes.UIDisplayMessage__c='UIDisplayMessage';
            insert errorCodes;
            
            ErrorCodes__c errorCodes1 =new ErrorCodes__c();
            errorCodes1.Name='defaultError';
            errorCodes1.ErrorDescription__c='ErrorDescription';
            errorCodes1.UIDisplayMessage__c='UIDisplayMessage';
            insert errorCodes1;
        }
    }
    
    public static testmethod void testmethod1()
    {
        Financial_Account__c finObj = [Select id,Name, Account_Number__c,Institution_Id__c from Financial_Account__c limit 1];
        Card__c crd = [Select id,People__c,Financial_Account__r.Account_Number__c, Financial_Account__r.Institution_Id__c from card__c Limit 1]; 
        Test.setCurrentPageReference(new PageReference('Page.RequestGDPRPage'));
        
        System.currentPageReference().getParameters().put('finId', finObj.Id);
        Test.setMock(WebServiceMock.class, new GetCustomerRightsMockClass());
        test.startTest();
        RequestGDPRController reqcont = new RequestGDPRController();
        string datestr=reqcont.printDate;
        reqcont.custAddressCity = 'SE';
        reqcont.custAddressCountry= 'N';
        reqcont.custAddressZip = '89990';
        reqcont.custName = 'Test';
        reqcont.custAddressStreet = 'testing';
        reqcont.submit();
        test.stopTest();
    }
    
}