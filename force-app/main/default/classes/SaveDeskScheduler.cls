/**********************************************************************
Name: SaveDeskScheduler
=======================================================================
Purpose: This class is used for Savedesk functionality in salesforce  User Story -SFD-1430

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         20-May-2020       Initial Version

**********************************************************************/
global class SaveDeskScheduler implements Schedulable {
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Status & Comments fields based on the Offer_Start_Date__c and Offer_End_Date__c.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    global void execute(SchedulableContext SC) {
        try {
            List<SaveDeskQueue__c> saveDeskList = [SELECT Id, Offer_Start_Date__c, Offer_End_Date__c 
                                                   FROM SaveDeskQueue__c
                                                   WHERE Agent_ID__c = ''];
            List<SaveDeskQueue__c> updateSDeskList = new List<SaveDeskQueue__c>();
            if(saveDeskList.size() > 0) {
                for(SaveDeskQueue__c sDeskObj : saveDeskList) {
                    sDeskObj.Status__c = (sDeskObj.Offer_Start_Date__c == Date.valueOf(system.today().format()) ? 
                                          StaticConstant.SAVEDESK_OPEN_STATUS:StaticConstant.SAVEDESK_TOBESTARTED_STATUS);
                    sDeskObj.Status__c = (sDeskObj.Offer_End_Date__c < Date.valueOf(system.today().format()) ? 
                                          StaticConstant.SAVEDESK_CLOSED_STATUS:StaticConstant.SAVEDESK_OPEN_STATUS);
                    sDeskObj.Comments__c = (sDeskObj.Offer_End_Date__c < Date.valueOf(system.today().format()) ? 
                                            StaticConstant.SAVEDESK_COMMENT:'');
                    
                    updateSDeskList.add(sDeskObj);
                }
                if(updateSDeskList.size() > 0) {
                    update updateSDeskList;
                }
            }
        } catch (Exception exp) {
            StatusLogHelper.logSalesforceError('SaveDeskScheduler', 'E005', 'execute', exp, false); // Class,method
        }
    }
}