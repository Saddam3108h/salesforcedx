public class UpdateCustomerDataInput{

    public integer InstitutionId;
    public setEmailRequest EmailReq;
    public setMobileRequest MobileReq;
    public closeAccountRequest CloseAccReq;
    public setInstalmentRequest InstalmentReq;
    public addExtraCardRequest addExtraCardReq;
    public replaceCardRequest replaceCardReq;
    public setPaymentHolidaysRequest PaymentHolidaysReq;
    public setPPIRequest setPPIReq;
    public setInsuranceRequest InsuranceReq;
    public cardReissueReprintRequest cardReissueReq;
    public PickupCodeRequest pickupCodeReq;
    public MTPRequest MTRReq;
    public Phone1Request Phone1Req;
    public StatementRequest StatementReq;
    
    public class setEmailRequest{
        public string SSN;
        public string Email;
    }
    public class setMobileRequest{
        public string SSN;
        public string mobile;
    }
    public class closeAccountRequest{
        public string AccountNo;
        public date ActionDate;
    }
    public class setInstalmentRequest{
        public Integer Serno;
        public String Number_x;
        public Decimal TransactionAmount;
        public String TransactionType;
        public String MessageText;
        public DateTime StartDate;
        public String InstalmentCode;
        public String MerchType;
        public String MerchId;
        public String MerchName;
        public String MerchCity;
        public String MerchCnt;
        public String AuthId;
        public Decimal TransactionFee;
    }
    public class addExtraCardRequest{
        public String AccountNo;
        public String ProductSerno;
        //===extraCardType===
        public String SSN;
        public String FirstName;
        public String LastName;
        public String Gender;
        public String EmbossingName;
        public String Phone;
        public String Email;
        //==== CardServices_element= list card type
        public list<cardServiceType> cardServiceTypeList;
    }
    public class cardServiceType{
        public String Id;
        public String ActionDelay;
        public String LogAction;
        public String Argument;
    }
    public class replaceCardRequest{
        public String OldCardSerno;
        //public String NewCardNumber;
        public String OldStatus;
        public String NewStatus;
        public Date BlockDate;
        public Date RenewDate;
        public String LostCardFee;
    } 
    public class setPaymentHolidaysRequest{
        public String AccountNo;
    }
    public class setPPIRequest{
        public String AccountNo;
        public String Action;
        public String PPI;
        public String InceptionDate;
    }
    public class setInsuranceRequest{
        public String CardSerno;
        //==insurenceListType = 1rememberInsurenceListType and 1 freeInsurenceListType 
        public List<rememberInsurenceType> rememberInsurenceTypelist;
        public String[] FreeInsurence;
    }
    public class rememberInsurenceType{
        public String[] RememberInsurenceName;
        public String[] RememberInsurenceAction;
    }
    public class cardReissueReprintRequest{
        public String Action;
        public String Reference;
        public String Serno;
        public String ServiceIndicator;
        public String ServiceType;
        public String ActionDate;
        public String LogAction;
        public String Argument;
    }
    public class PickupCodeRequest{
        public String AccountNo;
        public String PickupCode;
    }
    public class MTPRequest{
        public Integer Serno;
        public String AccountNo;
        public Decimal MTP;
    }
    public class Phone1Request{
        public String NewIndicator;
        public String Sex;
        public String DOB;
        public String MaritalStatus;
        public String MotherName;
        public Person_Type Person;
        public Person_Type PersonAlternateDetails;
        //===== address type =====
        public Long Id;
        public String Location;
        public String Address1;
        public String Address2;
        public String Address3;
        public String Address4;
        public String Address5;
        public String City;
        public String Country;
        public String Zip;
        public String Phone1;
        public String Phone2;
        public String Fax;
        public String Mobile;
        public String Email;
        public String Text;
        public String Contact;

    }
    public class Person_Type{
        public String Title;
        public String FirstName;
        public String MiddleName;
        public String LastName;
        public String SSN;
        public String CustomerId;
        public String LanguageCode;
        public Boolean VIP;
    }
    public class StatementRequest{
        public Integer Account;
        public String PhoneNumber;
        public String E_statement;
        public String Email;
        public String Bank;
        public String MTP;
    }
    //To Cover Test Class 100%
    public void test()
    {
        
    }
}