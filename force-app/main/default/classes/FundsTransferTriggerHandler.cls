/**********************************************************************
Name: FundsTransferTriggerHandler
=======================================================================
Purpose: This trigger handler class is created on FundsTransfer object to handle all the logics. User Story SFD-1628

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         30-July-2020       Initial Version
2.0			Kakasaheb Ekshinge	   11-AUG-2020		  Updated Trigger framework - SFD-1646
**********************************************************************/
public with sharing class FundsTransferTriggerHandler implements ITriggerHandler{
    
     // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false; 
    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled(){
      	
        
        if (TriggerSettings__c.getInstance().FundTransferExecution__c // false
            &&  TriggerSettings__c.getInstance().OrgExecution__c){ 
            return true;
        }
        else  
            return TriggerDisabled;        
   }
 
    public void BeforeInsert(List<SObject> newItems) {
       
    }
 
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {        
         Map<Id, FundsTransfer__c> newConItems = (Map<Id, FundsTransfer__c>) newItems; 
         Map<Id, FundsTransfer__c> oldConItems = (Map<Id, FundsTransfer__c>) oldItems;    
        
         throwErrorOnUpdate( newConItems.values(), oldConItems);
    }
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {
         Map<Id, FundsTransfer__c> oldConItems = (Map<Id, FundsTransfer__c>) oldItems;  
         throwErrorOnDelete(oldConItems.values());
    }
 
    public void AfterInsert(Map<Id, SObject> newItems) {} 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    } 
    public void AfterDelete(Map<Id, SObject> oldItems) {} 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}   

    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     30-July-2020
*   User Story : SFD-1628
*   Param: None
*   Return: None    
*   Description: In this method we are dispaying the error when delete the record if SASProcessed__c, SASRecieved__c fields are true.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public static void throwErrorOnDelete(List<FundsTransfer__c> fundTransferOldList) {
        for(FundsTransfer__c fundsObj : fundTransferOldList) {
            if(fundsObj.SASProcessed__c){
                fundsObj.adderror(StaticConstant.PROCESSED_DELETE_ERROR);
            }else if(fundsObj.SASRecieved__c) {
                fundsObj.adderror(StaticConstant.TRANSACTION_DELETE_ERROR); 
            }
        }
    }
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     30-July-2020
*   User Story : SFD-1628
*   Param: None
*   Return: None    
*   Description: In this method we are dispaying the error when update the record if SASProcessed__c is true.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public static void throwErrorOnUpdate(List<FundsTransfer__c> fundTransferList, Map<Id, FundsTransfer__c> fundTransferOldMap) {
        String currentUserProfileName = [SELECT Id, Name FROM Profile 
                                         WHERE id=:userinfo.getProfileId()].Name;
        String eC_SAS_AnonUser = Label.EC_SAS_AnonUser;
        for(FundsTransfer__c fundsObj : fundTransferList) {
            if(fundTransferOldMap.get(fundsObj.Id).SASRecieved__c 
               && currentUserProfileName != eC_SAS_AnonUser
               && fundsObj.SASProcessed__c==false
              ) {
                  fundsObj.adderror(StaticConstant.TRANSACTION_UPDATE_ERROR); 
              } else if(fundTransferOldMap.get(fundsObj.Id).SASProcessed__c) {
                  fundsObj.adderror(StaticConstant.PROCESSED_UPDATE_ERROR);
              }
        }
    }
}