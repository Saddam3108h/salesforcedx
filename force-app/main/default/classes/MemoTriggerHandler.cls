/********************************************************************************************
Name: MemoTriggerHandler
=============================================================================================
Purpose: This class is Handler class of memo Trigger
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                  Date              Detail
1.0         Saddam Hussain          02/06/2020        Initial Version
HelperTriggerToUpdateMemoRelatedTo
*********************************************************************************************/
public with sharing class MemoTriggerHandler
{
    Set<String> finAccSerno = new Set<String>();
    Set<String> cardSerno = new Set<String>();
    Set<String> contactSerno = new Set<String>();
    Set<String> accountSerno = new Set<String>();
    Set<Id> cardIds = new Set<Id>();//for manual
    Set<Id>FAIDS = new set<id>();//memo create from FA in SF UI// Added by spal 16/06/2017 for CSS-1292
    Set<Id>contIDS = new set<id>();//memo create from contact in SF UI// Added by spal 20/06/2017 for CSS-1292
    Map<String,ObjectIds> getMemoRelatedToPeopleId = new Map<String,ObjectIds>();
    Map<String,ObjectIds> getMemoRelatedTocaccountsId = new Map<String,ObjectIds>();
    Map<String,ObjectIds> getMemoRelatedTocardxId = new Map<String,ObjectIds>();
    Map<String,ObjectIds> getMemoRelatedToccustomerId = new Map<String,ObjectIds>();
    Map<Id,ObjectIds> getMemoRelatedTosfcardId = new Map<Id,ObjectIds>();//memo create from card in SF UI
    Map<Id,ObjectIds> getMemoRelatedTosfFAId = new Map<Id,ObjectIds>();//memo create from FA in SF UI// Added by spal 16/06/2017 for CSS-1292
    Map<Id,ObjectIds> getMemoRelatedTosfCONTId = new Map<Id,ObjectIds>();//memo create from CONTACT in SF UI// Added by spal 20/06/2017 for CSS-1292
    public static boolean inFutureContext = true;
    public MemoTriggerHandler()
    {
        
    }
    Public Void UpdateMemoRelatedTo(List<memo__c> newMemoList, List<memo__c> oldMemoMap)
    {
        if(oldMemoMap == null)
        {
            for(Memo__c objMemo : newMemoList)
            {
                if(objMemo.Entity__c !=null && objMemo.Entity_Serno__c!=null && objMemo.Entity__c !='' && objMemo.Entity_Serno__c!='')//BY SPAL 27MAR2017
                {
                    if(objMemo.Entity__c == 'people')
                    {
                        contactSerno.add(objMemo.Entity_Serno__c);
                    }
                    else if(objMemo.Entity__c == 'caccounts')
                    {
                        finAccSerno.add(objMemo.Entity_Serno__c);
                    }
                    else if(objMemo.Entity__c == 'cardx')
                    {
                        cardSerno.add(objMemo.Entity_Serno__c);
                    }
                    else if(objMemo.Entity__c == 'ccustomer')
                    {
                        accountSerno.add(objMemo.Entity_Serno__c);
                    }
                }
                if(objMemo.Entity__c ==null && objMemo.Card__c !=null)//memo create from card in SF UI
                    cardIds.add(objMemo.Card__c);
                if(objMemo.Entity__c ==null && objMemo.Financial_Account__c !=null && objMemo.People__c==null)//memo create from FA in SF UI// Added by spal 16/06/2017 for CSS-1292
                    FAIDS.add(objMemo.Financial_Account__c);
                if(objMemo.Entity__c ==null && objMemo.Financial_Account__c ==null && objMemo.People__c!=null)//memo create from CONTACT in SF UI// Added by spal 20/06/2017 for CSS-1292
                    contIDS.add(objMemo.People__c );
                
            }
            system.debug('accountSerno-->'+accountSerno);
            if(contactSerno.size() > 0)
            {
                // ******** // Added by spal 20/06/2017 for CSS-1292 [START] ********//
                Map<id,set<id>>PeopeVSFAMap=new Map<id,set<id>>();
                for(card__C cc : [SELECT id,Financial_Account__c,People__c from card__C where People__r.SerNo__c in : contactSerno])
                {
                    if(!PeopeVSFAMap.containskey(cc.People__c))    
                    {
                        set<id>temp=new set<id>();
                        temp.add(cc.Financial_Account__c);
                        PeopeVSFAMap.put(cc.People__c,temp);
                    }
                    else
                    {
                        set<id>temp=PeopeVSFAMap.get(cc.People__c);
                        temp.add(cc.Financial_Account__c);
                        PeopeVSFAMap.put(cc.People__c,temp);
                    }
                }
                // ******** // Added by spal 20/06/2017 for CSS-1292 [END] ********//
                for(Contact objCon:[SELECT id, serno__c,AccountId from Contact where serno__c IN: contactSerno])
                {
                    ObjectIds obConId = new ObjectIds();
                    obConId.contactId = objCon.id;
                    obConId.accountId = objCon.AccountId;
                    if(PeopeVSFAMap.containskey(objCon.id) && PeopeVSFAMap.get(objCon.id)!=null && PeopeVSFAMap.get(objCon.id).size()==1)    
                    {    
                        list<id>templist=new List<Id>(PeopeVSFAMap.get(objCon.id));
                        obConId.FAId=templist[0];
                    }
                    getMemoRelatedToPeopleId.put(objCon.serno__c, obConId);
                }
                
            }
            if(contIDS.size() > 0)//memo create from CONTACT in SF UI// Added by spal 20/06/2017 for CSS-1292
            {
                // ******** // Added by spal 20/06/2017 for CSS-1292 [START] ********//
                Map<id,set<id>>PeopeVSFAMap=new Map<id,set<id>>();
                for(card__C cc : [SELECT id,Financial_Account__c,People__c from card__C where People__c in : contIDS])
                {
                    if(!PeopeVSFAMap.containskey(cc.People__c))    
                    {
                        set<id>temp=new set<id>();
                        temp.add(cc.Financial_Account__c);
                        PeopeVSFAMap.put(cc.People__c,temp);
                    }
                    else
                    {
                        set<id>temp=PeopeVSFAMap.get(cc.People__c);
                        temp.add(cc.Financial_Account__c);
                        PeopeVSFAMap.put(cc.People__c,temp);
                    }
                }
                // ******** // Added by spal 20/06/2017 for CSS-1292 [END] ********//
                for(Contact objCon:[SELECT id,serno__c,AccountId from Contact where id IN: contIDS])
                {
                    ObjectIds obConId = new ObjectIds();
                    obConId.contactId = objCon.id;
                    obConId.accountId = objCon.AccountId;
                    if(PeopeVSFAMap.containskey(objCon.id) && PeopeVSFAMap.get(objCon.id)!=null && PeopeVSFAMap.get(objCon.id).size()==1)    
                    {    
                        list<id>templist=new List<Id>(PeopeVSFAMap.get(objCon.id));
                        obConId.FAId=templist[0];
                    }
                    getMemoRelatedTosfCONTId.put(objCon.id, obConId);
                }
                
            }
            if(finAccSerno.size() > 0)
            {
                for(Financial_Account__c objFA:[SELECT id,(select Id,People__c,People__r.AccountId from Cards__r where PrimaryCardFlag__c=true limit 1), Account_Serno__c from Financial_Account__c where Account_Serno__c IN: finAccSerno])
                {
                    ObjectIds obFAId = new ObjectIds();
                    obFAId.FAId = objFA.id;
                    //obFAId.cardId = objFA.Cards__r[0].Id;
                    if(objFA.Cards__r.size()>0){
                        obFAId.contactId = objFA.Cards__r[0].People__c;
                        obFAId.accountId = objFA.Cards__r[0].People__r.AccountId;
                    }
                    getMemoRelatedTocaccountsId.put(objFA.Account_Serno__c, obFAId);
                }
            }
            if(FAIDS.size() > 0)//memo create from FA in SF UI// Added by spal 16/06/2017 for CSS-1292
            {
                for(Financial_Account__c objFA:[SELECT id,(select Id,People__c,People__r.AccountId from Cards__r where PrimaryCardFlag__c=true limit 1), Account_Serno__c from Financial_Account__c where id IN: FAIDS])
                {
                    ObjectIds obFAId = new ObjectIds();
                    obFAId.FAId = objFA.id;
                    //obFAId.cardId = objFA.Cards__r[0].Id;
                    if(objFA.Cards__r.size()>0){
                        obFAId.contactId = objFA.Cards__r[0].People__c;
                        obFAId.accountId = objFA.Cards__r[0].People__r.AccountId;
                    }
                    getMemoRelatedTosfFAId.put(objFA.id, obFAId);
                }
            }
            if(cardSerno.size() > 0)
            {
                for(Card__c objCard:[SELECT id, Card_Serno__c,Financial_Account__c,People__c,People__r.AccountId from Card__c where Card_Serno__c IN: cardSerno])
                {
                    ObjectIds obCardId = new ObjectIds();
                    obCardId.cardId = objCard.id;
                    obCardId.contactId = objCard.People__c;
                    obCardId.FAId = objCard.Financial_Account__c;
                    obCardId.accountId = objCard.People__r.AccountId;
                    getMemoRelatedTocardxId.put(objCard.Card_Serno__c, obCardId);
                }
            }
            if(accountSerno.size() > 0)
            {
                for(Account objAccount:[SELECT id, Customer_Serno__c from Account where Customer_Serno__c IN: accountSerno])
                {
                    ObjectIds obAccId = new ObjectIds();
                    obAccId.accountId = objAccount.id;
                    getMemoRelatedToccustomerId.put(objAccount.Customer_Serno__c, obAccId);
                }
            }
            
            if(cardIds.size() > 0)
            {
                for(Card__c objCard:[SELECT id, Card_Serno__c,Financial_Account__c,People__c,People__r.AccountId from Card__c where Id IN: cardIds])
                {
                    ObjectIds obCardId = new ObjectIds();
                    obCardId.cardId = objCard.id;
                    obCardId.contactId = objCard.People__c;
                    obCardId.FAId = objCard.Financial_Account__c;
                    obCardId.accountId = objCard.People__r.AccountId;
                    getMemoRelatedTosfcardId.put(objCard.id, obCardId);
                }
            }
            for(Memo__c objMemo : newMemoList)
            {
                if(objMemo.Entity__c !=null && objMemo.Entity_Serno__c!=null && objMemo.Entity__c !='' && objMemo.Entity_Serno__c!='')//BY SPAL 27MAR2017
                {
                    if(objMemo.Entity__c == 'cardx')
                    {
                        ObjectIds objIds = getMemoRelatedTocardxId.get(objMemo.Entity_Serno__c);
                        if(objIds!=null){
                            objMemo.Card__c = objIds.cardId;
                            objMemo.Financial_Account__c = objIds.FAId;
                            objMemo.People__c = objIds.contactId;
                            objMemo.Customer__c = objIds.accountId;
                        }
                    }
                    else if(objMemo.Entity__c == 'caccounts')
                    {
                        ObjectIds objIds = getMemoRelatedTocaccountsId.get(objMemo.Entity_Serno__c);    
                        if(objIds!=null){               
                            objMemo.Financial_Account__c = objIds.FAId;
                            objMemo.People__c = objIds.contactId;
                            objMemo.Customer__c = objIds.accountId;
                        }
                    }
                    else if(objMemo.Entity__c == 'people')
                    {
                        ObjectIds objIds = getMemoRelatedToPeopleId.get(objMemo.Entity_Serno__c); 
                        if(objIds!=null){ 
                            objMemo.People__c = objIds.contactId;                                    
                            objMemo.Customer__c = objIds.accountId;
                            objMemo.Financial_Account__c = objIds.FAId;// Added by spal 20/06/2017 for CSS-1292
                        }
                    }
                    else if(objMemo.Entity__c == 'ccustomer')
                    {
                        ObjectIds objIds = getMemoRelatedToccustomerId.get(objMemo.Entity_Serno__c);  
                        if(objIds!=null)                                                        
                            objMemo.Customer__c = objIds.accountId;
                    }
                }
                if(objMemo.Entity__c ==null && objMemo.Card__c !=null)//memo create from card in SF UI
                {
                    ObjectIds objIds = getMemoRelatedTosfcardId.get(objMemo.Card__c);  
                    if(objIds!=null){
                        objMemo.Financial_Account__c = objIds.FAId;
                        objMemo.People__c = objIds.contactId;
                        objMemo.Customer__c = objIds.accountId;
                    }                                                        
                    
                }
                if(objMemo.Entity__c ==null && objMemo.Financial_Account__c !=null && objMemo.People__c==null)//memo create from FA in SF UI// Added by spal 16/06/2017 for CSS-1292
                {
                    ObjectIds objIds = getMemoRelatedTosfFAId.get(objMemo.Financial_Account__c);  
                    if(objIds!=null){               
                        objMemo.People__c = objIds.contactId;
                        if(objMemo.Customer__c==null)
                            objMemo.Customer__c = objIds.accountId;
                    }                                                        
                }
                if(objMemo.Entity__c ==null && objMemo.Financial_Account__c ==null && objMemo.People__c!=null)//memo create from CONTACT in SF UI// Added by spal 20/06/2017 for CSS-1292
                {
                    ObjectIds objIds = getMemoRelatedTosfCONTId.get(objMemo.People__c);  
                    if(objIds!=null){               
                        objMemo.Customer__c = objIds.accountId;
                        if(objMemo.Financial_Account__c ==null)
                            objMemo.Financial_Account__c = objIds.FAId;
                    }                                                        
                }
                if(objMemo.Customer__c==null && objMemo.People__c==null && objMemo.Financial_Account__c==null && objMemo.Card__c==null)// Added by spal 20/06/2017 for CSS-1292
                    objMemo.adderror('Select atleast Main Account Holder or Financial Account or Customer or Card');
            }
        }
    }
    
    // Added by saddam on 15-Mar-2018 [START SFD-452]
    @future(callout = true)
    Public static void insertmemo(Id memId){
        system.debug('----memoid----'+memId);
        
        Memo__c mem = [Select id,Comments__c,memo__c.Financial_Account__r.Account_Serno__c,memo__c.Financial_Account__r.Institution_Id__c from Memo__c where id=:memid];
        system.debug('--memo__c.People__r.SSN__c---'+ mem.Financial_Account__r.Account_Serno__c);
        
        if( mem.Financial_Account__r.Institution_Id__c == 2 && mem.Financial_Account__r.Account_Serno__c != ''){
            CustomerMemoExportDataOutput outputmemoexportdata = CSSServiceHelper.updateCustomerMemo('NewMemo','Account',mem.Financial_Account__r.Account_Serno__c,'css',mem.Comments__c,String.valueOf(mem.Financial_Account__r.Institution_Id__c));
            system.debug('***outputmemoexportdata ***'+outputmemoexportdata );
        } else { 
            CustomerMemoExportDataOutput outputmemoexportdata = CSSServiceHelper.updateCustomerMemo('NewMemo','Account',mem.Financial_Account__r.Account_Serno__c,'default',mem.Comments__c,String.valueOf(mem.Financial_Account__r.Institution_Id__c));
            system.debug('***outputmemoexportdata-1 ***'+outputmemoexportdata );
        }
    } 
    //  [END- SFD-452]
    private class ObjectIds{
        
        Id cardId;
        Id FAId;
        Id contactId;
        Id accountId;
        
    }
}