/**********************************************************************
Name: AddIndividualToContactBatch
=======================================================================
Purpose: This apex is used for to create/assign individual id to existing customer
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author          Date              Detail
1.0         Nagmani         28-Oct-2020      Initial Version
**********************************************************************/
global class AddIndividualToContactBatch implements Database.Batchable <sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
      String query = 'SELECT Id,FirstName,LastName,SerNo__c FROM Contact WHERE Individualid = null'; //LImit to be removed
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc,List<Contact> conList) {
        try{
            system.debug('---conslist---'+conList);
            Map<Contact, Individual> contactIndividualMap = new Map<Contact, Individual>();
            for (Contact contact : conList) {
                Individual individual =  new Individual();
                individual.LastName= contact.LastName;
                individual.FirstName= contact.Firstname;
                individual.External_Id__c= contact.SerNo__c;
                contactIndividualMap.put(contact, individual);
            }
            if(contactIndividualMap.size()>0){
                insert contactIndividualMap.values();
            }
            List<Contact> contactsToUpdate = new List<Contact>();
            for(Contact contact : contactIndividualMap.keySet()){
                contact.individualId = contactIndividualMap.get(contact).Id;
                contactsToUpdate.add(contact);
                
            }
            system.debug('contactsToUpdate' + contactsToUpdate);
            update contactsToUpdate;
        } catch (Exception Ex){
            StatusLogHelper.logSalesforceError('AddIndividualToContact_Batch', 'execute', 'Error in Batch class', ex, false);
        }
    }
    
    
    
    global void finish(Database.BatchableContext bc) {
        //Do Nothing.
    }
}