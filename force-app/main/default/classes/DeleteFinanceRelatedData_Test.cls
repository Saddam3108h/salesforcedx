/********************************************************************************************
Name: DeleteFinanceRelatedData_Test
=============================================================================================
Purpose: This test class is covering 'DeleteFinanceRelatedData' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0		    Gagan Pathak 	       14-August-2020	SFD-1596		     Intial version
********************************************************************************************/
@isTest
public class DeleteFinanceRelatedData_Test{
    Public static testMethod void myUnitTest(){
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account ac = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{	
                'Name'=>'Test',
                    'Customer_Serno__c' => '1122334455'
                    });
            
            Contact con = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'LastName' => 'Testing',
                    'SSN__c'=>'353535353535',
                    'SerNo__c'=>'4353077'
                    });
            
            Product_Custom__c pd = UnitTestDataGenerator.testProductCustom.buildInsert(new Map<String, Object>{
                'Name' => 'Test',
                    'Pserno__c' => '1542'
                    });
            
            List<Financial_Account__c> falist = new List<Financial_Account__c>();
            String clsDat = String.valueof(System.today());
            //Date dt=System.today().addmonths(-120);
            //String clsdDat=String.valueOf(dt);
            Financial_Account__c fa = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Product__c' => pd.id,
                    'Account_Serno__c' => '1200',
                    'Account_Number__c' => '125646456',
                    'Customer__c' => ac.Id,
                    'Customer_Serno__c' => '126464',
                    'Product_Serno__c' => '454564',
                    'FA_Status__c' => true,
                    'Fa_ClosedDate__c' => clsDat,
                    'X120_Months__c' => clsDat
                    });
            falist.add(fa);
            
            
            
            List<Memo__c> mlist = new List<Memo__c>();
            Memo__c mem = new Memo__c();
            mem.Financial_Account__c = fa.Id;
            mlist.add(mem);
            insert mlist;
            
            List<Prime_Action_Log__c> primelist = new List<Prime_Action_Log__c>();
            Prime_Action_Log__c prime = UnitTestDataGenerator.testPrimeAction.buildInsert(new Map<String, Object>{
                'Financial_Account__c' => fa.Id
                    });
            primelist.add(prime);
            
            
            List<Cross_Sell_Response__c> crosellist = new List<Cross_Sell_Response__c>();
            Cross_Sell_Response__c cros = UnitTestDataGenerator.testCrossSellResponse.buildInsert(new Map<String, Object>{
                'Financial_Account__c' => fa.id,
                    'Cross_Sell_Success__c' => 'Y',
                    'Service_Sold__c' => 'Test'
                    });
            crosellist.add(cros);
            
            
            List<Retention_Response__c> retentionlist = new List<Retention_Response__c>();
            Retention_Response__c retention = UnitTestDataGenerator.testRetentionResponse.buildInsert(new Map<String, Object>{
                'Financial_Account__c' => fa.id,
                    'Account_Closed__c' => 'Y'
                    });
            retentionlist.add(retention);
            
            
            List<Manage_Campaign__c> camplist = new List<Manage_Campaign__c>();
            Manage_Campaign__c camp = UnitTestDataGenerator.testManageCampaign.buildInsert(new Map<String, Object>{
                'FinAccount__c' => fa.Id,
                    'AccountSerno__c' => '1234566',
                    'Campaign_Code__c' => '12456'
                    });
            camplist.add(camp);
            
            
            
            Test.StartTest();
            DeleteFinanceRelatedData dbr = new DeleteFinanceRelatedData();
            dbr.errorMap.put(camplist[0].id,'test');
            Database.executeBatch(dbr,200);
            system.assertEquals('Y', cros.Cross_Sell_Success__c,'The Cross Sell Response is not success');
            Test.StopTest(); 
        }
    }
}