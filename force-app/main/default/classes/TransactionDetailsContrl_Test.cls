/**********************************************************************
Name:TransactionDetailsContrl_Test
=======================================================================
Purpose: This is the test class for 'TransactionDetailController' ApexClass
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date          CreatedforJira    Detail
1.0         Kakasaheb Ekshinge       06-Aug-2020   SFD-1535          Initial Version
**********************************************************************/
@isTest
public class TransactionDetailsContrl_Test {
    static testMethod void testTransactionDetailsContrlMethod(){        
        System.runAs(UnitTestDataGenerator.adminUser){ 
            Test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            
            Account accountObj=UnitTestDataGenerator.testAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c'=>'1123123'
                    });
            
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj =UnitTestDataGenerator.testContact.buildInsert();
            
            
            /*Product_Custom__c prodobj=UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            
            Financial_Account__c finAccObj=UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd', 
                    'Customer__c'=>accountObj.Id, 
                    'Account_Serno__c'=>'3123123', 
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            
            Card__c cardObj=UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id,
                    'Card_Serno__c'=>'90247211',
                    'Financial_Account_Serno__c'=>finAccObj.Id,
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c,
                    'Product__c'=>prodobj.Id
                    })*/
            
            Case cs = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'ContactId'=>contactObj.Id,
                    'Category__c'=>'Dispute',
                    'Origin'=>'Web',
                    'Department__c'=>'Fraud and Chargeback',
                    'Dispute_Type__c'=>'0: lost'
                    //'Card__c'=>cardObj.id
                    }); 
            
            DCMS_Transaction__c tran = UnitTestDataGenerator.TestDCMS_Transaction.buildInsert(new Map<String, Object>{
                'DisputeType__c' =>'7: multiple imprint fraud',
                    'MerchCountry__c' => 'DK',
                    'CaseNumber__c'=>cs.Id,
                    'Chargeback__c'=>true,
                    'Representment__c'=>true,
                    'Arbitration__c'=>false,
                    'WriteOff__c'=>false,
                    'FraudWriteOff__c'=>false,
                    'Recovery__c'=>false
                    //tran.CH_Liable__c = false;
                    //txnlist.add(tran);
                    });
            
            List<string> txnlist = new List<string>();
            txnlist.add(tran.id);
            
            String chekboxval;
            Id CsownerId =  tran.id;
            
            Decimal CHvalue = 200;
            
            TransactionDetailsContrl.getTrans(cs.id);
            TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
            
            TransactionDetailsContrl.getGross(cs.id);
            TransactionDetailsContrl.getCaseOwnerId(cs.id);
            TransactionDetailsContrl.getTotalRisk(cs.id);
                        
            chekboxval = 'cb';
            tran.Chargeback__c = false;
            update tran;
            TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
            
            chekboxval = 'cb';
            tran.Chargeback__c = true;
            tran.Representment__c = false;
            update tran;
            TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
            
            chekboxval = 'REP';
            tran.Chargeback__c = true;
            tran.Representment__c = false;
            update tran;
            TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
            
            chekboxval = 'REP';
            tran.Representment__c = true;
            tran.Arbitration__c = false;
            update tran;
            TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
            
            chekboxval = 'CHL';
            tran.Chargeback__c = false;
            update tran;
            TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
            
            chekboxval = 'WO';
            update tran;
            tran.WriteOff__c = false;
            TransactionDetailsContrl.updateTxns(tran.id, chekboxval);
            
            DCMS_FraudCase__c fruadcaselist = [SELECT id, TotalCb__c,FraudWO__c,NonFraudWO__c,TotalFraudAmt__c,
                                               TotalRefund__c,TotalToCh__c,TotalAtRisk__c FROM DCMS_FraudCase__c 
                                               WHERE Case_Number__c = :cs.id Limit 1];
            fruadcaselist.NonFraudWO__c = 1.00;
            update fruadcaselist;
            // TransactionDetailsContrl.updateChliable(cs.id, CHvalue);
            
            fruadcaselist.NonFraudWO__c = 0.00;
            fruadcaselist.FraudWO__c = 1.00;
            update fruadcaselist;
            TransactionDetailsContrl.updateChliable(cs.id, CHvalue);
            
            TransactionDetailsContrl.deleteTxns(txnlist);
            system.assertEquals(false, tran.Chargeback__c,'Charge Back is not ticked' );
            Test.stopTest();
        } 
    }
}