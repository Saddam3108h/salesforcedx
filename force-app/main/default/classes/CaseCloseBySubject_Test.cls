/**********************************************************************
Name: CaseCloseBySubject_Test
=======================================================================
Purpose: This is the Test class for CaseCloseBySubject class
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author               Date              Detail
1.0         HSR Krishna          29-07-2020	       Initial Version

**********************************************************************/
@isTest(SeeAllData=false)
public class CaseCloseBySubject_Test {
    /**********************************************************************************************
* @Author: HSR Krishna       
* @Date: 27/07/2020      
* @Description: Test method to cover the code for closing the case by subject, toaddress, category and department.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    static testmethod void caseCreationTest(){
        Test.starttest();   
        try{
            System.runAs(UnitTestDataGenerator.adminUser) {
                Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                    'Subject'=>'Ny överföring',
                        'To_Address__c'=>'tekwkorts@entercard.com',
                        'Category__c'=>'Fund Transfer/Bill Payment',
                        'Department__c'=>'Customer Service',
                        'Status'=>'New'    
                        });
                CaseCloseBySubject.fetchCases(new List<ID>{caseObj.Id});    
                System.assertEquals(caseObj.Category__c, 'Fund Transfer/Bill Payment');
            }          
        }catch(Exception exp){
            //StatusLogHelper.logSalesforceError('EmailHandler', 'defaultError', 'Error in SSN linking to the Case', exp, false);
        }
        Test.stopTest();
    }
}