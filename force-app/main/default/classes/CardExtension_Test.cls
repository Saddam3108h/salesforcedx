/********************************************************************************************
Name: CardExtension_Test
=============================================================================================
Purpose: To cover the 'CardExtension' apex class code coverage 
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0        Srinivas D             4th-April-2019    SFD-984             Intial version
2.0        Kakasaheb Ekshinge     22-July-2020      SFD-1596            Code coverage has increases
********************************************************************************************/
@isTest
public class CardExtension_Test {  
    @testSetup
    public static void testInsertData() 
    {
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(usr){
            Test.startTest();
            TriggerSettings__c triggerObj = new TriggerSettings__c();
            triggerObj.Name = 'Execution';
            triggerObj.CustomerExecution__c = false;
            triggerObj.OrgExecution__c = false;
            insert triggerObj;
            
            UnitTestDataGenerator_Test.Finance_test();
            Financial_Account__c finAccObj = [SELECT Id,Customer__c FROM Financial_Account__c LIMIT 1 ];
            Contact conObj = [SELECT Id FROM Contact LIMIT 1 ];
            Product_Custom__c  prodobj = [SELECT Id,Pserno__c FROM Product_Custom__c LIMIT 1 ];
            
            Card__c cardObj = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>conObj.Id,
                    'PrimaryCardFlag__c'=> false,
                    'Financial_Account__c'=> finAccObj.Id,
                    'Card_Serno__c'=>'524858519',
                    'Card_Number_Truncated__c'=>'7136785481583561', 
                    'Financial_Account_Serno__c'=>finAccObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c,
                    'Institution_Id__c'=>2,
                    'Product__c' =>prodobj.Id,
                    'People_Serno__c'=> '4530645'
                    });
            System.debug('finAccObj.Customer__c::'+finAccObj.Customer__c);
            System.debug('conObj::=>'+conObj);
            //UnitTestDataGenerator_Test.Case_test_WithParamter(finAccObj.Customer__c,conObj.Id,cardObj.Id,finAccObj.Id);
            //UnitTestDataGenerator_Test.Card_test_WithParamter(finAccObj.Customer__c,conObj.Id,finAccObj.Id,prodobj);
            
            List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
            GlobalSettings__c objGlobal = new GlobalSettings__c();
            objGlobal.Name = 'ServiceStrategies'; 
            objGlobal.Value__c = 'SOAPService';
            GlobalSettingsList.add(objGlobal);
            
            insert GlobalSettingsList;
            
            List<ServiceHeaders__c> serviceHeadersList =new List<ServiceHeaders__c>();
            ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
            objServiceHeaders.name = 'SOAPHTTP';
            serviceHeadersList.add(objServiceHeaders);
            
            ServiceHeaders__c objServiceHeaders1 = new ServiceHeaders__c();
            objServiceHeaders1.name = 'StatusLogServiceHeader';
            serviceHeadersList.add(objServiceHeaders1);
            insert serviceHeadersList;  
            
            List<ServiceSettings__c> serviceSettingList = new List<ServiceSettings__c>();
            ServiceSettings__c objServiceSettings = new ServiceSettings__c();
            objServiceSettings.Name = 'GetCustomerDetails';
            objServiceSettings.HeaderName__c = 'SOAPHTTP'; 
            //  objServiceSettings.EndPoint__c = 'https://services-test.entercard.com/gateway/services/CSSService?wsdl';
            objServiceSettings.EndPoint__c = 'https://midtlb4d2pub.entercard.com/gateway/services/CSSService?wsdl';
            objServiceSettings.OutputClass__c = 'GetCustomerOutput';
            objServiceSettings.ProcessingClass__c = 'GetCustomerCallout'; 
            objServiceSettings.Strategy__c = 'SOAPService';
            objServiceSettings.Input_Class__c = 'GetCustomerInput'; 
            objServiceSettings.Username__c='css_soa';
            objServiceSettings.Password__c='P1%gruzA';
            serviceSettingList.add(objServiceSettings);
            
            insert serviceSettingList;
            
            List<Common_Settings__c> commonSettingList =new List<Common_Settings__c>();
            Common_Settings__c commonSetting =new Common_Settings__c(
                Name='CorrelationId',
                Common_Value__c='1'
            );
            commonSettingList.add(commonSetting);
            Common_Settings__c commonSetting1 =new Common_Settings__c(
                Name='RequestorId',
                Common_Value__c='Salesforce'
            );
            commonSettingList.add(commonSetting1);
            Common_Settings__c commonSetting2 =new Common_Settings__c(
                Name='SystemId',
                Common_Value__c='1'
            );
            commonSettingList.add(commonSetting2);
            insert commonSettingList;
            
            ErrorCodes__c errorCodes =new ErrorCodes__c();
            errorCodes.Name='0';
            errorCodes.ErrorDescription__c='ErrorDescription';
            errorCodes.UIDisplayMessage__c='UIDisplayMessage';
            insert errorCodes;
            
            ErrorCodes__c errorCodes1 =new ErrorCodes__c();
            errorCodes1.Name='defaultError';
            errorCodes1.ErrorDescription__c='ErrorDescription';
            errorCodes1.UIDisplayMessage__c='UIDisplayMessage';
            insert errorCodes1;
            
            
            
            
            Test.stopTest();
        }
        
        
    }
    
    public static testmethod void testMethod1() {
        Card__c cardObj =[SELECT Id, Name,PrimaryCardFlag__c FROM Card__c where PrimaryCardFlag__c = false limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(cardObj);
      //  sc.Cardlist[0].PrimaryCardFlag__c = false;
        test.startTest();
        Test.setMock(WebServiceMock.class, new GetCustomerWebServiceMockclass());
        PageReference pageRef = Page.Card_Status;        
        Test.setCurrentPage(pageRef);        
        CardExtension cd=new CardExtension(sc);
        cd.cardgenStatus     =  'Tt';
        System.debug('=========>cardgenstatus: '+cd.cardgenStatus);
        cd.cardauthStatus    = 'Tesssst';
        cd.cardfinStatus     = 'New';
        cd.ExpiryDate        = Date.today() + 31;
        cd.WrongPinAttempts  = 1;
        cd.doCall();
        test.stopTest();
        
    }
    
    public static testmethod void testException() {
        
        test.startTest();
        Card__c cardObj =[SELECT Id, Name,PrimaryCardFlag__c,Financial_Account__c,Institution_Id__c,People__r.SSN__c FROM Card__c WHERE PrimaryCardFlag__c = false limit 1];
        // List<Card__c>   lstCard =  new List<Card__c>();
        System.debug('cardObj=>'+cardObj);
        System.debug('Institution_Id__c=>'+cardObj.Institution_Id__c);
        ApexPages.StandardController sc = new ApexPages.StandardController(cardObj);     
        CardExtension cd=new CardExtension(sc);    
        List<Card__c> lstCard =  null;//new List<Card__c>();        
        cd.Cardlist = lstCard;
        try{
            cd.doCall();
        }   catch(DMLException e){
            system.assert(e.getMessage().contains(e.getMessage()));
            
        }
        
        System.debug('CD::=>'+cd);// should be three exception method for test
        //System.assertEquals(cd.Cardlist, integer.valueof(Cardlist[0].Institution_Id__c)+Cardlist[0].People__r.SSN__c);
        test.stopTest();
    }
    
}