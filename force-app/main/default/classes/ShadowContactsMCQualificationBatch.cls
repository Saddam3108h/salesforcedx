public class ShadowContactsMCQualificationBatch implements Database.Batchable<sObject>{
    
    //Instance variable initializations
      public String query = null;
    
    //Constructor
    public ShadowContactsMCQualificationBatch(String operationType){
        query = 'SELECT Customer_Shadow__c FROM Card__c WHERE Customer_Shadow__c != null ';
        Switch on operationType {
            When 'full' {
                query += 'AND Financial_Account__r.Open_for_Marketing__c = true';
            }
            //Add more WHEN statements if required
        }
    }
    
    //Start method
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    //Actual code execution
    public void execute(Database.BatchableContext BC, List<Card__c> cards){
        Set<Id> contactIds = new Set<Id>();
        List<Contact> updatedContacts = new List<Contact>();          
        
        //Loop over Cards to find Contact IDs
        for(Card__c c : cards) contactIds.add(c.Customer_Shadow__c);
        
        //Update Shadow Contacts
        for(Contact con : [SELECT Id, Qualified_for_Marketing__c FROM Contact WHERE Id IN :contactIds AND Qualified_for_Marketing__c = false]) {
            con.Qualified_for_Marketing__c = true;
            updatedContacts.add(con);
        }        
        if(updatedContacts.size() >0) Database.update(updatedContacts,false);  
    }
    
    //Finish method
    public void finish(Database.BatchableContext BC){}
}