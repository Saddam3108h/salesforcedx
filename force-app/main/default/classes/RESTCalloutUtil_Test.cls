/********************************************************************************************
Name: RESTCalloutUtil_Test
=============================================================================================
Purpose: This test class is covering 'RESTCalloutUtil' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date             Created for Jira     Detail
1.0		    Kakasaaheb Ekshinge 	  17-Nov-2020		SFD-1840		     Intial version
********************************************************************************************/
@isTest
public class RESTCalloutUtil_Test {
    Public static testMethod void withFlagOff(){
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.StartTest();             
            AccessTokenOnOff__c accToken = new AccessTokenOnOff__c();
            accToken.Name = 'TokenOnOff';
            accToken.active__c = false;
            insert accToken;
            RESTCalloutUtil.validateUserAccTkn();
            Test.StopTest(); 
        }
    }
    
    Public static testMethod void withFlagOn(){
        User u1 = [SELECT id from User WHERE ID = : userinfo.getUserId()];
        System.runAs(u1){
            AccessTokenOnOff__c accToken = new AccessTokenOnOff__c();
            accToken.Name = 'TokenOnOff';
            accToken.active__c = true;
            insert accToken;
            
            ManageRESTServices__c manageRest = new ManageRESTServices__c();
            manageRest.Name = 'CurityEndPoint';
            manageRest.CertificateName__c =  Label.CertificateName;
            manageRest.Endpoint__c = system.Label.Curity_endpoint;
            manageRest.ClientId__c = system.Label.Curity_client_id;
            manageRest.ClientSecret__c = system.Label.Curity_client_secret;
            manageRest.GrantType__c = 'urn:ietf:params:oauth:grant-type:jwt-bearer';
            manageRest.ContentType__c = 'application/x-www-form-urlencoded';
            insert manageRest;
            
            DateTime dt = System.now();
            DateTime myDT = dt.addMinutes(-20);
            
            ManageCurityAccessToken__c manageCurityAccessTokn = new ManageCurityAccessToken__c();
            manageCurityAccessTokn.Access_Token__c = '1234567890';       
        	manageCurityAccessTokn.AccessTokenExpriy__c = 20;      
        	manageCurityAccessTokn.Created_Date__c = myDT;         
        	manageCurityAccessTokn.SetupOwnerId = userinfo.getUserId();       
        	manageCurityAccessTokn.Key__c ='11111';  
            insert manageCurityAccessTokn;
            String accountNumber ='12342';
            Map<String, String> attributeMap = new Map<String, String>();            
            String jsonInput = '{"accountNumber":'+accountNumber+',"startAtRowNum": 1,"stopAfterRowNum": 5}';
            attributeMap.put('accessToken','1234567890');
            attributeMap.put('certificate', system.Label.CertificateName);
            attributeMap.put('endpoint',system.Label.last5TranEndpoint);
            attributeMap.put('httpMethod','POST');
            attributeMap.put('ContentType','application/json');
            attributeMap.put('inputJson',jsonInput); 
            
			RESTCalloutUtil.fetchDataByMap(attributeMap);
            
            RESTCalloutUtil.AccessTokenWrapper accToknWrapObj = new RESTCalloutUtil.AccessTokenWrapper();
            Test.setMock(HttpCalloutMock.class, new CurityCalloutMock());   
            Test.StartTest(); 
         
            RESTCalloutUtil.validateUserAccTkn();
            Test.StopTest(); 
        }
    }   
    
    Public static testMethod void coverUpdateUser(){
        User u1 = [SELECT id from User WHERE ID = : userinfo.getUserId()];
        System.runAs(u1){
            AccessTokenOnOff__c accToken = new AccessTokenOnOff__c();
            accToken.Name = 'TokenOnOff';
            accToken.active__c = true;
            insert accToken;
            
            ManageRESTServices__c manageRest = new ManageRESTServices__c();
            manageRest.Name = 'CurityEndPoint';
            manageRest.CertificateName__c =  system.Label.CertificateName;
            manageRest.Endpoint__c = system.Label.Curity_endpoint;
            manageRest.ClientId__c = system.Label.Curity_client_id;
            manageRest.ClientSecret__c = system.Label.Curity_client_secret;            
            manageRest.GrantType__c = 'urn:ietf:params:oauth:grant-type:jwt-bearer';
            manageRest.ContentType__c = 'application/x-www-form-urlencoded';
            insert manageRest;
                        
            RESTCalloutUtil.AccessTokenWrapper accToknWrapObj = new RESTCalloutUtil.AccessTokenWrapper();
            Test.setMock(HttpCalloutMock.class, new CurityCalloutMock());   // ExampleCalloutMock
            Test.StartTest(); 
            RESTCalloutUtil.validateUserAccTkn();
            Test.StopTest(); 
        }
    }    
    
}