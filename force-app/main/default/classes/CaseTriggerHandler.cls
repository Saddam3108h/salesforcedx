/**********************************************************************
Name: CaseTriggerHandler
=======================================================================
Purpose: This trigger handler class is created on Case object to handle all the contact related logics. User Story SFD-1431

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         D.Srinu              20-Feb-2019         Initial Version
1.1         Saddam Hussain       04-August-2020       
**********************************************************************/
public with sharing class CaseTriggerHandler implements ITriggerHandler {
    public static Boolean TriggerDisabled = false;
    
    /*
Checks to see if the trigger has been disabled either by custom setting or by running code
*/  
    public Boolean IsDisabled()
    {
        
        if (TriggerSettings__c.getInstance('Execution').CaseExecution__c // false
            &&  TriggerSettings__c.getInstance('Execution').OrgExecution__c)
        { 
            return true;
        }
        else
        { 
            return TriggerDisabled;
        }
        
    }
    
    public void BeforeInsert(List<SObject> newItems) {
        //throwErrorOnChildCaseInsert(newItems);
        linkFinAccountCustomer(newItems);
        linkCustomerToCase(newItems);
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, Case> newCaseItems = (Map<Id, Case>) newItems;
        Map<Id, Case> oldCaseItems = (Map<Id, Case>) oldItems;
        if(checkRecursive.runOnce()){
            updateFraudCases(newCaseItems, oldCaseItems);
        }
        //throwErrorOnChildCaseUpdate(newItems.values(), oldItems.values());
        updateRetentionFields(newItems.values());
         // Added 22 March 
        if(!Test.isRunningTest()) {
              updateCaseHandlingTime(newItems.values(),newCaseItems, oldCaseItems);
         }        
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterInsert(Map<Id, SObject> newItems) {
        Map<Id, Case> newCaseItems = (Map<Id, Case>) newItems;
        createFraudCases(newCaseItems.values());
        updateCaseOwner(newCaseItems.values());
        updateOpenCaseCount(newCaseItems.values(), null,true, null);
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, Case> newCaseItems = (Map<Id, Case>) newItems;
        Map<Id, Case> oldCaseItems = (Map<Id, Case>) oldItems;
        updateCaseOwner(newCaseItems.values());
        updateOpenCaseCount(newCaseItems.values(), oldCaseItems.values(),null, true);
        //updateAssignedUser(newCaseItems, oldCaseItems);
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    public List<DCMS_Decision__c> decisionStatusCS; 
    public List<DCMS_CaseStatus__c> caseStatusCS;
    public List<DCMS_FraudRing__c> fraudRingCS;
    
    Public Map<String,Decimal> decisionStatusMap = new Map<String,Decimal>();
    Public Map<String,Decimal> caseStatusMap = new Map<String,Decimal>();
    Public Map<String,Decimal> fraudRingMap = new Map<String,Decimal>();
    Public Map<String,Id> queueMap = new Map<String,Id>();
    Public Map<String,String> qcsMap = new Map<String,String>();
    
    public void CaseTriggerHandler() {
        //Getting Custom Settings Data
        decisionStatusCS = DCMS_Decision__c.getall().values();
        caseStatusCS = DCMS_CaseStatus__c.getall().values();
        fraudRingCS = DCMS_FraudRing__c.getall().values();
        
        for(DCMS_Decision__c desCS : decisionStatusCS){
            decisionStatusMap.put(desCS.Name,desCS.DecisionId__c);
        }
        
        for(DCMS_CaseStatus__c stsCS : caseStatusCS){
            caseStatusMap.put(stsCS.Name,stsCS.CaseStatusId__c);
        }
        
        for(DCMS_FraudRing__c frdCS : fraudRingCS){
            fraudRingMap.put(frdCS.Name,frdCS.FraudRingId__c);
        }
        for(DCMS_Queue_Settings__c qcset : DCMS_Queue_Settings__c.getall().values()){
            qcsMap.put(qcset.Name,qcset.Queue_Name__c);
        }
        
        System.debug('qcsMap with SOQL'+qcsMap);
        // Below for loop is to store Queue details into Map
        for(Group grpqueue : [select Id,Name,DeveloperName from Group where Type = 'Queue']){
            queueMap.put(grpqueue.DeveloperName,grpqueue.Id);
        }
    }
    /**********************************************************************
*   Author: D Srinivas
*   Date:     20-Feb-2019
*   User Story : SFD-984
*   Param: None
*   Return: None    
*   Description:  In this method we are trying to create the fraud case based on case creation.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public void createFraudCases(List<Case> caseList){
        //Calling Constructor to get QueueMap data here
        try{
            CaseTriggerHandler();
            List<Case> caseNewList = [SELECT Id,CreatedBy.Name,Description,Card_SerNo__c,Card__c,Owner.Name,
                                      Card__r.Financial_Account__r.Account_Serno__c,Status,Fraud_Ring__c,
                                      Decision_Status__c,Dispute_Type__c,Institutionid__c,Gross_Amount__c,Card_Number_Masked__c,
                                      Pci_Dss_Token__c,CreatedDate,Contact.SerNo__c,Contact.Institution_Id__c,
                                      (SELECT Id,Institutionid__c,BillingAmount__c FROM DCMS_Transactios__r) 
                                      FROM Case 
                                      WHERE Id IN: caseList];
            List<DCMS_FraudCase__c> fraudCaseList = new List<DCMS_FraudCase__c>(); 
            for(Case caseObj : caseNewList) {
                if(caseObj.Dispute_Type__c != null) {
                    DCMS_FraudCase__c fraudCaseObj = new DCMS_FraudCase__c();
                    fraudCaseObj.Case_Number__c = caseObj.Id;
                    fraudCaseObj.TruncatedCardNumber__c = caseObj.Card_Number_Masked__c;
                    if(caseObj.Card_SerNo__c != null){
                        fraudCaseObj.Card_Serno__c = Decimal.valueOf(caseObj.Card_SerNo__c);
                    }
                    fraudCaseObj.Institutionid__c = caseObj.Institutionid__c; //dtcase.Contact.Institution_Id__c;
                    fraudCaseObj.CaseOpenDate__c = Date.valueOf(caseObj.CreatedDate);
                    fraudCaseObj.Description__c = caseObj.Description;
                    fraudCaseObj.DisputeType__c = caseObj.Dispute_Type__c;
                    fraudCaseObj.AssignedUser__c =caseObj.Owner.Name; //'Salesforce Support';
                    fraudCaseObj.DecisionId__c = decisionStatusMap.get(caseObj.Decision_Status__c);
                    fraudCaseObj.CaseStatusId__c = caseStatusMap.get(caseObj.Status);
                    fraudCaseObj.FraudRingId__c = fraudRingMap.get(caseObj.Fraud_Ring__c);
                    fraudCaseObj.FraudWO__c = 0;
                    fraudCaseObj.TotalCb__c = 0;
                    fraudCaseObj.NonFraudWO__c = 0;
                    fraudCaseObj.TotalAtRisk__c = caseObj.Gross_Amount__c; //dtcase.DCMS_Transactios__r[0].BillingAmount__c;
                    fraudCaseObj.TotalFraudAmt__c = caseObj.Gross_Amount__c;
                    if(caseObj.ContactId != null){
                        fraudCaseObj.Peopleserno__c = Decimal.valueOf(caseObj.Contact.SerNo__c);
                    }
                    fraudCaseObj.Pcidsstoken__c = caseObj.Pci_Dss_Token__c;
                    //fraudCaseObj.NumberOfTransactions__c= caseObj.DCMS_Transactios__r.size();                    
                    fraudCaseList.add(fraudCaseObj);
                }
            }
            if(fraudCaseList.size() > 0){
                insert fraudCaseList;
            }
        } catch(Exception exp){
            StatusLogHelper.logSalesforceError('CaseTriggerHandler', 'E004', 'Error in createFraudCases method ' , exp, false);
        }
    }
    /**********************************************************************
*   Author: D Srinivas
*   Date:     20-Feb-2019
*   User Story : SFD-984
*   Param: None
*   Return: None    
*   Description:  In this method we are trying to update the fraud case .
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/ 
    public void updateFraudCases(Map<id,Case> dispCases, Map<Id,Case> oldCases){
        try{
            //Calling Constructor to get QueueMap data here
            CaseTriggerHandler();
            //Declaring fraudCasesToupdateList to update fraudCases
            List<DCMS_FraudCase__c> fraudCasesToupdateList = new List<DCMS_FraudCase__c>();
            
            List<DCMS_Transaction__c> transToupdateList = new List<DCMS_Transaction__c>();
            List<Case> CasesToupdateList = new List<Case>();
            //Below Code is to get DCMS_FraudCase__c records related to updated Cases.  
            Map<Id,DCMS_FraudCase__c> fCaseMap = new  Map<Id,DCMS_FraudCase__c>();
            for(DCMS_FraudCase__c fcase : [SELECT Id,Description__c,CaseCloseDate__c,DisputeType__c,Case_Number__c,PoliceInquiry__c,AssignedUser__c
                                           FROM DCMS_FraudCase__c where Case_Number__c IN:dispCases.keyset()]){
                                               fCaseMap.put(fcase.Case_Number__c,fcase);
                                           }
            System.debug('fCaseMap--->> '+fCaseMap);
            Map<Id,Case> casesMap = new Map<Id,Case>([SELECT Id,Description,Card_SerNo__c,Gross_Amount__c,OwnerId, Owner.Name,
                                                      Dispute_Type__c,Institutionid__c,Note__c,Card_Number_Masked__c,Pci_Dss_Token__c,
                                                      CreatedDate,Contact.SerNo__c,Contact.Institution_Id__c,Police_Inquiry__c,
                                                      (SELECT Id,Institutionid__c,BillingAmount__c,DisputeType__c 
                                                       FROM DCMS_Transactios__r) 
                                                      FROM Case WHERE Id IN:dispCases.keyset()]);
            System.debug('casesMap--->> '+casesMap);
            for(Case cas : dispCases.values()){
                if(cas.Dispute_Type__c != null 
                   && cas.Institutionid__c != null
                  ) {
                      String temp= cas.Dispute_Type__c+cas.Institutionid__c;
                      System.debug('temp===>> '+temp);
                      if(queueMap.get(qcsMap.get(temp)) != null 
                         && (oldCases.get(cas.Id).Dispute_Type__c!=dispCases.get(cas.Id).Dispute_Type__c)
                        ) {
                            System.debug('queueMap===>> '+queueMap.get(qcsMap.get(temp)));
                            cas.OwnerId = queueMap.get(qcsMap.get(temp));
                            // CasesToupdateList.add(cas);
                        }           
                  }
        
                //Below code to assign values from Case to fraudCase 
                if(fCaseMap.get(cas.id) != null){ 
                    // cas.Gross_Amount__c=0.0;            
                    fCaseMap.get(cas.id).DisputeType__c= cas.Dispute_Type__c;
                    fCaseMap.get(cas.id).Description__c= cas.Note__c;
                    fCaseMap.get(cas.id).Institutionid__c= cas.Institutionid__c;
                    fCaseMap.get(cas.id).AssignedUser__c = cas.Owner.Name;
                    fCaseMap.get(cas.id).DecisionId__c = decisionStatusMap.get(cas.Decision_Status__c);
                    fCaseMap.get(cas.id).CaseStatusId__c = caseStatusMap.get(cas.Status);
                    fCaseMap.get(cas.id).FraudRingId__c = fraudRingMap.get(cas.Fraud_Ring__c);
                    fCaseMap.get(cas.id).PoliceInquiry__c = cas.Police_Inquiry__c;
                    fCaseMap.get(cas.id).NumberOfTransactions__c= casesMap.get(cas.Id).DCMS_Transactios__r.size();
                    fraudCasesToupdateList.add(fCaseMap.get(cas.id));
                }
                
                //Update Transaction Records when Case is updated
                for(DCMS_Transaction__c casTran : casesMap.get(cas.Id).DCMS_Transactios__r){
                    System.debug('Case Dispute===>>>> '+ cas.Dispute_Type__c);
                    casTran.DisputeType__c = cas.Dispute_Type__c;
                    transToupdateList.add(casTran);
                }
            }
            
            if(transToupdateList.size() > 0){
                update transToupdateList;
            }           
            //update fraudCasesToupdateList when case records updated and fraudCasesToupdateList size is grater than zero
            if(fraudCasesToupdateList.size() > 0){
                update fraudCasesToupdateList;
            }
        } catch(Exception exp) {
            StatusLogHelper.logSalesforceError('CaseTriggerHandler', 'E004', 'Error in updateFraudCases method ' , exp, false);
        }
    }
    
    /**********************************************************************
*   Author: SPAL
*   Date:     19/06/2017
*   User Story : CSS-1079
*   Param: None
*   Return: None    
*   Description:  In this method it throws the error if child created by another owner instead parent case owner.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/ 
    /*public void throwErrorOnChildCaseInsert(List<Case> caseList){
Set<Id> closeCase=new set<Id>();
Set<Id> newParentCaseID=new Set<Id>();//Child Case Inserted// Added by SPAL on 4 JULY 2017
Map<Id,id> parentidVSOwner=new Map<Id,Id>();//Child Case Inserted// Added by SPAL on 4 JULY 2017
for(Case caseObj : caseList) {
if(caseObj.Status.equalsignorecase('Closed')
||caseObj.status.equalsignorecase('Cancelled')
) {
CloseCase.add(caseObj.id);    
}
//Child Case Inserted// Added by SPAL on 4 JULY 2017
if(caseObj.parentid!=null) {
NewParentCaseID.add(caseObj.parentid);
}
}
//Child Case Inserted// Added by SPAL on 4 JULY 2017
for(case pcase : [SELECT id,ownerid FROM case WHERE Id IN: NewParentCaseID]) {
ParentidVSOwner.put(pcase.id,pcase.ownerid);
}
for(Case caseObj : caseList) {
if(caseObj.parentid!=null 
&& ParentidVSOwner.containskey(caseObj.parentid) 
&& ParentidVSOwner.get(caseObj.parentid)!=null 
&& ParentidVSOwner.get(caseObj.parentid)!=UserInfo.getUserId()
) {
caseObj.addERROR('Parent case Owner can only create a child case');
}
}

}*/
    /**********************************************************************
*   Author: SPAL
*   Date:     19/06/2017
*   User Story : CSS-1079
*   Param: None
*   Return: None    
*   Description:  In this method it throws the error if directly close the child before close the parent case.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/ 
    /* public void throwErrorOnChildCaseUpdate(List<Case> caseList, List<Case> caseOldList){
Set<Id> closeCaseSet = new Set<Id>();
integer i=0;
for(case csObj : caseList) {
case oldCase = caseOldList[i++];
if(csObj.status != oldCase.status 
&& (csObj.Status.equalsignorecase('Closed')
|| csObj.status.equalsignorecase('Cancelled'))
) {
closeCaseSet.add(csObj.id);
}
}
Map<Id,Integer> parentClindCaseMap = new Map<Id,Integer>();
for(case ccase : [SELECT Id,(SELECT Id FROM Cases WHERE (ISCLOSED=false OR Status!='Closed') AND status!='Cancelled') 
FROM case WHERE Id IN: closeCaseSet]) {
parentClindCaseMap.put(ccase.Id,ccase.Cases.size());
}

for(Case cseObj : caseList) {
if(ParentClindCaseMap.containskey(cseObj.id) 
&& ParentClindCaseMap.get(cseObj.id)!=null 
&& ParentClindCaseMap.get(cseObj.id)>0)
cseObj.addERROR('All the child cases need to be closed or cancelled before parent case is close or cancelled');
}
}*/
    
    /**********************************************************************
*   Author: SPAL
*   Date:     19/06/2017
*   User Story : CSS-1079
*   Param: None
*   Return: None    
*   Description:  In this method we are trying to update the FA, Customer before case inserted or updated.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/ 
    
    public void linkFinAccountCustomer(List<Case> caseList) {
        Set<Id> customerIdSet = new Set<Id>();
        Set<Id> financialAccIdSet = new Set<Id>();
        for(Case caseObj : caseList) {
            if(caseObj.ContactId!=null 
               && caseObj.Financial_Account__c==null
               && caseObj.Dispute_Type__c == ''
              ) {
                  customerIdSet.add(caseObj.ContactId);
              } else if(caseObj.ContactId==null 
                        && caseObj.Financial_Account__c!=null
                       ) {
                           financialAccIdSet.add(caseObj.Financial_Account__c);
                       }
        }
        for(Card__c cardObj : [SELECT People__c,Financial_Account__c 
                               FROM Card__c WHERE People__c IN: customerIdSet
                               AND Financial_Account__c!=null Limit 50000]
           ) {
               financialAccIdSet.add(cardObj.Financial_Account__c);
           }
        
        List<Financial_Account__C> financialList = [SELECT Id,Account_Number__c,(SELECT Id,People__c 
                                                                                 FROM Cards__r 
                                                                                 WHERE PrimaryCardFlag__c=true LIMIT 1) 
                                                    FROM Financial_Account__c 
                                                    WHERE Id IN: financialAccIdSet LIMIT 1];
        for(Case objCase : caseList) {
            if(financialAccIdSet.size() == 1) {
                objCase.Financial_Account__c=(new List<Id>(financialAccIdSet))[0]; 
            }
            if(financialList.size() > 0) {
                objCase.ContactId = financialList[0].Cards__r[0].People__c;
            }
        }
    }
    
    
    /**********************************************************************
*   Author: SPAL
*   Date:     19/06/2017
*   User Story : CSS-1079
*   Param: None
*   Return: None    
*   Description:  In this method we are trying to update the FA, Customer before case inserted or updated.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/ 
    //static boolean stopExecute = TRUE;
    public void updateCaseHandlingTime(List<Case> caseList, Map<Id, Case> caseNewMap, Map<Id, Case> caseOldMap) {
        
        system.debug('--case---'+caseList);
        Set<Id> caseIdSet = new Set<Id>();
        for(Case objCase : caseList){
            caseIdSet.add(objCase.Id);
        }
        system.debug('--caseIdSet---'+caseIdSet);
        CaseHistory[] lstStatusHistory = [SELECT id,Field,newvalue,oldvalue,createddate,caseid 
                                          FROM CaseHistory 
                                          WHERE caseid IN :caseIdSet and field='Status' ORDER BY caseid,createddate DESC];
        system.debug('--lstStatusHistory--'+lstStatusHistory);
        Map<Id,List<CaseHistory>> mapIdCaseHist = new Map<Id,List<CaseHistory>>();
        for(CaseHistory ch : lstStatusHistory)
        {
            Id cid = ch.caseid;
            //mapIdCaseHist.put(ch.caseid,(mapIdCaseHist.get(ch.caseid)!=null?mapIdCaseHist.get(ch.caseid).add(ch):new List<CaseHistory>(ch)));
            if(mapIdCaseHist.get(cid)!=null){
                mapIdCaseHist.get(cid).add(ch);
                mapIdCaseHist.put(cid,mapIdCaseHist.get(cid));
            }
            else
                mapIdCaseHist.put(cid,new List<CaseHistory>{ch});
            
        }
        for(Id cid : mapIdCaseHist.keyset()){
            if(caseNewMap.get(cid).status=='Closed' && caseOldMap.get(cid).status=='In Progress'){//there is status change
                List<CaseHistory> lstStatusHistoryCase = mapIdCaseHist.get(cid);
                Long totalHandlingTime=System.now().getTime()-lstStatusHistoryCase[0].createddate.getTime();
                for(Integer i=1;i<lstStatusHistoryCase.size();i++){
                    if(lstStatusHistoryCase[i].newvalue=='Closed' && lstStatusHistoryCase[i].oldvalue=='In Progress'){
                        if(i==(lstStatusHistoryCase.size()-1))//reached last element of the list and it is a status change to close
                            totalHandlingTime+=(lstStatusHistoryCase[i].createddate.getTime()-caseNewMap.get(cid).createddate.getTime());
                        else
                            totalHandlingTime+=(lstStatusHistoryCase[i].createddate.getTime()-lstStatusHistoryCase[i+1].createddate.getTime());
                        i++;
                    }
                }
                Integer minutes = (Integer) math.mod((totalHandlingTime/ (1000 * 60)) , 60);
                Integer hours = (Integer) math.mod((totalHandlingTime/ (1000 * 60 * 60)) , 24);
                Integer days = (Integer) (totalHandlingTime/ (1000 * 60 * 60 * 24));
                caseNewMap.get(cid).Case_Handling_Time__c = days+' Days,'+hours+' Hours,'+minutes+' Minutes';
                caseNewMap.get(cid).Case_Handling_Time_Minutes__c = (totalHandlingTime/ (1000 * 60));
                system.debug('Trigger.new[0].Case_Handling_Time__c-->'+caseNewMap.get(cid).Case_Handling_Time__c);
            }
        }
    }
    
    /**********************************************************************
*   Author: SPAL
*   Date:     19/06/2017
*   User Story : CSS-1079
*   Param: None
*   Return: None    
*   Description:  In this method we are trying to update the FA, Customer before case inserted or updated.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/ 
    
    public void updateCaseOwner(List<Case> caseList) {
        Set<id>ParentCase=new Set<id>();
        for(case caseObj : caseList)
        {
            if(caseObj.Status.equalsignorecase('Closed') 
               && caseObj.parentid!=null) {   
                   ParentCase.add(caseObj.parentid);
               }
        }
        
        List<case> UpdateParentCaseClose = new List<case>();
        List<case> RetuenbackOwner = new List<case>();
        Map<Id,Id>CaseOwnerMAP = new Map<Id,Id>();
        for(Case ccase : [SELECT Id,status,ownerid,
                          (SELECT Id,Status FROM Cases 
                           WHERE (ISCLOSED=false or Status!='Closed') 
                           AND Status!='Cancelled') 
                          FROM Case WHERE Id IN : ParentCase]
           ) {
               if(ccase.Cases.size() == 0)//All close child or cancelled
               {
                   ccase.status='Closed';
                   CaseOwnerMAP.put(ccase.id,ccase.ownerid);
                   ccase.ownerid=UserInfo.getUserId();// Change the OWNER to current user just to change status of the parent case
                   UpdateParentCaseClose.add(ccase);
                   system.debug('=='+CaseOwnerMAP+'=='+ccase.ownerid);
               }
           }
        
        if(UpdateParentCaseClose.size()>0)
            update UpdateParentCaseClose;        
        
        for(case cOWNER : [SELECT Id,OWNERID FROM Case 
                           WHERE Id IN : CaseOwnerMAP.keyset()]
           ) {
               cOWNER.OWNERID = CaseOwnerMAP.get(cOWNER.id);
               RetuenbackOwner.add(cOWNER);
           }
        
        if(RetuenbackOwner.size()>0)
            update RetuenbackOwner;
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     26/03/2018
*   User Story : SFD-314
*   Param: None
*   Return: None    
*   Description:  In this method we are trying to update the case owner.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/ 
    public void updateRetentionFields(List<Case> caseList) {
        try{
            List<Attachment> attachmentList = new List<Attachment>();
            set<Id> caseIdSet = new Set<Id>();
            for(Case caseObj : caseList){
                caseIdSet.add(caseObj.Id);
            }
            system.debug('---caseSet---'+caseIdSet);
            attachmentList =[SELECT Id, ParentId FROM Attachment WHERE ParentId IN: caseIdSet];
            system.debug('---attacth---'+attachmentList);
            for(Case caseObj : caseList){
                if(caseObj.status == 'Closed'){
                    if(attachmentList.isEmpty()){
                        caseObj.X18_Months__c = Date.today().addmonths(18).format();        
                    } else {
                        caseObj.X120_Months__c = Date.today().addmonths(120).addDays(1).format();
                    }
                } else if(caseObj.status != 'Closed' ){
                    caseObj.X18_Months__c = '';
                    caseObj.X120_Months__c = ''; 
                }
            } 
        } catch(Exception exp){
            StatusLogHelper.logSalesforceError('CaseTriggerHandler', 
                                               'E005', 'updateRetentionFields', exp, false);
        } 
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     26/03/2018
*   User Story : SFD-314
*   Param: None
*   Return: None    
*   Description:  In this method we are trying to update Open case count field in Customer record when case created or update 
based on status.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/ 
    public void  updateOpenCaseCount(List<Case> caseList, List<Case> caseOldList, Boolean isInsert, Boolean isUpdate) {
        try {
            Set<Id> customerIdSet = new Set<Id>();
            List<Contact> updateCustomerList = new List<Contact>();
            if(isInsert == true) {
                for(Case caseObj : caseList) {
                    if(caseObj.ContactId != null){
                        customerIdSet.add(caseObj.ContactId);   
                    }
                } 
            }
            if(isUpdate == true) {
                for(Case caseObj : caseOldList){
                    if(caseObj.ContactId != null) {
                        customerIdSet.add(caseObj.ContactId);   
                    }
                } 
            }
            system.debug('----conIds---'+customerIdSet);
            for(Contact conObj : [SELECT Id,name,Open_Cases__c,(SELECT id,status FROM cases WHERE
                                                                status Not IN ('Closed','Cancelled') Limit 200) 
                                  FROM Contact WHERE Id =: customerIdSet]
               ) {
                   List<Case> cases;
                   try{
                       cases = conObj.cases;
                   } catch( QueryException e ) {
                       cases = new List<Case>();
                       for(Case cse : conObj.cases){
                           cases.add(cse);
                       }
                   }
                   system.debug('----con---'+conObj.cases.size());
                   if(cases.size() > 0)
                   {
                       conObj.Open_Cases__c = String.valueOf(cases.size());  
                   }else {
                       conObj.Open_Cases__c = String.valueOf(0);
                   } 
                   updateCustomerList.add(conObj);
               }
            system.debug('---UpdateCons--'+updateCustomerList.size());
            if(updateCustomerList.size() > 0) {
                update updateCustomerList;
            } 
        } catch(Exception exp){
            StatusLogHelper.logSalesforceError('CaseTriggerHandler', 
                                               'E005', 'updateOpenCaseCount', exp, false);
        } 
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     29/09/2020
*   User Story : SFD-314
*   Param: None
*   Return: None    
*   Description:  In this method we are trying to update contactId based on customer serno on Case which is created from Bits
application via Micro service.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/ 
    public void linkCustomerToCase(List<Case> caseList) {
        Set<String> customerSernoSet = new Set<String>();
        Set<Id> customerIdSet = new Set<Id>();
        Map<String, Contact> contactSernoMap = new Map<String, Contact>();
        Map<Id, Contact> contactIdMap;
        system.debug('---caselist---'+caseList);
        for(Case objCase : caseList){
            if(String.isNotBlank(objCase.Customer_SerNo__c)){
                customerSernoSet.add(objCase.Customer_SerNo__c); 
            } else if(objCase.ContactId != null){
                customerIdSet.add(objCase.ContactId);
            }
        }
        system.debug('---customerIdSet---'+customerIdSet);
        List<Contact> contactList;
        if(customerSernoSet.size() > 0 
           || customerIdSet.size() > 0){
               contactList = [SELECT Id, SerNo__c FROM Contact 
                              WHERE SerNo__c IN : customerSernoSet
                              OR Id IN : customerIdSet];           
               
               if(contactList.size() > 0 && contactList != null){
                   if(customerIdSet.size() > 0){
                       contactIdMap = new Map<Id, Contact>(contactList);
                   }
                   for(Contact conObj : contactList){
                       contactSernoMap.put(conObj.SerNo__c, conObj);
                   }
               }
               for(Case caseObj : caseList){
                   if(caseObj.ContactId != null
                      && contactIdMap.containsKey(caseObj.ContactId)){
                          caseObj.Customer_SerNo__c = contactIdMap.get(caseObj.ContactId).SerNo__c;
                      } else if(String.isNotBlank(caseObj.Customer_SerNo__c) 
                                && contactSernoMap.containsKey(caseObj.Customer_SerNo__c) && caseObj.Subject == Label.Bankruptcy){
                                    caseObj.ContactId = contactSernoMap.get(caseObj.Customer_SerNo__c).Id;
                                    caseObj.Due_Date__c = system.now().addhours(Integer.valueOf(Label.Bankruptcy_Due_Date));
                                }
               }
           }
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     05/02/2021
*   User Story : SFD-1938
*   Param: None
*   Return: None    
*   Description:  In this method we are trying to update the assigned user field with case owner name when case is created/updated.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
/*    public void updateAssignedUser(Map<Id, Case> caseNewMap, Map<Id, Case> caseOldMap){
        Set<Id> caseId = new Set<Id>();
        Map<Id, Case> caseMap = new Map<Id, Case>();
        for(Case caseObj : [SELECT Id, OwnerId,Owner.Name FROM Case WHERE Id IN:caseNewMap.keySet()]){
            Case oldCaseObj = caseOldMap.get(caseObj.Id);
            if(caseObj.OwnerId != oldCaseObj.OwnerId){
                caseMap.put(caseObj.Id,caseObj);
            }
        }
        if(caseMap.size() > 0){
            List<DCMS_FraudCase__c> fraudUpdateList = new List<DCMS_FraudCase__c>();
            List<DCMS_FraudCase__c> fraudObjList =[SELECT Id,AssignedUser__c, Case_Number__c FROM DCMS_FraudCase__c WHERE Case_Number__c IN : caseMap.keyset()];
            for(DCMS_FraudCase__c fradObj : fraudObjList){
                fradObj.AssignedUser__c = caseMap.get(fradObj.Case_Number__c).Owner.Name;
                fraudUpdateList.add(fradObj);
            }
            update fraudUpdateList;
        }        
    }*/
}