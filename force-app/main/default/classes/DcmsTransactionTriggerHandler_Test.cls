/********************************************************************************************
Name: DcmsTransactionTriggerHandler_Test
=============================================================================================
Purpose: 'DcmsTransactionTrigger' Trigger and 'DcmsTransactionTriggerHandler' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0        Srinivas D             4th-April-2019    SFD-984   		    Intial version
2.0		   Kakasaheb Ekshinge 	  22-July-2020		SFD-1596			Code coverage has increases
********************************************************************************************/
@isTest(seeAllData = false)
public class DcmsTransactionTriggerHandler_Test {    
    
    public static testmethod void createTransTest(){  
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            DCMS_CaseStatus__c cstatus = UnitTestDataGenerator.testDCMS_Case.buildInsert();
            List<DCMS_CaseStatus__c> dcmsCaseStatusList = new List<DCMS_CaseStatus__c>();
            dcmsCaseStatusList.add(cstatus);
            List<String>  dcmsCaseStatusAttr = new List<String> {};
                List<DCMS_CaseStatus__c> caseStatusReload = UnitTestDataGenerator.testDCMS_Case.reloadList(dcmsCaseStatusList, dcmsCaseStatusAttr);
            
            DCMS_Decision__c desObj = UnitTestDataGenerator.testDCMSDecision.buildInsert();
            List<DCMS_Decision__c> dcmsDecisionList = new List<DCMS_Decision__c>();
            dcmsDecisionList.add(desObj);
            List<String>  dcmsDecisionAttr = new List<String> {};
                List<DCMS_Decision__c> dcmsDecisionReload = UnitTestDataGenerator.testDCMSDecision.reloadList(dcmsDecisionList, dcmsDecisionAttr);
            
            DCMS_FraudRing__c fring = UnitTestDataGenerator.testFraudRing.buildInsert(); 
            List<DCMS_FraudRing__c> dcmsfringList = new List<DCMS_FraudRing__c>();
            dcmsfringList.add(fring);
            List<String>  dcmsfringAttr = new List<String> {};
                List<DCMS_FraudRing__c> dcmsfringReload = UnitTestDataGenerator.testFraudRing.reloadList(dcmsfringList, dcmsfringAttr);
            
            DCMS_Queue_Settings__c qsetting = UnitTestDataGenerator.testQueueSettings.buildInsert();
            List<DCMS_Queue_Settings__c> dcmsQueueList = new List<DCMS_Queue_Settings__c>();
            dcmsQueueList.add(qsetting);
            List<String>  dcmsQueueAttr = new List<String> {};
                List<DCMS_Queue_Settings__c> dcmsQueueReload = UnitTestDataGenerator.testQueueSettings.reloadList(dcmsQueueList, dcmsQueueAttr);
            
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c' => '1123123' 
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123',
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id              
                    });
            
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert(); 
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd', 
                    'Customer__c'=>accountObj.Id,
                    'Account_Serno__c'=>'3123123',                   
                    'Customer_Serno__c'=>contactObj.SerNo__c,
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });   
            
            Card__c cardObj = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id,
                    'Card_Serno__c'=>'90247211', 
                    'Financial_Account_Serno__c'=>finAccObj.Id, 
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id                                
                    });  
            
            Case caseObj = UnitTestDataGenerator.testCase.buildInsert(new Map<String, Object>{
                'ContactId'=>contactObj.Id, 
                    'Category__c'=>'Dispute',
                    'Origin'=>'SOA',
                    'Department__c'=>'Fraud and Chargeback',
                    'Dispute_Type__c'=>'0: lost',
                    'Card__c'=>cardObj.id
                    });
            Map<Id, Case> caseOldMap = new Map<Id, Case>();
            caseOldMap.put(caseObj.Id, caseObj);
            DCMS_Transaction__c transObj = UnitTestDataGenerator.testDCMS_Transaction.buildInsert(new Map<String, Object>{
                'Chargeback__c'=> false
                    });
            List<DCMS_Transaction__c> dcmsTransList = new List<DCMS_Transaction__c>();
            dcmsTransList.add(transObj);
            List<String>  dcmsTransAttr = new List<String> {'Chargeback__c'};
                List<DCMS_Transaction__c> dcmsTransReload = UnitTestDataGenerator.testDCMS_Transaction.reloadList(dcmsTransList, dcmsTransAttr);            
            Map<Id, DCMS_Transaction__c> dcmsTransOldMap = new Map<Id, DCMS_Transaction__c>();
            dcmsTransOldMap.put(transObj.Id, transObj);
            
            Test.stopTest();
        }
    }
    
    public static testmethod void createTransIfTest(){  
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.startTest(); 
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c' => '1123123' 
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123',
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id              
                    });
            
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert(); 
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd', 
                    'Customer__c'=>accountObj.Id,
                    'Account_Serno__c'=>'3123123',                   
                    'Customer_Serno__c'=>contactObj.SerNo__c,
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });   
            
            Card__c cardObj = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id,
                    'Card_Serno__c'=>'90247211', 
                    'Financial_Account_Serno__c'=>finAccObj.Id, 
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id                                
                    });  
            
            Case caseObj = UnitTestDataGenerator.testCase.buildInsert(new Map<String, Object>{
                'ContactId'=>contactObj.Id, 
                    'Status'=>'In Progress',
                    'Category__c'=>'Dispute',
                    'Origin'=>'SOA',
                    'Department__c'=>'Fraud and Chargeback',
                    'Dispute_Type__c'=>'0: lost',
                    'Institutionid__c'=> 1,
                    'Card__c'=>cardObj.id,
                    'Gross_Amount__c'=>2000
                    });
            DCMS_Transaction__c transObj = UnitTestDataGenerator.testDCMS_Transaction.buildInsert(new Map<String, Object>{
                'Chargeback__c'=> false,
                    'DisputeType__c'=>'0: lost',
                    'Txn_CardSerno__c'=>90247211,
                    'CaseNumber__c'=>caseObj.Id
                    });
            List<DCMS_Transaction__c> dcmsTransList = new List<DCMS_Transaction__c>();
            dcmsTransList.add(transObj);
            List<String>  dcmsTransAttr = new List<String> {'Chargeback__c'};
                List<DCMS_Transaction__c> dcmsTransReload = UnitTestDataGenerator.testDCMS_Transaction.reloadList(dcmsTransList, dcmsTransAttr);            
            Map<Id, DCMS_Transaction__c> dcmsTransOldMap = new Map<Id, DCMS_Transaction__c>();
            dcmsTransOldMap.put(transObj.Id, transObj);
            DCMS_FraudCase__c fraudObj = UnitTestDataGenerator.testDcmsFraudCase.buildInsert(new Map<String, Object>{
                'Case_Number__c'=>caseObj.Id
                    });
            
            List<DCMS_Transaction__c> transList = [SELECT Id FROM DCMS_Transaction__c WHERE Id =:transObj.Id];
            delete transList;
            system.debug('---size---'+transList.size());
            system.assertNotEquals(0, transList.size());           
            Test.stopTest();
        }
    }
    
    public static testmethod void createTransIfElseTest(){  
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.startTest(); 
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c' => '1123123' 
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123',
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id              
                    });
            
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert(); 
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd', 
                    'Customer__c'=>accountObj.Id,
                    'Account_Serno__c'=>'3123123',                   
                    'Customer_Serno__c'=>contactObj.SerNo__c,
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });   
            
            Card__c cardObj = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id,
                    'Card_Serno__c'=>'90247211', 
                    'Financial_Account_Serno__c'=>finAccObj.Id, 
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id                                
                    });  
            
            Case caseObj = UnitTestDataGenerator.testCase.buildInsert(new Map<String, Object>{
                'ContactId'=>contactObj.Id, 
                    'Status'=>'In Progress',
                    'Category__c'=>'Dispute',
                    'Origin'=>'SOA',
                    'Department__c'=>'Fraud and Chargeback',
                    'Dispute_Type__c'=>'0: lost',
                    'Card__c'=>cardObj.id
                    });
            DCMS_Transaction__c transObj = UnitTestDataGenerator.testDCMS_Transaction.buildInsert(new Map<String, Object>{
                'Chargeback__c'=> false,
                    'DisputeType__c'=>'0: lost',
                    'Txn_CardSerno__c'=>90247211
                    });
            List<DCMS_Transaction__c> dcmsTransList = new List<DCMS_Transaction__c>();
            dcmsTransList.add(transObj);
            List<String>  dcmsTransAttr = new List<String> {'Chargeback__c'};
                List<DCMS_Transaction__c> dcmsTransReload = UnitTestDataGenerator.testDCMS_Transaction.reloadList(dcmsTransList, dcmsTransAttr);            
            system.assertEquals(false, transObj.Chargeback__c);
            Test.stopTest();
             
        }
    }
    
    public static testmethod void caseNullTest(){  
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.startTest(); 
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c' => '1123123' 
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123',
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id              
                    });
            
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert(); 
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd', 
                    'Customer__c'=>accountObj.Id,
                    'Account_Serno__c'=>'3123123',                   
                    'Customer_Serno__c'=>contactObj.SerNo__c,
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });   
            
            Card__c cardObj = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id,
                    'Card_Serno__c'=>'90247211', 
                    'Financial_Account_Serno__c'=>finAccObj.Id, 
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id                                
                    });  
            DCMS_Transaction__c transObj = UnitTestDataGenerator.testDCMS_Transaction.buildInsert(new Map<String, Object>{
                'Chargeback__c'=> false,
                    'DisputeType__c'=>'0: lost',
                    'Txn_CardSerno__c'=>90247211
                    });
            List<DCMS_Transaction__c> dcmsTransList = new List<DCMS_Transaction__c>();
            dcmsTransList.add(transObj);
            List<String>  dcmsTransAttr = new List<String> {'Chargeback__c'};
                List<DCMS_Transaction__c> dcmsTransReload = UnitTestDataGenerator.testDCMS_Transaction.reloadList(dcmsTransList, dcmsTransAttr);            
            system.assertEquals(false, transObj.Chargeback__c);
            Test.stopTest();
            
        }
    }
    
        public static testmethod void updateTotalAmountsTest(){  
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.startTest();
			TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});            
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c' => '1123123' 
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123',
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id              
                    });
            
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert(); 
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd', 
                    'Customer__c'=>accountObj.Id,
                    'Account_Serno__c'=>'3123123',                   
                    'Customer_Serno__c'=>contactObj.SerNo__c,
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });   
            
            Card__c cardObj = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id,
                    'Card_Serno__c'=>'90247211', 
                    'Financial_Account_Serno__c'=>finAccObj.Id, 
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id                                
                    });  
            
            Case caseObj = UnitTestDataGenerator.testCase.buildInsert(new Map<String, Object>{
                'ContactId'=>contactObj.Id, 
                    'Status'=>'In Progress',
                    'Category__c'=>'Dispute',
                    'Origin'=>'SOA',
                    'Department__c'=>'Fraud and Chargeback',
                    'Dispute_Type__c'=>'0: lost',
                    'Card__c'=>cardObj.id
                    });
            DCMS_Transaction__c transObj = UnitTestDataGenerator.testDCMS_Transaction.buildInsert(new Map<String, Object>{
                'Chargeback__c'=> false,
                    'Representment__c' => false,
                    'Arbitration__c' => false, 
            		'WriteOff__c' => false,
            		'FraudWriteOff__c' => false,
            		'Recovery__c' => false,
                    'DisputeType__c'=>'0: lost',
                    'Txn_CardSerno__c'=>9024721,
                    'CaseNumber__c'=>caseObj.Id
                    });
            Map<Id, DCMS_Transaction__c> dcmsTransOldMap = new Map<Id, DCMS_Transaction__c>();
            dcmsTransOldMap.put(transObj.Id, transObj);
            DCMS_FraudCase__c fraudObj = UnitTestDataGenerator.testDcmsFraudCase.buildInsert(new Map<String, Object>{
                'Case_Number__c'=>caseObj.Id
                    });
            fraudObj.TotalAtRisk__c = 0.00;
            fraudObj.NonFraudWO__c = 1.00;
            fraudObj.FraudWO__c = 1.00;
            update fraudObj;
            
            checkRecursive.run = true;
            transObj.Chargeback__c = true;
            transObj.Representment__c = true;
            transObj.Arbitration__c = true; 
            transObj.WriteOff__c = false;
            transObj.FraudWriteOff__c = false;
            transObj.Recovery__c = true;
            update transObj;
            system.assertEquals(true, transObj.Chargeback__c);     
            Test.stopTest();
        }
    }
    public static testmethod void updateTotalAmountsIfTest(){  
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});            
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c' => '1123123' 
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123',
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id              
                    });
            
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert(); 
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'12w23eeeeedd', 
                    'Customer__c'=>accountObj.Id,
                    'Account_Serno__c'=>'3123123',                   
                    'Customer_Serno__c'=>contactObj.SerNo__c,
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });   
            
            Card__c cardObj = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id,
                    'Card_Serno__c'=>'90247211', 
                    'Financial_Account_Serno__c'=>finAccObj.Id, 
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id                                
                    });  
            
            Case caseObj = UnitTestDataGenerator.testCase.buildInsert(new Map<String, Object>{
                'ContactId'=>contactObj.Id, 
                    'Status'=>'In Progress',
                    'Category__c'=>'Dispute',
                    'Origin'=>'SOA',
                    'Department__c'=>'Fraud and Chargeback',
                    'Dispute_Type__c'=>'0: lost',
                    'Card__c'=>cardObj.id
                    });
            DCMS_Transaction__c transObj = UnitTestDataGenerator.testDCMS_Transaction.buildInsert(new Map<String, Object>{
                'Chargeback__c'=> false,
                    'Representment__c' => false,
                    'Arbitration__c' => false, 
                    'WriteOff__c' => false,
                    'FraudWriteOff__c' => false,
                    'Recovery__c' => false,
                    'DisputeType__c'=>'0: lost',
                    'Txn_CardSerno__c'=>9024721,
                    'CaseNumber__c'=>caseObj.Id
                    });
            Map<Id, DCMS_Transaction__c> dcmsTransOldMap = new Map<Id, DCMS_Transaction__c>();
            dcmsTransOldMap.put(transObj.Id, transObj);
            DCMS_FraudCase__c fraudObj = UnitTestDataGenerator.testDcmsFraudCase.buildInsert(new Map<String, Object>{
                'Case_Number__c'=>caseObj.Id
                    });
            checkRecursive.run = true;
          fraudObj.TotalAtRisk__c = 0.00;
            fraudObj.NonFraudWO__c = 0.00;
            fraudObj.FraudWO__c = 0.00;
            update fraudObj;
            
            checkRecursive.run = true;
            transObj.Chargeback__c = false;
            transObj.Representment__c = false;
            update transObj;
            system.assertEquals(false, transObj.Chargeback__c);     
            Test.stopTest();
        }
    }
}