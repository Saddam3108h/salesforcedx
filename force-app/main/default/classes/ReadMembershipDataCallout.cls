public class ReadMembershipDataCallout extends SoapIO{
    SOAReadMembershipDataRequest.getMembershipRequestBody MembershipRequestInstance;
    ReadMembershipDataInput input;
    public  void convertInputToRequest(){
        input = (ReadMembershipDataInput)serviceInput;
        MembershipRequestInstance = new SOAReadMembershipDataRequest.getMembershipRequestBody();
        
        MembershipRequestInstance.Institution_ID=input.InstitutionId;
        //MembershipRequestInstance.PartnerName=input.PartnerName;
        MembershipRequestInstance.ProductId=input.ProductId;
        MembershipRequestInstance.EntityId=input.EntityId;
        MembershipRequestInstance.FromDate=input.FromDate;
        MembershipRequestInstance.ToDate=input.ToDate; 
    }
     public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        SOAReadMembershipDataTypelib10.headerType HeadInstance=new SOAReadMembershipDataTypelib10.headerType();
        HeadInstance.MsgId='SFDC consuming ReadMembershipData SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=string.valueof(input.InstitutionId);
        SOAReadMembershipDataInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance= new SOAReadMembershipDataInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=120000;

        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('ReadMembershipData');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAReadMembershipDataWssecurity.UsernameToken creds=new SOAReadMembershipDataWssecurity.UsernameToken();
        creds.Username=serviceObj.Username__c; //'css_soa';//'evry_access';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';//'9oKuwQioQ4';
        SOAReadMembershipDataWssecurity.Security_element security_ele=new SOAReadMembershipDataWssecurity.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOAReadMembershipDataResponse.GetMembershipResponse_element MembershipResponse_elementinstance;
        MembershipResponse_elementinstance =  invokeInstance.getMembershipData(HeadInstance,MembershipRequestInstance);
        system.debug('MembershipResponse_elementinstance -->'+MembershipResponse_elementinstance);
        return MembershipResponse_elementinstance;
     }
     
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
         if(response==null)
             return null;
         SOAReadMembershipDataResponse.GetMembershipResponse_element MembershipResponse_elementinstance = (SOAReadMembershipDataResponse.GetMembershipResponse_element)response;
         SOAReadMembershipDataTypelib10.headerType Header=MembershipResponse_elementinstance.Header;
         List<SOAReadMembershipDataResponse.getMembershipResponseBody> GetMembershipResponseBodyList=MembershipResponse_elementinstance.GetMembershipResponseBody;
         system.debug('~~~~~~```'+GetMembershipResponseBodyList);
         ReadMembershipDataOutput ReadMembershipOutput=new ReadMembershipDataOutput();
         ReadMembershipOutput.MsgId=Header.MsgId;
         ReadMembershipOutput.CorrelationId=Header.CorrelationId;
         ReadMembershipOutput.RequestorId=Header.RequestorId;
         ReadMembershipOutput.SystemId=Header.SystemId;
         ReadMembershipOutput.InstitutionId=Header.InstitutionId;
         
         List<ReadMembershipDataOutput.MembershipResponseBody>MembershipResponseList = New List<ReadMembershipDataOutput.MembershipResponseBody>();
         if(GetMembershipResponseBodyList!=null)
         {
             for(SOAReadMembershipDataResponse.getMembershipResponseBody iterator : GetMembershipResponseBodyList)
             {
                 ReadMembershipDataOutput.MembershipResponseBody MembershipResponseBodyInstance = new ReadMembershipDataOutput.MembershipResponseBody();
                 MembershipResponseBodyInstance.Institution_ID=iterator.Institution_ID;
                 MembershipResponseBodyInstance.MemberData_Id=iterator.MemberData_Id;
                 MembershipResponseBodyInstance.PartnerName=iterator.PartnerName;
                 MembershipResponseBodyInstance.ProductId=iterator.ProductId;
                 MembershipResponseBodyInstance.EntityId=iterator.EntityId;
                 MembershipResponseBodyInstance.EntityType=iterator.EntityType;
                 MembershipResponseBodyInstance.Updated_By=iterator.Updated_By;
                 MembershipResponseBodyInstance.FromDate=iterator.FromDate;
                 MembershipResponseBodyInstance.ToDate=iterator.ToDate;
                 
                 List<ReadMembershipDataOutput.memberField> MemberFieldTypeList=new List<ReadMembershipDataOutput.memberField>();
                 if(iterator.MemberFields!=null)
                 {
                     for(SOAReadMembershipDataResponse.memberFieldType iterator1 : iterator.MemberFields)
                     {
                         ReadMembershipDataOutput.memberField memberypeObj=new ReadMembershipDataOutput.memberField();
                         memberypeObj.MemberFieldName=iterator1.MemberFieldName;
                         memberypeObj.MemberFieldValues=iterator1.MemberFieldValues; 
                         MemberFieldTypeList.add(memberypeObj);           
                     }
                 }
                 MembershipResponseBodyInstance.MemberFieldList=MemberFieldTypeList;
                 MembershipResponseList.add(MembershipResponseBodyInstance);  
             }
         }
         ReadMembershipOutput.MembershipResponseBodyList=MembershipResponseList;
         return ReadMembershipOutput;
     }
}