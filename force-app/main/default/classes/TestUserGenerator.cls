/**************************************************************************************************
* @Author:      Priyanka Singh  
* @Date:        
* @PageCode:    
* @Description: Class to create test user during unit test and user perspective testmethod. 
*               Will be called from UnitTestGenerator.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***************************************************************************************************/
public class TestUserGenerator {  
    
    /**********************************************************************************************
    * @Author:      Priyanka Singh  
    * @Date:        
    * @Description: default method to create standard user for test data 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
     public static User createStandardUser_Default(){
        return createStandardUser(null,null,null,null,null,null,null,null,null);
    }
    /**********************************************************************************************
    * @Author:       
    * @Date:        
    * @Description: default method to create admin user for test data 
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
     public static User createAdminUser_Default(){
        return createAdminUser(null,null,null,null,null,null,null,null,null);
    }
    
    /**********************************************************************************************
    * @Author:        
    * @Date:        
    * @Description: Method to specifically create instance of standard user and pass to caller.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static User createStandardUser(String firstName,String lastName,String email,String username,String CompanyName,
                                     String title,String timeZoneSidKey,String EmailEncodingKey,String LocaleSidKey){
        Id customUserRoleId = createRoleUser();
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'NO - Operations'].Id;
        User standardUser = new User(FirstName= (firstName != null) ? firstName : 'firstName',
                                    LastName = (lastName != null) ? lastName : 'lastname',
                                    Email = (email != null) ? email : 'useremail@dummyaxa.com',
                                    Username = ((username != null) ? username : 'username')+ System.currentTimeMillis()+'@dummyaxa.com',
                                    CompanyName = (CompanyName != null) ? CompanyName : 'AXATEST',
                                    Title = (title != null) ? title : 'title',
                                    Alias = 'alias',
                                    TimeZoneSidKey = (timeZoneSidKey != null) ? timeZoneSidKey : 'America/Los_Angeles',
                                    EmailEncodingKey = (EmailEncodingKey != null) ? EmailEncodingKey : 'UTF-8',
                                    LanguageLocaleKey = (LocaleSidKey != null) ? LocaleSidKey :'en_US',
                                    ProfileId = profileId,
                                    UserRoleId = customUserRoleId,
                                    IsActive = true,
                                    //CallCenterId='04v6E000000CbhzQAC', // need to change label
                                    LocaleSidKey = (LocaleSidKey != null) ? LocaleSidKey : 'en_US');
         return standardUser;                                
    }
    /**********************************************************************************************
    * @Author:        
    * @Date:        
    * @Description: Method to specifically create instance of admin user and pass to caller.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static User createAdminUser(String firstName,String lastName,String email,String username,String CompanyName,
                                     String title,String timeZoneSidKey,String EmailEncodingKey,String LocaleSidKey){
        Id adminUserRoleId = [SELECT Id FROM userRole WHERE Name = 'EC Customer Care'].Id;
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User adminUser = new User(FirstName= (firstName != null) ? firstName : 'firstName',
                                    LastName = (lastName != null) ? lastName : 'lastname',
                                    Email = (email != null) ? email : 'user1email@dummyaxa.com',
                                    Username = ((username != null) ? username : 'username')+ System.currentTimeMillis()+'@dummyaxa.com',
                                    CompanyName = (CompanyName != null) ? CompanyName : 'AXATEST',
                                    Title = (title != null) ? title : 'title',
                                    Alias = 'alias1',
                                    TimeZoneSidKey = (timeZoneSidKey != null) ? timeZoneSidKey : 'America/Los_Angeles',
                                    EmailEncodingKey = (EmailEncodingKey != null) ? EmailEncodingKey : 'UTF-8',
                                    LanguageLocaleKey = (LocaleSidKey != null) ? LocaleSidKey :'en_US',
                                    ProfileId = profileId,     
                                    UserRoleId = adminUserRoleId,
                                    IsActive = true,
                                   // CallCenterId='04v6E000000CbhzQAC',// need to change label
                                    LocaleSidKey = (LocaleSidKey != null) ? LocaleSidKey : 'en_US');
         return adminUser;                                
    }
     /**********************************************************************************************
    * @Author:        
    * @Date:        
    * @Description: Method to specifically create instance of user role.
    * @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
    ***********************************************************************************************/
    public static Id createRoleUser(){
        UserRole customRole = new UserRole(DeveloperName = 'MyCustomRole1', Name = 'My Role1');
        insert customRole;
        return customRole.Id;
    }
}