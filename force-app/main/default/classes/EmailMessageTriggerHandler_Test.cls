/**********************************************************************
Name: EmailMessageTriggerHandler_Test
=======================================================================
Purpose: This class is test class of EmailMessageTriggerHandler

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Hari         		23-July-2020       Initial Version

**********************************************************************/
@isTest(SeeAllData=false) 
public class EmailMessageTriggerHandler_Test {
    /**********************************************************************************************
* @Author:        
* @Date: 02/06/2020      
* @Description: Test method to cover the code for 10 digits SSN
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    static testmethod void caseCreationTest(){
        Test.starttest();   
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj = UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Contact ContactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SSN__c'=>'6309030010'
                    });
            Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'Description'=>'kundens Personnummer : 6309030010 \n Kontonummer : 5438730000000630 \n Testing SSN',
                    //'To_Address__c'=>Label.SwedBank_EmailId,
                    'SuppliedEmail'=>System.Label.FundTransfer_EmailId
                    });
            EmailMessage EmailMessageObj = UnitTestDataGenerator.TestEmailMessage.buildInsert(new Map<String, Object>{
                'Incoming'=>True,
                    'ParentId'=>caseObj.Id,
                    'ToAddress'=>Label.SwedBank_EmailId
                    }); 
            system.assertEquals(true, EmailMessageObj.Incoming);
        }          
        Test.stopTest();
    }
    /**********************************************************************************************
* @Author:        
* @Date: 02/06/2020      
* @Description: Test method to cover the code for more than 10 digits SSN
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    static testmethod void caseCreationTocoverMoreTenDigits(){
        Test.starttest();   
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'Description'=>'kundens personnummer: 1963-09030010\n Kontonummer : 5438730000000630 \n Testing SSN',
                    'To_Address__c'=>Label.SwedBank_EmailId,
                    'SuppliedEmail'=>Label.FundTransfer_EmailId,
                    'status'=>'New'
                    });
            EmailMessage EmailMessageObj = UnitTestDataGenerator.TestEmailMessage.buildInsert(new Map<String, Object>{
                'Incoming'=>True,
                    'ParentId'=>caseObj.Id,
                    'ToAddress'=>'test@gmail.com'
                    });
            system.assertEquals(true, EmailMessageObj.Incoming);
        }
        Test.stopTest();
    }
    /**********************************************************************************************
* @Author:        
* @Date: 02/06/2020      
* @Description: Test method to cover the code for setting the email parent Id
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    static testmethod void updateCaseParentTest(){
        Test.starttest();   
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'Description'=>'kundens personnummer: 1963-09030010\nKontonummer : 5438730000000630 \nTesting SSN',
                    'Status'=>'Closed',
                    'To_Address__c'=>null,
                    'SuppliedEmail'=>Label.FundTransfer_EmailId,
                    'Subject'=>'RE: TestingTestClass: 2020-08-06 00:00:01'
                    });
            EmailMessage EmailMessageObj = UnitTestDataGenerator.TestEmailMessage.buildInsert(new Map<String, Object>{
                'Incoming'=>True,
                    'ParentId'=>caseObj.Id,
                    'ToAddress'=>null,
                    'Headers'=>'Return-Path: bankprio@entercard.com Authentication-Results:'
                    });
            system.assertEquals(true, EmailMessageObj.Incoming);
        }
        Test.stopTest();
    }
    /**********************************************************************************************
* @Author:        
* @Date: 02/06/2020      
* @Description: Test method to cover the code for PopulateCaseToAddressSSN class
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    static testmethod void populateCaseToAddressSSNTest(){ 
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Contact ContactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SSN__c'=>'6309030010'
                    });
            Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'Description'=>'kundens personnummer: 1963-09030010 \n Kontonummer : 5438730000000630 \nTesting SSN',
                    'Subject'=>'Testing',
                    'To_Address__c'=>'testtest@capgemini.comtesttest@capgemini.comtesttest@capgemini.comtesttest@capgemini.comtesttest@capgemini.comtesttest@capgemini.comtesttest@capgemini.com',
                    'Status'=>'New',
                    'Case_View__c'=>null,
                    'SuppliedEmail'=>Label.FundTransfer_EmailId,
                    'Department__c'=>'Customer Service'
                    });
            
            Country_Codes__c codeObj = UnitTestDataGenerator.TestCountryCodes.buildInsert(new Map<String, Object>{
                'Country_Code__c' =>'1',
                    'Name'=>'no'	
                    });
            EmailToCaseSettings__c ec=UnitTestDataGenerator.TestEmailToCaseSettings.buildInsert(new Map<String, Object>{
                    });
            
            EmailMessageTriggerHandler EMTH = new EmailMessageTriggerHandler();
            
            Group gr= new Group();
            gr.Name='queue';
            gr.Type='Queue';
            insert gr;
            
            Group[] grpqueue = [select Id from Group where Name =: ec.Queue__c and Type = 'Queue'];
            EmailMessage EmailMessageObj = UnitTestDataGenerator.TestEmailMessage.buildInsert(new Map<String, Object>{
                'Incoming'=>True,
                    'ParentId'=>caseObj.Id,
                    'Subject'=>'Testing'
                    });  
            system.assertEquals(true, EmailMessageObj.Incoming);
        }
    }
    /**********************************************************************************************
* @Author:        
* @Date: 02/06/2020      
* @Description: Test method to cover the code for PopulateCaseToAddressSSN class
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    static testmethod void populateCaseToAddressSSNTest1(){ 
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Contact ContactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SSN__c'=>'6309030010'
                    });
            Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'Description'=>'kundens personnummer: 1963-09030010 \nKontonummer : 5438730000000630 \n Testing SSN',
                    'Subject'=>'Testing',
                    'To_Address__c'=>'testtest@capgemini.com',
                    'Status'=>'New',
                    'Case_View__c'=>null,
                    'SuppliedEmail'=>Label.FundTransfer_EmailId,
                    'Department__c'=>'Customer Service'
                    });
            Country_Codes__c codeObj = UnitTestDataGenerator.TestCountryCodes.buildInsert(new Map<String, Object>{
                'Country_Code__c' =>'1',
                    'Name'=>'no'	
                    });
            EmailToCaseSettings__c ec=UnitTestDataGenerator.TestEmailToCaseSettings.buildInsert(new Map<String, Object>{
            'Email_Source__c'=>'Mobile Apps'        
            });
            
            Group gr= new Group();
            gr.Name='queue';
            gr.Type='Queue';
            insert gr;
            
            Group[] grpqueue = [select Id from Group where Name =: ec.Queue__c and Type = 'Queue'];
            EmailMessage EmailMessageObj = UnitTestDataGenerator.TestEmailMessage.buildInsert(new Map<String, Object>{
                'Incoming'=>True,
                    'ParentId'=>caseObj.Id,
                    'Subject'=>'Testing'
                    }); 
            system.assertEquals(true, EmailMessageObj.Incoming);
        }
    }
    /**********************************************************************************************
* @Author:        
* @Date: 02/06/2020      
* @Description: Test method to cover the code for 10 digits SSN
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    static testmethod void emailTestingWithOutgoing(){
        Test.starttest();   
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Contact ContactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SSN__c'=>'6309030010'
                    });
            Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'Description'=>'kundens personnummer: 6309030010 \nKontonummer : 5438730000000630 \n Testing SSN',
                    'To_Address__c'=>Label.SwedBank_EmailId,
                    'Status'=>'In Progress'
                    });
            EmailMessage EmailMessageObj = UnitTestDataGenerator.TestEmailMessage.buildInsert(new Map<String, Object>{
                'Incoming'=>False,
                    'ParentId'=>caseObj.Id,
                    'ToAddress'=>'test@gmail.com'
                    }); 
            system.assertEquals(False, EmailMessageObj.Incoming);
        }          
        Test.stopTest();
    }
    
    static testmethod void caseCreationTestTriggerFarmework(){
        Test.starttest();   
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Contact ContactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SSN__c'=>'6309030010'
                    });
            Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'Description'=>'kundens personnummer: 6309030010 \nKontonummer : 5438730000000630 \n Testing SSN',
                    'To_Address__c'=>Label.SwedBank_EmailId
                    });
            EmailMessage EmailMessageObj = UnitTestDataGenerator.TestEmailMessage.buildInsert(new Map<String, Object>{
                'Incoming'=>True,
                    'ParentId'=>caseObj.Id,
                    'ToAddress'=>'test@gmail.com'
                    }); 
            system.assertEquals(true, EmailMessageObj.Incoming);
        }          
        Test.stopTest();
    }
    /**********************************************************************************************
* @Author:        
* @Date: 02/06/2020      
* @Description: Test method to cover the code for getting application id from email and tagg to particular FA,customer,Mah
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    static testmethod void updateFAOnCaseByAppId(){           
        System.runAs(UnitTestDataGenerator.adminUser) {
            Test.starttest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
            Product_Custom__c objProd = UnitTestDataGenerator.testProductCustom.buildInsert(new Map<String, Object>{
                'Name'=>'Test Product', 
                    'Product_Description__c'=>'Test Desc',
                    'Pserno__c'=>'805'
                    }); 
            
            Account objAcc = UnitTestDataGenerator.testAccount.buildInsert(new Map<String, Object>{
                'Name'=>'Account for Test Class', 
                    'Customer_Serno__c'=>'123654'
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact  objCon = UnitTestDataGenerator.testContact.buildInsert(new Map<String, Object>{
                'AccountId'=>objAcc.Id, 
                    'Encrypted_SSN__c'=>encryptSSN,
                    'FirstName'=>'TestContact',
                    'SerNo__c' => '125895',
                    'SSN__c' =>'78958741',
                    'Email'=>'xyz@ab1c.com'
                    });
            
            
            Financial_Account__c objFA = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'4141144', 
                    'Customer__c'=>objAcc.Id,
                    'Account_Serno__c'=>'11166',
                    'Customer_Serno__c'=>objAcc.Customer_Serno__c,
                    'Product__c'=>objProd.Id,
                    'Product_Serno__c'=>objProd.Pserno__c,
                    'ApplicationId__c'=>'31231'
                    });
            
            
            Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'ContactId'=>objCon.Id, 
                    'AccountId'=>objAcc.Id, 
                    'Financial_Account__c'=>objFA.Id, 
                    'Description'=>'kundens personnummer: 1963-09030010\n <AppID>324-234S</AppID> \n Testing SSN',
                    'To_Address__c'=>'soknad@entercard.com',
                    'SuppliedEmail'=>Label.FundTransfer_EmailId,
                    'status'=>'New'
                    
                    
                    });
            EmailMessage EmailMessageObj = UnitTestDataGenerator.TestEmailMessage.buildInsert(new Map<String, Object>{
                'Incoming'=>True,
                    'ParentId'=>caseObj.Id,
                    'Subject'=>'Vår ref:',
                    'ToAddress'=>'soknad@entercard.com'
                    });
            
            
            /*Map<Id, EmailMessage> EmailNewMap = new Map<Id, EmailMessage>();
            EmailMessageTriggerHandler Email = new EmailMessageTriggerHandler();*/
            system.assertEquals(true, EmailMessageObj.Incoming);
            Test.stopTest();
        }        
    } 

      /**********************************************************************************************
* @Author:        
* @Date: 02/06/2020      
* @Description: Test method to cover the code for getting application id from email and tagg to particular FA,customer,Mah
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/
    /*static testmethod void updateFAOnCaseByAppIdSub(){
          
        System.runAs(UnitTestDataGenerator.adminUser) {
           Test.starttest(); 
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});
             
            Product_Custom__c objProd = UnitTestDataGenerator.testProductCustom.buildInsert(new Map<String, Object>{               
                    }); 
            
            Account objAcc = UnitTestDataGenerator.testAccount.buildInsert(new Map<String, Object>{
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact  objCon = UnitTestDataGenerator.testContact.buildInsert(new Map<String, Object>{
                'AccountId'=>objAcc.Id,
                    'LastName'=>objAcc.name,
                    'firstName'=>objAcc.name,
                    'Encrypted_SSN__c'=>encryptSSN,                    
                    'SerNo__c' => '125895',
                    'SSN__c' =>'78958741',
                    'Email'=>'xyz@ab1c.com'
                    });
            
            
            Financial_Account__c objFA = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Account_Number__c'=>'4141144', 
                    'Customer__c'=>objAcc.Id,// MAH
                    'Account_Serno__c'=>'11166',
                    'Customer_Serno__c'=>objAcc.Customer_Serno__c,
                    'Product__c'=>objProd.Id,
                    'Product_Serno__c'=>objProd.Pserno__c,
                    'ApplicationId__c'=>'3242345'
                    });
            
            
            Case caseObj = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                'ContactId'=>objCon.Id, 
                    'AccountId'=>objAcc.Id, 
                    'Financial_Account__c'=>objFA.Id, 
                    'Description'=>'kundens personnummer: 1963-09030010\n <AppID>3242345</AppID> \n Testing SSN',
                    'To_Address__c'=>Label.Soknad_Email_address,
                    'SuppliedEmail'=>Label.FundTransfer_EmailId,
                    'status'=>'New', 
                    'Subject'=>'Vår ref:3242345 test'
                    
                    
                    });
            EmailMessage EmailMessageObj = UnitTestDataGenerator.TestEmailMessage.buildInsert(new Map<String, Object>{
                'Incoming'=>True,
                    'ParentId'=>caseObj.Id,
                    'Subject'=>'Vår ref:3242345 test',
                    'ToAddress'=>Label.Soknad_Email_address,
                    'TextBody' =>'kundens personnummer: 1963-09030010\n <AppID>3242345</AppID> \n Testing SSN'
                    });
            /*Map<Id, EmailMessage> EmailNewMap = new Map<Id, EmailMessage>();
            EmailMessageTriggerHandler Email = new EmailMessageTriggerHandler();
            system.assertEquals(true, EmailMessageObj.Incoming);
            Test.stopTest();
        }
        
    } */
}