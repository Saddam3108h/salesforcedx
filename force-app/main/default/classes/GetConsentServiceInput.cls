/*
----------------------------------------------------------------------------------------
Author    Date         Description
Shameel   17/01/2017   Input class to implement SOA service using Integration framework.
----------------------------------------------------------------------------------------
*/
public class GetConsentServiceInput{

    public String InstitutionId;
    public String ConsentEntityId;
    public String ConsentEntityIdType;
    public DateTime FromDate;
    public DateTime ToDate;
    //To Cover Test Class 100%
    public void test()
    {
        
    }
   
}