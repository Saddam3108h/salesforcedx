/**********************************************************************
Name: DeleteManageCampaignBatch
=======================================================================
Purpose: This apex is a batch and used to delete Manage Campaign data which are inactive and more than 30 day SFD-1692

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                 Date              Detail
1.0         Priyanka Singh         01-10-2020      Initial Version
2.0			Kakasaaheb Ekshinge    13-Nov-2020	   Code optimization
3.0			Priyanka Singh         8-Feb-2021	   data purgeing discussion due to Marketing Cloud
**********************************************************************/
global class DeleteManageCampaignBatch implements Database.Batchable<sObject>,Schedulable{
    /**********************************************************************
*   Author: Priyanka Singh
*   Date:     
*   User Story : 
*   Param: None
*   Return: None    
*   Description: In this method we check whether logged in user has valid access token from custom setting
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    global Database.QueryLocator start(Database.BatchableContext context){
        Date today = System.today();
		Date lastmonth = today.addMonths(-1);
        // Below line of code commented for data purgeing discussion due to Marketing Cloud
        //return Database.getQueryLocator('Select IsActive__c,ID from Manage_Campaign__c where End_Date__c <=: lastmonth and IsActive__c = false');
        return Database.getQueryLocator('Select IsActive__c,ID from Manage_Campaign__c where IsActive__c = false');
       
    }
    
    global void execute(Database.BatchableContext context, List<Manage_Campaign__c> scope){   
        try{           
            if(scope!=null && !scope.isEmpty()) {
            	Database.delete(scope, false);
            	Database.emptyRecycleBin(scope); 
            }
        }
        catch(Exception e){
            System.debug('Unexpected error occurred: ' + e.getMessage());
            StatusLogHelper.logSalesforceError('DeleteManageCampaignBatch', 'E004', 'scope: ' + scope, e, true);
        }
    }
    
    global void execute(SchedulableContext ctx){
        Database.executeBatch(new DeleteManageCampaignBatch());
    }
    global void finish(Database.BatchableContext BC){}
}