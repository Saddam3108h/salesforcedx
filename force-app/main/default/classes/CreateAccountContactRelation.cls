public class CreateAccountContactRelation{

    @InvocableMethod
    public static void CreateAccountContactRelation(List<id> CardID)
    {
        List<card__C>cardList=[SELECT id,Financial_Account__r.Customer__c,People__c from card__C where id in:CardID];
        if(cardList!=null && cardList.size()>0){
            set<id>ACCid=new set<id>();
            set<id>CONTid=new set<id>();
            set<string>ACCandContSet=new set<string>();
            for(card__C cc : cardList)
            {
                ACCid.add(cc.Financial_Account__r.Customer__c);
                CONTid.add(cc.People__c);
            }
            for(AccountContactRelation acccont : [SELECT Id,accountid,Contactid from AccountContactRelation where accountid in:ACCid and Contactid in:CONTid])
            {
                ACCandContSet.add(acccont.accountid+'-'+acccont.Contactid);    
            }
            List<AccountContactRelation>toINSERT=new List<AccountContactRelation>();
            for(card__C cc : cardList)
            {
                if(!ACCandContSet.contains(cc.Financial_Account__r.Customer__c+'-'+cc.People__c))
                {
                    toINSERT.add(new AccountContactRelation(accountid=cc.Financial_Account__r.Customer__c,Contactid=cc.People__c,IsActive=true));
                    ACCandContSet.add(cc.Financial_Account__r.Customer__c+'-'+cc.People__c);    
                }
            }
            if(toINSERT!=null && toINSERT.size()>0)
                insert toINSERT;
        }
    }
}