/*
----------------------------------------------------------------------------------------
Author    Date         Description
Shameel   30/01/2017   Input class to implement SOA service using Integration framework.
----------------------------------------------------------------------------------------
*/
public class CLIApplicationServiceInput{
        
        public Integer InstitutionId;
        public String Product;
        public String SourceCode;
        public String CampaignCode;
        public String UniqueCode;
        public String CryptId;
        public String AccountNumber;
        public String AccountSerno;
        public String CardNumber;
        public String ApplicationID;
        public String BankName;
        public String BankID;
        public Decimal DesiredCreditLimit;
        public String UTMSource;
        public String UTMMedium;
        public String UTMCampaign;
        public String UTMTerm;
        public String UTMContent;
        public String UTMGclid;
        public String UTMPrisfakta;
        public string Channel;
        
        public ApplicantCLIDataType ApplicantCLIDetails;
        
      
        
        public class ApplicantCLIDataType {
        public String SSN;
        public String EmploymentType;
        public Decimal GrossYearlyIncome;
        public Decimal GrossMonthlyIncome;
        public Decimal NetMonthlyIncome;
        public String Employer;
        public String EmployedSince;
       // public Decimal MortgageDebt;
        public Decimal MonthlyCostOfHousing;
        public Integer NumberOfChildrenUnder18;
        public Integer NumberOfAdults;
        public Decimal OtherLoans;
        public Integer Age;
        public Date DateOfBirth;
        public String HousingSocieties;
        public String DesiredDueDate;
        public String MarketingConsent;
        public Decimal MembershipNumber;
        public Decimal TotalDebt;
        public String HousingType;
        
        // Sfd 1196
        
        public Decimal CreditCardDebt;
        public Decimal ConsumerOtherLoans;
        public String TypeOfHousing;
        public String MaritalStatus;
        public Integer SharingHouseholdPercentage; //sfd 1252
        public string HaveCar; //sfd 1252
        public Decimal Rent; //sfd 1252
        public Decimal OtherIncome; //sfd 1252
        public debtRegistryDataDetailsType[] DebtRegistryDataDetails;

    }
   
        public class debtRegistryDataDetailsType{
        public String LoanType;
        public Decimal LoanAmountCreditCardLimit;
        public Decimal RemainingLoanTerm;
        public Decimal NominalInterestRate;
   
   }    
}