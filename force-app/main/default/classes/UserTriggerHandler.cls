/**********************************************************************
Name: User
=======================================================================
Purpose: This apex is used for UserTriggerHandler

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         20-July-2020       Initial Version
2.0			Priyanka Singh		   10-Aug-2020	     Trigger Factory Implementation	

**********************************************************************/
public class UserTriggerHandler implements ITriggerHandler {
     //  // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
 
    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
      
        if (TriggerSettings__c.getInstance().UserExecution__c // false
            &&  TriggerSettings__c.getInstance().OrgExecution__c)
        { 
            return true;
        }
        else
        { 
            return TriggerDisabled;
        }
        
    }
 
    public void BeforeInsert(List<SObject> newItems) {}
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterInsert(Map<Id, SObject> newItems) {
         Map<Id, User> usersItems = (Map<Id, User>) newItems;  
         assignPermissionSet(usersItems.values());
        
    }
 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        
         Map<Id, User> usersItems = (Map<Id, User>) newItems;  
         assignPermissionSet(usersItems.values());
    } 
     
    public void AfterDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}

    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     
*   User Story : 
*   Param: None
*   Return: None    
*   Description: In this method we update permission set when user record gets created
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public static void assignPermissionSet(List<User> userList) {
        //Below rolesMap is to to hold Id and its Role Record from UserRole Object.
        Map<Id,UserRole> rolesMap = new Map<Id,UserRole>([Select  Id,
                                                          Name,
                                                          DeveloperName 
                                                          FROM UserRole]);
        
        Map<Id, Id> userPsetMap = new Map<Id,Id>();
        
        // Getting DCMS Permission Set Name and F&C team Role Names from Custom Settings
        String psetName = System.Label.DCMS_PSet_Name;
        String fcRoles = System.Label.DCMS_Role_Names;
        
        Id permSetId = [SELECT Id,Name FROM PermissionSet WHERE Name =: psetName LIMIT 1].Id;
        List<PermissionSetAssignment> insertPestAssgn = new List<PermissionSetAssignment>(); 
        
        for(PermissionSetAssignment psa : [SELECT Id,
                                           PermissionSetId,AssigneeId 
                                           FROM PermissionSetAssignment 
                                           WHERE PermissionSetId=: permSetId]){
                                               userPsetMap.put(psa.AssigneeId,psa.Id);
                                           }
        
        List<Id> removePestAssgn = new List<Id>(); 
        for(User usr : userList){
            if(usr.UserRoleId != NULL){
                if(fcRoles.contains(rolesMap.get(usr.UserRoleId).DeveloperName)){
                    PermissionSetAssignment psetAssign = new PermissionSetAssignment();
                    if(userPsetMap.get(usr.id) == NULL){
                        psetAssign.AssigneeId = usr.Id;
                        psetAssign.PermissionSetId = permSetId;
                        insertPestAssgn.add(psetAssign);
                    }
                }
                else{
                    removePestAssgn.add(usr.Id);
                }
            }
        }
        //Below 'if' is to remove DCMS Permission 
        if(removePestAssgn.size() > 0){
            Delete [SELECT Id from PermissionSetAssignment 
                    WHERE PermissionSetId = : permSetId 
                    AND AssigneeId IN : removePestAssgn];
        }
        if(insertPestAssgn.size() > 0){
            try{
               insert insertPestAssgn; 
            }
            catch(Exception exp){
                StatusLogHelper.logSalesforceError('UserTriggerHandler', 'assignPermissionSet', 'assignPermissionSet', exp, false);
			}
        }       
        
    }
}