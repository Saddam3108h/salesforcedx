/********************************************************************************************
Name: AlertSchedular_Test
=============================================================================================
Purpose: This test class is covering 'AlertSchedular' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0        Srinivas D             4th-April-2019    SFD-984   		    Intial version
2.0		  Kakasaheb Ekshinge   	  22-July-2020		SFD-1596			Code coverage has increases
********************************************************************************************/
@isTest
public class AlertSchedular_Test {
    
    static testMethod void alertSchedule(){
        System.runAs(UnitTestDataGenerator.adminUser){
            Test.startTest();
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c' => '1123123' 
                    });
            
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000061'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'SerNo__c'=>'2123123',
                    'Encrypted_SSN__c'=>encryptSSN,
                    'AccountId'=>accountObj.Id              
                    });  
            
             Alert__c AL = UnitTestDataGenerator.testAlert.buildInsert(new Map<String, Object>{
               	 	'People__c'=>contactObj.Id                             
                    });
            Alert__c AL1 = UnitTestDataGenerator.testAlert.buildInsert(new Map<String, Object>{
               	 	'People__c'=>contactObj.Id                             
                    });
                   
            AlertSchedular obj = new AlertSchedular();  
            obj.execute(null);  
            system.assertEquals(true,AL.Active__c,'invalid');
            Test.stopTest(); 
        }
    }
    
}