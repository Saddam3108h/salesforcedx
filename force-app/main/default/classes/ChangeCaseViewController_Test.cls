/**********************************************************************
Name:ChangeCaseViewController_Test
=======================================================================
Purpose: This is the test class for 'ChangeCaseViewController' Apex Class
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date            CreatedforJira  Detail
1.0         Kakasaheb Ekshinge       06-Aug-2020     SFD-1538        Initial Version
**********************************************************************/
@istest private class ChangeCaseViewController_Test 
{       
    static testmethod void changecaseviewcontrollermethod(){
        System.runAs(UnitTestDataGenerator.adminUser){
            test.startTest();
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            boolean isUpdatedSuccessfully = false;
            pagereference returl; 
            Case cas = UnitTestDataGenerator.TestCase.buildInsert(new Map<String, Object>{
                    'Priority' => 'Medium', 
                    'Origin' => 'Email',
                    'Case_View__c' =>'DK Chargeback'
                    });            
            ApexPages.StandardController sc = new ApexPages.StandardController(cas);    
            ChangeCaseViewController CCVC = new ChangeCaseViewController(sc);
            ccvc.saveCaseView();
            System.assertEquals('Medium',cas.Priority,'The Priority is not correct');
            test.stopTest();        
        }
    }
}