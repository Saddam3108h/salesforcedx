@isTest
public class NonCustomerController_Test {
    @testSetup
    public static void testInsertData() 
    {
        NonCustomerController noncust = new NonCustomerController();   
        noncust.SelectedInstId = '1';
        noncust.Firstname='Partner';
        noncust.Lastname='Noncustomer';
        noncust.Address1='Testing';
        noncust.City='SE';
        noncust.Zip='50002';
        noncust.SelectPartner='GDPR';
        noncust.selectIdentityType='SSN';
        noncust.IdentifierValue = '456755';
        noncust.UnionName='Test';
        noncust.DepartmentNumber='5876';
        noncust.code = 'Test2';
        noncust.description = 'Success2';
        
        //GlobalSettings__c
        List<GlobalSettings__c>GlobalSettingsList=new List<GlobalSettings__c>();
        GlobalSettings__c objGlobal = new GlobalSettings__c();
        objGlobal.Name = 'ServiceStrategies'; 
        objGlobal.Value__c = 'SOAPService';
        GlobalSettingsList.add(objGlobal);
        
        insert GlobalSettingsList;
        
        List<ServiceHeaders__c> serviceHeadersList =new List<ServiceHeaders__c>();
        //ServiceHeaders
        ServiceHeaders__c objServiceHeaders = new ServiceHeaders__c();
        objServiceHeaders.name = 'GetPartnerHeader';
        serviceHeadersList.add(objServiceHeaders);
        
        ServiceHeaders__c objServiceHeaders2 = new ServiceHeaders__c();
        objServiceHeaders2.name = 'GetNonCustomerHeader';
        serviceHeadersList.add(objServiceHeaders2);
        
        insert serviceHeadersList;
        
        //ServiceSettings
        List<ServiceSettings__c> serviceSettingList = new List<ServiceSettings__c>();
        
        ServiceSettings__c objService = new ServiceSettings__c();
        objService.name = 'GetPartnerDetails';
        objService.HeaderName__c = 'GetPartnerHeader'; 
        objService.EndPoint__c = 'http://midtlb1d2pub.entercard.com/CSSService/ProxyServices/CSSServicePS2?wsdl';
        objService.OutputClass__c = 'GetPartnerOutputs';
        objService.ProcessingClass__c = 'GetPartnerNameServiceCallout';
        objService.Strategy__c = 'SOAPService';
        objService.LogRequest__c = false;
        objService.LogResponse__c = false;
        objService.LogWithCallout__c = false;
        objService.Input_Class__c = 'GetPartnerInputs';
        objService.Username__c='Username';
        objService.Password__c='Password';
        serviceSettingList.add(objService);
        
        ServiceSettings__c objService1 = new ServiceSettings__c();
        objService1.name = 'GetNonCustomerDetails';
        objService1.HeaderName__c = 'GetNonCustomerHeader'; 
        objService1.EndPoint__c = 'http://midtlb1d2pub.entercard.com/CSSService/ProxyServices/CSSServicePS2?wsdl';
        objService1.OutputClass__c = 'GetNonCustomerOutputs';
        objService1.ProcessingClass__c = 'GetNonCustomerServiceCallout';
        objService1.Strategy__c = 'SOAPService';
        objService1.LogRequest__c = false;
        objService1.LogResponse__c = false;
        objService1.LogWithCallout__c = false;
        objService1.Input_Class__c = 'GetNonCustomerInputs';
        objService1.Username__c='Username';
        objService1.Password__c='Password';
        serviceSettingList.add(objService1);
        
        insert serviceSettingList;
        
        ServiceSettings__c objService2 = new ServiceSettings__c();
        objService2.name = 'StatusLogService';
        objService2.HeaderName__c = 'RESTJSONOAuth'; 
        objService2.EndPoint__c = 'https://cs82.salesforce.com/services/apexrest/StatusLogRecord/';
        objService2.OutputClass__c = '';
        objService2.Operation__c = 'POST';
        objService2.ProcessingClass__c = 'StatusLogServiceIO';
        objService2.Strategy__c = 'RESTService';
        objService2.LogRequest__c = false;
        objService2.LogResponse__c = false;
        objService2.LogWithCallout__c = false;
        objService2.Input_Class__c = '';
        objService2.Username__c='';
        objService2.Password__c='';
        serviceSettingList.add(objService2);
        
        //Common_Settings__c
        List<Common_Settings__c> commonSettingList =new List<Common_Settings__c>();
        Common_Settings__c commonSetting =new Common_Settings__c(
            Name='CorrelationId',
            Common_Value__c='1'
        );
        commonSettingList.add(commonSetting);
        Common_Settings__c commonSetting1 =new Common_Settings__c(
            Name='RequestorId',
            Common_Value__c='Salesforce'
        );
        commonSettingList.add(commonSetting1);
        Common_Settings__c commonSetting2 =new Common_Settings__c(
            Name='SystemId',
            Common_Value__c='1'
        );
        commonSettingList.add(commonSetting2);
        insert commonSettingList;
        
        ErrorCodes__c errorCodes =new ErrorCodes__c();
        errorCodes.Name='0';
        errorCodes.ErrorDescription__c='ErrorDescription';
        errorCodes.UIDisplayMessage__c='UIDisplayMessage';
        insert errorCodes;
        
        ErrorCodes__c errorCodes1 =new ErrorCodes__c();
        errorCodes1.Name='defaultError';
        errorCodes1.ErrorDescription__c='ErrorDescription';
        errorCodes1.UIDisplayMessage__c='UIDisplayMessage';
        insert errorCodes1;
    }
    
    public static testmethod void testmethod1()
    {
        test.startTest();
        Test.setMock(WebServiceMock.class, new GetPartnerWebServiceMockclass());
        PageReference pageRef = Page.CreateNonCustomer_VF;
        Test.setCurrentPage(pageRef);
        NonCustomerController noncust1 = new NonCustomerController();
        noncust1.getInstitutionIds();
        
        noncust1.getPartnerNames();
        noncust1.SelectedInstId = '--None--';
        noncust1.getPartnerNames();
        noncust1.option.clear();
        noncust1.option.add(new SelectOption('--None--', '--None--'));
        test.stopTest();
    }
    
    public static testmethod void testmethod3()
    {
        test.startTest();
        Test.setMock(WebServiceMock.class, new GetNonCustomerWebServiceMockclass());
        PageReference pageRef = Page.CreateNonCustomer_VF;
        Test.setCurrentPage(pageRef);
        
        NonCustomerController noncust2 = new NonCustomerController();
        noncust2.getInstitutionIds();
        noncust2.getIdentifierType();
        noncust2.SelectedInstId = '--None--';
        noncust2.Firstname = '';
        noncust2.Lastname = '';
        noncust2.City = '';
        noncust2.Zip = '';
        noncust2.SelectPartner='--None--';
        noncust2.selectIdentityType = '--None--';
        noncust2.IdentifierValue = '';
        noncust2.insertNonCustomer();
        
        noncust2.SelectedInstId = '1';
        noncust2.Firstname='Partner2';
        noncust2.Lastname='Noncustomer2';
        noncust2.Address1='Testing2';
        noncust2.City='SE2';
        noncust2.Zip='500022';
        noncust2.SelectPartner='6';
        noncust2.selectIdentityType='SSN';
        noncust2.IdentifierValue = '7895233';
        noncust2.UnionName='Test2';
        noncust2.DepartmentNumber='58762';
        noncust2.code = '0';
        noncust2.description = 'success';
        noncust2.insertNonCustomer();
        
        noncust2.SelectedInstId = '1';
        noncust2.Firstname='Partner2';
        noncust2.Lastname='Noncustomer2';
        noncust2.Address1='Testing2';
        noncust2.City='SE2';
        noncust2.Zip='500022';
        noncust2.SelectPartner='--None--';
        noncust2.selectIdentityType='--None--';
        noncust2.IdentifierValue = '58221';
        noncust2.UnionName='Test2';
        noncust2.DepartmentNumber='58762';
        noncust2.insertNonCustomer();
        
        
        
        test.stopTest();
    }
    
}