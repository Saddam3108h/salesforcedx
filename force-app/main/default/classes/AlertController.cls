/********************************************************************************************
Name: AlertController
=============================================================================================
Purpose: This class will send true false to LWC comp to display High risk Customer alerts
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author               Date         Created for Jira     Detail
1.0        Kakasaheb Ekshinge    6-Nov-2020   SFD-1804   		    Intial version
********************************************************************************************/
public class AlertController {
    
    @AuraEnabled(cacheable=true)
    public static Boolean validHighRiskAlert(String recordId){       
        List<Alert__c> alertList = [select Id,active__c,CreatedDate from Alert__c WHERE People__c = : recordId AND Category__c = 'High Risk Customer'];
        Boolean flag = FALSE;
        for(Alert__c al : alertList){
            If(al.active__c)
                flag = TRUE;            
        }
       system.debug('AlertController Test==>'+flag);
        return flag;
    }
    
}