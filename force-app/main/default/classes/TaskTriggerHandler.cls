/**********************************************************************
Name: TaskTriggerHandler
=======================================================================
Purpose: This trigger handler class is created on Task object to handle all the Task related logics.User Story -SFD-1363

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         03-June-2020       Initial Version
2.0			Priyanka Singh		   11-Aug-2020	   Trigger Factory Implementation

**********************************************************************/
public with sharing class TaskTriggerHandler implements ITriggerHandler {
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {
        
        if (TriggerSettings__c.getInstance().TaskExecution__c 
            &&  TriggerSettings__c.getInstance().OrgExecution__c)
        { 
            return true;
        }
        else
        { 
            return TriggerDisabled;
        }
        
    }
    
    public void BeforeInsert(List<SObject> newItems) {
        updateSernos(newItems);        
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {
        Map<Id, Task> addressItems = (Map<Id, Task>) oldItems;
        updateTaskCount(addressItems.values());
    }
    
    public void AfterInsert(Map<Id, SObject> newItems) {}
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, Task> addressItems = (Map<Id, Task>) newItems; 
        updateTaskCount(addressItems.values());
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     03-June-2020
*   User Story : SFD-1363
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update task count and Assign to field in SaveDesk object.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    public static void updateTaskCount(List<Task> taskList) { 
        try {
            Set<Id> saveDeskIdSet = new Set<Id>();
            for(Task ts : taskList) {
                if(ts.SaveDesk__c != null) {
                    saveDeskIdSet.add(ts.saveDesk__C);  
                }
            }
            
            List<SaveDeskQueue__c> saveDeskList = new List<SaveDeskQueue__c>();
            system.debug('---saveDeskIdSet---'+saveDeskIdSet);
            if(saveDeskIdSet.size() > 0) {
                for(AggregateResult taskRec : [SELECT COUNT(Id), saveDesk__C, OwnerId FROM Task WHERE  
                                               saveDesk__C IN : saveDeskIdSet 
                                               GROUP BY saveDesk__C,OwnerId]
                   ) {
                       saveDeskList.add(new SaveDeskQueue__c(Id = (Id)taskRec.get('saveDesk__C'), 
                                                             Task_Count__c =(INTEGER)taskRec.get('expr0'),
                                                             Agent_ID__c = (Id)taskRec.get('OwnerId'))
                                       );
                   }
            }
            if(saveDeskList.size() > 0){
                StaticConstant.SAVEDESK_ISRECURSIVE = true;
                Update saveDeskList;
            }
        } catch(Exception exp) {
            StatusLogHelper.logSalesforceError('TaskTriggerHandler', 'E005', 'updateTaskCount', exp, false); // Class,method
        }
    }
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     03-June-2020
*   User Story : SFD-1363
*   Param: None
*   Return: None    
*   Description: In this method we are trying to prevent the task record deletion.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    public static void deleteTaskRecord(List<Task> taskList) {
        String profileId = UserInfo.getProfileId();
        List<String> profileNameList = new List<String>{StaticConstant.DK_OPERATION,StaticConstant.NO_OPERATION,
            StaticConstant.SE_OPERATION,StaticConstant.SE_NORWAYEMAILACCESS,
            StaticConstant.SE_VBC};
                List<Profile> profiles=[SELECT Id FROM Profile WHERE Name IN: profileNameList];
        for(Task taskObj : taskList) {
            for(Profile prObj : profiles) {
                if(profileId == prObj.Id) {
                    taskObj.addError(StaticConstant.TASK_DELETE_MESSAGE); 
                }
            }
        }
    }
    
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     16-June-2020
*   User Story : SFD-1567
*   Param: None
*   Return: None    
*   Description: In this method we are trying to update whoId and MAH,Customer,FA,Card Serno's in Task table 
when memo data pushs from Prime.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    
    public static void updateSernos(List<Task> taskList) {    
        Set<String> mahSernoSet = new Set<String>();
        Set<String> contactSernoSet = new Set<String>();
        Set<String> finAccSernoSet = new Set<String>();
        Set<String> cardSernoSet = new Set<String>();
        TaskWrapper objWrapper = new TaskWrapper();
        Map<String, TaskWrapper> sernoMap = new Map<String, TaskWrapper>();
        system.debug('--taskList-'+taskList);
        for(Task taskObj : taskList) {
            if( taskObj.Entity__c !=null      
               && taskObj.Entity__c !=''
               && (String.isNotBlank(taskObj.MAH_Serno__c)
                  || String.isNotBlank(taskObj.Customer_Serno__c)
                  || String.isNotBlank(taskObj.Account_Serno__c)
                  || String.isNotBlank(taskObj.Card_Serno__c))
              ) {
                  taskObj.Status = StaticConstant.TASK_COMPLETE_STATUS;
                  taskObj.Subject = StaticConstant.TASK_MEMO_SUBJECT;
                  if(taskObj.Entity__c == 'ccustomers'){
                      mahSernoSet.add(taskObj.MAH_Serno__c);
                  }else if(taskObj.Entity__c == 'people') {
                      contactSernoSet.add(taskObj.Customer_Serno__c);
                  } else if(taskObj.Entity__c == 'caccounts') {
                      finAccSernoSet.add(taskObj.Account_Serno__c);
                  } else if(taskObj.Entity__c == 'cardx') {
                      cardSernoSet.add(taskObj.Card_Serno__c);
                  }
              }
            system.debug('--mahSernoSet-'+mahSernoSet);
            if(mahSernoSet.size() > 0) {
                for(AccountContactRelation accConObj : [SELECT Id,ContactId, Contact.SerNo__c, Account.Customer_Serno__c
                                                        FROM AccountContactRelation 
                                                        WHERE IsDirect = true 
                                                        AND Account.Customer_Serno__c IN : mahSernoSet
                                                        ORDER BY
                                                        CreatedDate ASC Limit 1]
                   ) {
                       system.debug('----173--');
                       for(Card__c cardObj : [SELECT Id, Card_Serno__c,Financial_Account__c,
                                              People_Serno__c,Financial_Account_Serno__c
                                              FROM Card__c 
                                              WHERE People__c=:accConObj.contactId 
                                              ORDER BY ExpiryDate__C DESC Limit 1]
                          ) {
                              system.debug('----180--');
                              taskObj.WhoId = accConObj.contactId;
                              taskObj.Customer_Serno__c = accConObj.contact.SerNo__c;
                              taskObj.Account_Serno__c = cardObj.Financial_Account_Serno__c;
                              taskObj.Card_Serno__c = cardObj.Card_Serno__c;
                              taskObj.WhatId = cardObj.Financial_Account__c;
                          }
                   }
            } else if(contactSernoSet.size() > 0) {
                for(Contact conObj : [SELECT Id, Serno__c, Account.Customer_Serno__c
                                      FROM Contact 
                                      WHERE Serno__c IN:contactSernoSet]
                   ) {
                       for(Card__c cardObj : [SELECT Id, Card_Serno__c,Financial_Account__c,
                                              People_Serno__c,Financial_Account_Serno__c
                                              FROM Card__c 
                                              WHERE People__c=:conObj.Id 
                                              ORDER BY ExpiryDate__C DESC Limit 1]
                          ) {
                              taskObj.MAH_Serno__c = conObj.Account.Customer_Serno__c;
                              taskObj.Account_Serno__c = cardObj.Financial_Account_Serno__c;
                              taskObj.Card_Serno__c = cardObj.Card_Serno__c;
                              taskObj.WhatId = cardObj.Financial_Account__c;
                          }
                   }
            } else if(finAccSernoSet.size() > 0) {
                for(Card__c crdObj : [SELECT Id,People__c, People_Serno__c,Financial_Account_Serno__c,
                                      Card_Serno__c,People__r.Account.Customer_Serno__c,Financial_Account__c
                                      FROM Card__c 
                                      WHERE Financial_Account_Serno__c IN : finAccSernoSet]
                   ) {
                       taskObj.MAH_Serno__c = crdObj.People__r.Account.Customer_Serno__c;
                       taskObj.WhoId = crdObj.People__c;
                       taskObj.Customer_Serno__c = crdObj.People_Serno__c;
                       taskObj.Card_Serno__c = crdObj.Card_Serno__c;
                       taskObj.WhatId = crdObj.Financial_Account__c;
                   }
            } else if(cardSernoSet.size() > 0) {
                for(Card__c crdObj : [SELECT Id,People__c, People_Serno__c,Financial_Account_Serno__c,
                                      People__r.Account.Customer_Serno__c,Financial_Account__c,
                                      Card_Serno__c
                                      FROM Card__c 
                                      WHERE Card_Serno__c IN : cardSernoSet]
                   ) {
                       taskObj.MAH_Serno__c = crdObj.People__r.Account.Customer_Serno__c;
                       taskObj.WhoId = crdObj.People__c;
                       taskObj.Customer_Serno__c = crdObj.People_Serno__c;
                       taskObj.Account_Serno__c = crdObj.Financial_Account_Serno__c;
                       taskObj.WhatId = crdObj.Financial_Account__c;
                   }
            }
        }
    }    
    /**********************************************************************
Name: TaskWrapper
=======================================================================
Purpose: This below wrapper class is used in updateSernos method.  User Story -SFD-1567

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         16-June-2020       Initial Version

**********************************************************************/

    public class TaskWrapper {
        Id contactId;
        Id financialId;
        String mahSerno;
        String contactSerno;
        String finAccountSerno;
        String cardSerno;
    }
}