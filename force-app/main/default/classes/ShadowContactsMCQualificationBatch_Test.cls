@isTest
public class ShadowContactsMCQualificationBatch_Test {
    Public static testMethod void mCDisQualification(){
        System.runAs(UnitTestDataGenerator.adminUser){
            
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});  
            Account accountObj=UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{});
            Individual individual =UnitTestDataGenerator.TestIndividual.buildInsert(new Map<String, Object>{});
            Contact contactObj =UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id,
                    'IndividualId'=>individual.Id,
                    'RecordTypeId'=>Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Shadow Contact RT').getRecordTypeId()
                    });  
            Product_Custom__c prodobj=UnitTestDataGenerator.TestProductCustom.buildInsert(new Map<String, Object>{});
            Financial_Account__c finAccObj=UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id, 
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Account_Open_Flag__c'=> false,
                    'Account_Closed_Date__c'=>System.today()-100,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            Card__c cardObj=UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Financial_Account_Serno__c'=>finAccObj.Id,
                    'Customer_Shadow__c'=> contactObj.Id,
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id,
                    'Customer_Shadow__c'=>contactObj.Id
                    });
            
            ShadowContactsMCDisqualificationBatch shn = new ShadowContactsMCDisqualificationBatch('full');
            database.executeBatch(shn );
            ShadowContactsMCDisqualificationBatch shn1 = new ShadowContactsMCDisqualificationBatch('delta');
            database.executeBatch(shn1 );
            
            System.assertEquals(true, cardObj.PrimaryCardFlag__c,'PrimaryCardFlag is not correct');
        }        
    }
    
    Public static testMethod void mCQualification(){
        System.runAs(UnitTestDataGenerator.adminUser){
            
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{});  
            Account accountObj=UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{});
            Individual individual =UnitTestDataGenerator.TestIndividual.buildInsert(new Map<String, Object>{});
            Contact contactObj =UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id,
                    'IndividualId'=>individual.Id,
                    'RecordTypeId'=>Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Shadow Contact RT').getRecordTypeId()
                    });  
            Product_Custom__c prodobj=UnitTestDataGenerator.TestProductCustom.buildInsert(new Map<String, Object>{});
            Financial_Account__c finAccObj=UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id, 
                    'Customer_Serno__c'=>contactObj.SerNo__c, 
                    'Product__c'=>prodobj.Id,
                    'Account_Open_Flag__c'=> true,
                    'Account_Closed_Date__c'=>null,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            Card__c cardObj=UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Financial_Account_Serno__c'=>finAccObj.Id,
                    'Customer_Shadow__c'=> contactObj.Id,
                    'People_Serno__c'=>contactObj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c, 
                    'Product__c'=>prodobj.Id,
                    'Customer_Shadow__c'=>contactObj.Id
                    });
            
            ShadowContactsMCQualificationBatch shn = new ShadowContactsMCQualificationBatch('full');
            database.executeBatch(shn );
            ShadowContactsMCQualificationBatch shn1 = new ShadowContactsMCQualificationBatch('delta');
            database.executeBatch(shn1 );
            
            System.assertEquals(true, cardObj.PrimaryCardFlag__c,'PrimaryCardFlag is not correct');
        }        
    }
}