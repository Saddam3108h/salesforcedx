public with sharing class PeopleDetailController{ 

 public Contact contactObj {get;set;}
 public List<Financial_Account__c> financialAccountList {get;set;}
 //to store best segment from financial account list.
 public Financial_Account__c bestfinancialAcc{get;set;}
 public Financial_Account__c totalCL{get;set;}
 public Integer totalFinAccount{get;set;}
 public Boolean isAlert3{get;set;}
 public Map<String,Decimal> mapsernoBalanceFA{get;set;}
 // Added by Shameel : Start
 public Map<String,FinancialAccountWrapper> serNoToFinancialAccountMap{get;set;}
 // Added by Shameel : End
 
 // default constructor
 public PeopleDetailController(){
     //Instantiate object of Financial_Account__c
     bestfinancialAcc = new Financial_Account__c();
 }
 
 // standard constructor
 public PeopleDetailController(ApexPages.StandardController sc){
     // Added by Shameel : Start
    serNoToFinancialAccountMap =new Map<String,FinancialAccountWrapper>();
    // Added by Shameel : End
    if(!Test.isRunningTest()) { 
        sc.AddFields(new List<String>{'Email'});
        sc.AddFields(new List<String>{'MobilePhone','Institution_Id__c','SSN__c'});//added by swarnava
    }
    this.isAlert3=false;
    this.contactObj =(Contact)sc.getRecord();
    
    List<Alert__c> alertList=[SELECT Id, DueDate__c, Name, Active__c, People__c FROM Alert__c where (Active__c=true and (DueDate__c=null or DueDate__c>=today)) and People__c=:contactObj.Id]; 
    
    if(alertList.size()>0)
    {
    this.isAlert3=true;
    }
    system.debug('<<---std controller--->>'+contactObj);
 }
 
 public void getFinancialAccountsByPeopleId()
 {
     
     financialAccountList =new List<Financial_Account__c>();
     if(contactObj!=null) 
     {
     set<Id> financialAccIdSet = new set<Id>();
     List<Card__c> cardList = [SELECT Id, Name, People__c, Financial_Account__c FROM Card__c where People__c=:contactObj.Id and Financial_Account__c!=null Limit 50000];
     
         
             for(Card__c card:cardList)
             {
             financialAccIdSet.add(card.Financial_Account__c);
             }
        
         
             financialAccountList =[select Id,Name,DirectDebit_Percentage__c,E_invoice_flag__c,Account_Serno__c,Customer__c,Interest_Rate__c,Credit_Limit__c,
             Partner_Membership_number__c, Account_Number__c,Segment__c,PPI__c,Risk_Indicator__c,Customer__r.Name,
             Product__r.Name,Product__c,Colour_Code__c from Financial_Account__c where Id IN :financialAccIdSet order by Name desc];
             
             //to count loop iteration.
             Integer countLoop = 0;
             totalFinAccount = financialAccountList.size();
             Set<Id> fsIds = new Set<Id>();
             //to check for duplicate fin accounts:Swarnava
             Integer fssizebf = 0;
             Integer fssizeaf = 0;
             totalCL = new Financial_Account__c();
             totalCL.Credit_Limit__c =0;//instantiate total CL:Swarnava             
             //for loop to get the best segment from financialAccountList.
             for(Financial_Account__c objfinancialAcc : financialAccountList)
             {
                 if(objfinancialAcc.Segment__c!=null && objfinancialAcc.Segment__c!=''){
                 countLoop++;
                 //to store 1st record of the list.
                 if(countLoop == 1)
                 {
                     bestfinancialAcc = objfinancialAcc;
                 }
                 //to get best segment from the list
                 if(bestfinancialAcc.Segment__c != null && bestfinancialAcc.Segment__c != '')
                 {
                     if(objfinancialAcc.Segment__c < bestfinancialAcc.Segment__c)
                     {
                         bestfinancialAcc = objfinancialAcc;
                     }
                     
                 }
                 }
                 //Calculate total Credit Limit added by Swarnava:US784                 
                 system.debug('objfinancialAcc.Credit_Limit__c->'+objfinancialAcc.Credit_Limit__c);               
                 fssizebf = fsIds.size();
                 fsIds.add(objfinancialAcc.id);
                 fssizeaf = fsIds.size();
                 if((fssizebf<fssizeaf) && objfinancialAcc.Credit_Limit__c!=null)
                 totalCL.Credit_Limit__c+=objfinancialAcc.Credit_Limit__c;
                 system.debug('totalCL-->'+totalCL.Credit_Limit__c);
             }
      //added by swarnava for 1031:Start       
     GetCustomerOutput gcoutput;        
     if(this.contactObj.Institution_Id__c!=null &&  this.contactObj.SSN__c!=null)       
         gcoutput= CSSServiceHelper.getCustomerDetails(integer.valueof(this.contactObj.Institution_Id__c),this.contactObj.SSN__c,'Y','Y');
         mapsernoBalanceFA = new Map<String,Decimal>();
         // Added by Shameel : Start
         Map<String,GetCustomerOutput.CustomerFinancialAccount> tempSerNoToFinAccMap = new Map<String,GetCustomerOutput.CustomerFinancialAccount>();
         // Added by Shameel : End
         if(gcoutput!=null && gcoutput.FinancialAccounts!=null){
         for(GetCustomerOutput.CustomerFinancialAccount cfa : gcoutput.FinancialAccounts){
             
             // Added by Shameel : Start
             if(cfa.Serno!=null)
             tempSerNoToFinAccMap.put(String.valueOf(cfa.Serno),cfa);
             // Added by Shameel : End
          }  
         
          system.debug('tempSerNoToFinAccMap->'+tempSerNoToFinAccMap); 
          }
         for(Financial_Account__c objfinancialAcc : financialAccountList){
             
             // Added by Shameel : Start
             GetCustomerOutput.CustomerFinancialAccount custFinAcc = tempSerNoToFinAccMap.get(objfinancialAcc.Account_Serno__c)!=null?tempSerNoToFinAccMap.get(objfinancialAcc.Account_Serno__c):new GetCustomerOutput.CustomerFinancialAccount();
             serNoToFinancialAccountMap.put(objfinancialAcc.Account_Serno__c,new FinancialAccountWrapper(custFinAcc.OTB,custFinAcc.StGen,custFinAcc.Balance,custFinAcc.Einvoice));
             // Added by Shameel : End
          }            
         system.debug('mapsernoBalanceFA->'+mapsernoBalanceFA);
         
     //added by swarnava for 1031:End    
     }

     system.debug('***financialAccountList***'+financialAccountList);
     
     //return financialAccountList;
    
 }
 
 // Wrapper Class
 // Added by Shameel : Start
 public class FinancialAccountWrapper
 {
     
    public Decimal OTB{get;set;}
    public String StGen{get;set;}
    public Decimal Balance{get;set;}
    public string Einvoice{get;set;}
    
    public FinancialAccountWrapper(Decimal oTB, String stGen,Decimal balance,string Einvoice)
    {
        this.OTB=oTB;
        this.StGen=stGen;
        this.Balance=balance;
        this.Einvoice=Einvoice;
    }
 }
  // Added by Shameel : End
}