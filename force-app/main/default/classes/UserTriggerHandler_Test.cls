/********************************************************************************************
Name: UserTriggerHandler_test
=============================================================================================
Purpose: To cover the 'PSetAssignment' apex code coverage 
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date         Created for Jira     Detail
1.0        Srinivas D             4th-April-2019    SFD-984   		    Intial version
2.0		   Kakasaheb Ekshinge 	  22-July-2020		SFD-1596			Code coverage has increases
********************************************************************************************/

@isTest
public class UserTriggerHandler_Test {
    testMethod static void userTriggerHandlerMethod() 
    {
        Id fCRoleId=[SELECT Id, Name FROM UserRole WHERE DeveloperName = 'DK_FC_Agent'].Id;
        User userObj = [SELECT Id, UserroleId, UserRole.Name, Email FROM User Where Id =: UnitTestDataGenerator.adminUser.Id];
        userObj.UserroleId = fCRoleId;
        update userObj;
        system.assertEquals('EC Customer Care', userObj.Userrole.Name);
    }
    
    testMethod static void rolesMapMethod() 
    {
        Id fCRoleId=[Select id, Name from UserRole where DeveloperName = 'DK_FC_Agent'].Id;
        User u = TestUserGenerator.createAdminUser(null, 'last', 'puser000@amamama.com', 'puser000@amamama.com1'+System.currentTimeMillis(), 
                                                   'TEST', 'title', 'America/Los_Angeles', 'UTF-8', 'en_US');
        u.LocaleSidKey = 'en_US'; 
        u.UserroleId = fCRoleId;
        insert u;
        System.assertEquals('puser000@amamama.com', u.email,'Email is not correct'); 
    }
}