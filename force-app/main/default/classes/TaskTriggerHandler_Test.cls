/**********************************************************************
Name: TaskTriggerHandler_Test
=======================================================================
Purpose: This test class is used to cove the code of TaskTriggerHandler class.User Story -SFD-1363

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         03-June-2020       Initial Version
**********************************************************************/
@isTest(seeAllData = false)
public class TaskTriggerHandler_Test {
    /**********************************************************************
*   Author: Saddam Hussain
*   Date:     20-May-2020
*   User Story : SFD-1430
*   Param: None
*   Return: None    
*   Description: In this method we are trying to Create the list of SaveDesk records to cover the code of updateTaskCount method.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   

**********************************************************************/
    static testMethod void updateTaskCountTest() {
        Test.startTest();
        System.runAs(UnitTestDataGenerator.adminUser) {
            TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account accountObj = UnitTestDataGenerator.TestAccount.buildInsert(new Map<String, Object>{
                'Customer_Serno__c'=>'1245'            
                    });
            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf('0000000062'));
            string encryptSSN=EncodingUtil.convertToHex(hash);
            Contact contactObj = UnitTestDataGenerator.TestContact.buildInsert(new Map<String, Object>{
                'AccountId'=>accountObj.Id, 
                    'Encrypted_SSN__c'=>encryptSSN,
                    'Serno__c' => '4589'
                    });
            
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert();
            
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c'=>accountObj.Id,
                    'Customer_Serno__c'=>accountObj.Customer_Serno__c, 
                    'Product__c'=>prodobj.Id,
                    'Product_Serno__c'=>prodobj.Pserno__c
                    });
            
            Card__c objCard = UnitTestDataGenerator.TestCard.buildInsert(new Map<String, Object>{
                'People__c'=>contactObj.Id,
                    'Financial_Account__c'=>finAccObj.Id, 
                    'Financial_Account_Serno__c'=>finAccObj.Account_Serno__c,
                    'People__c'=>contactObj.Id,
                    'People_Serno__c'=>contactObj.SerNo__c,
                    'Product__c'=>prodobj.Id,
                    'Prod_Serno__c'=>prodobj.Pserno__c  
                    });
            
            SaveDeskQueue__c sdObj = UnitTestDataGenerator.TestSaveDesk.buildInsert(new Map<String, Object>{
                'Status__c'=>'Open',
                    'Response__c'=>'Test',
                    'Task_Count__c'=>2                   
                    });
            List<SaveDeskQueue__c> saveDeskList = new List<SaveDeskQueue__c>();
            saveDeskList.add(sdObj);
            List<String>  sDeskAttr = new List<String> {'Status__c','Response__c','Calling_Attempts__c'};
                List<SaveDeskQueue__c> saveDeskReload = UnitTestDataGenerator.TestSaveDesk.reloadList(saveDeskList, sDeskAttr);
            Task taskObj = UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'WhatId' => accountObj.Id,
                    'Savedesk__c'=>saveDeskList[0].Id,
                    'Entity__c'=>'ccustomers',
                    'MAH_Serno__c' => '1245',
                    'Entity_Serno__c'=> '1245',
                    'CreatedById'=>UserInfo.getUserId()
                    });
            Task taskObj1 = UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'WhatId' => accountObj.Id,
                    'Savedesk__c'=>saveDeskList[0].Id,
                    'Entity__c'=>'people',            
                    'Entity_Serno__c'=> '4589',
                    'Customer_Serno__c' => '4589',
                    'MAH_Serno__c' => '4589',
                    'CreatedById'=>UserInfo.getUserId()
                    });
            Task taskObj2 = UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'WhatId' => accountObj.Id,
                    'Savedesk__c'=>saveDeskList[0].Id,
                    'Entity__c'=>'caccounts',
                    'Entity_Serno__c'=> 'Test Tets',
                    'MAH_Serno__c' => accountObj.Customer_Serno__c,
                    'CreatedById'=>UserInfo.getUserId()
                    });
            Task taskObj3 = UnitTestDataGenerator.TestTask.buildInsert(new Map<String, Object>{
                'WhatId' => accountObj.Id,
                    'Savedesk__c'=>saveDeskList[0].Id,
                    'Entity__c'=>'cardx',
                    'Entity_Serno__c'=> '524858519',
                    'MAH_Serno__c' => '4599',
                    'Card_Serno__c'=>'524858519', 
                    'CreatedById'=>UserInfo.getUserId()
                    });
            
            List<Task> taskList = new List<Task>();
            taskList.add(taskObj);
            taskList.add(taskObj1);
            taskList.add(taskObj2);
            taskList.add(taskObj3);
            List<String>  taskAttr = new List<String> {'Savedesk__c','CreatedById'};
                List<Task> taskReload = UnitTestDataGenerator.TestTask.reloadList(taskList,taskAttr);
            taskObj.Comments__c = 'Test';
            TaskTriggerHandler.deleteTaskRecord(taskList);
            
            update taskObj;
            system.assertEquals(2, sdObj.Task_Count__c);
            delete taskObj;
            TaskTriggerHandler.updateTaskCount(taskList);
            TaskTriggerHandler.updateSernos(taskList);
            Test.stopTest(); 
        }
    }
}