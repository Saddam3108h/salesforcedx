@isTest
global class ViewInsuranceWebServiceMockclass implements WebServiceMock 
{
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) 
        {
        
           SOAViewInsuranceResponse.ViewInsurancesResponse_element response_element = new SOAViewInsuranceResponse.ViewInsurancesResponse_element();
           
           response_element.ViewInsurancesResponseRecord = new SOAViewInsuranceResponse.ViewInsurancesResponseRecordType();
           SOAViewInsuranceResponse.ViewInsurancesResponseRecordType respType =new SOAViewInsuranceResponse.ViewInsurancesResponseRecordType();
           respType.SubscribedInsuranceList =new SOAViewInsuranceResponse.SubscribedInsurance();
           respType.SubscribeToInsuranceList = new SOAViewInsuranceResponse.SubscribeToInsurance();
           
           SOAViewInsuranceResponse.SubscribeToInsurance subscribeToInstance =new SOAViewInsuranceResponse.SubscribeToInsurance();
           SOAViewInsuranceResponse.SubscribedInsurance subscribedInsurance =new SOAViewInsuranceResponse.SubscribedInsurance();
           subscribeToInstance.SubscribeToInsurance = new List<SOAViewInsuranceResponse.ViewInsurancesResponseBody>();
           subscribedInsurance.SubscribedInsurance = new List<SOAViewInsuranceResponse.ViewInsurancesResponseBody>();
           
           
           SOAViewInsuranceResponse.ViewInsurancesResponseBody respbody = new SOAViewInsuranceResponse.ViewInsurancesResponseBody();
           respbody.InsuranceCode='f01';
           respbody.Description='Test RememberInsurance';
           respbody.LogAction='ppi1';
           subscribedInsurance.SubscribedInsurance.add(respbody);
           
           SOAViewInsuranceResponse.ViewInsurancesResponseBody respbody1 = new SOAViewInsuranceResponse.ViewInsurancesResponseBody();
           respbody1.InsuranceCode='f15';
           respbody1.Description='Test RememberMoreInsurance';
           respbody1.LogAction='ppi1';
           subscribedInsurance.SubscribedInsurance.add(respbody1);
           SOAViewInsuranceResponse.ViewInsurancesResponseBody respbody2 = new SOAViewInsuranceResponse.ViewInsurancesResponseBody();
           respbody2.InsuranceCode='ppi1';
           respbody2.Description='Test PPI Insurance';
           respbody2.LogAction='ppi1';
           subscribedInsurance.SubscribedInsurance.add(respbody2);
           SOAViewInsuranceResponse.ViewInsurancesResponseBody respbody3 = new SOAViewInsuranceResponse.ViewInsurancesResponseBody();
           respbody3.InsuranceCode='InsuranceCode';
           respbody3.Description='Description';
           respbody3.LogAction='LogAction';
           subscribeToInstance.SubscribeToInsurance.add(respbody3);
            SOAViewInsuranceResponse.ViewInsurancesResponseBody respbody4 = new SOAViewInsuranceResponse.ViewInsurancesResponseBody();
           respbody4.InsuranceCode='f1g';
           respbody4.Description='f1g';
           respbody4.LogAction='f1g';
           subscribeToInstance.SubscribeToInsurance.add(respbody4);
           
           SOAViewInsuranceResponse.ViewInsurancesResponseBody respbody5 = new SOAViewInsuranceResponse.ViewInsurancesResponseBody();
           respbody5.InsuranceCode='f2g';
           respbody5.Description='f2g';
           respbody5.LogAction='f2g';
           subscribeToInstance.SubscribeToInsurance.add(respbody5);
           
           SOAViewInsuranceResponse.ViewInsurancesResponseBody respbody6 = new SOAViewInsuranceResponse.ViewInsurancesResponseBody();
           respbody6.InsuranceCode='f3g';
           respbody6.Description='f3g';
           respbody6.LogAction='f3g';
           subscribeToInstance.SubscribeToInsurance.add(respbody6);
           
           SOAViewInsuranceResponse.ViewInsurancesResponseBody respbody7 = new SOAViewInsuranceResponse.ViewInsurancesResponseBody();
           respbody7.InsuranceCode='f4g';
           respbody7.Description='f4g';
           respbody7.LogAction='f4g';
           subscribeToInstance.SubscribeToInsurance.add(respbody7);
           respType.SubscribedInsuranceList=subscribedInsurance;
           respType.SubscribeToInsuranceList=subscribeToInstance;
           response_element.ViewInsurancesResponseRecord=respType;
           response.put('response_x', response_element);
           
        }
}