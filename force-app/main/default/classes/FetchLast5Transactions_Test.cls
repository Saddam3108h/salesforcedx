/********************************************************************************************
Name: FetchLast5Transactions_Test
=============================================================================================
Purpose: This test class is covering 'FetchLast5Transaction' Apex Class
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                     Date             Created for Jira     Detail
1.0		    Kakasaaheb Ekshinge 	  17-Nov-2020		SFD-1840		     Intial version
********************************************************************************************/
@isTest
public class FetchLast5Transactions_Test {
    
   /* public static testMethod void fetchFinAccountNumberMethod(){
        System.runAs(UnitTestDataGenerator.adminUser){  
            TriggerSettings__c triggerObj = UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account acc = UnitTestDataGenerator.TestAccount.buildInsert(); 
            Contact testContact = UnitTestDataGenerator.TestContact.buildInsert(); 
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert();
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c' => acc.id,
                    'Customer_Serno__c' => testContact.Id,
                    'Product__c' => prodobj.Id,
                    'Product_Serno__c' => prodobj.Pserno__c
                    });
            FetchLast5Transactions.fetchFinAccountNumber(finAccObj.id);
            FetchLast5Transactions.fetchFinAccountNumber(prodobj.id);
            
            ManageRESTServices__c manageRest = new ManageRESTServices__c();
            manageRest.Name = 'Last5Transactions';
            manageRest.CertificateName__c =  Label.CertificateName;
            manageRest.Method__c = 'POST';
            manageRest.Endpoint__c = Label.last5TranEndpoint;
            manageRest.ContentType__c = 'application/json';
            insert manageRest;
            
            FetchLast5Transactions.fetchTransactionList('1111111111111','1111111111111111');
            Date fromDate = System.today() - 300;
            Date todate = System.today();
            FetchLast5Transactions.fetchTxnBetDate('1111111111111','1111111111111111',fromDate,todate);
        }
    }*/
    
    public static testMethod void fetchFinAccountNumberMethod_Sucess(){
        System.runAs(UnitTestDataGenerator.adminUser){   
             TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
            Account acc = UnitTestDataGenerator.TestAccount.buildInsert(); 
            Contact testContact = UnitTestDataGenerator.TestContact.buildInsert(); 
            Product_Custom__c prodobj = UnitTestDataGenerator.TestProductCustom.buildInsert();
            Financial_Account__c finAccObj = UnitTestDataGenerator.TestFinancialAccount.buildInsert(new Map<String, Object>{
                'Customer__c' => acc.id,
                    'Customer_Serno__c' => testContact.Id,
                    'Product__c' => prodobj.Id,
                    'Product_Serno__c' => prodobj.Pserno__c
                    });
            FetchLast5Transactions.fetchFinAccountNumber(finAccObj.id);
            FetchLast5Transactions.fetchFinAccountNumber(prodobj.id);
            ManageRESTServices__c manageRest = new ManageRESTServices__c();
            manageRest.Name = 'Last5Transactions';
            manageRest.CertificateName__c =  Label.CertificateName;
            manageRest.Method__c = 'POST';
            manageRest.Endpoint__c = Label.last5TranEndpoint;
            manageRest.ContentType__c = 'application/json';
            insert manageRest;
            
            Date fromDate = System.today() - 300;
            Date todate = System.today();
            
            Test.setMock(HttpCalloutMock.class, new CurityCalloutMock());   // ExampleCalloutMock
            Test.StartTest(); 
            FetchLast5Transactions.fetchTransactionList('1111111111111','1111111111111111');
            Test.StopTest(); 
            
            //Test.setMock(HttpCalloutMock.class, new CurityCalloutMock());   // ExampleCalloutMock
            //Test.StartTest(); 
            FetchLast5Transactions.fetchTxnBetDate('1111111111111','1111111111111111',fromDate,todate);
            //Test.StopTest(); 
            
            
        }
    }
    
    
}