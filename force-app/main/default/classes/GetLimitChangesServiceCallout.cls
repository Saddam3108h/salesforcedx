public class GetLimitChangesServiceCallout extends SoapIO{
   
    SOAPCSSServiceRequest.getLimitChangesRequestRecordType LimitChangesRequestRecordInstance;
    GetLimitChangesServiceInput input;
    
    public  void convertInputToRequest(){
    
        input = (GetLimitChangesServiceInput)serviceInput;
        LimitChangesRequestRecordInstance = new SOAPCSSServiceRequest.getLimitChangesRequestRecordType();
        LimitChangesRequestRecordInstance.AccountNo=input.AccountNo;
        LimitChangesRequestRecordInstance.From_x=input.From_x;
        
    }
    
    public  override object invokeWebserviceCallout(SoapRequest soapRequest){
        
        SOAPschemasEntercardComTypelib10.headerType HeadInstance=new SOAPschemasEntercardComTypelib10.headerType();
        HeadInstance.MsgId='SFDC consuming get limit changes SOA service. Time Stamp=>'+system.now();
        HeadInstance.CorrelationId=Common_Settings__c.getValues('CorrelationId').Common_Value__c;
        HeadInstance.RequestorId=Common_Settings__c.getValues('RequestorId').Common_Value__c;
        HeadInstance.SystemId=Common_Settings__c.getValues('SystemId').Common_Value__c;
        HeadInstance.InstitutionId=string.valueof(input.InstitutionId);//'2';
        SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT invokeInstance= new SOAPCSSServiceInvoke.x_xsoap_CSSServiceESB_CSSServicePT();
        invokeInstance.timeout_x=30000;
         // Added 12/13/2016
        ServiceSettings__c serviceObj = ServiceSettings__c.getValues('GetLimitChanges');
        if(serviceObj!=null && serviceObj.EndPoint__c!='' && serviceObj.EndPoint__c!=null)
        invokeInstance.endpoint_x = serviceObj.EndPoint__c;
        // Added 12/13/2016
        //String username = 'evry_access';
        //String password = '9oKuwQioQ4';
        
        String username = serviceObj.Username__c; //'css_soa';
        String password = serviceObj.Password__c;//'P1%gruzA';
        
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
        invokeInstance.inputHttpHeaders_x = new Map<String, String>();
        invokeInstance.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken creds=new SOAPdocsOasisOpenOrgWss200401Oasis20.UsernameToken();
        creds.Username=serviceObj.Username__c; //'css_soa';//'evry_access';
        creds.Password=serviceObj.Password__c;//'P1%gruzA';//'9oKuwQioQ4';
        SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element security_ele=new SOAPdocsOasisOpenOrgWss200401Oasis20.Security_element();
        security_ele.UsernameToken=creds;
        invokeInstance.SecurityPart=security_ele;
        SOAPCSSServiceResponse.GetLimitChangesResponse_element GetLimitChangesResponse_elementInstance;
         GetLimitChangesResponse_elementInstance =  invokeInstance.getLimitChanges(HeadInstance,LimitChangesRequestRecordInstance);
        system.debug('GetLimitChangesResponse_elementInstance -->'+GetLimitChangesResponse_elementInstance);
        return GetLimitChangesResponse_elementInstance;
     }  
        
     public override object convertResponseToOutput(object response, map<string, string> responseHeader, ServiceStatus serviceStatus){
       if(response==null)
           return null;
       
       SOAPCSSServiceResponse.GetLimitChangesResponse_element LimitChangesResponse_elementInstance = (SOAPCSSServiceResponse.GetLimitChangesResponse_element)response;
       SOAPCSSServiceResponse.getLimitChangesResponseRecordType  LimitChangesResponseRecordInstance=LimitChangesResponse_elementInstance.GetLimitChangesResponseRecord;
       SOAPCSSServiceResponse.limitChangeType[] limitChangeTypeInstance=LimitChangesResponseRecordInstance.LimitChange;
       
       system.debug('***LimitChangesResponse_elementInstance***'+LimitChangesResponse_elementInstance);
       system.debug('***LimitChangesResponseRecordInstance***'+LimitChangesResponseRecordInstance);
       /*SOAPschemasEntercardComTypelib10.headerType hType=LimitChangesResponse_elementInstance.Header;
       GetLimitChangesServiceOutput.HeaderType htinstance =new GetLimitChangesServiceOutput.HeaderType();
       htinstance.MsgId=hType.MsgId;
       htinstance.CorrelationId=hType.CorrelationId;
       htinstance.RequestorId=hType.RequestorId;
       htinstance.SystemId=hType.SystemId;
       htinstance.InstitutionId=hType.InstitutionId;*/
       
       GetLimitChangesServiceOutput.GetLimitChangesResponse LimitChangesResponseInstance = new GetLimitChangesServiceOutput.GetLimitChangesResponse();
       LimitChangesResponseInstance.AccountNo=LimitChangesResponseRecordInstance.AccountNo;
       LimitChangesResponseInstance.From_x=LimitChangesResponseRecordInstance.From_x;
       
       if(LimitChangesResponseRecordInstance.LimitChange != null)
       {
           LimitChangesResponseInstance.limitChange =new List<GetLimitChangesServiceOutput.LimitChange>();
           for(SOAPCSSServiceResponse.limitChangeType li: LimitChangesResponseRecordInstance.LimitChange)
           {
               GetLimitChangesServiceOutput.LimitChange lim = new GetLimitChangesServiceOutput.LimitChange();
               lim.Date_x=li.Date_x;
               lim.OldLimit=li.OldLimit;
               lim.NewLimit=li.NewLimit;
               LimitChangesResponseInstance.limitChange.add(lim);
           }
           
       }
       
       system.debug('***LimitChangesResponseInstance***'+LimitChangesResponseInstance);
       
       GetLimitChangesServiceOutput LimitChangesOutput=new GetLimitChangesServiceOutput();
        
       //LimitChangesOutput.HT=htinstance;
       LimitChangesOutput.GLCR =LimitChangesResponseInstance;
      
       return LimitChangesOutput;
    }        
}