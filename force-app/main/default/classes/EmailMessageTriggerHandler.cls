/********************************************************************************************
Name: EmailMessageTriggerHandler
=============================================================================================
Purpose: This class is extracting the SSN from the case and link the customer to case - SFD-1261
============================================================================================= 
History
---------------------------------------------------------------------------------------------        
Version     Author                      Date             Detail
1.0         Harisivaramakrishna         02/06/2020      Initial Version
2.0         Priyanka Singh              12-Aug-2020     Trigger Factory Implementation
3.0         Harisivaramakrishna         18-Sep-2020     Added the fund transfer functionality
*********************************************************************************************/
public class EmailMessageTriggerHandler implements ITriggerHandler {
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {
        
        if (TriggerSettings__c.getInstance('Execution').EmailMessageExecution__c 
            &&  TriggerSettings__c.getInstance('Execution').OrgExecution__c)
        { 
            return true;
        }
        else
        { 
            return TriggerDisabled;
        }
        
    }
    
    public void BeforeInsert(List<SObject> newItems) {
        updateCaseParent(newItems);                
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, EmailMessage> newAddressItems = (Map<Id, EmailMessage>) newItems; 
        updateCaseParent(newAddressItems.values());    
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {
        
    }
    
    public void AfterInsert(Map<Id, SObject> newItems) {
        Map<Id, EmailMessage> newAddressItems = (Map<Id, EmailMessage>) newItems; 
         PopulateCaseToAddressSSN(newAddressItems.values(),'true');//adding due date check User Story SFD-1947
        updateSSNFromCaseDescription(newAddressItems.values());
        updateFAOnCaseByAppId(newAddressItems.values());// adding App Id  User story : 1772
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, EmailMessage> newAddressItems = (Map<Id, EmailMessage>) newItems; 
        PopulateCaseToAddressSSN(newAddressItems.values(),'false'); // Due date get increase after case details update hence commenting the code.
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    
    /**********************************************************************************************
* @Author:        
* @Date: 02/06/2020      
* @Description: This class is used to get application id from email and tagg to particular FA,customer,Mah
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/ 
    public void updateFAOnCaseByAppId(List<EmailMessage> listEmailMes){
        Set<ID> caseSet = new Set<ID>();
        Map<Id,String> caseIdByAppIdMap = new Map<Id,String>();
        Map<String,Financial_Account__c> appFAMap = new Map<String,Financial_Account__c>(); 
        Map<Id,Id> FAPCustMap = new Map<Id,Id>();// FAId,Customer Id
        Map<Id,Id> FAMAHMap = new Map<Id,Id>();// FAid,MAH Id
        Map<Id,Id> MAHCustIdPMap = new Map<Id,Id>();// stores Primary customer er MAH
        List<Case> lstToUpdate = new List<case>();
        String appId ='';
        try {
            for (EmailMessage so : listEmailMes) {
                
                if(so.ParentId!=NULL && so.ParentId.getSObjectType() == Case.sObjectType && so.Incoming &&  String.isNotEmpty(so.Subject)){
                    caseSet.add(so.parentid); 
                }
            }
            
            If(caseSet.size()>0) {
                    Map<Id,case> caseMAP = new Map<Id,case>([SELECT id, Description, To_Address__c, SuppliedEmail ,
                                                             Subject FROM Case 
                                                             WHERE id in:caseSet ]); 
            
            
            if(caseMAP.size() > 0){                    
                for(Id csObj:caseMAP.keySet()){ 
                    case cs = caseMAP.get(csObj);
                    if( String.isNotBlank(cs.Subject) 
                       && cs.Subject.toLowerCase().contains(Label.ApplicationId)
                       && (String.isNotBlank(cs.To_Address__c) 
                           && cs.To_Address__c.contains(Label.Soknad_Email_address))
                      )
                    {  // checking if Subject contains App Id  : Vår ref:                         
                        String[] st1 = cs.Subject.toLowerCase().split(Label.ApplicationId);
                        if(st1.size()>=2){
                            String[] str1 = st1[1].trim().split(' ');
                            appId = str1[0].replaceAll('[^a-zA-Z0-9\\s+]', ''); 
                        }                  
                    }
                    else if( String.isNotBlank(cs.Description)                            
                            && (String.isNotBlank(cs.To_Address__c) 
                                && cs.To_Address__c.contains(Label.Soknad_Email_address))
                           ){
                               // checking if Description contains <App Id>XXXX</AppId> 
                               
                               appId = cs.Description.replaceAll('[^a-zA-Z0-9\\s+]', '').toLowerCase().substringBetween('appid', 'appid'); // need to change contains
                           }
                    if(String.isNotBlank(appId)&& appId != null){
                        appid = appId.replaceAll( '\\s+', '');  
                        caseIdByAppIdMap.put(cs.id, appId);
                    }
                }
                
            }
            system.debug('@@--caseIdByAppIdMap'+caseIdByAppIdMap);
            if(caseIdByAppIdMap.size() > 0){
                system.debug('@@--caseIdByAppIdMap.values()'+caseIdByAppIdMap.values());
                for(Financial_Account__c fin: [SELECT Id,Customer__c,ApplicationId__c 
                                               FROM Financial_Account__c 
                                               WHERE ApplicationId__c IN : caseIdByAppIdMap.values()]){
                    
                    appFAMap.put(fin.ApplicationId__c,fin); 
                    FAMAHMap.put(fin.id,fin.Customer__c); // MAH
                }                   
            }
            system.debug('@@--appFAMap'+appFAMap);
            system.debug('@@--FAMAHMap'+FAMAHMap);
            // checking Primary Card Holder from MAH
            if(FAMAHMap.size()>0){
                
                // getting MAH primary card Holder
                for(Account accObj :[Select Id,name ,(select id,Firstname,LastName from Contacts order by createddate ASC) from Account where Id IN : FAMAHMap.values()])
                {
                    
                    List<Contact> lstContact = accObj.Contacts;                    
                    if(lstContact.size()>0){
                        Contact con = lstContact[0];                       
                        if((accObj.Name).contains(con.Firstname) && (accObj.Name).contains(con.LastName)){
                            MAHCustIdPMap.put(accObj.id,con.id);
                             
                        }                      
                    }
                }
            }
            
            if(caseIdByAppIdMap.size()>0){
                for(Id Id:caseIdByAppIdMap.keySet() ){// case lookup
                    appId =  caseIdByAppIdMap.get(id);                      
                    case caseObj = caseMAP.get(Id);
                    if(appFAMap.containskey(appId)){
                        caseObj.Financial_Account__c = appFAMap.get(appId).id; 
                        caseObj.AccountId = appFAMap.get(appId).Customer__c;
                        caseObj.ContactId = MAHCustIdPMap.get(appFAMap.get(appId).Customer__c);
                    }
                    
                    lstToUpdate.add(caseObj);
                } 
            }
            
           }  
            if(lstToUpdate.size()>0){               
                update lstToUpdate;                
            }
            
            
        }catch(exception ex){
            StatusLogHelper.logSalesforceError('EmailMessageTriggerHandler', 'updateFAOnCaseByAppId', 'Error in Appid linking to the Case', ex, false);
        }
        
    }    
    
    /**********************************************************************************************
* @Author:        
* @Date: 02/06/2020      
* @Description: This class is extracting the SSN from the case and link the customer to case
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description]   
***********************************************************************************************/ 
    public void updateSSNFromCaseDescription(List<EmailMessage> listEmailMes){         
        
        String[] caseDescription;
        String[] descAfterSSN;
        String[] descAfterAcc;
        Set<ID> caseSet = new Set<ID>();
        String decAcc;
        Financial_Account__c[] lstfinacc;
        Set<String> encryptAccNumberSet = new Set<String>();
        Map<String, Id> ssnConIdMap = new Map<String, Id>();
        Map<String,Case> CaseIDSSNMap = new Map<String,Case>();  
        List<Case> updateCaseList = new List<Case>();
        Map<Id, String> emailMap = new Map<Id, String>{};
            try {
                for (EmailMessage so : listEmailMes) {
                    if(so.ParentId!=NULL && so.ParentId.getSObjectType() == Case.sObjectType && so.Incoming){
                        caseSet.add(so.parentid);
                        emailMap.put(so.ParentId, so.TextBody);
                    }
                }
                Map<Id,case> caseMAP = new Map<Id,case>([SELECT id, Description, To_Address__c, SuppliedEmail FROM Case WHERE id in:caseSet]); 
                if(caseMAP.size() > 0){
                    for(Case cs:caseMAP.values()){ 
                        if( String.isNotBlank(cs.Description) 
                           && ((String.isNotBlank(cs.To_Address__c) && cs.To_Address__c.contains(Label.SwedBank_EmailId)) 
                               || (String.isNotBlank(cs.SuppliedEmail) && cs.SuppliedEmail.contains(Label.FundTransfer_EmailId)))
                          ){
                              
                              cs.Description = cs.Description.toLowerCase().replaceAll(' ', '');                          
                              
                              // Account number capturing
                              If(cs.Description.contains('kontonummer:')){
                                  descAfterAcc=cs.Description.split('kontonummer:'); 
                                  if(descAfterAcc.size() > 0){
                                      decAcc = (descAfterAcc[1].Length()>=16 ? descAfterAcc[1].SubString(0,16) : '');
                                      Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(decAcc));
                                      String encryptAccNumber=EncodingUtil.convertToHex(hash);
                                      if(String.isNotBlank(encryptAccNumber)){
                                          encryptAccNumberSet.add(encryptAccNumber);
                                      }
                                  }
                              } 
                              if(cs.Description.contains('personnummer:')){
                                  caseDescription=cs.Description.split('personnummer:');
                                  String descAfter = caseDescription[1].replace(' ','');
                                  descAfter = descAfter.replace('\n',' ').replace('\r',' ');
                                  descAfterSSN = descAfter.split(' ');
                                  String decSSN = descAfterSSN[0].replace('-', '').replace(' ','');
                                  if(caseDescription.size() > 1){
                                      if (decSSN.length() == 10)
                                      {
                                          /*lastTenDigits = decSSN; //result;*/
                                          System.debug('10 Digit SSN==> '+decSSN);
                                          //String ssn = decSSN.replace('\n','').replace('\r','');
                                          Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(decSSN));
                                          String temp =EncodingUtil.convertToHex(hash);
                                          CaseIDSSNMap.put(temp, cs);
                                          
                                      }
                                      else if (decSSN.length() > 10)
                                      {  
                                          //lastTenDigits = decSSN.substring(decSSN.length() - 10);
                                          System.debug('10 Digit SSN==> '+decSSN.substring(decSSN.length() - 10));
                                          //String ssn = lastTenDigits.replace('\n','').replace('\r','').replaceAll( '\\s+', '');
                                          Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(decSSN.substring(decSSN.length() - 10)));
                                          String temp =EncodingUtil.convertToHex(hash);   
                                          CaseIDSSNMap.put(temp, cs);
                                          
                                      }
                                  }
                              }  
                              
                          }
                    }
                    if(CaseIDSSNMap.size() > 0){
                        if(encryptAccNumberSet.size() > 0){
                            lstfinacc = [SELECT Id FROM Financial_Account__c WHERE Encrypted_Account_Number__c IN : encryptAccNumberSet LIMIT 1];
                        }
                        for(Contact con : [SELECT Id, SSN__c,Encrypted_SSN__c FROM Contact WHERE Encrypted_SSN__c IN: CaseIDSSNMap.keySet()]){
                            case csObj = new Case();
                            if(CaseIDSSNMap.containsKey(con.Encrypted_SSN__c)){
                                csObj.Id = CaseIDSSNMap.get(con.Encrypted_SSN__c).ID;
                                csObj.ContactId = con.id;
                                if(lstfinacc != null && lstfinacc.size() > 0){
                                    csObj.Financial_Account__c = lstfinacc[0].Id;
                                }
                                updateCaseList.add(csObj);
                            }
                        }
                    }
                    if(updateCaseList.size() > 0) {
                        Update updateCaseList; 
                    }
                }
            }catch(DMLException exp) {  
                StatusLogHelper.logSalesforceError('EmailMessageTriggerHandler', 'updateSSNFromCaseDescription', 'Error in SSN linking to the Case', exp, false);
            } 
    }
    /**********************************************************************************************
* @Author:        
* @Date: 05/08/2020      
* @Description: This class is set the case parent id and throw the error message whiel sending 
the email if the case status is not inprogress.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description] 
05/08/2020 Hari
***********************************************************************************************/
    public void updateCaseParent(List<EmailMessage> listEmails){
        set<string> caseIdSet =new set<string>();
        for(EmailMessage em:listEmails)
        {
            if(em.ParentId!=null)
                caseIdSet.add(em.ParentId);
        }
        system.debug('***caseIdSet***'+caseIdSet);
        
        Map<Id,Case> caseMap = new Map<Id,Case>();
        
        for(Case cs:[select Id,status,casenumber from case where Id In:caseIdSet limit 50000])
        {
            caseMap.put(cs.Id,cs);
        }
        
        system.debug('***caseMap***'+caseMap);
        
        for(EmailMessage em:listEmails)
        {
            system.debug('***em.Incoming***'+em.Incoming);
            system.debug('***em.ParentId***'+em.ParentId);
            
            Case caseObj =caseMap!=null?caseMap.get(em.ParentId): null;
            
            if(!em.Incoming && caseObj!=null && caseObj.status!='In Progress')
            {
                em.adderror('Cannot send email when case status is not In Progress'); 
            }
        }
    }
    /**********************************************************************************************
* @Author:        
* @Date: 05/08/2020      
* @Description: This class is handling all the incoming & outgoing emails, prepopulate the fields 
values and assign the cases to proper queues & List views.
* @Revision(s): [Date] - [Change Reference] - [Changed By] - [Description] 
05/08/2020 Hari
***********************************************************************************************/
    public void populateCaseToAddressSSN(List<EmailMessage> listEmails, string changeDueDate){
        try{
            Id emmId = listEmails[0].id;
            boolean successfullOwnerUpdate = false;
            string ssn;
            string countryCode;
            String VBCByPass = Label.ByPass_Email_for_VBC;
            List<String> VBCEmails = new List<String>();
            if(String.isNotEmpty(VBCByPass)){
                VBCEmails = VBCByPass.split(',');
            }
            EmailMessage em = [select Id,FromAddress,Incoming,Parentid,MessageDate,ToAddress,HtmlBody,TextBody,
                               Headers from EmailMessage where id =:emmId 
                               and (Not ToAddress like :VBCEmails)];
            //ToAddress !=: Label.ByPass_Email_for_VBC];
            Case casetoUpdate = [select Id,To_Address__c,ContactId,createdDate,
                                 Secured__c,Due_Date__c,Department__c,Sub_Origin__c,Subject,
                                 Country__c,SuppliedEmail,status,Case_View__c,Category__c,
                                 OwnerId,Owner.Name from Case where Id =:em.Parentid]; //Hari add Category__C, OwnerId and Owner.Name
            system.debug(casetoUpdate.Due_Date__c+'*** Case to Update Due Date ***');
            if(em.Incoming){  
                if(casetoUpdate.To_Address__c==null)      
                    casetoUpdate.To_Address__c = em.ToAddress;
                if(casetoUpdate.status=='Closed')
                    casetoUpdate.status='Re Open';            
                
                //Closing the Fund Transfer Cases
                if(casetoUpdate.status=='New' 
                   && casetoUpdate.SuppliedEmail.contains(Label.FundTransfer_EmailId) 
                   && casetoUpdate.Subject != null 
                   && casetoUpdate.Subject.startsWith('Ny överföring')){
                       casetoUpdate.status='Closed';
                   }
                
                // Handling of 'Re: 'logic
                string casesubject;
                if(casetoUpdate.Subject != null && casetoUpdate.Subject.startsWithIgnoreCase('RE: ')){
                    casesubject = casetoUpdate.Subject.removeStartIgnoreCase('RE: ');
                }else{
                    casesubject = casetoUpdate.Subject;
                }
                
                if(casesubject!=null && casesubject.indexOf(':')>-1)//some subject is coming as Ændre Forfaldsdato: YYYY-MM-DD HH:MM:SS. extracting upto :
                    casesubject = casesubject.substring(0,casesubject.indexOf(':')+1);
                
                // Added null check for casesubject to avoid null pointer expection => SFD-1977
                if( casesubject!=null && casesubject.contains(':')) {
                    casesubject = casesubject.removeEnd(':');
                }
                
                casetoUpdate.Origin='Email';
                //Assign Case to proper queue
                if((casetoUpdate.To_Address__c==null 
                    || casetoUpdate.To_Address__c=='') 
                   && em.Headers!=null 
                   && em.Headers.indexOfIgnoreCase('Return-Path: ')>-1)// for toaddress change 19NOV2017,SPAL
                {
                    integer starIndex;
                    integer endIndex;
                    starIndex=em.Headers.indexOfIgnoreCase('Return-Path: ')+('Return-Path: '.length());
                    endIndex=em.Headers.indexOfIgnoreCase('@entercard.com',starIndex)+('@entercard.com'.length());
                    
                    if(endIndex>-1 && starIndex<endIndex){
                        
                        string toemailid=em.Headers.substring(starIndex,endIndex);
                        casetoUpdate.To_Address__c=((toemailid!=null && toemailid.endswithignorecase('@entercard.com'))?toemailid:em.ToAddress);
                        
                    }
                }// for toaddress change 19NOV2017,SPAL
                
                String toAddressLike = '%'+casetoUpdate.To_Address__c+'%';//modified for SIT demo NETBANK
                String casesubjectLike = '%'+casesubject;
                EmailToCaseSettings__c[] ec = [SELECT Id,Queue__c,Secured__c,Department__c,Email_Source__c,
                                               sla__c,Country_Code__c,Auto_Response_Email_Template__c,
                                               Is_Auto_Response_Required__c,Category__c,Case_View__c 
                                               FROM EmailToCaseSettings__c 
                                               WHERE (To_Email__c=: casetoUpdate.To_Address__c 
                                                      AND Subject__c=null) 
                                               OR (To_Email__c like :toAddressLike 
                                                   AND Subject__c like :casesubject)];                
                
                if(ec.size()==0){
                    EmailToCaseSettings__c[] ecfull = [SELECT Id,Queue__c,Secured__c,Department__c,
                                                       Email_Source__c,sla__c,Country_Code__c,Auto_Response_Email_Template__c,
                                                       Is_Auto_Response_Required__c,Category__c,To_Email__c,Subject__c,Case_View__c 
                                                       FROM EmailToCaseSettings__c];
                    for(EmailToCaseSettings__c e : ecfull)
                        if(String.isNotBlank(em.ToAddress)
                           && em.ToAddress.contains(e.To_Email__c) 
                           && (casesubject!=null && casesubject.equals(e.Subject__c))
                          ) {
                              ec = new List<EmailToCaseSettings__c>{e}; 
                                  break;
                          }
                    if(ec.size()==0){//still custom settings not found
                        for(EmailToCaseSettings__c e : ecfull)
                            if(em.ToAddress != null && em.ToAddress.contains(e.To_Email__c)){
                                ec = new List<EmailToCaseSettings__c>{e}; 
                                    break;
                            }
                    }
                }
                if(casetoUpdate.To_Address__c!=null && casetoUpdate.To_Address__c.length()>255 && ec.size()>0)
                    casetoUpdate.To_Address__c = ec[0].To_Email__c;
                //SSN population part :Start
                //for kantega
                if(em.ToAddress != null 
                   && em.ToAddress.startswith('forsikringer')
                  ) {                    
                      string strtextbody = em.textbody;
                      string strtextbody1= strtextbody.substring(strtextbody.indexof('Personnummer:')+13).trim();
                      string strtextbody2 = strtextbody1.substring(0,strtextbody1.indexOf('\n'));
                      
                      Integer instId;
                      if(Country_Codes__c.getValues('no')!=null)
                          instId = (Integer)Country_Codes__c.getValues('no').Institution_Id__c;
                      Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(strtextbody2));
                      string encryptSSN=EncodingUtil.convertToHex(hash);       
                      Contact[] con = [select id from contact where Encrypted_SSN__c = :encryptSSN and Institution_Id__c=:instId]; 
                      if(con.size()>0)
                          casetoUpdate.ContactId = con[0].id;
                  }
                
                if((ec.size()==0 
                    && em.FromAddress.indexOf('_')>-1) 
                   || (ec.size()>0 
                       && (ec[0].Email_Source__c=='Netbank' || ec[0].Email_Source__c=='Mobile Apps') 
                       && em.FromAddress.indexOf('_')>-1)
                  ) {
                      ssn = em.FromAddress.substring(0,em.FromAddress.indexOf('_'));
                      countryCode = em.FromAddress.substring(em.FromAddress.indexOf('@')-2,em.FromAddress.indexOf('@'));
                      Integer instId;
                      if(Country_Codes__c.getValues(countryCode)!=null)
                          instId = (Integer)Country_Codes__c.getValues(countryCode).Institution_Id__c;
                      Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(ssn));
                      string encryptSSN=EncodingUtil.convertToHex(hash);                           
                      Contact[] con = [select id from contact where Encrypted_SSN__c = :encryptSSN and Institution_Id__c=:instId];       
                      if(ssn!=null && con.size()>0)
                          casetoUpdate.ContactId = con[0].id;
                  }    
                //SSN population part :End
                if(ec.size()>0)
                {
                    string queueName =  ec[0].Queue__c;
                    
                    Group[] grpqueue = [select Id from Group where Name = :queueName and Type = 'Queue'];
                    
                    if(grpqueue.size()>0){
                        casetoUpdate.OwnerId = grpqueue[0].Id;
                        casetoUpdate.Is_Owner_Assigned_Through_Trigger__c=true;
                    }
                    casetoUpdate.Secured__c = ec[0].Secured__c;
                    casetoUpdate.Department__c = ec[0].Department__c;
                    casetoUpdate.Sub_Origin__c = ec[0].Email_Source__c; 
                    casetoUpdate.Country__c = ec[0].Country_Code__c;
                    
                    if(ec[0].Category__c!=null)
                    {
                        casetoUpdate.Category__c = ec[0].Category__c; //Added by Hari for SFD-647
                    }
                    if (casetoUpdate.Case_View__c==null) // Updated by Hari for SFD-647 - removed != and kept == 
                    {
                        casetoUpdate.Case_View__c = ec[0].Case_View__c;                 
                    }
                    if(ec[0].sla__c!=null && changeDueDate=='true' )  //Added check for SFD-1947
                    {
                        casetoUpdate.Due_Date__c = system.now().addhours(Integer.valueOf(ec[0].sla__c));
                    }
                    else{
                        //casetoUpdate.Due_Date__c = null; due date get updated with null hence commented the code.
                    }
                    
                    if((ec[0].Email_Source__c=='Netbank' 
                        || ec[0].Email_Source__c=='Mobile Apps') && ec[0].Category__c!=null)
                        casetoUpdate.Category__c = ec[0].Category__c;
                    if(ec[0].Email_Source__c=='Mobile Apps'){
                        string mailbody2;
                        try{
                            em.htmlbody=em.htmlbody.replaceAll( '\\s+', '');
                            string mailbody=em.htmlbody.substring(0,em.htmlbody.indexOf('</td></tr>'));
                            string mailbody1=mailbody.substring(mailbody.lastindexof('<b>')+'<b>'.length(),mailbody.lastindexof('</b>')); 
                            mailbody2=mailbody1.replace(' ','');     
                            Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(mailbody2));
                            string encryptAccNumber=EncodingUtil.convertToHex(hash);                                        
                            Financial_Account__c[] lstfinacc = [SELECT Id,Account_Number__c,(SELECT Id,People__c FROM Cards__r 
                                                                                             WHERE PrimaryCardFlag__c=true LIMIT 1) 
                                                                FROM Financial_Account__c 
                                                                WHERE Encrypted_Account_Number__c=:encryptAccNumber LIMIT 1];
                            if(lstfinacc.size()>0 && lstfinacc[0].Cards__r.size()>0)
                                casetoUpdate.Contactid = lstfinacc[0].Cards__r[0].People__c; 
                            casetoUpdate.Financial_Account__c = lstfinacc[0].Id; 
                            casetoUpdate.Card__c = lstfinacc[0].Cards__r[0].Id;                           
                        }catch(Exception e){
                            //StatusLogHelper.logSalesforceError('PopulateCaseToAddressSSNTrigger', 'E001', 'PopulateCaseToAddressSSNTrigger: ' + 'Account Number extraction with FA->'+mailbody2, e, true);
                        }
                    }     
                    successfullOwnerUpdate = true;
                }
                Database.DMLOptions dmo = new Database.DMLOptions();
                if(!successfullOwnerUpdate)
                    dmo.assignmentRuleHeader.useDefaultRule= true;
                else
                    dmo.assignmentRuleHeader.useDefaultRule= false;
                dmo.EmailHeader.triggerAutoResponseEmail = true;
                casetoUpdate.setoptions(dmo);
                update casetoUpdate;
                /*if(ec.size()>0 
                   && ec[0].Is_Auto_Response_Required__c 
                   && ec[0].Auto_Response_Email_Template__c!=null
                  ){
                      User usr = [select id,Email from User where Profile.Name='System Administrator' and isActive=true limit 1];
                      Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                      List<EmailTemplate> lstemailTemplate = [select id from EmailTemplate where Name=:ec[0].Auto_Response_Email_Template__c];
                      if(lstemailTemplate.size()>0){
                          msg.setTemplateId( lstemailTemplate[0].id );
                          msg.setWhatId(casetoUpdate.Id);
                          msg.setTargetObjectId(usr.id);
                          msg.setToAddresses(new List<String>{casetoUpdate.SuppliedEmail});
                          Savepoint sp = Database.setSavepoint();
                          Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{msg}); // Dummy email send
                          Database.rollback(sp); // Email will not send as it is rolled Back
                          Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
                          emailToSend.setToAddresses(msg.getToAddresses());
                          emailToSend.setPlainTextBody(msg.getPlainTextBody());
                          emailToSend.setHTMLBody(msg.getHTMLBody());
                          emailToSend.setSubject(msg.getSubject());
                          emailToSend.setSaveAsActivity(true);
                          Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{emailToSend});
                      }
                  }*/
            } else {
                if(casetoUpdate.Department__c != 'Fraud and Chargeback')
                { 
                    casetoUpdate.status='Closed'; 
                }
                update casetoUpdate;
            }           
        }
        catch(Exception e)
        {
            // StatusLogHelper.logSalesforceError('PopulateCaseToAddressSSNTrigger', 'E001', 'PopulateCaseToAddressSSNTrigger: ' + 'Exception occured Case Assignment', e, true);
        }
    }
}