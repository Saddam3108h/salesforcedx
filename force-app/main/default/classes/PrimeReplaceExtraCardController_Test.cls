@isTest 
private class PrimeReplaceExtraCardController_Test{
    static testMethod void testPrimeReplaceExtraCardActions() {
      
         System.runAs(UnitTestDataGenerator.adminUser){
          TriggerSettings__c triggerObj=UnitTestDataGenerator.TestTriggerSettings.buildInsert(new Map<String, Object>{      
            });
        
        
      Product_Custom__c objProd = new Product_Custom__c();
      objProd.Pserno__c = '60';
      objProd.Product_Description__c = 'Test Desc';
      objProd.Name = 'Test Prod';
      
      insert objProd;
      
      Account accountObj=new Account(
      Name='Test Account', 
      Institution_Id__c=2, 
      Customer_Serno__c='1123123'
      );
      insert accountObj;
      
      Contact contactObj =new Contact(
      LastName='1Test Contact', 
      FirstName='1First Test Contact',
      SerNo__c='12123123', 
      Institution_Id__c=2, 
      SSN__c='0000000061',
      AccountId=accountObj.Id,
      Phone='1123123123', 
      Fax='198789879', 
      MobilePhone='198789879', 
      HomePhone='1123123123', 
      Email='1testemail@test.com'
      );
      insert contactObj;
      
      Financial_Account__c finAccObj=new Financial_Account__c(
      Account_Number__c='Test5700192', 
      Customer__c=accountObj.Id, 
      Account_Serno__c='3123123', 
      Institution_Id__c=2,
      Customer_Serno__c = '1123123',
      Product_Serno__c = '60',
      Product__c = objProd.id
      );
      insert finAccObj;
      
      Card__c card =new Card__c (
      Card_Serno__c='111', 
      Institution_Id__c=2,
      People__c=contactObj.Id,
      Financial_Account__c=finAccObj.Id,
      Card_Number_Truncated__c = '561616****156166',
      Financial_Account_Serno__c = '3123123',
      People_Serno__c = '2123123',
      Prod_Serno__c = '60',
      Product__c = objProd.id
      );
      insert card ;
      
      Case caseObj=new Case(
      ContactId=contactObj.Id, 
      AccountId=accountObj.Id, 
      Financial_Account__c=finAccObj.Id, 
      Category__c='Account closure', 
      Origin='Web',
      card__c=card.id 
      
      );
      insert caseObj;
      
        
      
      PrimeActionController primeActionCon = new PrimeActionController();
      
      Test.setCurrentPageReference(new PageReference('Page.PrimeActionWizard')); 
      System.currentPageReference().getParameters().put('Id', caseObj.Id);
      
      primeActionCon.primeActionConInstance = new PrimeActionController();
      primeActionCon.caseObj=caseObj;
      primeActionCon.caseId=caseObj.Id;
      primeActionCon.getfinancialAccounts();
      primeActionCon.getcardNumber();
      primeActionCon.getItems();
      primeActionCon.getParameterValue('Id');
      primeActionCon.oldFnStatus='CLSC';
      primeActionCon.productSernum='1';
      primeActionCon.productSerno='1';
      primeActionCon.primeValue ='Manage Replace Card requests';
      primeActionCon.getprimeActions();
      
      // replaceCardWrapper
      primeActionCon.getcallgetCustomer();
      
      primeActionCon.replaceCardWrapper = new PrimeActionController.replaceCardWrapper();
      primeActionCon.replaceCardWrapper.blockdatevalue='testdate';
      primeActionCon.replaceCard();
      
      primeActionCon.replaceCardWrapper.blockdatevalue='2016-11-27';
      primeActionCon.replaceCard();
      primeActionCon.productSer();

      
      primeActionCon.replaceCardWrapper.blockdatevalue='2016-11-28';
      primeActionCon.replaceCardWrapper.cardNO='111';
      primeActionCon.replaceCardWrapper.newstatus='NEW';
      primeActionCon.replaceCardWrapper.oldstatus='CLSC';
      primeActionCon.replaceCardWrapper.lostfee=TRUE;
      primeActionCon.replaceCardWrapper.productSerno='1';
      primeActionCon.replaceCardWrapper.institutionId=2;
      primeActionCon.productSer();
      primeActionCon.replaceCard();

      primeActionCon.extraCardWrapper= new PrimeActionController.extraCardWrapper();
      primeActionCon.extraCardWrapper.ProductSerno='111';
      primeActionCon.extraCardWrapper.financialVal='';
      primeActionCon.extraCardWrapper.SSN='';
      primeActionCon.productSerforFA();
      primeActionCon.extraCard();
      
      primeActionCon.extraCardWrapper.financialVal='Test5700192';
      primeActionCon.extraCardWrapper.ProductSerno='111';
      primeActionCon.extraCardWrapper.SSN='16';
      primeActionCon.extraCardWrapper.FirstName='F123';
      primeActionCon.extraCardWrapper.LastName='L123';
      primeActionCon.extraCardWrapper.Gender='F';
      primeActionCon.extraCardWrapper.EmbossingName='FL1034';
      primeActionCon.extraCardWrapper.phoneNo='';
      primeActionCon.extraCardWrapper.emailId='';
      primeActionCon.extraCardWrapper.institutionId=2;
      primeActionCon.productSerforFA();
      primeActionCon.extraCard();
      
      
         }     
      
    }
}