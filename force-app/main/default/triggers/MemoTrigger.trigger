/**********************************************************************
Name: MemoTrigger
=======================================================================
Purpose: This Task trigger handler class is created on Email Message logics.User Story -SFD-1363

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         03-June-2020       Initial Version
2.0			Priyanka Singh		   12-Aug -2020		  Trigger Factory Implementation
TriggerToUpdateMemoRelatedTo
**********************************************************************/
trigger MemoTrigger on Memo__c (before insert, after insert, after update)
{
    MemoTriggerHandler objUpdateMemo = new MemoTriggerHandler();
    if(Trigger.isBefore)
    {
        Id Uid = userinfo.getUserId();
        List<User> objUser = new List<User>();
        objUser = [Select Name from User where id=: Uid limit 1];
        //if(objUser[0].Name == 'SOA Integration User')//BY SPAL 27MAR2017
        //{
        if(Trigger.IsInsert)
        {
            objUpdateMemo.UpdateMemoRelatedTo(trigger.new,null);
        }
        //}
        /*if(Trigger.IsUpdate)
	{
		objUpdateMemo.UpdateMemoRelatedTo(trigger.newMap,trigger.oldMap);
	}*/
    }
    // Added by saddam on 15-Mar-2018 [SFD-452]
    if(MemoTriggerHandler.inFutureContext){
        if(Trigger.isAfter){
            Id Uid = userinfo.getUserId();
            List<User> objUser = new List<User>();
            objUser = [Select Name from User where id=: Uid limit 1];
            system.debug('------objuser-----'+objUser[0].name);
            if(objUser[0].name != 'SOA Integration User'){
                MemoTriggerHandler.inFutureContext = false;
                MemoTriggerHandler.insertmemo(Trigger.new[0].id);
            }
        }
    }
    //[END- SFD-452]
}