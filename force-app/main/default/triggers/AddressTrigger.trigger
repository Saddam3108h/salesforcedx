/**********************************************************************
Name: AddressTrigger
=======================================================================
Purpose: This is triger on Address

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author               Date              Detail
1.0         HSR Krishna          29-07-2020	       Initial Version
2.0			Priyanka Singh		 11-Aug-2020	   Trigger Factory Implementation	
**********************************************************************/
trigger AddressTrigger on Address__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    TriggerDispatcher.Run(new AddressTriggerHandler());
}