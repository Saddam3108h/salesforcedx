/**********************************************************************
Name: User
=======================================================================
Purpose: This Apex Trigger is used for execute the UserTriggerHandler methods.  
Auto Assign(F&C Roles)/Remove(other than F&C Roles) DCMS Permission Set to users based on user Role.

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         20-July-2020       Initial Version
2.0			Priyanka Singh		   10-Aug-2020	     Trigger Factory Implementation	
**********************************************************************/
trigger UserTrigger on User (before insert, before update, before delete, after insert, after update, after delete, after undelete)
{
   TriggerDispatcher.Run(new UserTriggerHandler());  
}