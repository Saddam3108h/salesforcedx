/**********************************************************************
Name: CardTrigger
=======================================================================
Purpose: This Apex Trigger is used for execute the CardTriggerHandler methods.  User Story Marketing Cloud Stories

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author           Date              Detail
1.0         Saddam           09-10-2020       Initial Version

**********************************************************************/
trigger CardTrigger on Card__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatcher.Run(new CardTriggerHandler());  
}