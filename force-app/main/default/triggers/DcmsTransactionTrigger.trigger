/**********************************************************************
Name: DcmsTransactionTrigger
=======================================================================
Purpose: This Apex Trigger is used for execute the DcmsTransactionTriggerHandler methods.  User Story -SFD-984

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Srinivas D           05-Feb-2019       Initial Version
2.0         Priyanka Singh       10-Aug-2020         Trigger Factory Implementation 
**********************************************************************/
trigger DcmsTransactionTrigger on DCMS_Transaction__c (before insert, before update, before delete, after insert, after update, 
                                                       after delete, after undelete) {
   TriggerDispatcher.Run(new DcmsTransactionTriggerHandler()); 
}