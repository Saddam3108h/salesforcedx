/**********************************************************************
Name: FundsTransfer__c
=======================================================================
Purpose: This Apex Trigger is used for show error based on condition.  
======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         30-July-2020       Initial Version
2.0			Kakasaheb Ekshinge	   11-AUG-2020		  Updated Trigger framework - SFD-1646
**********************************************************************/
trigger FundsTransferTrigger on FundsTransfer__c (before update, before delete) {   
   
    TriggerDispatcher.Run(new FundsTransferTriggerHandler()); 
}