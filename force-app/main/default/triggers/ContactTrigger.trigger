/**********************************************************************
Name: ContactTrigger
=======================================================================
Purpose: This Apex Trigger is used for execute the ContactTriggerHandler methods.  User Story -SFD-1430

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         20-May-2020       Initial Version
2.0         Priyanka Singh         05-Aug-2020       second Version

**********************************************************************/
trigger ContactTrigger on Contact (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    TriggerDispatcher.Run(new ContactTriggerHandler()); 
}