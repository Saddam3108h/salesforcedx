/**********************************************************************
Name: FinancialAccountTrigger
=======================================================================
Purpose: This Apex Trigger is used for execute the FinancialAccountTriggerHandler methods.  User Story -SFD-1602

======================================================================= 
History
-----------------------------------------------------------------------         
Version     Author                   Date              Detail
1.0         Saddam Hussain         29-July-2020       Initial Version
2.0			Priyanka Singh		   10-Aug-2020	     Trigger Factory Implementation	

**********************************************************************/
trigger FinancialAccountTrigger on Financial_Account__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    
    TriggerDispatcher.Run(new FinancialAccountTriggerHandler());  
}