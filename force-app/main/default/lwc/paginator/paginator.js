import {
    LightningElement,
    api,
    track
} from 'lwc';

const DELAY = 300;
const recordsPerPage = [5, 10, 25, 50, 100];
const pageNumber = 1;
const showIt = 'visibility:visible';
const hideIt = 'visibility:hidden'; //visibility keeps the component space, but display:none doesn't
export default class Paginator extends LightningElement {
    @api showSearchBox = false; //Show/hide search box; valid values are true/false
    @api showPagination; //Show/hide pagination; valid values are true/false
    @api pageSizeOptions = recordsPerPage; //Page size options; valid values are array of integers
    @api totalRecords; //Total no.of records; valid type is Integer
    @api records; //All records available in the data table; valid type is Array 
    @track pageSize; //No.of records to be displayed per page
    @track totalPages; //Total no.of pages
    @track totalRecountCount = 0; //total record count received from all retrieved records
    @track pageNumber = pageNumber; //Page number
    @track searchKey; //Search Input
    @track controlPagination = showIt;
    @track controlPrevious = hideIt; //Controls the visibility of Previous page button
    @track controlNext = showIt; //Controls the visibility of Next page button
    recordsToDisplay = []; //Records to be displayed on the page
    @track searchRecords = []; //store search records.

    //Called after the component finishes inserting to DOM
    connectedCallback() {
        if (this.pageSizeOptions && this.pageSizeOptions.length > 0)
            this.pageSize = this.pageSizeOptions[0];
        else {
            this.pageSize = this.totalRecords;
            this.showPagination = false;
        }
        this.controlPagination = this.showPagination === false ? hideIt : showIt;

        this.setRecordsToDisplay();
    }

    handleRecordsPerPage(event) {
        this.pageSize = event.target.value;
        if (this.searchRecords.length > 0) {
            this.serchResult();
        } else {
            this.setRecordsToDisplay();
        }
    }
    handlePageNumberChange(event) {
        console.log('---keycode---' + event.keyCode);
        if (event.keyCode === 13) {
            this.pageNumber = event.target.value;
            if (this.searchRecords.length > 0) {
                this.serchResult();
            } else {
                this.setRecordsToDisplay();
            }
        }
    }
    previousPage() {
        this.pageNumber = this.pageNumber - 1;
        this.setRecordsToDisplay();
    }
    nextPage() {
        this.pageNumber = this.pageNumber + 1;
        if (this.searchRecords.length > 0) {
            this.serchResult();
        } else {
            this.setRecordsToDisplay();
        }
    }

    @api
    handleRefreshTask(result) {
        this.records = result;
        this.setRecordsToDisplay();
    }

    setRecordsToDisplay() {
        this.recordsToDisplay = [];
        this.totalRecountCount = this.totalRecords;
        if (!this.pageSize)
            this.pageSize = this.totalRecords;

        this.totalPages = Math.ceil(this.totalRecords / this.pageSize);

        this.setPaginationControls();

        console.log('--pageNumber--' + this.pageNumber);
        console.log('--pageSize--' + this.pageSize);
        console.log('--formula--' + (this.pageNumber - 1) * this.pageSize);
        console.log('--formula-105-' + this.pageNumber * this.pageSize);
        for (let i = (this.pageNumber - 1) * this.pageSize; i < this.pageNumber * this.pageSize; i++) {
            console.log('--i value---' + i);
            console.log('-- totalRecords---' + this.totalRecords);
            if (i === this.totalRecords) break;
            this.recordsToDisplay.push(this.records[i]);
        }
        console.log('--recordsToDisplay--' + JSON.stringify(this.recordsToDisplay));
        this.dispatchEvent(new CustomEvent('paginatorchange', {
            detail: this.recordsToDisplay
        })); //Send records to display on table to the parent component
    }

    setPaginationControls() {
        //Control Pre/Next buttons visibility by Total pages
        if (this.totalPages === 1) {
            this.controlPrevious = hideIt;
            this.controlNext = hideIt;
        } else if (this.totalPages > 1) {
            this.controlPrevious = showIt;
            this.controlNext = showIt;
        }
        //Control Pre/Next buttons visibility by Page number
        if (this.pageNumber <= 1) {
            this.pageNumber = 1;
            this.controlPrevious = hideIt;
        } else if (this.pageNumber >= this.totalPages) {
            this.pageNumber = this.totalPages;
            this.controlNext = hideIt;
        }
        //Control Pre/Next buttons visibility by Pagination visibility
        if (this.controlPagination === hideIt) {
            this.controlPrevious = hideIt;
            this.controlNext = hideIt;
        }
    }

    handleKeyChange(event) {
        window.clearTimeout(this.delayTimeout);
        const searchKey = event.target.value;
        if (searchKey) {
            this.delayTimeout = setTimeout(() => {
                this.searchKey = searchKey;
                //Use other field name here in place of 'Name' field if you want to search by other field
                //this.recordsToDisplay = this.records.filter(rec => rec.includes(searchKey));
                //Search with any column value (Updated as per the feedback)
                // this.recordsToDisplay = this.records.filter(rec => JSON.stringify(rec).includes(searchKey));
                this.recordsToDisplay = this.records.filter(rec => JSON.stringify(rec).toLowerCase().includes(searchKey.toLowerCase()));
                this.serchResult();
                console.log('---recordCount---' + this.recordsToDisplay.length);
            }, DELAY);
        } else {
            this.controlPagination = showIt;
            this.setRecordsToDisplay();
        }
    }

    serchResult() {
        if (Array.isArray(this.recordsToDisplay) && this.recordsToDisplay.length > 0) {
            this.totalRecountCount = this.recordsToDisplay.length;
            if (!this.pageSize)
                this.pageSize = this.totalRecords;
            this.totalPages = Math.ceil(this.totalRecountCount / this.pageSize);
            this.setPaginationControls();
            this.searchRecords = [];
            for (let i = (this.pageNumber - 1) * this.pageSize; i < this.pageNumber * this.pageSize; i++) {
                if (i === this.totalRecords) break;
                this.searchRecords.push(this.recordsToDisplay[i]);
            }
            console.log('--recordsToDisplay-145-' + this.searchRecords.length);
            console.log('--recordsToDisplay--' + JSON.stringify(this.searchRecords));
            this.dispatchEvent(new CustomEvent('paginatorchange', {
                detail: this.searchRecords
            }));
        }
    }
}