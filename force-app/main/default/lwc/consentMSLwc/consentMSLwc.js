import { LightningElement,track,wire,api } from 'lwc';
import sendCalloutAPex from '@salesforce/apex/ConsentMSLwcController.sendCallout';
import validateUserAccTkn from '@salesforce/apex/RESTCalloutUtil.validateUserAccTkn';
import getConsentList from '@salesforce/apex/ConsentMSLwcController.getConsentList';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import {
    getRecord
} from 'lightning/uiRecordApi';

import USER_ID from '@salesforce/user/Id';
import FED_FIELD from '@salesforce/schema/User.FederationIdentifier';
import NOT_HAVING_FEDID from '@salesforce/label/c.NotHavingFedId';
import SELECT_CONSENT from '@salesforce/label/c.selectConsent';


export default class ConsentMSLwc extends LightningElement {

     // reactive variables
     //userId = Id;
     messageFed  = NOT_HAVING_FEDID;
     selectConsent = SELECT_CONSENT;
     @api recordId;
     @track data = [];
     @track error;
     @track bShowModal = false;
     selectedCons = [];
     selectedConsOld = [];
     accessToken;
     checkBoxValue;
     consList;
     @track preSelectedRows;
     mapConsent = [];
     @track isLoading = false;

     // opening the modal
     openModal() { this.bShowModal = true; }
     // closeing the modal
     closeModal() { this.bShowModal = false;}
   
     
     // Getting Contacts using Wire Service
     @wire(getConsentList,{faId: "$recordId"})
     contacts(result) {
         if (result.data) {
             this.data = result.data;
             this.error = undefined;
          
 
         } else if (result.error) {
             this.error = result.error;
             this.data = undefined;
         }
     }
     
    @track fedId;

    @wire(getRecord, {
        recordId: USER_ID,
        fields: [FED_FIELD]
    }) wireuser({
        error,
        data
    }) {
        if (error) {
           this.error = error ; 
        } else if (data) {
            this.fedId = data.fields.FederationIdentifier.value;
            console.log('Test fedId ==>'+this.fedId);
        }
    }

     captureConsents() { 

       // alert(JSON.stringify(this.mapConsent));
        if(this.fedId === null){
            this.showNotificationError(this.messageFed);
        }
       else if(this.mapConsent.length > 0){
            this.sendCallout(this.mapConsent); 
        }else {
            this.showNotificationError(this.selectConsent);
           // alert('Please select consents');
        }
        this.mapConsent = [];
     }

     
     handleChange(event) {

        const editBtnId = event.target.dataset.id;
        console.log('Value =='+event.target.checked);
        //let row = event.detail.row;
        this.mapConsent.push({value:event.target.checked, key:editBtnId});
        
    }

     sendCallout(consentId){
        this.isLoading = true;
        var data =  JSON.stringify(consentId);
        let jsonParamData ={};
        jsonParamData.accessToken = this.accessToken;
        jsonParamData.loopdata = consentId;
        sendCalloutAPex({ jsonParamData: JSON.stringify(jsonParamData)})
        .then((result) => { 
           //alert(result);
           this.isLoading = false; 
           var messageResponjse = result;
           if(messageResponjse.startsWith('All consents')) {
            this.showNotification(messageResponjse);
           }
            else if(messageResponjse.startsWith('The following')) {
            this.showNotificationError(messageResponjse);
           }  else if(messageResponjse.startsWith('The Consent')) {
            this.showNotification(messageResponjse);
           } 


        })
        .catch((error) => {
            this.error = error;
            this.isLoading = false; 
            console.log('Between To date issue: '+error);               
        });
    }

        callValidateAccessToken(){ 
            validateUserAccTkn()
            .then(result => {
                this.accessToken = result;
                
            })
            .catch(error => {
                console.log('@- Error validateUserAccTkn Response-----'+JSON.stringify(error));
                console.log('---Exception Type-----'+JSON.stringify(error.exceptionType));             
            });
        } 

        showNotification(customMes) {
            const evt = new ShowToastEvent({ 
                title: 'Consents Status',
                message: customMes,
                variant: 'success'
                
            });
            this.dispatchEvent(evt);
        }

        showNotificationError(customMes) {
            const evt = new ShowToastEvent({ 
                title: 'Consents Status',
                message: customMes,
                variant: 'Error'
               
            });
            this.dispatchEvent(evt);
        }

    connectedCallback(){
        this.callValidateAccessToken(); 
       // this.captureOldConsents();
       
    } 

}