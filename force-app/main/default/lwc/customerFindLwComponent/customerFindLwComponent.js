import { LightningElement, track, wire} from 'lwc';
import {
    NavigationMixin
} from "lightning/navigation";
import {
    ShowToastEvent
} from "lightning/platformShowToastEvent";
import {
    getRecord
    } from "lightning/uiRecordApi";
import USER_ID from "@salesforce/user/Id";
import USERDEPARTMENT from "@salesforce/schema/User.User_Department__c";
import NOSSNLabel from '@salesforce/label/c.No_SSN_msg';
import NOCUSTOMERLabel from '@salesforce/label/c.No_Customer_msg';
import NONELabel from '@salesforce/label/c.None';
import CATEGORYLabel from '@salesforce/label/c.Category_not_select_msg';
import redirctcustomer from "@salesforce/apex/LogActivityController.redirctcustomer";
import updateActivity from "@salesforce/apex/LogActivityController.updateActivity";
import fetchCategories from "@salesforce/apex/LogActivityController.fetchCategories";
export default class CustomerFindLwComponent extends NavigationMixin (LightningElement)
{
    @track sSn;
    @track errorMessage;
    @track flag = false;
    @track category;
    @track department = '';
    @track type;
    @track comments;
    @track categoryOptions = [];
    @track userDepartment = '';

    handleSsnChange(event)
    {
        console.log('----code---' + event.key);
        if (event.key === "Enter")
        {
            this.sSn = this.template.querySelector('lightning-input').value;;
            console.log('----Ssn---' + this.sSn);
            if (this.sSn != "")
            {
                redirctcustomer({
                    sSn: this.sSn
                })
                    .then((result) =>
                    { 
                        console.log("--result----" + JSON.stringify(result));
                        if (result != null)
                        {
                            this[NavigationMixin.Navigate]({
                                    type: "standard__recordPage",
                                    attributes: {
                                        recordId: result,
                                        objectApiName: 'Contact', // objectApiName is optional
                                        actionName: 'view'
                                    }
                                },
                                true
                            );
                            this.sSn = "";
                        } else
                        {
                            this.flag = true;                        
                            this.errorMessage = NOCUSTOMERLabel;                                             
                        }
                    }) 
            } else
            {               
                this.sSn = "";           
                this.errorMessage = NOSSNLabel;
                this.flag = false;            
            }  
        }        
    }

    @wire(getRecord, {
        recordId: USER_ID,
        fields: [USERDEPARTMENT]
    })
    wireuser({
        error,
        data
    }) {
        if (error) {
            this.error = error;
        } else if (data) {
			this.userDepartment = data.fields.User_Department__c.value;
            console.log('---user--dep--'+this.userDepartment);
        }
    }

    handleDepartmentChange(event)
    {
        this.userDepartment = event.target.value;
    }

     @wire(fetchCategories, { departmentVal: '$userDepartment'})
     wiredCategories({ error, data }) {
        if (data)
        {
            console.log('----data----'+JSON.stringify(data));
            this.categoryOptions = [];
            for(let i=0; i<data.length; i++) {
                console.log('id=' + data[i].Id);
                this.categoryOptions = [...this.categoryOptions ,{value: data[i].Category__c , label: data[i].Category__c}];                                   
            }                
            this.error = undefined;
        } else if (error) {
            this.error = error;
        }
    }

     get categoryOptions() {
        console.log(this.categoryOptions);
        return this.categoryOptions;
    }

    handleCategoryChange(event)
    {
        this.category = event.target.value;
    }
    handleCommentChange(event)
    {
        this.comments = event.target.value;
    }

    handleUpdateActivity()
    {
        console.log('---category---'+this.category);
        if (this.category === null ||
            this.category === undefined ||
            this.category === NONELabel) {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: "Error!",
                    message: CATEGORYLabel,
                    variant: "error"
                })
            );
            return null;
           // eval("$A.get('e.force:refreshView').fire();");
        }         
        if (this.comments != "" || this.category != "")
        {
            updateActivity({
                categoryVal: this.category,
                typeVal: this.type,
                commentVal: this.comments
            })
                .then((result) =>
                { 
                    if (result.includes("success"))
                    {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: "Success!",
                                message: result,
                                variant: "Success"
                            })
                        );
                        var close = true;
                        const closeclickedevt = new CustomEvent('closeclicked', {
                         detail: { close },
                        });

                        // Fire the custom event
                        this.dispatchEvent(closeclickedevt);
                        //eval("$A.get('e.force:refreshView').fire();");
                    }
                })
        }
    }
}