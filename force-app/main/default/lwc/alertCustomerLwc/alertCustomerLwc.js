import { LightningElement,track,api } from 'lwc';
import validHighRiskAlert from '@salesforce/apex/AlertController.validHighRiskAlert';
export default class AlertCustomerLwc extends LightningElement {

@api recordId;
@track isHighRiskCust = false;


validateHighRiskCustomner(recordId){
   validHighRiskAlert({ recordId: recordId })
        .then((result) => {           
            console.log('Test===>'+result);
            this.isHighRiskCust = result; 
        })
        .catch((error) => {          
            this.error = error;   
        });
     
    } 
    
connectedCallback(){
    this.validateHighRiskCustomner(this.recordId);
}   

}