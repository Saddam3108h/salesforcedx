import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';
import pubsub from 'c/pubSub';
import taskListActivity from '@salesforce/apex/ViewActivityHistoryController.getTaskData';
import {
    refreshApex
} from '@salesforce/apex';

const columns = [{
        label: "Subject",
        fieldName: "recordLink",
        type: "url",
        typeAttributes: {
            label: {
                fieldName: "Subject"
            },
            tooltip: "Subject",
            target: "_self"
        }
    },
    {
        label: 'Comments',
        fieldName: 'Description',
        type: 'Text'
    },

    {
        label: 'Create Date',
        fieldName: 'CreatedDate',
        type: 'date',
        typeAttributes: {
            day: 'numeric',
            month: 'numeric',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit'
        }
    },
    {
        label: 'Created By',
        fieldName: 'AgentName',
        type: 'Text'
    }
];

export default class ActivityHistoryLwc extends LightningElement {
    @api recordId;
    @track columns = columns;
    @track activityList = []; //All tasks available for data table    
    @track showTable = false; //Used to render table after we get the data from apex controller    
    @track recordsToDisplay = []; //Records to be displayed on the page
    @track rowNumberOffset; //Row number
    @track wiredTaskList = [];

    @wire(taskListActivity, {
        customerId: '$recordId'
    }) taskList(
        result
    ) {
        this.wiredTaskList = result;
        if (result.data) {
            let taskData = [];
            for (let i = 0; i < result.data.length; i++) {
                let tasks = Object.assign({}, result.data[i]); //cloning object 
                tasks.recordLink = '/' + result.data[i].Id;
                tasks.AgentName = tasks.CreatedBy.Name;
                tasks.rowNumber = (i + 1);
                tasks = Object.assign(tasks, result.data[i]);
                taskData.push(tasks);
            }
            this.showTable = true;
            this.activityList = taskData;
            if (this.recordsToDisplay.length > 0) {
                this.template.querySelector('c-paginator').handleRefreshTask(this.activityList);
            }
        } else if (result.error) {
            this.activityList = [];
        }
    }
    handlePaginatorChange(event) {
        this.recordsToDisplay = [];
        this.recordsToDisplay = event.detail;
        console.log('---records-89--' + this.recordsToDisplay.length);
        if (this.recordsToDisplay.length > 0)
            this.rowNumberOffset = this.recordsToDisplay[0].rowNumber - 1;
    }

    connectedCallback() {
        this.regiser();
    }

    regiser() {
        window.console.log('event registered ');
        pubsub.register('refreshActivity', this.refresh.bind(this));
    }

    refresh() {
        return refreshApex(this.wiredTaskList);
    }
}