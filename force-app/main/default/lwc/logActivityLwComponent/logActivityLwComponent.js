import { LightningElement,
    api,
    wire,
    track } from 'lwc';
import {
    ShowToastEvent
    } from "lightning/platformShowToastEvent";
import {
    getRecord,
    getFieldValue,
    updateRecord
    } from "lightning/uiRecordApi";
import {
    getPicklistValues,
    getObjectInfo
    } from "lightning/uiObjectInfoApi";
import {
    NavigationMixin
    } from "lightning/navigation";
/*Importing custom labels*/
import CATEGORYLabel from '@salesforce/label/c.Category_not_select_msg';
import FINANCIALACCOUNTLabel from '@salesforce/label/c.Financial_Value';
import ACCOUNT_CLOSURELabel from '@salesforce/label/c.Account_Closure';
import REASONLabel from '@salesforce/label/c.Account_Closure_Reason';
import OFFERLabel from '@salesforce/label/c.Offer';
import RETAINEDLabel from '@salesforce/label/c.Saved';
import SAVEATTEMPTLabel from '@salesforce/label/c.Save_Attempt';
import PAYMENT_HOLIDAYLabel from '@salesforce/label/c.Payment_Holiday';
import CARDREPLACELabel from '@salesforce/label/c.Card_Replace';
import PINLabel from '@salesforce/label/c.PIN';
import CLICLDLabel from '@salesforce/label/c.CLI_CLD';
import CASEUPDATELabel from '@salesforce/label/c.Case_update_message';
import OTHERLabel from '@salesforce/label/c.Other';
import NONELabel from '@salesforce/label/c.None';
import DUEDATELabel from '@salesforce/label/c.Due_Date';
import ALERTCATEGORYLabel from '@salesforce/label/c.Alert_Category';
import ACTIVITYNOTAVAILLabel from '@salesforce/label/c.Activity_not_available_msg';
import CUSTOMERNOTAVAILLabel from '@salesforce/label/c.Change_Customer_Name_Message';
import CASEOBJLabel from '@salesforce/label/c.Case_Object';
import CONTACTOBJLabel from '@salesforce/label/c.Contact_Object';
import DUEDATEVALIDLabel from '@salesforce/label/c.Due_Date_Validation';
import CROSSELL_RESPONSELabel from '@salesforce/label/c.CrossSell_Response';
import SERVICE_SOLDLabel from '@salesforce/label/c.Service_Sold';
import COUNTRYLabel from '@salesforce/label/c.Country';
import CROSSSELL_SUCCESSLabel from '@salesforce/label/c.Cross_Sell_Success';
import LIVECHATOBJ from '@salesforce/label/c.LiveChatTranscript_Object';
import LIVECHATMESSAGE from '@salesforce/label/c.Chat_Update_Message';
import pubsub from "c/pubSub";
import USER_ID from "@salesforce/user/Id";
import FEDERATION_ID from "@salesforce/schema/User.FederationIdentifier";
import USERDEPARTMENT from "@salesforce/schema/User.User_Department__c";	
import ALERT_OBJECT from "@salesforce/schema/Alert__c";
import TARGET from "@salesforce/schema/Alert__c.Target__c";
import ALERT_CATEGORY from "@salesforce/schema/Alert__c.Category__c";
import RETENTION_OBJECT from "@salesforce/schema/Retention_Response__c";
import SAVEATTEMPT from "@salesforce/schema/Retention_Response__c.Save_Attempt__c";
import ACCOUNTRETAINED from "@salesforce/schema/Retention_Response__c.Account_Retained__c";
import OFFER from "@salesforce/schema/Retention_Response__c.Offer__c";
import ACCOUNTCLOSURE from "@salesforce/schema/Retention_Response__c.Reason_for_Account_Closure__c";
/* Below fields from Case object*/
import CASE_OBJECT from "@salesforce/schema/Case";
import CASEID from "@salesforce/schema/Case.Id";
import CONTACTID from "@salesforce/schema/Case.ContactId";
import FINACCOUNTID from "@salesforce/schema/Case.Financial_Account__c";
import NONEXISTSFA from "@salesforce/schema/Case.NonExistingFA__c";
import CARDID from "@salesforce/schema/Case.Card__c";
import CATEGORY from "@salesforce/schema/Case.Category__c";
import TYPE from "@salesforce/schema/Case.Type";
import DEPARTMENT from "@salesforce/schema/Case.Department__c";
/* Below methods from LogActivityController*/  
import getFinancialAccounts from "@salesforce/apex/LogActivityController.getFinancialAccounts";
import getCardsRecs from "@salesforce/apex/LogActivityController.getCards";
import updateTask from "@salesforce/apex/LogActivityController.updateTask";
import doUpdateCase from "@salesforce/apex/LogActivityController.doCaseUpdate";
import fetchChatData from "@salesforce/apex/LogActivityController.fetchChatData";  
import updateChat from "@salesforce/apex/LogActivityController.updateChat"; 
import fetchCategories from "@salesforce/apex/LogActivityController.fetchCategories";

    const fields = [CONTACTID, FINACCOUNTID, CARDID, CATEGORY, TYPE, NONEXISTSFA, DEPARTMENT];
export default class LogActivityLwComponent extends LightningElement
{
     @api objectApiName;
        @api recordId;
        @track financialData = [];
        @track cardData = [];
        @track contactId = "";
        @track customerId = "";
        @track buttonVisible = false;
        @track fieldsVisible = false;
        @track chekboxValue = false;
        @track finvalue = "";
        @track cardvalue = "";
        @track categoryValue = "";
        @track catgryval = "";
        @track comment = "";
        @track typeValue = "";
        @track federationId;
        @track errorMessage;
		
		@track categoryOptions = [];
		@track agentDepartment = '';
		@track selectDepartment = '';
		@track userDepartment = '';

    @wire(getRecord, {
        recordId: "$recordId",
        fields
    })
    caseData;
        
        //below function to display contactId
    get customerName()
    {
        if (this.objectApiName == CONTACTOBJLabel && this.customerId == "")
        {
            this.buttonVisible = true;            
            this.contactId = this.recordId;
            console.log('---contactd--103-' + this.customerId);
        
        }
        if (this.objectApiName == CASEOBJLabel && this.customerId == "")
        {
            this.contactId = getFieldValue(this.caseData.data, CONTACTID);
        }
        if (this.objectApiName == LIVECHATOBJ && this.customerId == "")
        {
                fetchChatData({
                        chatRecordId: this.recordId
            })
            .then((result) =>
            { 
                this.contactId = result
                console.log("--result----" + this.contactId);
                
            })  
        }
        
        return this.contactId;
    }

    handleNameChange(event)
    {
        this.customerId = event.target.value;  
        console.log('---contactd--121-' + this.customerId);
        this.contactId = this.customerId;
        console.log('---contactd--127-' + this.contactId);
        return this.contactId;
    }

    @wire(getFinancialAccounts, {
        customerId: "$contactId"
    })
    wiredFinRecs(
        {data, error}){
        if (data)
        {
            this.financialData = [];
            console.log("--fin-data----" + JSON.stringify(data));
            for (let key in data)
            {
                if (data.hasOwnProperty(key))
                {
                    this.financialData = [...this.financialData, { value: key, label: data[key] }];
                }
                this.finAccountId = getFieldValue(
                    this.caseData.data,
                    FINACCOUNTID
                );
                console.log("--fin-data--1--" + this.finAccountId);
                if (this.finAccountId != null && this.finAccountId != undefined)
                {
                    this.finvalue = this.finAccountId;
                } else
                {
                    this.finvalue = this.financialData[0].value;
                } 
            }    
        } else if(error)
        {
            this.error = error;
        }
    }
        
    get finOptions()
    {
        return this.financialData;
    }
    
    @wire(getCardsRecs, {
        financialId: "$finvalue",
        customerId: "$contactId"
    })
    CardRecs({
        error,
        data
    })
    {
        if (data)
        {
            this.cardData = [];
            // eslint-disable-next-line no-console
            //console.log("--card-data----" + JSON.stringify(data));
            for (let key in data)
            {
                if (data.hasOwnProperty(key))
                {
                    this.cardData = [...this.cardData, { value: key, label: data[key] }];              
                }
                this.cardId = getFieldValue(this.caseData.data, CARDID);
                if (this.cardId != null)
                {
                    console.log('---cardId---'+this.cardId);
                    this.cardvalue = this.cardId;
                    if (this.cardvalue != this.cardData[0].value)
                    {
                        this.cardvalue = this.cardData[0].value;
                    }
                } else
                {                    
                    this.cardvalue = this.cardData[0].value;
                }
            }
            this.error = undefined;
        } else if (error)
        {
            this.error = error;
        }
    }    

    get cardOptions()
    {
        return this.cardData;
    }        
        
    handleFinAccountChange(event) {
        this.finvalue = "";
        const selectedfin = event.detail.value;
        // eslint-disable-next-line no-console
        console.log("selected value=" + selectedfin);
        this.finvalue = selectedfin;
        return this.finvalue;
    }
            
    handleCardChange(event) {
        this.cardvalue = "";
        const selectedCard = event.detail.value;
        this.cardvalue = selectedCard;
        return this.cardvalue;
    }

    handleCommentChange(event) {
        this.comment = event.target.value;
    }

    @wire(getObjectInfo, {
        objectApiName: CASE_OBJECT
    })
    objectInfo;
	
	@wire(getRecord, {
        recordId: USER_ID,
        fields: [FEDERATION_ID, USERDEPARTMENT]
    })
    wireuser({
        error,
        data
    }) {
        if (error) {
            this.error = error;
        } else if (data) {
            this.federationId = data.fields.FederationIdentifier.value;
			this.userDepartment = data.fields.User_Department__c.value;
            console.log('---user--dep--'+this.userDepartment);
        }
    }
	
	@wire(getPicklistValues, {
        recordTypeId: "$objectInfo.data.defaultRecordTypeId",
        fieldApiName: DEPARTMENT
    })
    departmentPicklistValues;
	
	get Department()
    {
        if (this.objectApiName == CASEOBJLabel && this.selectDepartment == "")
        {
            this.agentDepartment = getFieldValue(this.caseData.data, DEPARTMENT);
        } 
        if ((this.objectApiName == LIVECHATOBJ || this.objectApiName == CONTACTOBJLabel ) && this.selectDepartment == "")
        {   
            console.log('---64----' + this.userDepartment);
            if (this.userDepartment !== null)
            {
                this.agentDepartment = this.userDepartment;
            }           
        }
        console.log('----selectDepartment---'+this.selectDepartment);
        if (this.selectDepartment !== "")
        {
            this.agentDepartment = this.selectDepartment;
        }  
         return this.agentDepartment;
    }
	
	 @wire(fetchCategories, { departmentVal: '$agentDepartment'})
     wiredCategories({ error, data }) {
        if (data)
        {
            console.log('----data----'+JSON.stringify(data));
            this.categoryOptions = [];
            for(let i=0; i<data.length; i++) {
                console.log('id=' + data[i].Id);
                this.categoryOptions = [...this.categoryOptions ,{value: data[i].Category__c , label: data[i].Category__c}];                                   
            }                
            this.error = undefined;
        } else if (error) {
            this.error = error;
        }
    }

     get categoryOptions() {
        console.log(this.categoryOptions);
        return this.categoryOptions;
    }

    get Category() {
		if (this.objectApiName == 'Case' && this.categoryValue === "")
        {
            this.catgryval = getFieldValue(this.caseData.data, CATEGORY);
            console.log('--category--'+this.catgryval);  
        }
		if (this.categoryValue !== "")
        {
            this.catgryval = this.categoryValue;
            console.log('---111---cat--'+this.catgryval);
        }
        return this.catgryval;
    }
	
	handleDepartmentChange(event)
    {
        this.agentDepartment = "";
        this.selectDepartment = event.detail.value;
    } 

    handleCategoryChange(event) {
        this.categoryValue = event.detail.value;
        console.log('--categoryValue---'+this.categoryValue);
        if ((this.categoryValue === CROSSELL_RESPONSELabel ||
            this.categoryValue === ACCOUNT_CLOSURELabel ||
            this.categoryValue === PAYMENT_HOLIDAYLabel ||
            this.categoryValue === CARDREPLACELabel ||
            this.categoryValue === PINLabel ||
            this.categoryValue === CLICLDLabel) &&
            this.fieldsVisible === true)
        {
            this.fieldsVisible = false;
        }
    }

    @wire(getPicklistValues, {
        recordTypeId: "$objectInfo.data.defaultRecordTypeId",
        fieldApiName: TYPE
    })
    typePicklistValues;

    get Type() {
        this.type = getFieldValue(this.caseData.data, TYPE);
        if (this.typeValue != "") {
            this.type = this.typeValue;
        }
        return this.type;
    }

    handleTypeChange(event) {
        this.typeValue = event.detail.value;
    }

    get crossSellFieldVisible() {
        //console.log('--fieldsVisible--256' + this.fieldsVisible)        
        return (this.catgryval === CROSSELL_RESPONSELabel ||
                this.categoryValue === CROSSELL_RESPONSELabel) &&
            this.fieldsVisible === false ? true : false;
    }
    handleSuccessChange(event) {
        this.succcess = event.detail.value;
    }
    handleCountryChange(event) {
        this.country = event.detail.value;
    }
    handleServiceSoldChange(event) {
        this.serviceSold = event.detail.value;
    }
    get retentionFieldVisible() {
        //console.log('--fieldsVisible-274' + this.fieldsVisible);        
        return (this.catgryval === ACCOUNT_CLOSURELabel ||
                this.categoryValue === ACCOUNT_CLOSURELabel) &&
            this.fieldsVisible === false ? true : false;
    }
    @wire(getObjectInfo, {
        objectApiName: RETENTION_OBJECT
    })
    objectInfo;

    @wire(getPicklistValues, {
        recordTypeId: "$objectInfo.data.defaultRecordTypeId",
        fieldApiName: SAVEATTEMPT
    })
    saveAttemptPicklistValues;

    handleSaveAttemptChange(event) {
        this.saveAttempt = event.detail.value;
    }

    @wire(getPicklistValues, {
        recordTypeId: "$objectInfo.data.defaultRecordTypeId",
        fieldApiName: ACCOUNTRETAINED
    })
    accountRetainedPicklistValues;

    handleAccountRetainedChange(event) {
        this.accountRetained = event.detail.value;
    }

    @wire(getPicklistValues, {
        recordTypeId: "$objectInfo.data.defaultRecordTypeId",
        fieldApiName: OFFER
    })
    offerPicklistValues;

    handleOfferChange(event) {
        this.offer = event.detail.value;
    }

    @wire(getPicklistValues, {
        recordTypeId: "$objectInfo.data.defaultRecordTypeId",
        fieldApiName: ACCOUNTCLOSURE
    })
    accountClosurePicklistValues;

    handleAccountClosureChange(event) {
        this.accountClosure = event.detail.value;
        //alert(this.accountClosure);
    }

    get alertFieldVisible() {
        ///console.log('--fieldsVisible-329' + this.fieldsVisible);
        //this.fieldsVisible = false;
        return (this.catgryval === PAYMENT_HOLIDAYLabel ||
            this.categoryValue === PAYMENT_HOLIDAYLabel ||
                this.catgryval === CARDREPLACELabel ||
            this.categoryValue === CARDREPLACELabel ||
            this.catgryval === PINLabel ||
            this.categoryValue === PINLabel ||
            this.catgryval === CLICLDLabel ||
            this.categoryValue === CLICLDLabel) &&
            this.fieldsVisible === false ? true : false;
    }

    selectCheckBox(event)
    {
        this.chekboxValue = event.target.checked;
        return this.chekboxValue;
    }

    @wire(getObjectInfo, {
        objectApiName: ALERT_OBJECT
    })
    objectInfo;

    @wire(getPicklistValues, {
        recordTypeId: "$objectInfo.data.defaultRecordTypeId",
        fieldApiName: TARGET
    })
    targetPicklistValues;

    @wire(getPicklistValues, {
        recordTypeId: "$objectInfo.data.defaultRecordTypeId",
        fieldApiName: ALERT_CATEGORY
    })
    alertCategoryPicklistValues;

    handleDueDateChange(event) {
        this.dueDate = event.detail.value;
    }

    handleTargetChange(event) {
        this.target = event.detail.value;
    }

    handleAlertCategoryChange(event) {
        this.alertCategory = event.detail.value;
    }
    
    handleUpdateCTITask()
    {
        let dt = new Date();
        const dtf = new Intl.DateTimeFormat('en', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit'
        })
        const [{value: mo}, , {value: da}, , {value: ye}] = dtf.formatToParts(dt);
        let formatedDate = `${ye}-${mo}-${da}`;
        console.log('formatedDate ===> '+formatedDate);
        this.errorMessage = "";
        console.log('---this.finvalue--'+this.finvalue);
        if (this.finvalue == null || this.finvalue == ""
        || this.finvalue == '--None--')
        {
            this.errorMessage = FINANCIALACCOUNTLabel;
        }
        console.log('--category---'+this.catgryval);
        if (this.catgryval === null ||
            this.catgryval === undefined ||
            this.catgryval === "" ||
            this.catgryval === NONELabel) {
            this.errorMessage = CATEGORYLabel;
        } else {
            if (this.catgryval === ACCOUNT_CLOSURELabel ||
                this.categoryValue === ACCOUNT_CLOSURELabel
            ) {
                if (this.accountClosure == "" || this.accountClosure == undefined) {
                    this.errorMessage = REASONLabel;
                }
                if (this.offer == "" || this.offer == undefined) {
                    this.errorMessage = OFFERLabel;
                }
                if (this.accountRetained == "" || this.accountRetained == undefined) {
                    this.errorMessage = RETAINEDLabel;
                }
                if (this.saveAttempt == "" || this.saveAttempt == undefined) {
                    this.errorMessage = SAVEATTEMPTLabel;
                }
                if (this.finvalue == OTHERLabel) {
                    this.errorMessage = FINANCIALACCOUNTLabel;
                }
            }
            if (this.chekboxValue == true)
            {                
                if (this.dueDate < formatedDate){
                    this.errorMessage = DUEDATEVALIDLabel;  
                }
                if (this.alertCategory == undefined || this.alertCategory == "")
                {
                    this.errorMessage = ALERTCATEGORYLabel;  
                }
                if (this.dueDate == undefined || this.dueDate == "")
                {
                    this.errorMessage = DUEDATELabel;  
                }
            }
            if (this.catgryval === CROSSELL_RESPONSELabel ||
                this.categoryValue === CROSSELL_RESPONSELabel
            ) {
                if (this.serviceSold == "" || this.serviceSold == undefined) {
                    this.errorMessage = SERVICE_SOLDLabel;
                }
                if (this.country == "" || this.country == undefined) {
                    this.errorMessage = COUNTRYLabel;
                }
                if (this.succcess == "" || this.succcess == undefined) {
                    this.errorMessage = CROSSSELL_SUCCESSLabel;
                }
                if (this.finvalue == OTHERLabel) {
                    this.errorMessage = FINANCIALACCOUNTLabel;
                }
            }
        }

        if (this.errorMessage != "") {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: "Error!",
                    message: this.errorMessage,
                    variant: "error"
                })
            );
            return null;
        }
        if (this.objectApiName == CASEOBJLabel &&
            (this.finvalue != getFieldValue(this.caseData.data, FINACCOUNTID) ||
                this.catgryval != getFieldValue(this.caseData.data, CATEGORY))
        )
        {
            console.log('---finvalue---'+this.finvalue);
            const fields = {};
            fields[CASEID.fieldApiName] = this.recordId;
            fields[CATEGORY.fieldApiName] = this.catgryval;
            fields[CONTACTID.fieldApiName] = this.contactId;
            if (this.finvalue != OTHERLabel && this.cardvalue != OTHERLabel) {
                fields[FINACCOUNTID.fieldApiName] = this.finvalue;
                fields[CARDID.fieldApiName] = this.cardvalue;
            } else
            {
                fields[NONEXISTSFA.fieldApiName] = true;
                fields[FINACCOUNTID.fieldApiName] = null;
                fields[CARDID.fieldApiName] = null;
            }
            fields[DEPARTMENT.fieldApiName] = this.agentDepartment;
            fields[TYPE.fieldApiName] = this.type;
            const recordInput = {
                fields
            };
            updateRecord(recordInput)
                .then(() => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: "Success",
                            message: CASEUPDATELabel,
                            variant: "success"
                        })
                    );
                    pubsub.fire(
                        "refreshActivity",
                        "Calling refresh function of activityHistoryLwc"
                    );
                    eval("$A.get('e.force:refreshView').fire();");
                })
                .catch((error) => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: "Updating error",
                            message: error.body.message,
                            variant: "error"
                        })
                    );
                });
        }

        if (this.objectApiName == LIVECHATOBJ)
        {
             updateChat({
                    chatRecordId: this.recordId,
                    customerId: this.contactId,
                    financialId: this.finvalue,
                    cardId: this.cardvalue,
                    category: this.catgryval,
                })
                .then((result) => {
                    if (result !== null)
                    {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: "Success!",
                                message: LIVECHATMESSAGE,
                                variant: "Success"
                            })
                        );
                        eval("$A.get('e.force:refreshView').fire();");
                    }
                    })
                    .catch ((error) =>
                    {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: "Updating error",
                            message: error.body.message,
                            variant: "error"
                        })
                    );
                });
        }
        
        if (this.comment != "") {
            doUpdateCase({
                    caseId: this.recordId,
                    customerId: this.contactId,
                    financialId: this.finvalue,
                    cardId: this.cardvalue,
                    category: this.catgryval,
                    comment: this.comment,
                    typeVal: this.typeValue,
                    saveAttempt: this.saveAttempt,
                    saved: this.accountRetained,
                    reason: this.accountClosure,
                    offer: this.offer,
                    dueDate: this.dueDate,
                    target: this.target,
                    alertCategory: this.alertCategory,
                    alertFlag: this.chekboxValue,
                    country: this.country,
                    crossSellSucess: this.succcess,
                    serviceSold: this.serviceSold,
                    federationId: this.federationId
                })
                .then((result) => {
                    if (result.includes("success")) {
                        //eval("$A.get('e.force:refreshView').fire();");
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: "Success!",
                                message: result,
                                variant: "Success"
                            })
                        );
                        if (this.catgryval == CROSSELL_RESPONSELabel) {
                            this.country = "";
                            this.succcess = "";
                            this.serviceSold = "";
                            this.fieldsVisible = this.objectApiName == CASEOBJLabel ? true : false;
                        }
                        if (this.chekboxValue == true)
                        {
                            this.dueDate = "";
                            this.target = "";
                            this.alertCategory = "";
                            this.chekboxValue = false;
                            this.fieldsVisible = this.objectApiName == CASEOBJLabel ? true : false;


                        }
                        if (this.catgryval == ACCOUNT_CLOSURELabel) {
                            this.saveAttempt = "";
                            this.accountRetained = "";
                            this.accountClosure = "";
                            this.offer = "";
                            this.fieldsVisible = this.objectApiName == CASEOBJLabel ? true : false;
                        }
                        this.template.querySelector("form").reset();
                        if (this.objectApiName == CONTACTOBJLabel) {
                            this.template
                                .querySelectorAll("lightning-combobox.norm")
                                .forEach((each) => {
                                    each.value = ""; //null;
                                });
                            this.categoryValue = "";
                            this.comment = "";
                            this.finvalue = "";
                            this.cardvalue = "";
                        }
                        pubsub.fire(
                            "refreshActivity",
                            "Calling refresh function of activityHistoryLwc"
                        );
                        eval("$A.get('e.force:refreshView').fire();");
                    } else {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: "Error!",
                                message: result,
                                variant: "error"
                            })
                        );
                    }
                })
                .catch((error) => {
                    this.error = error.message;
                });
        }
    }

    navigateToChangeCustomer() {
        if (this.contactId != this.recordId) {
            updateTask({
                    whoId: this.contactId,
                    customerId: this.recordId
                })
                .then((result) => {
                    if (result === true) {
                        this[NavigationMixin.Navigate]({
                                type: "standard__webPage",
                                attributes: {
                                    url: "/lightning/r/Contact/" + this.contactId + "/view"
                                }
                            },
                            true
                        );
                    } else {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: "Error!",
                                message: ACTIVITYNOTAVAILLabel,
                                variant: "error"
                            })
                        );
                    }
                })
                .catch((error) => {
                    this.error = error;
                    this.accounts = undefined;
                });
        } else {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: "Error!",
                    message: CUSTOMERNOTAVAILLabel,
                    variant: "error"
                })
            );
        }
    }
}