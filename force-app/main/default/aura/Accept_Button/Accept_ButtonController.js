({
    
    createRecord : function (component, event, helper) 
    {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams
        (
            {
                "entityApiName": "Case"
            }
        );
        createRecordEvent.fire();
    },
    
    primeAction: function(component, event, helper) {
        var action = component.get('c.getStatus');
        action.setParams({
            "csId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS"){
                var cas = response.getReturnValue();
                component.set("v.CaseRecord", cas);
                var caseStatus = component.get("v.CaseRecord.Status");
                //alert("---status----"+caseStatus);
                var caseowner = component.get("v.CaseRecord.OwnerId");  
                //caseowner = caseowner.slice(0,-3);
                //alert("-----ownerId----"+caseowner);
                var userId = $A.get("$SObjectType.CurrentUser.Id");
                //alert("----userId---"+userId);
                var custId = component.get("v.CaseRecord.ContactId");
                //alert("-----contactid-----"+custId);
                
                if(caseStatus =='In Progress'){
                    if(caseowner == userId){
                        if(custId){
                            component.set("v.isOpen", true);
                        } else {
                            // alert('No People linked with the case, hence Prime action is not possible');
                            var toastEvent = $A.get("e.force:showToast"); 
                            toastEvent.setParams({ 
                                "title": "Error", 
                                "type": "error", 
                                "message": "No People linked with the case, hence Prime action is not possible." 
                            }); 
                            toastEvent.fire();
                        }
                    } else {
                        // alert('only case owner can perform PRIME action');
                        var toastEvent = $A.get("e.force:showToast"); 
                        toastEvent.setParams({ 
                            "title": "Error", 
                            "type": "error", 
                            "message": "Case Owner can only perform PRIME action, Please check." 
                        }); 
                        toastEvent.fire();
                    }
                } else {
                    //alert('Prime action can be performed only when case is In Progress');
                    var toastEvent = $A.get("e.force:showToast"); 
                    toastEvent.setParams({ 
                        "title": "Error", 
                        "type": "error", 
                        "message": "Prime action can be performed only when case is In Progress." 
                    }); 
                    toastEvent.fire();
                }
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
        var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
        dismissActionPanel.fire();       
    },
    
    Accept: function(component, event, helper){
        var action = component.get('c.updateStatus');
        action.setParams({
            "caseId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS"){
                var cse = response.getReturnValue();
                component.set("v.CaseRecord", cse);
                //alert('cse'+cse);
                var ownerId = component.get("v.CaseRecord.OwnerId");
                var userId = $A.get("$SObjectType.CurrentUser.Id");
                var stas = component.get("v.CaseRecord.Status");
                //ownerId = ownerId.slice(0,-3);
                if(ownerId.startsWith('005') && ownerId != userId && (stas == 'In Progress' || stas == 'Re Open' || stas == 'Closed')){
                    component.set("v.flag", true);
                }
                else{
                    $A.enqueueAction(component.get('c.OwnerShip'));
                }
                
                
                //alert('------s-tatus---'+stas);
                if((stas == 'New') && ownerId != userId){
                    //alert("==owner name----"+v.CaseRecord.Owner.name);
                    var toastEvent = $A.get("e.force:showToast"); 
                    toastEvent.setParams({ 
                        "title": "Success", 
                        "type": "success", 
                        "message": "The case is successfully assigned to you" 
                    }); 
                    toastEvent.fire();
                }
                
                if(ownerId === userId){
                    // alert("You are already owner of this Case");
                    var toastEvent = $A.get("e.force:showToast"); 
                    toastEvent.setParams({ 
                        "title": "Error", 
                        "type": "error", 
                        "message": "You are already owner of this Case." 
                    }); 
                    toastEvent.fire();
                }
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    closeCase : function(component, event, helper){
        var action = component.get('c.StatusUpdate');
        action.setParams({
            "caseId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS"){
                var cse = response.getReturnValue();
                component.set("v.CaseRecord", cse);
                var ownerId = component.get("v.CaseRecord.OwnerId");  
                var userId = $A.get("$SObjectType.CurrentUser.Id");                
                if(ownerId != userId){
                    var toastEvent = $A.get("e.force:showToast"); 
                    toastEvent.setParams({ 
                        "title": "Error", 
                        "type": "error", 
                        "message": "You are not the owner of the case. Please check" 
                    }); 
                    toastEvent.fire();
                }
                var stas = component.get("v.CaseRecord.Status");
                if(stas == 'In Progress' &&
                   component.get("v.CaseRecord.Category__c") == 'None'
                   || (component.get("v.CaseRecord.Financial_Account__c") == null
                       && component.get("v.CaseRecord.ContactId") != null)
                  ){
                    var toastEvent = $A.get("e.force:showToast");
                    if(component.get("v.CaseRecord.Category__c") == 'None'){
                        toastEvent.setParams({ 
                            "title": "Error", 
                            "type": "error", 
                            "message": "Please fill the Category." 
                        });
                    } else {
                        toastEvent.setParams({ 
                            "title": "Error", 
                            "type": "error", 
                            "message": "Please fill the Financial Account" 
                        });
                    }
                    toastEvent.fire();
                }
                if(stas == 'Closed'){
                    var toastEvent = $A.get("e.force:showToast"); 
                    toastEvent.setParams({ 
                        "title": "Error", 
                        "type": "error", 
                        "message": "Case is already closed. Please check" 
                    }); 
                    toastEvent.fire();
                }
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    reopen : function(component, event, helper) {        
        var action = component.get('c.StatusUpdates');
        action.setParams({
            "caseId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS"){
                var cse = response.getReturnValue();
                component.set("v.CaseRecord", cse);
                var ownerId = component.get("v.CaseRecord.OwnerId");  
                //ownerId = ownerId.slice(0,-3);
                //alert("-----ownerId----"+ownerId);
                var userId = $A.get("$SObjectType.CurrentUser.Id");
                //alert("----userId---"+userId);
                
                if(ownerId != userId){
                    // alert("You are not the owner of the case");
                    var toastEvent = $A.get("e.force:showToast"); 
                    toastEvent.setParams({ 
                        "title": "Error", 
                        "type": "error", 
                        "message": "You are not the owner of the case. Please check" 
                    }); 
                    toastEvent.fire();
                }
                var stas = component.get("v.CaseRecord.Status");
                //alert("------Status-----"+stas);
                if(stas == 'Re Open'){
                    // alert("Case is already Re open.");
                    var toastEvent = $A.get("e.force:showToast"); 
                    toastEvent.setParams({ 
                        "title": "Error", 
                        "type": "error", 
                        "message": "Case is already Re open. Please check" 
                    }); 
                    toastEvent.fire();
                }
                if(stas == 'In Progress' || stas == 'New'){
                    // alert("Case is already opened.");
                    var toastEvent = $A.get("e.force:showToast"); 
                    toastEvent.setParams({ 
                        "title": "Error", 
                        "type": "error", 
                        "message": "Case is already opened. Please check" 
                    }); 
                    toastEvent.fire();
                }
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    OwnerShip: function(component, event, helper){
        var action = component.get('c.ownerAssign');
        action.setParams({
            "caseId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS" ){
                component.set("v.flag", false);
            }
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
    },
    
    handleCloseModal: function(component, event, helper) {
        //For Close Modal, Set the "openModal" attribute to "fasle"  
        component.set("v.flag", false);
    }
    
})