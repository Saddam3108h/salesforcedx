({
    primeAction: function(component, event, helper) {
         component.set("v.isPrimeOpen", true);
	
    },
         
        
    customerRights: function(component, event, helper) {
        component.set("v.isCRightsOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isPrimeOpen", false);
        component.set("v.isRResponseOpen", false);
        component.set("v.isCRightsOpen", false);
        var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
        dismissActionPanel.fire();       
    },
})