({
    doInit : function(component, event, helper) {
        helper.getFindata(component, event);
    },
    
    navigateTofin : function(component, event, helper) {
        var finId = event.srcElement.id;
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openSubtab({
            recordId: finId,
            focus: true
        }).then(function(response) {
            workspaceAPI.getTabInfo({
                tabId: response
            }).then(function(tabInfo) {
                console.log("The url for this tab is: " + tabInfo.url);
            });
        })
        .catch(function(error) {
            console.log(error);
        });
        //alert(finId);
    },
})