({
    getFindata : function(component, pageNumber, pageSize) {
        var action = component.get("c.getFindetails");
        action.setParams({PeopleId : component.get("v.recordId")});
        action.setCallback(this, function(res){
            //alert('-----findata-----'+res.getState());
            if(res.getState() === "SUCCESS"){
                var fins = [];
                var findata = res.getReturnValue();
                //alert('-----findata-----'+JSON.stringify(findata));
                for(var key in findata){
                    //alert('-----findata-----'+JSON.stringify(value.colCode));
                    fins.push({value:findata[key], key:key});
                }
                component.set("v.FinData", fins);
                component.set("v.FinCount", fins.length);
            }
        });
        $A.enqueueAction(action);
    },
})