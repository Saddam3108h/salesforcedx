({
    loadTxns : function(component, event, helper) {
        var action = component.get("c.getTrans");
        //alert('-----'+action);
        action.setParams({CasId : component.get("v.recordId")});
        action.setCallback(this, function(res){
            if(res.getState() === "SUCCESS"){
                // alert('----res---'+res.getState());
                var txns = res.getReturnValue();
                //alert('---count---'+txns.length);
                component.set("v.ListOfTxns", txns);                
                component.find("box3").set("v.value", false);
                component.set("v.selectedTxn", txns.length);
            } else if(res.getState() === "ERROR"){
                console.log("oof");
            }
            this.txnAmount(component, event);
            this.totalRiskAmt(component, event);
        });
        $A.enqueueAction(action);
    },
    
    // this function automatic call by aura:waiting event  
    showSpinner: function (component, event, helper) {
        component.set("v.Spinner", true);
        /*var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");*/
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner: function (component, event, helper) {
        component.set("v.Spinner", false);
        /*var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");*/
    },
    
    
    onCheck : function(component, event, helper){
        this.showSpinner(component);
        var getAllCheckboxes = component.find("chekbx");
        var headername = event.getSource().get("v.name");
        var txnId = event.getSource().get("v.text");
        var action = component.get("c.updateTxns");
        action.setParams({TxnId : txnId, chekboxval : headername})
        action.setCallback(this, function(res){
            if(res.getState() === "SUCCESS"){
                //alert('-------'+res.getState());
                this.hideSpinner(component);
                if(res.getReturnValue() != ''){
                   // alert(''+res.getReturnValue()); 
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({ 
                        "title": "Error!", 
                        "type": "Error", 
                        "message": ''+res.getReturnValue()
                    });
                    toastEvent.fire();
                } else if(res.getState() === "ERROR"){
                    console.log("oof");
                }
                this.loadTxns(component, event); 
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    deleSelectTxnshelper : function(component, event, deleTxnIds){
        var action = component.get('c.deleteTxns');
        action.setParams({"lstRecordId" : deleTxnIds});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var returnval = response.getReturnValue();
                if(returnval != 'Del'){
                    alert(returnval);
                }
                if(returnval == 'Del'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({ 
                        "title": "Success!", 
                        "type": "Success", 
                        "message": "The record has been Deleted successfully."
                    });
                    toastEvent.fire();
                }
                //alert('No Transactions got Selected');
                console.log('check it--> delete successful');
                this.loadTxns(component, event);
            }
        });
        $A.enqueueAction(action);
    },
    
    txnAmount : function(component, event, helper){
        var action = component.get("c.getGross");
        //alert('-----'+action);
        var getAllTxns = [];
        action.setParams({CasId : component.get("v.recordId")});
        action.setCallback(this, function(res){
            if(res.getState() === "SUCCESS"){
                // alert('----res---'+res.getState());
                var grAmt = res.getReturnValue();
                //alert('---grAmt---'+grAmt);
                component.set("v.GrsAmt", grAmt);                
            } else if(res.getState() === "ERROR"){
                console.log("oof");
            }
        });
        $A.enqueueAction(action);
    },
    
    totalRiskAmt : function(component, event, helper){
        var action = component.get("c.getTotalRisk");
        //alert('-----'+action);
        action.setParams({CasId : component.get("v.recordId")});
        action.setCallback(this, function(res){
            if(res.getState() === "SUCCESS"){
                // alert('----res---'+res.getState());
                var TotalRskAmt = res.getReturnValue();
                /*alert('-CHLiable----'+TotalRskAmt[3]);
                alert('-Writeoff----'+TotalRskAmt[4]);
                alert('-FraudWF----'+TotalRskAmt[5]);*/
                
                component.set("v.TotalRskAmt", TotalRskAmt[0]); 
                component.set("v.CBAmt", TotalRskAmt[1]);
                component.set("v.TotalRefund", TotalRskAmt[2]);
                component.set("v.chLiable", TotalRskAmt[3]);
                component.set("v.Writeoff", TotalRskAmt[4]);
                component.set("v.FraudWF", TotalRskAmt[5]);
            } else if(res.getState() === "ERROR"){
                console.log("oof");
            }
        });
        $A.enqueueAction(action);
    },
    
    flagAmount : function(component, event, TxId){
        var action = component.get("c.getFlagAmt");
        action.setParams({CasId : component.get("v.recordId"), TxnsId : TxId});
        action.setCallback(this, function(res){
            if(res.getState() === "SUCCESS"){
                //alert('----res---'+res.getState());
                var tRfndAmt = res.getReturnValue();
                //alert('---tRfndAmt---'+tRfndAmt);
                component.set("v.TotalRefund", tRfndAmt);                
            } else if(res.getState() === "ERROR"){
                console.log("oof");
            }
        });
        $A.enqueueAction(action);
    },
})