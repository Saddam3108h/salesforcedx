({
    getTxns : function(component, event, helper) {
        helper.loadTxns(component, event);
        //helper.flagAmount(component, event,txnId);
        
    },
    createNewTrax : function(component, event, helper) {
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var action = component.get("c.getCaseOwnerId");
        action.setParams({CasId : component.get("v.recordId")});
        action.setCallback(this, function(res){
            if(res.getState() === "SUCCESS"){
                var ownerId = res.getReturnValue();
                if(ownerId == userId){
                    component.set('v.showNewRec',true);
                 }
                else{
                   var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Please accept the case before you add a new Tranaction."
                    });
                    toastEvent.fire(); 
                }
             } else if(res.getState() === "ERROR"){
                console.log("somthing went wrong");
            }
          });
        $A.enqueueAction(action);
    },
    handleComponentEvent : function(component, event, helper) {
        var hideModel = event.getParam("hideModel");
        //alert('handleComponentEvent  '+hideModel);
        component.set('v.showNewRec',hideModel);
        helper.loadTxns(component, event);
    },
    closeModel: function(component, event, helper) {
        component.set('v.showNewRec',false);
    },
    upDateAmt : function(component, event, helper) {
        //helper.txnAmount(component, event);
    },
    
    onChange : function(component, event, helper){
        helper.onCheck(component, event); 
    },    
    
    selecteAllTxn : function(component, event, helper){
        var selcttxn = event.getSource().get("v.value");
        //alert('-----'+selcttxn);
        var getAllTxn = component.find("boxPack");
        if(! Array.isArray(getAllTxn)){
            if(selcttxn == true){
                component.find("boxPack").set("v.value", true);
            }else{
                component.find("boxPack").set("v.value", false);
            }
        }else{
            if(selcttxn == true){
                for(var i=0; i<getAllTxn.length; i++){
                    component.find("boxPack")[i].set("v.value", true);
                }
            } else {
                for(var i=0; i<getAllTxn.length; i++){
                    component.find("boxPack")[i].set("v.value", false);
                }
            }
        }
    },
    
    delSelectTxns : function(component, event, helper){
        if(component.get('v.selectedTxn') > 0){
        var delId = [];
        var getAllTxn = component.find("boxPack");
        if(! Array.isArray(getAllTxn)){
            if(getAllTxn.get("v.value") == true){
                delId.push(getAllTxn.get("v.text"));
            }
        } else {
            for(var i=0; i<getAllTxn.length; i++){
                if(getAllTxn[i].get("v.value") == true){
                    delId.push(getAllTxn[i].get("v.text"));
                }
            }
        }
        helper.deleSelectTxnshelper(component, event, delId);
        }
        else{
          var toastEvent = $A.get("e.force:showToast");
              toastEvent.setParams({
                  "title": "Error!",
                  "type": "error",
                  "message": "Sorry, There are no tranactions to delete."
                    });
              toastEvent.fire(); 
          }
    },
    
    updateChLiableAmount : function(component, event, helper){
        //alert('----alert-chLiable---');
        var action = component.get("c.updateChliable");
        var chLiableval = component.get("v.chLiable");
        if(chLiableval == ''){
            chLiableval = 0.00;
        }
        if ( event.keyCode == 13 ){
            action.setParams({caseId : component.get("v.recordId"), chLiableValue : chLiableval});
            action.setCallback(this, function(res){
                if(res.getState() === "SUCCESS"){
                    //alert('-------'+res.getReturnValue());
                    if(res.getReturnValue() != null){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({ 
                            "title": "Error!", 
                            "type": "Error", 
                            "message": ''+res.getReturnValue()
                        });
                        toastEvent.fire();
                        component.set("v.chLiable", 0.00);
                    }
                    helper.totalRiskAmt(component, event, helper);
                }
            });
            $A.enqueueAction(action); 
        }
    },
})