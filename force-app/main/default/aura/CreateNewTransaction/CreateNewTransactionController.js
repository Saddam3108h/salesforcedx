({
    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "type":"success",
            "message": 'Transaction Record has been created Successfully!!..'
        });
        toastEvent.fire();
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            "hideModel" : false
        });
        cmpEvent.fire();
        $A.get('e.force:refreshView').fire();
    },
    handleSubmit : function(component, event, helper) {
        event.preventDefault(); // Prevent default submit
        var fields = event.getParam("fields");
        // start
        var authDate= fields["AuthDateTime__c"];
        if(authDate == null){
            //get current  time in ISO 8601 format
             var dateTimeNow = new Date().toISOString();
           fields["AuthDateTime__c"] = dateTimeNow;
        }
        else
        {
           fields["AuthDateTime__c"] = authDate; 
        }
        // end
        var cid=component.get('v.caseRecId');
        fields["CaseNumber__c"] = cid; // Prepopulate Case Id
        component.find('createTraxForm').submit(fields); // Submit form
        console.log('Successfully Submitted Transaction Record');
    },
    /* load : function(component, event, helper) {
      // alert('this is onload ==>> '+component.get('v.caseRecId'));
    },*/
    handleCancel : function(component, event, helper) {
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            "hideModel" : false
        });
        cmpEvent.fire();
      //$A.get('e.force:refreshView').fire();
    },
})